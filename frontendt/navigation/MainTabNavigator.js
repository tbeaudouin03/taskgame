import React from "react";
import { Platform } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import Hero from "../screens/Hero";
import Team from "../screens/Team";
import Adventure from "../screens/Adventure";
import Judge from "../screens/Judge";
import Shop from "../screens/Shop";
import Menu from "../screens/Menu";
import LeaderBoard from "../screens/LeaderBoard";

const config = require("../constants/config.json");

const HeroStack = createStackNavigator({
  Hero: Hero
});

HeroStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? `ios-contact` : "md-contact"}
    />
  )
};

const TeamStack = createStackNavigator({
  Team: Team
});

TeamStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-contacts" : "md-contacts"}
    />
  )
};

const AdventureStack = createStackNavigator({
  Adventure: Adventure
});

AdventureStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-compass" : "md-compass"}
    />
  )
};

const JudgeStack = createStackNavigator({
  Judge: Judge
});

JudgeStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-school" : "md-school"}
    />
  )
};

const ShopStack = createStackNavigator({
  Shop: Shop
});

ShopStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-basket" : "md-basket"}
    />
  )
};

const MenuStack = createStackNavigator({
  Menu: Menu
});

MenuStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === "ios"
          ? "ios-information-circle"
          : "md-information-circle"
      }
    />
  )
};

const LeaderBoardStack = createStackNavigator({
  LeaderBoard: LeaderBoard
});

LeaderBoardStack.navigationOptions = {
  tabBarLabel: () => {
    return null;
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-podium" : "md-podium"}
    />
  )
};

export default createBottomTabNavigator({
  HeroStack,
  TeamStack,
  AdventureStack,
  JudgeStack,
  ShopStack,
  LeaderBoardStack,
  MenuStack
});
