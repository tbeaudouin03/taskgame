import React from "react";
import { createStackNavigator, createSwitchNavigator } from "react-navigation";
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Image,
  ImageBackground
  //Button
} from "react-native";
import { Button, Body, Content, Root } from "native-base";
import { LinearGradient } from "expo";

import Loader from "../components/Loader";
import SignInScreen from "../components/SignInScreen";
import ShopTuto from "../components/Shop/ShopTuto";
import WelcomeTuto from "../components/WelcomeTuto";
import MainTabNavigator from "./MainTabNavigator";

// waits for accessToken to be defined. If not defined: go to Sign In, else: go to Application itself
class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const accessToken = await AsyncStorage.getItem("accessToken");

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(accessToken ? "App" : "Auth");
  };

  // Render loading content
  render() {
    return <Loader />;
  }
}

const AuthStack = createStackNavigator({ SignIn: SignInScreen });
const ShopTutoStack = createStackNavigator({ ShopTuto: ShopTuto });
const WelcomeTutoStack = createStackNavigator({ WelcomeTuto: WelcomeTuto });


const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: MainTabNavigator,
    Auth: AuthStack,
    ShopTuto: ShopTutoStack,
    WelcomeTuto: WelcomeTutoStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default () => (
  <Root >
    <AppNavigator />
  </Root>
);
