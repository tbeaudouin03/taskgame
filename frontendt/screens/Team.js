import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity,
  ScrollView
} from "react-native";

import { withNavigation } from "react-navigation";

import Popover from "react-native-popover-view";

import TypeWriter from "react-native-typewriter";

import Loader from "../components/Loader";
import TeamFlatItem from "../components/Team/TeamFlatItem";
import FilterLeaderBoard from "../components/FilterLeaderBoard";
import TeamTopMood from "../components/Team/TeamTopMood";
import TeamTopSocial from "../components/Team/TeamTopSocial";
import TeamAddGroup from "../components/Team/TeamAddGroup";
import TeamUpdateName from "../components/Team/TeamUpdateName";
import TeamUpdateStatus from "../components/Team/TeamUpdateStatus";
import TeamMembershipRequest from "../components/Team/TeamMembershipRequest";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

class Team extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      LeaderBoardType: "hero",
      BuyPressStatus: [],
      UsePressStatus: [],
      TeamMembershipRequest: [
        {
          IDHero: "aaa",
          IDTeam: "aaa",
          URL: "aaa",
          GivenName: "aaa",
          Mood: "very_happy",
          Status: "Nothing to say!",
          Level: 1,
          JudgeLevel: 1,
          TaskCount: 1,
          TaskVerifyCount: 1,
          JudgementCount: 1,
          JudgementVerifyCount: 1,
          Rank: 1,
          Score: 1,
          Coins: 1,
          LoveEnergy: 1,
          DarkEnergy: 1
        }
      ],
      LeaderBoardDataHero: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "hero"
          }
        ]
      },
      LeaderBoardDataJudge: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "judge"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        TeamFacebookGroup: "Facebook Group URL",
        TeamLinkedinGroup: "Linkedin Group URL"
      }
    };
  }

  PostTeamLeaderChoice = async (index, screen) => {
    let LeaderBoardDataHero = this.state.LeaderBoardDataHero;
    let LeaderBoardDataJudge = this.state.LeaderBoardDataJudge;
    let HeroData = this.state.HeroData;
    if (screen == "hero") {
      await help.PostTeamLeaderChoice(
        LeaderBoardDataHero.LeaderBoardObjects[index].GivenName,
        LeaderBoardDataHero.LeaderBoardObjects[index].key,
        HeroData.IDTeam
      );
    }
    if (screen == "judge") {
      await help.PostTeamLeaderChoice(
        LeaderBoardDataJudge.LeaderBoardObjects[index].GivenName,
        LeaderBoardDataJudge.LeaderBoardObjects[index].key,
        HeroData.IDTeam
      );
    }
    this._onRefresh();
  };

  OnSegmentPress = LeaderBoardType => {
    this.setState({ LeaderBoardType: LeaderBoardType });
  };

  RenderFlatList = () => {
    if (this.state.LeaderBoardType == "hero") {
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          initialNumToRender={10}
          windowSize={3}
          removeClippedSubviews={true}
          //ItemSeparatorComponent={this.renderSeparator}
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={this.state.LeaderBoardDataHero.LeaderBoardObjects}
          renderItem={({ item, index }) => (
            <TeamFlatItem
              PostTeamLeaderChoice={(() =>
                this.PostTeamLeaderChoice(index, "hero")).bind(this)}
              item={item}
              index={index}
            />
          )}
        />
      );
    }

    if (this.state.LeaderBoardType == "judge") {
      return (
        <FlatList
          initialNumToRender={10}
          windowSize={3}
          removeClippedSubviews={true}
          //ItemSeparatorComponent={this.renderSeparator}
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={this.state.LeaderBoardDataJudge.LeaderBoardObjects}
          renderItem={({ item, index }) => (
            <TeamFlatItem
              item={item}
              index={index}
              PostTeamLeaderChoice={(() =>
                this.PostTeamLeaderChoice(index, "judge")).bind(this)}
            />
          )}
        />
      );
    }
  };

  SetStateTeamLeaderName = async () => {
    let TeamLeaderObject = this.state.LeaderBoardDataHero.LeaderBoardObjects.filter(
      LeaderBoardObject => LeaderBoardObject.IsBondedTeamLeader == true
    );
    TeamLeaderName = TeamLeaderObject[0].GivenName;

    this.setState({ TeamLeaderName });
  };

  SetTeamRank = async () => {
    let HeroObject = this.state.LeaderBoardDataHero.LeaderBoardObjects.filter(
      LeaderBoardObject => LeaderBoardObject.Email == this.state.HeroData.Email
    );
    TeamRankHero = HeroObject[0].Rank;

    let JudgeObject = this.state.LeaderBoardDataJudge.LeaderBoardObjects.filter(
      LeaderBoardObject => LeaderBoardObject.Email == this.state.HeroData.Email
    );

    TeamRankJudge = JudgeObject[0].JudgeRank;

    this.setState({
      TeamRankHero,
      TeamRankJudge
    });
  };

  SetMood = async Mood => {
    HeroData = this.state.HeroData;
    HeroData.Mood = Mood;
    this.setState({ HeroData });
  };

  SetStatus = async Status => {
    HeroData = this.state.HeroData;
    HeroData.Status = Status;
    this.setState({ HeroData });
  };

  SetTeamFacebookGroup = async TeamFacebookGroup => {
    HeroData = this.state.HeroData;
    HeroData.TeamFacebookGroup = TeamFacebookGroup;
    this.setState({ HeroData });
  };

  SetTeamLinkedinGroup = async TeamLinkedinGroup => {
    HeroData = this.state.HeroData;
    HeroData.TeamLinkedinGroup = TeamLinkedinGroup;
    this.setState({ HeroData });
  };

  SetTeamName = async TeamName => {
    HeroData = this.state.HeroData;
    HeroData.TeamName = TeamName;
    this.setState({ HeroData });
  };

  SetIsSocialTutoVisible = async IsSocialTutoVisible => {
    this.setState({ IsSocialTutoVisible });
  };

  SetIsSocialTuto2Visible = async IsSocialTuto2Visible => {
    this.setState({ IsSocialTuto2Visible });
  };

  SetIsMoodTutoVisible = async IsMoodTutoVisible => {
    this.setState({ IsMoodTutoVisible });
  };

  ShowEditTeamGroup = () => {
    this.setState({ visible: true });
  };

  HideEditTeamGroup = () => {
    this.setState({ visible: false });
  };

  ShowEditTeamName = () => {
    this.setState({ visibleName: true });
  };

  HideEditTeamName = () => {
    this.setState({ visibleName: false });
  };

  ShowEditStatus = () => {
    this.setState({ visibleStatus: true });
  };

  HideEditStatus = () => {
    this.setState({ visibleStatus: false });
  };

  ShowMembershipRequest = () => {
    this.setState({ visibleMembershipRequest: true });
  };

  HideMembershipRequest = () => {
    this.setState({ visibleMembershipRequest: false });
  };

  DealWithTutoState = async HeroData => {
    IsAvailableMode = this.state.HeroData.TutorialLevel >= 9;
    this.setState({ IsAvailableMode });

    IsMoodTutoVisible = HeroData.TutorialLevel == 9;
    this.SetIsMoodTutoVisible(IsMoodTutoVisible);

    IsSocialTutoVisible = HeroData.TutorialLevel == 10;
    this.SetIsSocialTutoVisible(IsSocialTutoVisible);

    IsSocialTuto2Visible = HeroData.TutorialLevel == 13;
    this.SetIsSocialTuto2Visible(IsSocialTuto2Visible);
  };

  DealWithTutoAnim = async HeroData => {
    IsSocialTutoVisible = HeroData.TutorialLevel == 10;
    IsSocialTuto2Visible = HeroData.TutorialLevel == 13;
    if (IsSocialTutoVisible || IsSocialTuto2Visible) {
      this.refs.ScrollY.scrollTo({
        x: 0,
        y: 0,
        animated: true
      });
      await help.Wait(500)
      this.refs.ScrollY.scrollTo({
        x: 0,
        y: 100,
        animated: true
      });
    }
  };

  _onRefresh = async () => {
    this.setState({ refreshing: true });
    this.SetStateTeamLeaderName();
    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    let LeaderBoardData = await help.GetLeaderBoardData(HeroData.IDTeam);

    this.setState({
      refreshing: false,
      LeaderBoardDataHero: LeaderBoardData.LeaderBoardDataHero,
      LeaderBoardDataJudge: LeaderBoardData.LeaderBoardDataJudge
    });

    let TeamMembershipRequest = await help.GetTeamMembershipRequest(
      HeroData.IDTeam
    );

    this.SetTeamRank();

    this.setState({ TeamMembershipRequest });

    this.DealWithTutoAnim(HeroData);
  };

  // ------------------------------------------------------------------------------------------------------------------------

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._onRefresh();
    });

    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroDataSimple();
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    let LeaderBoardData = await help.GetLeaderBoardData(HeroData.IDTeam);

    await this.setState({
      loading: false,
      LeaderBoardDataHero: LeaderBoardData.LeaderBoardDataHero,
      LeaderBoardDataJudge: LeaderBoardData.LeaderBoardDataJudge
    });

    this.SetStateTeamLeaderName();

    let TeamMembershipRequest = await help.GetTeamMembershipRequest(
      HeroData.IDTeam
    );

    this.SetTeamRank();

    this.setState({ TeamMembershipRequest });

    this.DealWithTutoAnim(HeroData);
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    if (this.state.visibleMembershipRequest) {
      return (
        <TeamMembershipRequest
          HideMembershipRequest={this.HideMembershipRequest.bind(this)}
          TeamMembershipRequest={this.state.TeamMembershipRequest}
        />
      );
    }
    return (
      <Container style={{ backgroundColor: "transparent" }}>
        <TeamAddGroup
          HeroData={this.state.HeroData}
          visible={this.state.visible}
          HideEditTeamGroup={this.HideEditTeamGroup.bind(this)}
          SetTeamFacebookGroup={this.SetTeamFacebookGroup.bind(this)}
          SetTeamLinkedinGroup={this.SetTeamLinkedinGroup.bind(this)}
        />
        <TeamUpdateName
          HeroData={this.state.HeroData}
          visibleName={this.state.visibleName}
          HideEditTeamName={this.HideEditTeamName.bind(this)}
          SetTeamName={this.SetTeamName.bind(this)}
        />

        <TeamUpdateStatus
          HeroData={this.state.HeroData}
          visibleStatus={this.state.visibleStatus}
          HideEditStatus={this.HideEditStatus.bind(this)}
          SetStatus={this.SetStatus.bind(this)}
          _onRefresh={this._onRefresh.bind(this)}
          SetIsMoodTutoVisible={this.SetIsMoodTutoVisible.bind(this)}
        />

        <View
          style={{
            height: 66.5,
            marginTop: 40,
            backgroundColor: "transparent"
          }}
        >
          <FilterLeaderBoard
            LeaderBoardType={this.state.LeaderBoardType}
            HeroData={this.state.HeroData}
            OnSegmentPress={this.OnSegmentPress.bind(this)}
            TeamRankHero={this.state.TeamRankHero}
            TeamRankJudge={this.state.TeamRankJudge}
          />
        </View>
        <ScrollView
          ref="ScrollY"
          style={{
            height: 66.5,
            marginTop: 0,
            backgroundColor: "transparent"
          }}
        >
          <TeamTopMood
            ref="Mood"
            HeroData={this.state.HeroData}
            SetMood={this.SetMood.bind(this)}
            ShowEditStatus={this.ShowEditStatus.bind(this)}
            _onRefresh={this._onRefresh.bind(this)}
            SetIsMoodTutoVisible={this.SetIsMoodTutoVisible.bind(this)}
            IsMoodTutoVisible={this.state.IsMoodTutoVisible}
          />

          <TeamTopSocial
            HeroData={this.state.HeroData}
            LeaderBoardDataHero={this.state.LeaderBoardDataHero}
            ShowEditTeamGroup={this.ShowEditTeamGroup.bind(this)}
            ShowMembershipRequest={this.ShowMembershipRequest.bind(this)}
            TeamMembershipRequest={this.state.TeamMembershipRequest}
            TeamLeaderName={this.state.TeamLeaderName}
            _onRefresh={this._onRefresh.bind(this)}
            SetIsSocialTutoVisible={this.SetIsSocialTutoVisible.bind(this)}
            IsSocialTutoVisible={this.state.IsSocialTutoVisible}
          />

          <TouchableOpacity onPress={this.ShowEditTeamName}>
            <Text
              style={{
                marginTop: 20,
                margin: 25,
                marginBottom: -10,
                color: "rgba(0,0,0,0.5)",
                fontSize: 24,
                textAlign: "center"
              }}
            >
              {this.state.HeroData.TeamName
                ? this.state.HeroData.TeamName
                : "Team LeaderBoard"}
            </Text>
          </TouchableOpacity>

          {this.RenderFlatList()}
        </ScrollView>

        {this.RenderIsNotAvailableMode()}
        {this.RenderTutoAddStatus()}
        {this.RenderTutoFollowLeader()}
        {this.RenderTutoUnfollowLeader()}
      </Container>
    );
  }

  RenderIsNotAvailableMode() {
    if (!this.state.IsAvailableMode) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            if (this.state.HeroData.TutorialLevel >= 3) {
              this.props.navigation.navigate("Adventure");
            } else {
              this.props.navigation.navigate("Hero");
            }
            this._onRefresh();
          }}
          style={{
            backgroundColor: "rgba(255,255,255,0.7)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              zIndex: 3,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 15,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 4,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              Beat one monster in adventure mode to access team mode
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  RenderTutoAddStatus() {
    if (this.state.IsMoodTutoVisible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <TouchableOpacity
            activeOpacity={1}
            onPress={async () => {
              this.ShowEditStatus();
            }}
            style={{
              backgroundColor: "transparent",
              width: "90%",
              height: 60,
              zIndex: 2,
              alignSelf: "center",
              top: 158
            }}
          />
          <Image
            source={require("../assets/images/short_blue_arrow_up.png")}
            style={{
              width: 70,
              height: 140,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              alignSelf: "center",
              top: 152
            }}
          />
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 158,
              minHeight: 166,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Tap here to set your status.`}
            </Text>
            <TypeWriter
              typing={1}
              fixed={true}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 14,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`
You joined a team!

You can communicate with them by setting your mood and status up here.
`}
            </TypeWriter>
          </View>
        </View>
      );
    }
  }

  RenderTutoFollowLeader() {
    if (this.state.IsSocialTutoVisible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 160,
              minHeight: 130,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Tap here to follow the team leader.`}
            </Text>
            <TypeWriter
              typing={1}
              fixed={true}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 14,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`
The only way to play as a team is to follow the leader or to become the leader!`}
            </TypeWriter>
          </View>
          <Image
            source={require("../assets/images/short_blue_arrow_down.png")}
            style={{
              width: 70,
              height: 140,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              marginRight: 130,
              alignSelf: "center",
              top: 163
            }}
          />
          <TouchableOpacity
            activeOpacity={1}
            onPress={async () => {
              help.PostHelpTeamLeader(true, this.state.HeroData.IDTeam);
              help.PostTutorialLevel(this.state.HeroData.TutorialLevel + 1);
              await help.Wait(1500);
              this.props.navigation.navigate("Adventure");
              this._onRefresh();
              this.SetIsSocialTutoVisible(false);
            }}
            style={{
              backgroundColor: "transparent",
              width: "90%",
              height: 60,
              zIndex: 2,
              alignSelf: "center",
              top: 130
            }}
          />
        </View>
      );
    }
  }

  RenderTutoUnfollowLeader() {
    if (this.state.IsSocialTuto2Visible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 160,
              minHeight: 130,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Tap here to detach from the team leader.`}
            </Text>
            <TypeWriter
              typing={1}
              fixed={true}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 14,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`
This fight did not go well! Or was it just a dream? 
We had better follow our own path for now.
`}
            </TypeWriter>
          </View>
          <Image
            source={require("../assets/images/short_blue_arrow_down.png")}
            style={{
              width: 70,
              height: 140,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              marginLeft: 130,
              alignSelf: "center",
              top: 163
            }}
          />
          <TouchableOpacity
            activeOpacity={1}
            onPress={async () => {
              help.PostHelpTeamLeader(false, this.state.HeroData.IDTeam);
              help.PostTutorialLevel(this.state.HeroData.TutorialLevel + 1);
              await help.Wait(1500);
              this.props.navigation.navigate("Adventure");
              this._onRefresh();
              this.SetIsSocialTuto2Visible(false);
            }}
            style={{
              backgroundColor: "transparent",
              width: "90%",
              height: 60,
              zIndex: 2,
              alignSelf: "center",
              top: 130
            }}
          />
        </View>
      );
    }
  }
}

export default withNavigation(Team);

const styles = StyleSheet.create({
  segment: {
    height: "80%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "80%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
