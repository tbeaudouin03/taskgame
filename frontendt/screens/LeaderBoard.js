import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert
} from "react-native";
import Loader from "../components/Loader";
import LeaderBoardFlatItem from "../components/LeaderBoardFlatItem";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

class LeaderBoard extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      LeaderBoardType: "hero",
      BuyPressStatus: [],
      UsePressStatus: [],
      LeaderBoardDataHero: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "hero"
          }
        ]
      },
      LeaderBoardDataJudge: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "judge"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0
      }
    };
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true });
    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    let LeaderBoardData = await help.GetLeaderBoardData("");
    this.setState({
      refreshing: false,
      LeaderBoardDataHero: LeaderBoardData.LeaderBoardDataHero,
      LeaderBoardDataJudge: LeaderBoardData.LeaderBoardDataJudge
    });
  };

  OnSegmentPress = LeaderBoardType => {
    this.setState({ LeaderBoardType: LeaderBoardType });
  };

  RenderFlatList = () => {
    if (this.state.LeaderBoardType == "hero") {
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          initialNumToRender={10}
          windowSize={3}
          removeClippedSubviews={true}
          //ItemSeparatorComponent={this.renderSeparator}
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={this.state.LeaderBoardDataHero.LeaderBoardObjects}
          renderItem={({ item, index }) => (
            <LeaderBoardFlatItem
              ref={child => {
                this._LeaderBoardFlatItem = child;
              }}
              PostLeaderBoardTutorialLevel={help.PostLeaderBoardTutorialLevel.bind(
                this
              )}
              _signOutAsync={help._signOutAsync.bind(this)}
              item={item}
              index={index}
            />
          )}
        />
      );
    }

    if (this.state.LeaderBoardType == "judge") {
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          initialNumToRender={10}
          windowSize={3}
          removeClippedSubviews={true}
          //ItemSeparatorComponent={this.renderSeparator}
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={this.state.LeaderBoardDataJudge.LeaderBoardObjects}
          renderItem={({ item, index }) => (
            <LeaderBoardFlatItem item={item} index={index} />
          )}
        />
      );
    }
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroDataSimple();
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    let LeaderBoardData = await help.GetLeaderBoardData("");
    this.setState({
      refreshing: false,
      LeaderBoardDataHero: LeaderBoardData.LeaderBoardDataHero,
      LeaderBoardDataJudge: LeaderBoardData.LeaderBoardDataJudge
    });

    if (this.state.HeroData.LeaderBoardTutorialLevel == 0) {
      this.props.start();
    }

    //on copilot stop
    this.props.copilotEvents.on("stop", () => {
      help.PostLeaderBoardTutorialLevel(1);
      //this._LeaderBoardFlatItem.start();
    });

    this.setState({ loading: false });
  }

  componentWillUnmount() {
    // Don't forget to disable event handlers to prevent errors
    this.props.copilotEvents.off("stop");
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Container style={{ backgroundColor: "transparent" }}>
        <View
          style={{
            height: 66.5,
            marginTop: 40,
            backgroundColor: "transparent"
          }}
        >
          <Content
            style={{ backgroundColor: "transparent" }}
            contentContainerStyle={{
              alignItems: "center",
              backgroundColor: "transparent",
              justifyContent: "center"
            }}
          >
            <Card
              style={{
                width: "91%",
                marginBottom: 5,
                backgroundColor: "white",
                justifyContent: "center"
              }}
            >
              <CardItem
                style={{
                  backgroundColor: "transparent",
                  justifyContent: "center"
                }}
              >
                <Body
                  style={{
                    flex: 1,
                    backgroundColor: "#fff",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <CopilotStep
                      text="This is your rank among all players: 1 means you are first, 2 second etc."
                      order={1}
                      name="rank"
                    >
                      <CView
                        style={{
                          marginTop: 0,
                          marginRight: 20,
                          backgroundColor: "rgba(142, 176, 211, 1)",
                          borderColor: "rgba(255,255,255,0.5)",
                          borderRadius: 10
                        }}
                      >
                        <Text
                          style={{
                            margin: 5,
                            color: "rgba(255,255,255,1)",
                            fontSize: 18,
                            textAlign: "center",
                            fontWeight: "bold"
                          }}
                        >
                          {" "}
                          {this.state.LeaderBoardType == "hero"
                            ? this.state.HeroData.Rank
                            : this.state.HeroData.JudgeRank}{" "}
                        </Text>
                      </CView>
                    </CopilotStep>
                    <CopilotStep
                      text="This is your score"
                      order={2}
                      name="score"
                    >
                      <CView style={{ marginTop: 0, marginRight: 0 }}>
                        <Text
                          style={{
                            margin: 5,
                            color: "rgba(0,0,0,0.8)",
                            fontSize: 18,
                            //lineHeight: 16,
                            textAlign: "center",
                            fontWeight: "bold"
                          }}
                        >
                          {this.state.LeaderBoardType == "hero"
                            ? this.state.HeroData.Score
                            : this.state.HeroData.JudgeScore}{" "}
                        </Text>
                      </CView>
                    </CopilotStep>
                  </View>

                  <CopilotStep
                    text="Choose the leaderboard of heros or judges."
                    order={3}
                    name="filter"
                  >
                    <CView style={{ height: 30 }}>
                      <Segment
                        style={{
                          backgroundColor: "transparent",
                          marginLeft: 10,
                          marginRight: -5
                        }}
                      >
                        <Button
                          first
                          style={
                            this.state.LeaderBoardType == "hero"
                              ? styles.segmentP
                              : styles.segment
                          }
                          onPress={() =>
                            this.setState({ LeaderBoardType: "hero" })
                          }
                        >
                          <Text
                            style={
                              this.state.LeaderBoardType == "hero"
                                ? styles.segmentTextP
                                : styles.segmentText
                            }
                          >
                            Hero
                          </Text>
                        </Button>

                        <Button
                          last
                          style={
                            this.state.LeaderBoardType == "judge"
                              ? styles.segmentP
                              : styles.segment
                          }
                          onPress={() =>
                            this.setState({ LeaderBoardType: "judge" })
                          }
                        >
                          <Text
                            style={
                              this.state.LeaderBoardType == "judge"
                                ? styles.segmentTextP
                                : styles.segmentText
                            }
                          >
                            Judge
                          </Text>
                        </Button>
                      </Segment>
                    </CView>
                  </CopilotStep>
                </Body>
              </CardItem>
            </Card>
          </Content>
        </View>

        {this.RenderFlatList()}
      </Container>
    );
  }
}

export default copilot()(LeaderBoard);

const styles = StyleSheet.create({
  segment: {
    height: "80%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "80%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
