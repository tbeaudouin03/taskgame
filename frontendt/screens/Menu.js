import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Header,
  Left,
  List,
  ListItem,
  Icon,
  Right,
  Switch
} from "native-base";

import {
  ScrollView,
  View,
  StyleSheet,
  Image,
  Text as Txt,
  AsyncStorage,
  RefreshControl,
  Platform
} from "react-native";

import ResetTutorial from "../components/ResetTutorial";
import ConnectToOtherApp from "../components/ConnectToOtherApp";

import Loader from "../components/Loader";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

export default class Menu extends React.Component {
  static navigationOptions = {
    //header:null
    title: "Menu",
    headerTitleStyle: {
      textAlign: "center",
      flex: 1
    }
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      ResetTutorial: false,
      ConnectToOtherApp: false,
      refreshing: false,
      loading: true,
      screen: "judge",
      heroGreeting: "Hello again!",
      heroMessage: "Scroll down, verify the task and earn coins!",
      heroPunctuation: "!",
      CoinValueChosen: 1,
      FontSize: 15,
      SkillChosen: "Reputation",
      ImageToCheck: {
        IDTaskCheck: "aaaa",
        IDTask1: "bbbb",
        IDTask2: "cccc",
        IDHero: "dddd",
        Title: `seems like nobody needs you now.
Good job: rest and enjoy!`,
        URL:
          "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fteddy_bear.gif?alt=media&token=dcfd2c73-f609-4698-a83c-e8a7ba1774b1"
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        URL: `aaaa`
      }
    };
  }

  SetResetTutorialFalse() {
    this.setState({ ResetTutorial: false });
  }
  SetConnectToOtherAppFalse() {
    this.setState({ ConnectToOtherApp: false });
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    if (this.state.ResetTutorial) {
      return (
        <ResetTutorial
          SetResetTutorialFalse={this.SetResetTutorialFalse.bind(this)}
          _signOutAsync={() => help._signOutAsync(this.props.navigation)}
          WelcomeTuto={this.state.HeroData.TutorialLevel > 0 ? false : true}
          HeroTuto={this.state.HeroData.HeroTutorialLevel > 0 ? false : true}
          JudgeTuto={this.state.HeroData.JudgeTutorialLevel > 0 ? false : true}
          ShopTuto={this.state.HeroData.ShopTutorialLevel > 0 ? false : true}
          LeaderBoardTuto={
            this.state.HeroData.LeaderBoardTutorialLevel > 0 ? false : true
          }
        />
      );
    }
    if (this.state.ConnectToOtherApp) {
      return (
        <ConnectToOtherApp
          SetConnectToOtherAppFalse={this.SetConnectToOtherAppFalse.bind(this)}
          _signOutAsync={() => help._signOutAsync(this.props.navigation)}
        />
      );
    }
    return (
      <Container>
        <Content style={{ marginTop: 20 }}>
          <ListItem button>
            <Button
              full
              info
              style={{ width: "100%" }}
              onPress={() => help.OnGoogleTaskPress()}
            >
              <Text>Get Google Tasks</Text>
            </Button>
          </ListItem>

          <ListItem
            icon
            onPress={() => this.setState({ ConnectToOtherApp: true })}
          >
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-swap" : "md-swap"}
                />
              </Button>
            </Left>

            <Body>
              <Text>Connect to other Apps</Text>
            </Body>
            <Right>
              <Text />
              <Icon
                active
                name={
                  Platform.OS === "ios"
                    ? "ios-arrow-forward"
                    : "md-arrow-forward"
                }
              />
            </Right>
          </ListItem>

          <ListItem icon onPress={() => this.setState({ ResetTutorial: true })}>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>

            <Body>
              <Text>Reset Tutorial</Text>
            </Body>
            <Right>
              <Text />
              <Icon
                active
                name={
                  Platform.OS === "ios"
                    ? "ios-arrow-forward"
                    : "md-arrow-forward"
                }
              />
            </Right>
          </ListItem>

          <ListItem button>
            <Button
              full
              info
              style={{ width: "100%" }}
              onPress={() => help._signOutAsync(this.props.navigation)}
            >
              <Text>Sign out</Text>
            </Button>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
