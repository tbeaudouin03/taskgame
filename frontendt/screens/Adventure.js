import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated,
  Alert
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

import { withNavigation } from "react-navigation";

import HeroAnimation from "../components/Adventure/HeroAnimation";
import MonsterAnimation from "../components/Adventure/MonsterAnimation";
import TeamAnimation from "../components/Adventure/TeamAnimation";

import HeroHitAnimation from "../components/Adventure/HeroHitAnimation";
import MonsterHitAnimation from "../components/Adventure/MonsterHitAnimation";
import TeamHitAnimation from "../components/Adventure/TeamHitAnimation";

import LoveCostBonusAnimation from "../components/Adventure/LoveCostBonusAnimation";
import CoinsCostBonusAnimation from "../components/Adventure/CoinsCostBonusAnimation";

import LoveGiftAnimation from "../components/Adventure/LoveGiftAnimation";

import BlueArrowUpTuto from "../components/Adventure/BlueArrowUpTuto";
import BlueArrowUpTutoHero from "../components/Adventure/BlueArrowUpTutoHero";

import Loader from "../components/Loader";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);
const helpAdvAnim = require(`../components/Adventure/HelperAdventureAnimation`);

const HitImage = [
  require("../assets/images/100_1.png"),
  require("../assets/images/bam_2.png"),
  require("../assets/images/bang_1.png"),
  require("../assets/images/boink_1.png"),
  require("../assets/images/boom_1.png"),
  require("../assets/images/kaboom_1.png"),
  require("../assets/images/kapow_1.png"),
  require("../assets/images/pow_1.png"),
  require("../assets/images/pow_3.png"),
  require("../assets/images/pow_4.png"),
  require("../assets/images/wow_1.png")
];

const LoveImage = [
  require("../assets/images/cute_cat_love.gif"),
  require("../assets/images/cat_grab_love.gif"),
  require("../assets/images/cats_love.gif"),
  require("../assets/images/cute_love_cat.gif"),
  require("../assets/images/cute_love_rabbit.gif"),
  require("../assets/images/dancing_cat_love.gif"),
  require("../assets/images/panda_cup_love.gif")
];

class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      OpacityHitImage1: new Animated.Value(0),
      OpacityHitImage2: new Animated.Value(0),
      OpacityHitImage3: new Animated.Value(0),
      OpacityHitImage4: new Animated.Value(0),
      OpacityHitImage5: new Animated.Value(0),
      OpacityHitImage6: new Animated.Value(0),
      LoveCostBonusOpacity: new Animated.Value(1),
      CoinsCostBonusOpacity: new Animated.Value(0),
      HeroLifeBarOpacity: new Animated.Value(1),
      HeroFollowerOpacity: new Animated.Value(0),
      HeroLoveOpacity: new Animated.Value(0),
      HeroCoinsOpacity: new Animated.Value(0),
      HeroShieldOpacity: new Animated.Value(0),
      HeroSwordOpacity: new Animated.Value(0),
      HeroBookOpacity: new Animated.Value(0),
      MonsterLifeBarOpacity: new Animated.Value(1),
      MonsterShieldOpacity: new Animated.Value(0),
      MonsterSwordOpacity: new Animated.Value(0),
      MonsterBookOpacity: new Animated.Value(0),
      TeamLifeBarOpacity: new Animated.Value(0),
      TeamShieldOpacity: new Animated.Value(0),
      TeamSwordOpacity: new Animated.Value(0),
      TeamBookOpacity: new Animated.Value(0),
      OpacityTeammate1: new Animated.Value(0),
      OpacityTeammate2: new Animated.Value(0),
      OpacityTeammate3: new Animated.Value(0),
      OpacityHeroDamage: new Animated.Value(0),
      OpacityTeamDamage: new Animated.Value(0),
      OpacityMonsterDamage: new Animated.Value(0),
      OpacityTopCompanionCount: new Animated.Value(0),
      LoveGiftOpacity: new Animated.Value(0),
      OpacityBlueArrowUpTuto: new Animated.Value(0),
      OpacityBlueArrowUpTutoHero: new Animated.Value(0),
      IsTutoVisible: false,
      active: false,
      loading: true,
      refreshing: false,
      MonsterPressCount: 0,
      MonsterTalkCount: 0,
      HeroPressCount: 0,
      TeamPressCount: 0,
      LoveGift: {
        MinuteUntilNextLoveGift: 0,
        LoveEnergyGift: 0
      },
      SetIsChoosingCompanionVisible: false,
      TutorialTextShow: "",
      screen: "adventure",
      GivenName: "",
      heroGreeting: "Hello",
      heroPunctuation: "!",
      heroMessage: "Verify your Tasks and get more coins!",
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],

        Tasks: [
          {
            IDTask1: "aaaa",
            IDTask2: "bbbb",
            Title: "Please wait or drag down to refresh...",
            Checked: true,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        URL: `aaaa`
      },
      FightStatus: {
        IDShopObject: "",
        IDMap: "im1",
        URL:
          "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire_evil_tree.png?alt=media\u0026token=1e0fbefa-e906-458b-b5ae-49d13a14a39c",
        Title: "Evil Tree of Istan Varul",
        Description:
          "Looming to the West, you see dead trees dancing into the wind. No doubt here: a dark power is at play. Trees are life but here: trees are dead. Suddenly one of the trees starts growing and growing! And you can hear a deep gravelous laugh that freezes you to death and makes you shiver from tip to toes. The other trees have magically gathered around you and the only way forwards is towards this creature: what shall we do?",
        OnBuyMessage:
          "Ahahahah! See how we are rotting? We are thirsty... There never was water on this side of Nightmare Shire! We had to adapt: our roots need blood now! It is nothing against you.",
        OnUseMessage:
          "The trees also block your way out. If you do not fight, this monstruous tree will kill us! DO something! DOUBLE-TAP that monster tree now!",
        OnSellMessage:
          'After this tough fight, you finally reduced the tree to an amount of wood beeches. You set a fire to cook your tofu and enjoy your victory with your friends. While the campfire crackles, you can distinctively hear through the flames: "This is not finished: you will never beat your own wretchedness!". This monster tree really is a party pooper!',
        CharacterURL:
          "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Ftree_monster.gif?alt=media\u0026token=ce10fb95-fb12-4624-a899-f597352f73e1",
        MonsterIsIntroduced: false,
        DamageToMonster: 28,
        MonsterRemainingLife: -25,
        DamageToHero: 6,
        HeroRemainingLife: 29,
        MapStepOrder: 3,
        CharacterReputation: 3,
        CharacterIntelligence: 3,
        CharacterFitness: 5,
        CharacterCoinsBonus: 15,
        CharacterLoveBonus: 2,
        CharacterDarkBonus: 0
      }
    };
  }

  SetHeroTutoText = async () => {
    let TutorialText = "",
      TutorialTextShow = "",
      i;
    TutorialLevel = this.state.HeroData.TutorialLevel;
    if (TutorialLevel == 3) {
      TutorialTextShow = `Tap on this Elf to see what he has to say!`;
      this.setState({ TutorialTextShow });
    }
    if (TutorialLevel == 4) {
      TutorialTextShow = `Press longer on the Elf to accept his quest!`;
      this.setState({ TutorialTextShow });
    }
    if (TutorialLevel == 5) {
      TutorialTextShow = `Tap on this monster to see his strength!`;
      this.setState({ TutorialTextShow });
    }
    if (TutorialLevel == 6) {
      TutorialTextShow = `Tap again on the monster to see what he has to say!`;
      this.setState({ TutorialTextShow });
    }
    if (TutorialLevel == 7) {
      TutorialTextShow = `Press longer on the monster to fight!`;
      this.setState({ TutorialTextShow });
    }
    if (TutorialLevel == 11) {
      TutorialTextShow = `How did we get here?! 

I understand: this is not our world... This is the universe of our team leader!

If we help the leader, we will share the rewards of victories. 

This ghost is too strong though: we need backup!`;

      this.setState({ TutorialTextShow });
    }
  };

  SetIsTutoVisible = async IsTutoVisible => {
    this.setState({ IsTutoProcess: IsTutoVisible });
    await this.setState({ IsTalkingMonsterVisible: false });
    await this.setState({ IsChoosingCompanionVisible: false });
    await help.Wait(500);
    await this.setState({ IsTutoVisible });
    this.SetHeroTutoText();
  };

  SetIsTalkingMonsterVisible = async IsTalkingMonsterVisible => {
    await this.setState({ IsTalkingMonsterVisible: false });
    await this.setState({ IsChoosingCompanionVisible: false });
    await help.Wait(500);
    this.setState({ IsTalkingMonsterVisible });
  };

  SetIsChoosingCompanionVisible = async IsChoosingCompanionVisible => {
    if (!this.state.IsTutoProcess) {
      await this.setState({ IsTalkingMonsterVisible: false });
      await this.setState({ IsChoosingCompanionVisible: false });
      await help.Wait(500);
      this.setState({ IsChoosingCompanionVisible });
    }
  };

  SetHitImageState = async (
    HitImage1,
    HitImage2,
    HitImage3,
    HitImage4,
    HitImage5,
    HitImage6
  ) => {
    this.setState({
      HitImage1,
      HitImage2,
      HitImage3,
      HitImage4,
      HitImage5,
      HitImage6
    });
  };

  SetLoveImageState = async LoveImage => {
    this.setState({
      LoveImage
    });
  };

  HandleTapMonster = async () => {
    TutorialLevel = this.state.HeroData.TutorialLevel;
    now = new Date();
    if (
      TutorialLevel == 3 ||
      TutorialLevel == 5 ||
      TutorialLevel == 6 ||
      TutorialLevel > 7
    ) {
      if (TutorialLevel == 3 || TutorialLevel == 6) {
        help.PostTutorialLevel(TutorialLevel + 1);
        helpAdvAnim.BlueArrowUpTutoDisappear(this.state.OpacityBlueArrowUpTuto);
      }
      if (TutorialLevel == 5) {
        help.PostTutorialLevel(TutorialLevel + 1);
        helpAdvAnim.BlueArrowUpTutoDisappear(this.state.OpacityBlueArrowUpTuto);
        this._onRefresh(false, false, true);
      }
      let MonsterPressCount = this.state.MonsterPressCount;
      if (
        MonsterPressCount % 2 == 0 &&
        this.state.FightStatus.MonsterRemainingLife !=
          -this.state.FightStatus.DamageToMonster
      ) {
        helpAdvAnim.MakeMonsterFightStatAppear(
          this.state.MonsterShieldOpacity,
          this.state.MonsterSwordOpacity,
          this.state.MonsterBookOpacity
        );
        // if there is no tuto popover and if (last time the monster talked was more than 2 seconds ago OR the monster never talked before)
      } else if (
        !this.state.IsTutoProcess &&
        (now - this.state.LastMonsterSpeak < 5000 ||
          !this.state.LastMonsterSpeak)
      ) {
        let MonsterTalkCount = this.state.MonsterTalkCount;
        this.SetIsTalkingMonsterVisible(true);
        MonsterTalkCount += 1;
        this.setState({ MonsterTalkCount });
      }
      MonsterPressCount += 1;
      this.setState({ MonsterPressCount });
    }
  };

  HandleTapHero = async () => {
    if (this.state.HeroData.TutorialLevel > 7) {
      let HeroPressCount = this.state.HeroPressCount;
      if (HeroPressCount % 2 == 0) {
        helpAdvAnim.MakeHeroFightStatAppear(
          this.state.HeroLifeBarOpacity,
          this.state.HeroFollowerOpacity,
          this.state.HeroLoveOpacity,
          this.state.HeroCoinsOpacity,
          this.state.HeroShieldOpacity,
          this.state.HeroSwordOpacity,
          this.state.HeroBookOpacity
        );
      } else {
        if (this.state.HeroData.TutorialLevel > 7) {
          LoveGift = await help.GetLoveGift(this.state.LoveGift);
          if (LoveGift) {
            await this.setState({ LoveGift });
            if (LoveGift.LoveEnergyGift > 0) {
              randIndex1 = Math.floor(Math.random() * LoveImage.length);
              this.SetLoveImageState(LoveImage[randIndex1]);
              helpAdvAnim.LoveGiftAnim(
                this.state.HeroX3Position,
                this.state.HeroY3Position,
                this.state.LoveGiftMove,
                this.state.LoveGiftOpacity,
                this.state.LoveCostBonusOpacity
              );
            }
          }
        }

        helpAdvAnim.MakeHeroStatAppear(
          this.state.HeroLifeBarOpacity,
          this.state.HeroFollowerOpacity,
          this.state.HeroLoveOpacity,
          this.state.HeroCoinsOpacity,
          this.state.HeroShieldOpacity,
          this.state.HeroSwordOpacity,
          this.state.HeroBookOpacity
        );
      }
      HeroPressCount += 1;
      this.setState({ HeroPressCount });

      if (this.state.HeroData.TutorialLevel > 15) {
        LoveGift = await help.GetLoveGift(this.state.LoveGift);
        if (LoveGift) {
          await this.setState({ LoveGift });
          if (LoveGift.LoveEnergyGift > 0) {
            randIndex1 = Math.floor(Math.random() * LoveImage.length);
            this.SetLoveImageState(LoveImage[randIndex1]);
            helpAdvAnim.LoveGiftAnim(
              this.state.HeroX3Position,
              this.state.HeroY3Position,
              this.state.LoveGiftMove,
              this.state.LoveGiftOpacity,
              this.state.LoveCostBonusOpacity
            );
          }
        }
      }
    }
  };

  HandleTapTeam = async () => {
    if (this.state.HeroData.TutorialLevel > 7) {
      let TeamPressCount = this.state.TeamPressCount;
      if (TeamPressCount % 2 == 0) {
        helpAdvAnim.MakeTeamFightStatAppear(
          this.state.TeamShieldOpacity,
          this.state.TeamSwordOpacity,
          this.state.TeamBookOpacity,
          this.state.TeamLifeBarOpacity,
          this.state.OpacityTopCompanionCount
        );
      } else {
        helpAdvAnim.MakeTeamFightStatAppear(
          this.state.TeamShieldOpacity,
          this.state.TeamSwordOpacity,
          this.state.TeamBookOpacity,
          this.state.TeamLifeBarOpacity,
          this.state.OpacityTopCompanionCount
        );
      }
      TeamPressCount += 1;
      this.setState({ TeamPressCount });
    }
  };

  OnLookForQuest = (FightStatus, HeroData) => {
    if (
      !FightStatus.IDShopObject &&
      HeroData.IsBondedToTeam &&
      !HeroData.IsBondedTeamLeader
    ) {
      Alert.alert(
        "",
        `Your team leader needs to look for a new quest. 
You can contact your leader or follow your own path for now.`,
        [
          {
            text: "Go to Team",
            onPress: () => this.props.navigation.navigate("Team")
          },
          {
            text: "Unfollow Leader",
            onPress: () => help.PostHelpTeamLeader(false, HeroData.IDTeam)
          }
        ],
        { cancelable: true }
      );
    } else {
      Alert.alert(
        "",
        `You finished your quest!
You can find new quests in the shop.`,
        [
          {
            text: "Go to Shop",
            onPress: () => this.props.navigation.navigate("Shop")
          }
        ],
        { cancelable: true }
      );
    }
  };

  OnAttack = async () => {
    let TutorialLevel = this.state.HeroData.TutorialLevel;
    if (TutorialLevel != 11) {
      if (!this.state.IsAttacking) {
        this.setState({ IsAttacking: true });

        if (
          TutorialLevel == 4 ||
          TutorialLevel == 7 ||
          TutorialLevel == 8 ||
          TutorialLevel >= 12
        ) {
          if (TutorialLevel == 4 || TutorialLevel == 7) {
            help.PostTutorialLevel(TutorialLevel + 1);
            helpAdvAnim.BlueArrowUpTutoDisappear(
              this.state.OpacityBlueArrowUpTuto
            );
          }
          // if friend
          if (
            this.state.FightStatus.MonsterRemainingLife ==
            -this.state.FightStatus.DamageToMonster
          ) {
            await help.PostMonsterIsIntroduced();
            await help.Wait(500);

            await this._onRefresh(false, true, false);
            this.SetIsTalkingMonsterVisible(true);

            // if not enough coins
          } else if (
            2 * this.state.FightStatus.CharacterFitness >
            HeroData.Coins + this.state.HeroData.Reputation
          ) {
            this.setState({
              CoinsCostBonus:
                `Need
` + (2 * this.state.FightStatus.CharacterFitness).toString()
            });
            helpAdvAnim.NotEnoughCoinsAnim(
              this.state.LoveCostBonusOpacity,
              this.state.CoinsCostBonusMove,
              this.state.CoinsCostBonusOpacity,
              this.state.HeroCoinsOpacity,
              this.state.HeroX3Position,
              this.state.HeroY3Position
            );
            // if not enough love
          } else if (
            this.state.HeroData.LoveEnergy < this.state.FightStatus.LovePrice
          ) {
            helpAdvAnim.NotEnoughLoveAnim(
              this.state.LoveCostBonusMove,
              this.state.LoveCostBonusOpacity,
              this.state.HeroLoveOpacity,
              this.state.HeroX3Position,
              this.state.HeroY3Position
            );
            // else attack
          } else {
            if (TutorialLevel == 12) {
              helpAdvAnim.BlueArrowUpTutoDisappear(
                this.state.OpacityBlueArrowUpTuto
              );
              help.PostTutorialLevel(TutorialLevel + 1);
            }
            await this.FightAnim();
            if (TutorialLevel != 12) {
              await this._onRefresh(true, false, false);
              await this.DamageRewardAnim();
            }
            if (TutorialLevel == 12) {
              await help.Wait(1500);
              this.props.navigation.navigate("Team");
            }
          }
        }
        this.setState({ IsAttacking: false });
      }
    }
  };

  DealWithTutoState = async (HeroData, FightStatus) => {
    if (HeroData.TutorialLevel == 11 || HeroData.TutorialLevel == 12) {
      FightStatus.URL =
        "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Funforeseen_island%2Funforeseen_island.jpg?alt=media&token=dbd3d197-3a25-4485-8390-fc76e9472a4c";
      FightStatus.CharacterURL =
        "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Funforeseen_island%2Fbloody_succubus.png?alt=media&token=660d404c-da92-485b-ac69-51e8f66ea299";
      FightStatus.Title = "The Bloody Succubus";
      FightStatus.Description =
        "I am in your dreams... Voluptuous dreams... Why bother waking up?";
      FightStatus.OnBuyMessage =
        "This island is such a cold place: thank you for warming my heart! Let me warm yours!";
      FightStatus.CharacterReputation = 20;
      FightStatus.CharacterIntelligence = 40;
      FightStatus.CharacterFitness = 0; // so that companion can always attack
      FightStatus.MonsterRemainingLife = 13;
      FightStatus.TeamXPosition = 300;
      FightStatus.TeamYPosition = 230;
      FightStatus.MonsterXPosition = 250;
      FightStatus.MonsterYPosition = 330;
      FightStatus.HeroX1Position = 600;
      FightStatus.HeroY1Position = 150;
      FightStatus.HeroX2Position = 500;
      FightStatus.HeroY2Position = 200;
      FightStatus.HeroX3Position = 350;
      FightStatus.HeroY3Position = 260;
    }

    if (HeroData.TutorialLevel == 12) {
      FightStatus.FightStatusTeam = [
        {
          IDHero: "aaaa",
          GivenName: "Kickass",
          NewDamageToMonster: 0,
          DamageToHero: 0,
          NewDamageToHero: 0,
          HeroRemainingLife: 1,
          HeroURL:
            "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fzora2.gif?alt=media&token=12e0ecbd-a937-4fdd-91fc-96601d33a099",
          HeroReputation: 1,
          HeroIntelligence: 1,
          HeroFitness: 1
        }
      ];
    }

    return FightStatus;
  };

  // ----------------------------------------------------------------------------------------------------
  _onRefresh = async (IsFight, IsAdventureStepStart, IsTutoRefresh) => {
    this.setState({ refreshing: true });
    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    if (HeroData.TutorialLevel > 15) {
      LoveGift = await help.GetLoveGift(this.state.LoveGift);
      if (LoveGift) {
        await this.setState({ LoveGift });
        if (LoveGift.LoveEnergyGift > 0) {
          randIndex1 = Math.floor(Math.random() * LoveImage.length);
          this.SetLoveImageState(LoveImage[randIndex1]);
          helpAdvAnim.LoveGiftAnim(
            this.state.HeroX3Position,
            this.state.HeroY3Position,
            this.state.LoveGiftMove,
            this.state.LoveGiftOpacity,
            this.state.LoveCostBonusOpacity
          );
        }
      }
    }

    let FightStatus = await help.GetFightStatus(IsFight);

    FightStatus = await this.DealWithTutoState(HeroData, FightStatus);

    if (FightStatus.FightStatusTeam) {
      helpAdvAnim.MakeTeamAppear(
        this.state.OpacityTeammate1,
        this.state.OpacityTeammate2,
        this.state.OpacityTeammate3,
        this.state.TeamLifeBarOpacity,
        this.state.OpacityTopCompanionCount
      );
    }

    TeamFitness = await help.GetTeamFitness(FightStatus.FightStatusTeam);
    TeamIntelligence = await help.GetTeamIntelligence(
      FightStatus.FightStatusTeam
    );
    TeamReputation = await help.GetTeamReputation(FightStatus.FightStatusTeam);
    WeakestCompanionLifeRatio = await help.GetWeakestCompanionLifeRatio(
      FightStatus.FightStatusTeam
    );

    let IsAvailableMode = HeroData.TutorialLevel >= 3;

    if (IsAdventureStepStart) {
      this.setState({ MonsterPressCount: 0, MonsterTalkCount: 0 });
      this.AdventureStepStart(
        FightStatus,
        TeamFitness,
        TeamIntelligence,
        TeamReputation,
        WeakestCompanionLifeRatio,
        IsAvailableMode
      );
    } else if (!FightStatus.URL) {
      Animated.timing(this.state.HeroMove, {
        toValue: {
          x: 180,
          y: 100
        },
        duration: 6000
      });
    }

    this.setState({
      IsAvailableMode,
      refreshing: false,
      FightStatus,
      WeakestCompanionLifeRatio,
      TeamFitness,
      TeamIntelligence,
      TeamReputation,
      LoveCostBonus: FightStatus.LovePrice
    });

    if (IsTutoRefresh) {
      TutorialLevel = HeroData.TutorialLevel;
      if (
        TutorialLevel == 3 ||
        TutorialLevel == 4 ||
        TutorialLevel == 6 ||
        TutorialLevel == 7
      ) {
        this.SetIsTutoVisible(true);
      }
      if (TutorialLevel == 5 || TutorialLevel == 11) {
        await help.Wait(6000);
        this.SetIsTutoVisible(true);
      }
    }
  };

  // ---------------------------------------------------------------------------------------------------
  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._onRefresh(false, true, true);
    });

    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    Advice = help.GetAdvice(this.state.screen, HeroData.TaskCount);
    if (!!Advice.Message) {
      this.setState({
        heroGreeting: Advice.Greeting,
        heroPunctuation: Advice.Punctuation,
        heroMessage: Advice.Message
      });
    }

    FightStatus = await help.GetFightStatus(false);
    FightStatus = await this.DealWithTutoState(HeroData, FightStatus);
    // NB: helpAdvAnim.MakeTeamAppear is used in AdventureStepStart as opposed to refresh because we need to create opacity etc.

    TeamFitness = await help.GetTeamFitness(FightStatus.FightStatusTeam);
    TeamIntelligence = await help.GetTeamIntelligence(
      FightStatus.FightStatusTeam
    );
    TeamReputation = await help.GetTeamReputation(FightStatus.FightStatusTeam);
    WeakestCompanionLifeRatio = await help.GetWeakestCompanionLifeRatio(
      FightStatus.FightStatusTeam
    );

    let IsAvailableMode = HeroData.TutorialLevel >= 3;

    this.AdventureStepStart(
      FightStatus,
      TeamFitness,
      TeamIntelligence,
      TeamReputation,
      WeakestCompanionLifeRatio,
      IsAvailableMode
    );

    this.setState({
      FightStatus,
      WeakestCompanionLifeRatio,
      TeamFitness,
      TeamIntelligence,
      TeamReputation,
      LoveCostBonus: FightStatus.LovePrice
    });

    TutorialLevel = HeroData.TutorialLevel;
    if (
      TutorialLevel == 3 ||
      TutorialLevel == 4 ||
      TutorialLevel == 6 ||
      TutorialLevel == 7
    ) {
      this.SetIsTutoVisible(true);
    }
    if (TutorialLevel == 5 || TutorialLevel == 11) {
      await help.Wait(6000);
      this.SetIsTutoVisible(true);
    }
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Container>
        <Image
          blurRadius={100}
          source={require("../assets/images/map_background.jpg")}
          style={{
            width: "100%",
            height: "100%",
            resizeMode: "stretch",
            opacity: 1,
            position: "absolute",
            zIndex: 0
          }}
        />
        <View
          style={{
            flex: 1,
            alignSelf: "center"
          }}
        >
          {this.RenderStartNewQuest()}

          <ScrollView ref="ScrollY">
            <ScrollView ref="ScrollX" horizontal={true}>
              <View style={{ alignSelf: "center", zIndex: 1 }}>
                <HeroHitAnimation
                  HitImage1={this.state.HitImage1}
                  HitImage2={this.state.HitImage2}
                  OpacityHitImage1={this.state.OpacityHitImage1}
                  OpacityHitImage2={this.state.OpacityHitImage2}
                  HeroHitMove={this.state.HeroHitMove}
                />

                <MonsterHitAnimation
                  HitImage3={this.state.HitImage3}
                  HitImage4={this.state.HitImage4}
                  OpacityHitImage3={this.state.OpacityHitImage3}
                  OpacityHitImage4={this.state.OpacityHitImage4}
                  MonsterHitMove={this.state.MonsterHitMove}
                />

                <TeamHitAnimation
                  HitImage5={this.state.HitImage5}
                  HitImage6={this.state.HitImage6}
                  OpacityHitImage5={this.state.OpacityHitImage5}
                  OpacityHitImage6={this.state.OpacityHitImage6}
                  TeamHitMove={this.state.TeamHitMove}
                />

                <CoinsCostBonusAnimation
                  CoinsCostBonusOpacity={this.state.CoinsCostBonusOpacity}
                  CoinsCostBonusMove={this.state.CoinsCostBonusMove}
                  CoinsCostBonus={this.state.CoinsCostBonus}
                />

                <LoveGiftAnimation
                  LoveGift={this.state.LoveGift}
                  LoveGiftOpacity={this.state.LoveGiftOpacity}
                  LoveGiftMove={this.state.LoveGiftMove}
                  LoveImage={this.state.LoveImage}
                />

                <BlueArrowUpTuto
                  BlueArrowUpTutoMove={this.state.BlueArrowUpTutoMove}
                  OpacityBlueArrowUpTuto={this.state.OpacityBlueArrowUpTuto}
                  HeroData={this.state.HeroData}
                />

                <BlueArrowUpTutoHero
                  BlueArrowUpTutoMoveHero={this.state.BlueArrowUpTutoMoveHero}
                  OpacityBlueArrowUpTutoHero={
                    this.state.OpacityBlueArrowUpTutoHero
                  }
                  HeroData={this.state.HeroData}
                />

                {this.RenderLoveCostBonusAnimation()}

                {this.RenderHeroAnimation()}

                {this.RenderMonsterAnimation()}

                {this.RenderTeamAnimation()}

                <Image
                  source={{
                    uri: this.state.FightStatus.URL
                      ? this.state.FightStatus.URL
                      : "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566"
                  }}
                  style={{
                    width: 750,
                    height: 1000,
                    resizeMode: "stretch",
                    opacity: 0.85,
                    alignSelf: "center",
                    zIndex: 1
                  }}
                />
              </View>
            </ScrollView>
          </ScrollView>
        </View>
        {this.RenderIsNotAvailableMode()}
      </Container>
    );
  }

  RenderIsNotAvailableMode() {
    if (!this.state.IsAvailableMode) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            this.props.navigation.navigate("Hero");
            this._onRefresh(false, true, true);
          }}
          style={{
            backgroundColor: "rgba(255,255,255,0.7)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              zIndex: 3,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 15,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 4,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              Add your first task to access adventure mode
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  RenderLoveCostBonusAnimation() {
    if (
      this.state.FightStatus.URL &&
      this.state.FightStatus.MonsterRemainingLife !=
        -this.state.FightStatus.DamageToMonster
    ) {
      return (
        <LoveCostBonusAnimation
          LoveCostBonusOpacity={this.state.LoveCostBonusOpacity}
          LoveCostBonusMove={this.state.LoveCostBonusMove}
          LoveCostBonus={this.state.LoveCostBonus}
        />
      );
    }
  }

  RenderHeroAnimation() {
    return (
      <HeroAnimation
        HeroData={this.state.HeroData}
        HeroMove={this.state.HeroMove}
        HeroLifeBarOpacity={this.state.HeroLifeBarOpacity}
        HeroLoveOpacity={this.state.HeroLoveOpacity}
        HeroCoinsOpacity={this.state.HeroCoinsOpacity}
        HeroShieldOpacity={this.state.HeroShieldOpacity}
        HeroSwordOpacity={this.state.HeroSwordOpacity}
        HeroBookOpacity={this.state.HeroBookOpacity}
        HeroFollowerOpacity={this.state.HeroFollowerOpacity}
        FightStatus={this.state.FightStatus}
        HandleTapHero={this.HandleTapHero.bind(this)}
        _onRefresh={this._onRefresh.bind(this)}
        SetIsChoosingCompanionVisible={this.SetIsChoosingCompanionVisible.bind(
          this
        )}
        IsChoosingCompanionVisible={this.state.IsChoosingCompanionVisible}
        OpacityHeroDamage={this.state.OpacityHeroDamage}
        IsOnQuest={!!this.state.FightStatus.URL}
        IsFriend={
          this.state.FightStatus.MonsterRemainingLife ==
          -this.state.FightStatus.DamageToMonster
        }
        LoveGift={this.state.LoveGift}
        IsTutoVisible={this.state.IsTutoVisible}
        SetIsTutoVisible={this.SetIsTutoVisible.bind(this)}
        BlueArrowUpTutoAnim={async () =>
          helpAdvAnim.BlueArrowUpTutoAnim(
            this.state.MonsterXPosition,
            this.state.MonsterYPosition,
            this.state.BlueArrowUpTutoMove,
            this.state.OpacityBlueArrowUpTuto
          )
        }
        BlueArrowUpTutoAnimHero={async () =>
          helpAdvAnim.BlueArrowUpTutoAnimHero(
            this.state.HeroX3Position,
            this.state.HeroY3Position,
            this.state.BlueArrowUpTutoMoveHero,
            this.state.OpacityBlueArrowUpTutoHero
          )
        }
        TutorialTextShow={this.state.TutorialTextShow}
        BlueArrowUpTutoDisappearHero={async () =>
          helpAdvAnim.BlueArrowUpTutoDisappear(
            this.state.OpacityBlueArrowUpTutoHero
          )
        }
      />
    );
  }

  RenderMonsterAnimation() {
    if (this.state.FightStatus.URL) {
      return (
        <MonsterAnimation
          HeroData={this.state.HeroData}
          MonsterMove={this.state.MonsterMove}
          MonsterShieldOpacity={this.state.MonsterShieldOpacity}
          MonsterSwordOpacity={this.state.MonsterSwordOpacity}
          MonsterBookOpacity={this.state.MonsterBookOpacity}
          FightStatus={this.state.FightStatus}
          HandleTapMonster={this.HandleTapMonster.bind(this)}
          SetIsTalkingMonsterVisible={this.SetIsTalkingMonsterVisible.bind(
            this
          )}
          IsTalkingMonsterVisible={this.state.IsTalkingMonsterVisible}
          OnAttack={this.OnAttack.bind(this)}
          OpacityMonsterDamage={this.state.OpacityMonsterDamage}
          MonsterTalkCount={this.state.MonsterTalkCount}
          NewDamageToMonster={this.state.NewDamageToMonster}
          _onRefresh={this._onRefresh.bind(this)}
          IsFriend={
            this.state.FightStatus.MonsterRemainingLife ==
            -this.state.FightStatus.DamageToMonster
          }
          navigation={this.props.navigation}
        />
      );
    }
  }

  RenderTeamAnimation() {
    if (
      this.state.FightStatus.URL ||
      this.state.FightStatus.MonsterRemainingLife !=
        -this.state.FightStatus.DamageToMonster
    ) {
      return (
        <TeamAnimation
          FightStatusTeam={this.state.FightStatus.FightStatusTeam}
          TeamMove={this.state.TeamMove}
          TeamLifeBarOpacity={this.state.TeamLifeBarOpacity}
          TeamShieldOpacity={this.state.TeamShieldOpacity}
          TeamSwordOpacity={this.state.TeamSwordOpacity}
          TeamBookOpacity={this.state.TeamBookOpacity}
          OpacityTeammate1={this.state.OpacityTeammate1}
          OpacityTeammate2={this.state.OpacityTeammate2}
          OpacityTeammate3={this.state.OpacityTeammate3}
          FightStatus={this.state.FightStatus}
          HandleTapTeam={this.HandleTapTeam.bind(this)}
          WeakestCompanionLifeRatio={this.state.WeakestCompanionLifeRatio}
          TeamFitness={this.state.TeamFitness}
          TeamIntelligence={this.state.TeamIntelligence}
          TeamReputation={this.state.TeamReputation}
          OpacityTeamDamage={this.state.OpacityTeamDamage}
          OpacityTopCompanionCount={this.state.OpacityTopCompanionCount}
          NewDamageToTeam={this.state.NewDamageToTeam}
          IsFriend={
            this.state.FightStatus.MonsterRemainingLife ==
            -this.state.FightStatus.DamageToMonster
          }
        />
      );
    }
  }

  RenderStartNewQuest() {
    if (!this.state.FightStatus.IDShopObject) {
      return (
        <View
          style={{
            flex: 1,
            marginTop: 290,
            marginLeft: 14,
            zIndex: 2,
            position: "absolute"
          }}
        >
          <Button
            onPress={() => {
              this.OnLookForQuest(this.state.FightStatus, this.state.HeroData);
            }}
            info
            style={{ alignSelf: "center" }}
          >
            <Txt> Start New Quest </Txt>
          </Button>
        </View>
      );
    }
  }

  AdventureStepStart = async (
    FightStatus,
    TeamFitness,
    TeamIntelligence,
    TeamReputation,
    WeakestCompanionLifeRatio,
    IsAvailableMode
  ) => {
    MonsterXPosition =
      FightStatus.MonsterXPosition + 10 * (Math.random() - 0.5);
    MonsterYPosition =
      FightStatus.MonsterYPosition + 10 * (Math.random() - 0.5);
    TeamXPosition = FightStatus.TeamXPosition;
    TeamYPosition = FightStatus.TeamYPosition;
    HeroX1Position = FightStatus.HeroX1Position;
    HeroY1Position = FightStatus.HeroY1Position;
    HeroX2Position = FightStatus.HeroX2Position;
    HeroY2Position = FightStatus.HeroY2Position;
    HeroX3Position = FightStatus.HeroX3Position;
    HeroY3Position = FightStatus.HeroY3Position;

    this.setState({
      MonsterMove: new Animated.ValueXY({
        x: MonsterXPosition,
        y: MonsterYPosition
      }),
      HeroMove: new Animated.ValueXY({
        x: FightStatus.URL ? HeroX1Position : 180,
        y: FightStatus.URL ? HeroY1Position : 100
      }),
      TeamMove: new Animated.ValueXY({
        x: TeamXPosition,
        y: TeamYPosition
      }),
      HeroHitMove: new Animated.ValueXY({
        x: HeroX3Position,
        y: HeroY3Position
      }),
      MonsterHitMove: new Animated.ValueXY({
        x: MonsterXPosition,
        y: MonsterYPosition
      }),
      TeamHitMove: new Animated.ValueXY({
        x: TeamXPosition,
        y: TeamYPosition
      }),
      LoveCostBonusMove: new Animated.ValueXY({
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      }),
      CoinsCostBonusMove: new Animated.ValueXY({
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      }),
      DarkCostBonusMove: new Animated.ValueXY({
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      }),
      LoveGiftMove: new Animated.ValueXY({
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      }),
      BlueArrowUpTutoMove: new Animated.ValueXY({
        x: MonsterXPosition - 40,
        y: MonsterYPosition + 110
      }),
      BlueArrowUpTutoMoveHero: new Animated.ValueXY({
        x: HeroX3Position - 20,
        y: HeroY3Position + 110
      }),
      HeroX3Position,
      HeroY3Position,
      MonsterXPosition,
      MonsterYPosition,
      TeamXPosition,
      TeamYPosition
    });

    this.setState({
      IsAvailableMode,
      loading: false
    });

    // start the move loop of the monster
    helpAdvAnim.MonsterMoveLoop(
      this.state.MonsterMove,
      MonsterXPosition,
      MonsterYPosition
    );

    // make hero go to the zone of combat
    helpAdvAnim.HeroMove(
      this.state.HeroMove,
      HeroX2Position,
      HeroY2Position,
      HeroX3Position,
      HeroY3Position,
      !!FightStatus.URL
    );

    // make the screen follow the hero
    await this.FollowHeroWithScroll();

    if (FightStatus.FightStatusTeam) {
      helpAdvAnim.MakeTeamAppear(
        this.state.OpacityTeammate1,
        this.state.OpacityTeammate2,
        this.state.OpacityTeammate3,
        this.state.TeamLifeBarOpacity,
        this.state.OpacityTopCompanionCount
      );
    }

    if (this.state.HeroData.TutorialLevel > 16) {
      await help.Wait(1000);
      LoveGift = await help.GetLoveGift(this.state.LoveGift);
      if (LoveGift) {
        await this.setState({ LoveGift });
        if (LoveGift.LoveEnergyGift > 0) {
          randIndex1 = Math.floor(Math.random() * LoveImage.length);
          this.SetLoveImageState(LoveImage[randIndex1]);
          helpAdvAnim.LoveGiftAnim(
            this.state.HeroX3Position,
            this.state.HeroY3Position,
            this.state.LoveGiftMove,
            this.state.LoveGiftOpacity,
            this.state.LoveCostBonusOpacity
          );
        }
      }
    }
  };

  FollowHeroWithScroll = async () => {
    var i;
    for (i = 0; i < 85; i++) {
      xMove = this.state.HeroMove.x;
      yMove = this.state.HeroMove.y;
      xMoveNumber = xMove[Object.keys(xMove)[1]];
      yMoveNumber = yMove[Object.keys(yMove)[1]];

      await help.Wait(6);
      this.refs.ScrollY.scrollTo({
        x: xMoveNumber - 100,
        y: yMoveNumber - 100,
        animated: true
      });
      this.refs.ScrollX.scrollTo({
        x: xMoveNumber - 100,
        y: yMoveNumber - 100,
        animated: true
      });
    }

    xMove = this.state.MonsterMove.x;
    yMove = this.state.MonsterMove.y;
    xMoveNumber = xMove[Object.keys(xMove)[1]];
    yMoveNumber = yMove[Object.keys(yMove)[1]];

    await help.Wait(3000);
    this.refs.ScrollY.scrollTo({
      x: xMoveNumber - 100,
      y: yMoveNumber - 200,
      animated: true
    });
    this.refs.ScrollX.scrollTo({
      x: xMoveNumber - 100,
      y: yMoveNumber - 200,
      animated: true
    });
  };

  DamageRewardAnim = async () => {
    let FightStatus = this.state.FightStatus;
    let FightStatusTeam = FightStatus.FightStatusTeam,
      i;

    // damage to monster from hero
    let NewDamageToMonster = FightStatus.NewDamageToMonster;
    // add damage to monster from each team member
    if (FightStatusTeam) {
      for (i = 0; i < FightStatusTeam.length; i++) {
        NewDamageToMonster += FightStatusTeam[i].NewDamageToMonster;
      }
    }
    if (NewDamageToMonster > 0) {
      this.setState({ NewDamageToMonster: -NewDamageToMonster });
      helpAdvAnim.NewDamageToMonster(this.state.OpacityMonsterDamage);
    }

    if (FightStatus.NewDamageToHero > 0) {
      this.setState({ NewDamageToHero: -FightStatus.NewDamageToHero });
      helpAdvAnim.NewDamageToHero(this.state.OpacityHeroDamage);
    }

    let NewDamageToTeam = 0;
    if (FightStatusTeam) {
      for (i = 0; i < FightStatusTeam.length; i++) {
        NewDamageToTeam += FightStatusTeam[i].NewDamageToHero;
      }
    }
    if (NewDamageToTeam > 0) {
      this.setState({ NewDamageToTeam: -NewDamageToTeam });
      helpAdvAnim.NewDamageToTeam(
        this.state.OpacityTeamDamage,
        this.state.OpacityTopCompanionCount
      );
    }

    await help.Wait(3000);

    if (FightStatus.HeroRemainingLife < 0) {
      // if -NewDamageToHero is more than HeroRemainingLife then hero's life just got negative so we take HeroRemainingLife which is negattive
      // else, it means Hero's life was already negative and we take -NewDamageToHero
      // if -life < newdamage then life else -newdamage
      HeroRemainingLife = FightStatus.HeroRemainingLife;
      NewDamageToHero = FightStatus.NewDamageToHero;
      CoinsCostBonus =
        -HeroRemainingLife < NewDamageToHero
          ? HeroRemainingLife
          : -NewDamageToHero;
      if (CoinsCostBonus < 0) {
        this.setState({ CoinsCostBonus });
        helpAdvAnim.SendHeroCoinsToMonster(
          this.state.CoinsCostBonusOpacity,
          this.state.CoinsCostBonusMove,
          this.state.HeroX3Position,
          this.state.HeroY3Position,
          this.state.MonsterXPosition,
          this.state.MonsterYPosition
        );
        await help.Wait(3000);
      }
    }

    // same thing for each hero in the combat party, but sum the coins lost of each hero
    let TeamCoinsCostBonus = 0;
    if (FightStatusTeam) {
      for (i = 0; i < FightStatusTeam.length; i++) {
        if (FightStatusTeam[i].HeroRemainingLife < 0) {
          HeroRemainingLife = FightStatusTeam[i].HeroRemainingLife;
          NewDamageToHero = FightStatusTeam[i].NewDamageToHero;
          TeamCoinsCostBonus +=
            -HeroRemainingLife < NewDamageToHero
              ? HeroRemainingLife
              : -NewDamageToHero;
        }
      }
    }

    if (TeamCoinsCostBonus < 0) {
      this.setState({ CoinsCostBonus: TeamCoinsCostBonus });
      helpAdvAnim.SendTeamCoinsToMonster(
        this.state.CoinsCostBonusOpacity,
        this.state.CoinsCostBonusMove,
        this.state.TeamXPosition,
        this.state.TeamYPosition,
        this.state.MonsterXPosition,
        this.state.MonsterYPosition
      );
      await help.Wait(3000);
    }

    //teamRewardRatio := int(math.Ceil(math.Pow(arrayOfIDHeroAllLength64, 0.85) / arrayOfIDHeroAllLength64))

    let FollowerRewardRatio = FightStatusTeam
      ? Math.ceil(
          Math.pow(FightStatusTeam.length, 0.85) / FightStatusTeam.length
        )
      : 1;

    if (FightStatus.MonsterRemainingLife <= 0) {
      this.setState({
        CoinsCostBonus: FightStatus.CharacterCoinsBonus * FollowerRewardRatio
      });
      helpAdvAnim.SendMonsterCoinsToHero(
        this.state.CoinsCostBonusOpacity,
        this.state.CoinsCostBonusMove,
        this.state.HeroX3Position,
        this.state.HeroY3Position,
        this.state.MonsterXPosition,
        this.state.MonsterYPosition
      );

      await help.Wait(3000);

      this.setState({
        LoveCostBonus: FightStatus.CharacterLoveBonus * FollowerRewardRatio
      });

      helpAdvAnim.SendMonsterLoveToHero(
        this.state.LoveCostBonusOpacity,
        this.state.LoveCostBonusMove,
        this.state.HeroX3Position,
        this.state.HeroY3Position,
        this.state.MonsterXPosition,
        this.state.MonsterYPosition
      );
      await help.Wait(3000);

      if (FightStatusTeam) {
        this.setState({
          CoinsCostBonus:
            FightStatus.CharacterCoinsBonus *
            FollowerRewardRatio *
            FightStatusTeam.length
        });
        helpAdvAnim.SendMonsterCoinsToTeam(
          this.state.CoinsCostBonusOpacity,
          this.state.CoinsCostBonusMove,
          this.state.TeamXPosition,
          this.state.TeamYPosition,
          this.state.MonsterXPosition,
          this.state.MonsterYPosition
        );
        await help.Wait(3000);

        this.setState({
          LoveCostBonus:
            FightStatus.CharacterLoveBonus *
            FollowerRewardRatio *
            FightStatusTeam.length
        });
        helpAdvAnim.SendMonsterLoveToTeam(
          this.state.LoveCostBonusOpacity,
          this.state.LoveCostBonusMove,
          this.state.TeamXPosition,
          this.state.TeamYPosition,
          this.state.MonsterXPosition,
          this.state.MonsterYPosition
        );
        await help.Wait(3000);
      }

      this.SetIsTalkingMonsterVisible(true);
    }

    this.setState({ LoveCostBonus: FightStatus.LovePrice });

    helpAdvAnim.MakeAttackLoveCostAppear(
      this.state.LoveCostBonusMove,
      this.state.LoveCostBonusOpacity
    );
  };

  FightAnim = async () => {
    randIndex1 = Math.floor(Math.random() * HitImage.length);
    randIndex2 = Math.floor(Math.random() * HitImage.length);
    randIndex3 = Math.floor(Math.random() * HitImage.length);
    randIndex4 = Math.floor(Math.random() * HitImage.length);
    randIndex5 = Math.floor(Math.random() * HitImage.length);
    randIndex6 = Math.floor(Math.random() * HitImage.length);

    await this.SetHitImageState(
      HitImage[randIndex1],
      HitImage[randIndex2],
      HitImage[randIndex3],
      HitImage[randIndex4],
      HitImage[randIndex5],
      HitImage[randIndex6]
    );

    helpAdvAnim.SendHeroLoveToMonster(
      this.state.HeroX3Position,
      this.state.HeroY3Position,
      this.state.MonsterXPosition,
      this.state.MonsterYPosition,
      this.state.LoveCostBonusMove,
      this.state.LoveCostBonusOpacity
    );

    await help.Wait(2000);

    helpAdvAnim.OneBamAttackOnMonster(
      this.state.MonsterXPosition,
      this.state.MonsterYPosition,
      this.state.OpacityHitImage3,
      this.state.MonsterHitMove
    );

    await help.Wait(100 + 800 * Math.random());

    helpAdvAnim.OneBamAttackOnMonster(
      this.state.MonsterXPosition,
      this.state.MonsterYPosition,
      this.state.OpacityHitImage4,
      this.state.MonsterHitMove
    );

    await help.Wait(100 + 800 * Math.random());

    helpAdvAnim.OneBamAttackOnMonster(
      this.state.MonsterXPosition,
      this.state.MonsterYPosition,
      this.state.OpacityHitImage3,
      this.state.MonsterHitMove
    );

    await help.Wait(700 + 800 * Math.random());

    helpAdvAnim.OneBamAttackOnHero(
      this.state.HeroX3Position,
      this.state.HeroY3Position,
      this.state.OpacityHitImage1,
      this.state.HeroHitMove
    );

    await help.Wait(100 + 800 * Math.random());

    helpAdvAnim.OneBamAttackOnHero(
      this.state.HeroX3Position,
      this.state.HeroY3Position,
      this.state.OpacityHitImage2,
      this.state.HeroHitMove
    );

    await help.Wait(100 + 800 * Math.random());

    helpAdvAnim.OneBamAttackOnHero(
      this.state.HeroX3Position,
      this.state.HeroY3Position,
      this.state.OpacityHitImage1,
      this.state.HeroHitMove
    );

    // only if fight companion
    if (this.state.FightStatus.FightStatusTeam) {
      await help.Wait(700 + 800 * Math.random());

      helpAdvAnim.OneBamAttackOnTeam(
        this.state.TeamXPosition,
        this.state.TeamYPosition,
        this.state.OpacityHitImage5,
        this.state.TeamHitMove
      );

      await help.Wait(100 + 800 * Math.random());

      helpAdvAnim.OneBamAttackOnTeam(
        this.state.TeamXPosition,
        this.state.TeamYPosition,
        this.state.OpacityHitImage6,
        this.state.TeamHitMove
      );

      await help.Wait(100 + 800 * Math.random());

      helpAdvAnim.OneBamAttackOnTeam(
        this.state.TeamXPosition,
        this.state.TeamYPosition,
        this.state.OpacityHitImage5,
        this.state.TeamHitMove
      );
    }
  };
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});

export default withNavigation(Adventure);

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
