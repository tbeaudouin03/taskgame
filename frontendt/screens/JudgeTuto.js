import React from "react";
import {
  Button,
  Text,
  Header,
  Body,
  Card,
  CardItem,
  Content,
  Container
} from "native-base";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Text as Txt,
  ScrollView,
  Linking,
  Platform,
  Image
} from "react-native";
import Loader from "../components/Loader";
import Swiper from "react-native-swiper";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

export default class Shop extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  OnBeginJudgePress = async () => {
    await help.PostJudgeTutorialLevel(1);
    HeroData = await this.props.RefreshHeroDataSimple(this.props.navigation);
    this.props.SetHeroDataState(HeroData);
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <Swiper style={styles.wrapper} showsButtons={false} loop={false}>
        <View style={styles.slide1}>
          <Text style={[styles.text, { fontSize: 30 }]}>
            Get more points: assess other players' tasks!
          </Text>
        </View>

        <View style={styles.slide1}>
          <Text style={styles.text}>Task completed by another player</Text>
          <Image
            source={require("../assets/images/task_text.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>
            Does the image prove they completed the task?
          </Text>
          <Image
            source={require("../assets/images/task_image.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide3}>
          <Text style={styles.text}>Swipe down: Proof is insufficent</Text>
          <Image
            source={require("../assets/images/swipe_down.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>Swipe left: Task is OK</Text>
          <Image
            source={require("../assets/images/swipe_left.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>Swipe right: Well done!</Text>
          <Image
            source={require("../assets/images/swipe_right.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>Swipe up: Cool!</Text>
          <Image
            source={require("../assets/images/swipe_up.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>Tap + swipe up: Wow! Super cool!</Text>
          <Image
            source={require("../assets/images/tap_up.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>
            Double tap + swipe up: OMGGGG! Awesome! This is craaaaazy!
          </Text>
          <Image
            source={require("../assets/images/double_tap_up.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>
            Lower bar reminds you of swipes values
          </Text>
          <Image
            source={require("../assets/images/lower_bar.jpg")}
            style={styles.image}
          />
        </View>

        <View style={styles.slide1}>
          <Button
            info
            onPress={this.OnBeginJudgePress}
            style={{ alignSelf: "center" }}
          >
            <Text>Let us begin</Text>
          </Button>
        </View>
      </Swiper>
    );
  }
}

const styles = StyleSheet.create({
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5"
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9"
  },
  text: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    alignItems: "center",
    textAlign: "center",
    marginRight: 10,
    marginLeft: 10
  },
  image: {
    flex: 1,
    maxHeight: "75%",
    maxWidth: "75%",
    marginTop: 10,
    marginRight: 0,
    marginLeft: 0,
    borderColor: "rgba(142, 176, 211, 0.3)",
    borderWidth: 1.5
  }
});
