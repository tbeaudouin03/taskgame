import React from "react";
import {
  StyleSheet,
  View,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity
} from "react-native";
import uuid from "uuid";
import {
  Input,
  TextInput,
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";
import FAB from "react-native-fab";
import Dialog, {
  DialogFooter,
  DialogButton,
  DialogContent
} from "react-native-popup-dialog";

import TypeWriter from "react-native-typewriter";

import { withNavigation } from "react-navigation";

import TopScreenHero from "../components/TopScreenHero";
import TopScreenStat from "../components/TopScreenStat";
import Loader from "../components/Loader";
import Tasks1 from "../components/Tasks/Tasks";
import * as firebase from "firebase";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

// used to store pictures into firebase
firebase.initializeApp({
  apiKey: "AIzaSyDrMqT4Fo0qRRSRpYJPJ-_-ExdoXTOmQXw",
  authDomain: "taskgame-225414.firebaseapp.com",
  databaseURL: "https://taskgame-225414.firebaseio.com",
  projectId: "taskgame-225414",
  storageBucket: "taskgame-225414.appspot.com",
  messagingSenderId: "943300155553"
});
class Hero extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      Text: "",
      IsTuto: false,
      dialogVisible: false,
      VisibleFAB: true,
      active: false,
      loading: true,
      refreshing: false,
      screen: "hero",
      GivenName: "",
      heroGreeting: "Hello",
      heroPunctuation: "!",
      heroMessage: "Verify your Tasks and get more coins!",
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],

        Tasks: [
          {
            IDTask1: "aaaa",
            IDTask2: "bbbb",
            Title: "Please wait or drag down to refresh...",
            Checked: true,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        URL: `aaaa`
      }
    };
  }

  onCheckPress = async i => {
    HeroData = this.state.HeroData;
    if (HeroData.TutorialLevel >= 18) {
      if (!HeroData.Tasks[i].Checked) {
        HeroData.Tasks[i].Checked = !HeroData.Tasks[i].Checked;
        this.setState({ HeroData });
        ok = await help.TakeAndStorePicture(HeroData.Tasks[i]);
        // if any issue or cancelled then revert the check
        if (!ok) {
          HeroData.Tasks[i].Checked = !HeroData.Tasks[i].Checked;
          this.setState({ HeroData });
        } else if (HeroData.TutorialLevel == 18) {
          help.PostTutorialLevel(19);
          this.SetIsCameraTutoVisible(false);
        }
      }
    } else {
      this.props.navigation.navigate("Shop");
    }
  };

  SetIsCameraTutoVisible = async IsCameraTutoVisible => {
    this.setState({ IsCameraTutoVisible });
  };

  SetIsTuto = async IsTuto => {
    this.setState({ IsTuto });
  };

  SetHeroDataState = async HeroData => {
    this.setState({ HeroData });
  };

  SetVisibleFABState = async VisibleFAB => {
    this.setState({ VisibleFAB });
  };

  DealWithTutoState = async HeroData => {
    IsTuto = HeroData.TutorialLevel == 2;
    this.SetIsTuto(IsTuto);

    IsCameraTutoVisible = HeroData.TutorialLevel == 18;
    this.SetIsCameraTutoVisible(IsCameraTutoVisible);
  };

  // when user drag down to refresh
  _onRefresh = async () => {
    this.setState({ refreshing: true });
    HeroData = await help.RefreshHeroData(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      if (HeroData.Tasks == null) {
        HeroData.Tasks = [
          {
            IDTask1: "aaaa",
            IDTask2: "bbbb",
            Title:
              "You should at least COMPLETE one task in Google Tasks. Then DRAG DOWN to refresh this page.",
            Checked: true,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ];
      }

      this.setState({ HeroData });
    }
    await this.DealWithTutoState(HeroData);
    this.setState({ refreshing: false });
  };

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._onRefresh();
    });
    Advice = await help.GetAdvice(
      this.state.screen,
      this.state.HeroData.TaskCount
    );
    if (!!Advice.Message) {
      this.setState({
        heroGreeting: Advice.Greeting,
        heroPunctuation: Advice.Punctuation,
        heroMessage: Advice.Message
      });
    }

    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroData(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      if (HeroData.Tasks == null) {
        HeroData.Tasks = [
          {
            IDTask1: "aaaa",
            IDTask2: "bbbb",
            Title:
              "You should at least COMPLETE one task in Google Tasks. Then DRAG DOWN to refresh this page.",
            Checked: true,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ];
      }
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    const GivenName = await AsyncStorage.getItem("givenName");

    this.setState({ loading: false, GivenName: GivenName });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Container>
        <Dialog
          height={140}
          width={220}
          alignSelf={"center"}
          visible={this.state.dialogVisible}
          footer={
            <DialogFooter
              style={{ height: 50, width: 200, alignSelf: "center" }}
            >
              <DialogButton
                textStyle={{ color: "gray", fontSize: 15 }}
                text="CANCEL"
                onPress={() => {
                  this.setState({
                    dialogVisible: false
                  });
                }}
              />

              <DialogButton
                textStyle={{ color: "gray", fontSize: 15 }}
                text="OK"
                onPress={async () => {
                  help.PostInAppTask(
                    this.state.Text,
                    this._onRefresh,
                    this.state.HeroData,
                    this.props.navigation,
                    this.SetIsTuto.bind(this)
                  );
                  await help.Wait(800);
                  this.setState({
                    dialogVisible: false,
                    Text: ""
                  });
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent
            style={{
              height: 70,
              width: 218,
              marginTop: 10,
              alignSelf: "center"
            }}
          >
            <Input
              placeholder="Write your task here..."
              style={{
                height: 70,
                borderColor: "#C8C8C8",
                borderWidth: 2,
                marginTop: 12
              }}
              onChangeText={Text => this.setState({ Text })}
              value={this.state.Text}
            />
          </DialogContent>
        </Dialog>

        <View
          style={{
            height: 84,
            marginTop: 40,
            backgroundColor: "transparent"
          }}
        >
          <TopScreenStat HeroData={this.state.HeroData} Screen={"hero"} />
        </View>
        <Content
          onScrollBeginDrag={() => this.SetVisibleFABState(false)}
          onMomentumScrollEnd={() => this.SetVisibleFABState(true)}
          onScrollEndDrag={() => this.SetVisibleFABState(true)}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
        >
          <TopScreenHero
            ref={child => {
              this._TopScreenHero = child;
            }}
            screen={this.state.screen}
            heroGreeting={this.state.heroGreeting}
            heroMessage={this.state.heroMessage}
            heroPunctuation={this.state.heroPunctuation}
            GivenName={this.state.GivenName}
            HeroData={this.state.HeroData}
          />

          <Tasks1
            ref={child => {
              this._Tasks1 = child;
            }}
            Tasks={this.state.HeroData.Tasks}
            onCheckPress={this.onCheckPress.bind(this)}
            RefreshHeroData={help.RefreshHeroData.bind(this)}
            navigation={this.props.navigation}
            SetHeroDataState={this.SetHeroDataState.bind(this)}
          />
        </Content>

        <FAB
          buttonColor={"#1B88DD"}
          iconTextColor={"#FFFFFF"}
          onClickAction={() => {
            this.setState({
              dialogVisible: true
            });
          }}
          visible={this.state.VisibleFAB}
          iconTextComponent={<Icon name="md-create" />}
        />
        {this.RenderTutoAddTask()}
        {this.RenderCameraTuto()}
      </Container>
    );
  }

  RenderCameraTuto() {
    if (this.state.IsCameraTutoVisible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 60,
              minHeight: 130,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Tap here to take a picture of your accomplishment.`}
            </Text>
            <TypeWriter
              typing={1}
              fixed={true}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 14,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`
You can take a random picture this time, it does not matter - but generally speaking, the more verifiable and the more impressive your task, the more points you get!`}
            </TypeWriter>
          </View>

          <Image
            source={require("../assets/images/short_blue_arrow_down.png")}
            style={{
              width: 70,
              height: 140,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              left: 5,
              top: 60
            }}
          />
          <TouchableOpacity
            activeOpacity={1}
            onPress={async () => {
              this.onCheckPress(0);
            }}
            style={{
              backgroundColor: "transparent",
              width: 60,
              height: 120,
              zIndex: 2,
              left: 5,
              top: 30
            }}
          />
        </View>
      );
    }
  }

  RenderTutoAddTask() {
    if (this.state.IsTuto) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(0,0,0,0.5)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2
          }}
        >
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              this.setState({
                dialogVisible: true
              });
            }}
            style={{
              backgroundColor: "rgba(255,255,255,0.5)",
              position: "absolute",
              width: 96,
              height: 96,
              borderRadius: 50,
              bottom: 2,
              right: 0,
              zIndex: 2
            }}
          />
          <Text
            style={{
              position: "absolute",
              bottom: 140,
              right: 100,
              zIndex: 1,
              color: "white",
              fontSize: 18,
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            {`Tap here to add a task 
and power-up!`}
          </Text>
          <Image
            source={require("../assets/images/blue_arrow.png")}
            style={{
              width: 200,
              height: 100,
              resizeMode: "stretch",
              opacity: 0.9,
              position: "absolute",
              bottom: 45,
              right: 100,
              zIndex: 0
            }}
          />
        </View>
      );
    }
  }
}

export default withNavigation(Hero);

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});
