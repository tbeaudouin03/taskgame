import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import { withNavigation } from "react-navigation";

import TypeWriter from "react-native-typewriter";

import ShopFlatItem from "../components/Shop/ShopFlatItem";
import TopScreenStatShop from "../components/Shop/TopScreenStatShop";
import Loader from "../components/Loader";

const help = require(`../components/HelperFunction`);

class Shop extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      ShopType: "hero",
      BuyPressStatus: [],
      UsePressStatus: [],
      ShopDataSpirit: {
        ShopObjects: [
          {
            key: "aaa",
            Title: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Description: "",
            Price: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            IsUnique: true,
            ObjectCount: 0,
            ShopType: "hero"
          }
        ]
      },
      ShopDataArtefact: {
        ShopObjects: [
          {
            key: "aaa",
            Title: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Description: "",
            Price: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            IsUnique: true,
            ObjectCount: 0,
            ShopType: "artefact"
          }
        ]
      },
      ShopDataMap: {
        ShopObjects: [
          {
            key: "aaa",
            Title: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Description: "",
            Price: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            IsUnique: true,
            ObjectCount: 0,
            ShopType: "map"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0
      }
    };
  }

  DealWithTutoState = async HeroData => {
    IsAvailableMode = this.state.HeroData.TutorialLevel >= 15;
    this.setState({ IsAvailableMode });

    IsMapBuyTutoVisible = HeroData.TutorialLevel == 15;
    if (IsMapBuyTutoVisible) {
      this.setState({ ShopType: "map" });
      this.SetIsMapBuyTutoVisible(IsMapBuyTutoVisible);
    }

    IsMapUseTutoVisible = HeroData.TutorialLevel == 16;
    if (IsMapUseTutoVisible) {
      this.setState({ ShopType: "map" });
      this.SetIsMapUseTutoVisible(IsMapUseTutoVisible);
    }

    IsArtefactBuyTutoVisible = HeroData.TutorialLevel == 17;
    if (IsArtefactBuyTutoVisible) {
      this.setState({ ShopType: "artefact" });
      this.SetIsArtefactBuyTutoVisible(IsArtefactBuyTutoVisible);
    }
  };

  SetIsArtefactBuyTutoVisible = async IsArtefactBuyTutoVisible =>
    this.setState({ IsArtefactBuyTutoVisible });
  SetIsMapBuyTutoVisible = async IsMapBuyTutoVisible =>
    this.setState({ IsMapBuyTutoVisible });
  SetIsMapUseTutoVisible = async IsMapUseTutoVisible =>
    this.setState({ IsMapUseTutoVisible });
  SetHeroDataState = async HeroData => this.setState({ HeroData });
  SetShopDataArtefactState = async ShopDataArtefact =>
    this.setState({ ShopDataArtefact });
  SetShopDataSpiritState = async ShopDataSpirit =>
    this.setState({ ShopDataSpirit });
  SetShopDataMapState = async ShopDataMap => this.setState({ ShopDataMap });

  OnSegmentPress = ShopType => {
    this.setState({ ShopType: ShopType });
  };

  _onRefresh = async () => {
    this.setState({ refreshing: true });

    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    let ShopData = await help.GetShopData(HeroData);

    this.setState({
      refreshing: false,
      ShopDataArtefact: ShopData.ShopDataArtefact,
      ShopDataSpirit: ShopData.ShopDataSpirit,
      ShopDataMap: ShopData.ShopDataMap
    });
  };

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._onRefresh();
    });

    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    let ShopData = await help.GetShopData(HeroData);

    this.setState({
      loading: false,
      ShopDataArtefact: ShopData.ShopDataArtefact,
      ShopDataSpirit: ShopData.ShopDataSpirit,
      ShopDataMap: ShopData.ShopDataMap
    });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Container style={{ backgroundColor: "transparent" }}>
        <Image
          blurRadius={100}
          source={require("../assets/images/map_background.jpg")}
          style={{
            width: "100%",
            height: "100%",
            resizeMode: "stretch",
            opacity: 1,
            position: "absolute",
            zIndex: 0
          }}
        />

        <View
          style={{
            flex: 1,
            alignSelf: "center"
          }}
        >
          <View
            style={{
              position: "absolute",
              top: 0,
              alignSelf: "center",
              zIndex: 2,
              height: "100%",
              width: "100%"
            }}
          >
            <View
              style={{
                height: 95,
                marginTop: 40,
                backgroundColor: "transparent"
              }}
            >
              <TopScreenStatShop
                HeroData={this.state.HeroData}
                ShopType={this.state.ShopType}
                OnSegmentPress={this.OnSegmentPress.bind(this)}
              />
            </View>

            {this.RenderFlatList()}
          </View>
          <Image
            source={require("../assets/images/forest_of_dreams.gif")}
            style={{
              width: 1736,
              height: 894,
              resizeMode: "stretch",
              opacity: 0.85,
              alignSelf: "center",
              zIndex: 1
            }}
          />
        </View>
        {this.RenderIsNotAvailableMode()}
        {this.RenderMapBuyTuto(this.state.ShopDataMap)}
        {this.RenderMapUseTuto(this.state.ShopDataMap)}
        {this.RenderArtefactBuyTuto(this.state.ShopDataArtefact)}
      </Container>
    );
  }
  // tutorial 14
  RenderIsNotAvailableMode() {
    if (!this.state.IsAvailableMode) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            if (this.state.HeroData.TutorialLevel >= 3) {
              this.props.navigation.navigate("Adventure");
            } else {
              this.props.navigation.navigate("Hero");
            }

            this._onRefresh();
          }}
          style={{
            backgroundColor: "rgba(255,255,255,0.7)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              zIndex: 3,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 15,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 4,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              Finish your first quest to access the shop
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  RenderArtefactBuyTuto = ShopDataArtefact => {
    if (this.state.IsArtefactBuyTutoVisible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <TouchableOpacity
            activeOpacity={1}
            onLongPress={async () => {
              await Promise.all([
                help.PostTutorialLevel(18),
                help.PostPurchasedShopObject(
                  ShopDataArtefact.ShopObjects[0].Item1
                )
              ]);
              this._onRefresh();
              this.SetIsArtefactBuyTutoVisible(false);
              this.props.navigation.navigate("Hero");
            }}
            style={{
              backgroundColor: "transparent",
              width: "90%",
              height: 160,
              zIndex: 2,
              alignSelf: "center",
              top: 145
            }}
          />
          <Image
            source={require("../assets/images/short_blue_arrow_up.png")}
            style={{
              width: 60,
              height: 120,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              alignSelf: "center",
              top: 130
            }}
          />
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 120,
              minHeight: 60,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Long press to buy the Magic Flower Camera.`}
            </Text>
            <TypeWriter
              typing={1}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 14,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`
This artefact lets you take pictures to prove your accomplishments to the Gods of Truth.`}
            </TypeWriter>
          </View>
        </View>
      );
    }
  };

  RenderMapBuyTuto = ShopDataMap => {
    if (this.state.IsMapBuyTutoVisible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <TouchableOpacity
            activeOpacity={1}
            onLongPress={async () => {
              await help.PostTutorialLevel(16);
              await help.PostPurchasedShopObject(
                ShopDataMap.ShopObjects[0].Item2
              );
              await help.Wait(800);
              await this._onRefresh();
              await this.SetIsMapBuyTutoVisible(false);
            }}
            style={{
              backgroundColor: "transparent",
              width: "90%",
              height: 160,
              marginLeft: 200,
              zIndex: 2,
              alignSelf: "center",
              top: 145
            }}
          />
          <Image
            source={require("../assets/images/short_blue_arrow_up.png")}
            style={{
              width: 60,
              height: 120,
              marginLeft: 185,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              alignSelf: "center",
              top: 100
            }}
          />
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 90,
              minHeight: 60,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <TypeWriter
              typing={1}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Long press here to get this new quest.`}
            </TypeWriter>
          </View>
        </View>
      );
    }
  };

  RenderMapUseTuto = ShopDataMap => {
    if (this.state.IsMapUseTutoVisible) {
      return (
        <View
          activeOpacity={1}
          style={{
            backgroundColor: "rgba(255,255,255,0.3)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 2,
            alignSelf: "center"
          }}
        >
          <TouchableOpacity
            activeOpacity={1}
            onLongPress={async () => {
              await help.PostTutorialLevel(17);
              await help.PostUsedShopObject(ShopDataMap.ShopObjects[0].Item2);
              await help.Wait(800);
              await this._onRefresh();
              this.SetIsMapUseTutoVisible(false);
              this.props.navigation.navigate("Adventure");
            }}
            style={{
              backgroundColor: "transparent",
              width: "90%",
              height: 160,
              marginLeft: 200,
              zIndex: 2,
              alignSelf: "center",
              top: 145
            }}
          />
          <Image
            source={require("../assets/images/short_blue_arrow_up.png")}
            style={{
              width: 60,
              height: 120,
              marginLeft: 185,
              resizeMode: "stretch",
              opacity: 0.9,
              zIndex: 3,
              alignSelf: "center",
              top: 100
            }}
          />
          <View
            style={{
              zIndex: 1,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 10,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              top: 90,
              minHeight: 60,
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <TypeWriter
              typing={1}
              style={{
                zIndex: 1,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              {`Long press again to start the quest.`}
            </TypeWriter>
          </View>
        </View>
      );
    }
  };

  RenderFlatList = () => {
    if (this.state.ShopType == "hero") {
      ShopData = this.state.ShopDataSpirit;
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={ShopData.ShopObjects}
          renderItem={({ item, index }) => (
            <ShopFlatItem
              item={item}
              index={index}
              ShopData={ShopData}
              HeroData={this.state.HeroData}
              SetHeroDataState={this.SetHeroDataState.bind(this)}
              SetShopDataArtefactState={this.SetShopDataArtefactState.bind(
                this
              )}
              SetShopDataSpiritState={this.SetShopDataSpiritState.bind(this)}
              SetShopDataMapState={this.SetShopDataMapState.bind(this)}
              navigation={this.props.navigation}
            />
          )}
        />
      );
    }

    if (this.state.ShopType == "artefact") {
      ShopData = this.state.ShopDataArtefact;
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          //ItemSeparatorComponent={this.renderSeparator}
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={ShopData.ShopObjects}
          renderItem={({ item, index }) => (
            <ShopFlatItem
              item={item}
              index={index}
              ShopData={ShopData}
              HeroData={this.state.HeroData}
              SetHeroDataState={this.SetHeroDataState.bind(this)}
              SetShopDataArtefactState={this.SetShopDataArtefactState.bind(
                this
              )}
              SetShopDataSpiritState={this.SetShopDataSpiritState.bind(this)}
              SetShopDataMapState={this.SetShopDataMapState.bind(this)}
              navigation={this.props.navigation}
            />
          )}
        />
      );
    }
    if (this.state.ShopType == "map") {
      ShopData = this.state.ShopDataMap;
      return (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          style={{ flex: 1, backgroundColor: "transparent" }}
          contentContainerStyle={{
            justifyContent: "flex-start",
            backgroundColor: "transparent",
            marginTop: 20
          }}
          data={ShopData.ShopObjects}
          renderItem={({ item, index }) => (
            <ShopFlatItem
              item={item}
              index={index}
              ShopData={ShopData}
              HeroData={this.state.HeroData}
              SetHeroDataState={this.SetHeroDataState.bind(this)}
              SetShopDataArtefactState={this.SetShopDataArtefactState.bind(
                this
              )}
              SetShopDataSpiritState={this.SetShopDataSpiritState.bind(this)}
              SetShopDataMapState={this.SetShopDataMapState.bind(this)}
              navigation={this.props.navigation}
            />
          )}
        />
      );
    }
  };
}

export default withNavigation(Shop);

const styles = StyleSheet.create({
  segment: {
    height: "60%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "60%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
