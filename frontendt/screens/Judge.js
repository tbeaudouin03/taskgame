import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Header,
  Card,
  CardItem,
  CheckBox,
  DeckSwiper,
  Left,
  Thumbnail,
  Icon,
  Badge
} from "native-base";
import {
  ScrollView,
  View,
  StyleSheet,
  Image,
  Text as Txt,
  AsyncStorage,
  RefreshControl,
  Platform,
  TouchableOpacity
} from "react-native";

import { withNavigation } from "react-navigation";

import TopScreenStat from "../components/TopScreenStat";
import Loader from "../components/Loader";
import Swiper from "react-native-deck-swiper";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

class Judge extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      screen: "judge",
      heroGreeting: "Hello again!",
      heroMessage: "Scroll down, verify the task and earn coins!",
      heroPunctuation: "!",
      Bonus: 0,
      BackgroundColor: "transparent",
      CoinValueChosen: 0,
      FontSize: 15,
      SkillChosen: "Reputation",
      ImageToCheck: [
        {
          IDTaskCheck: "aaaa",
          IDTask1: "bbbb",
          IDTask2: "cccc",
          IDHero: "dddd",
          Title: `seems like nobody needs you now.
Good job: rest and enjoy!`,
          URL:
            "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fteddy_bear.gif?alt=media&token=dcfd2c73-f609-4698-a83c-e8a7ba1774b1"
        }
      ],
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        URL: `aaaa`
      }
    };
  }

  SetHeroDataState = async HeroData => this.setState({ HeroData });

  DealWithTutoState = async HeroData => {
    IsAvailableMode = HeroData.TaskVerifyCount >= 3;
    console.log(IsAvailableMode)
    this.setState({ IsAvailableMode });
  };

  AddBonus = () => {
    Bonus = this.state.Bonus;
    if (Bonus == 0) {
      this.setState({
        Bonus: 1,
        BackgroundColor: "rgba(79, 171, 75, 0.5)"
      });
    } else if (Bonus == 1) {
      this.setState({
        Bonus: 2,
        BackgroundColor: "rgba(79, 171, 75, 1)"
      });
    } else {
      this.setState({
        Bonus: 0,
        BackgroundColor: "transparent"
      });
    }
  };

  OnSwipe = async (cardIndex, CoinValueChosen) => {
    randSkill = Math.random();
    let SkillChosen = "Reputation";
    if (randSkill <= 0.4) {
      SkillChosen = "Intelligence";
    } else if (randSkill <= 0.8) {
      SkillChosen = "Fitness";
    }
    ImageToCheck = this.state.ImageToCheck[cardIndex];
    IDToken = await AsyncStorage.getItem("idToken");

    // if swiped top then add bonus from the state
    if (CoinValueChosen == 3) {
      CoinValueChosen == CoinValueChosen + this.state.Bonus;
    }

    HeroData = this.state.HeroData;
    HeroData.JudgementCount = HeroData.JudgementCount + 1;
    this.setState({ Bonus: 0, BackgroundColor: "transparent", HeroData });
    await help.PostJudgement(
      ImageToCheck,
      CoinValueChosen,
      SkillChosen,
      IDToken
    );
  };

  BackgroundStyle = function(BackgroundColor) {
    return { backgroundColor: BackgroundColor };
  };

  FormatTaskTitle = function(Title) {
    TitleF = Title.trim();
    TitleF2 = TitleF[0].toUpperCase() + TitleF.slice(1, TitleF.length);
    limitCharacter = Math.min(100, TitleF.length);
    TitleF3 =
      limitCharacter < TitleF.length
        ? TitleF2.slice(0, limitCharacter) + "[...]"
        : TitleF2.slice(0, limitCharacter);

    return TitleF3;
  };

  RefreshImageToCheck = async () => {
    this.setState({
      ImageToCheck: [
        {
          IDTaskCheck: "aaaa",
          IDTask1: "bbbb",
          IDTask2: "cccc",
          IDHero: "dddd",
          Title: `seems like nobody needs you now.
Good job: rest and enjoy!`,
          URL:
            "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fteddy_bear.gif?alt=media&token=dcfd2c73-f609-4698-a83c-e8a7ba1774b1"
        }
      ]
    });
    let ImageToCheck = await help.GetImageToCheck();
    if (!!ImageToCheck) {
      this.setState({ ImageToCheck });
    }
  };

  _onRefresh = async () => {
    this.setState({ refreshing: true });

    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    await this.RefreshImageToCheck();
    this.setState({ refreshing: false });
  };

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
      this._onRefresh();
    });

    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    HeroData = await help.RefreshHeroDataSimple(this.props.navigation);
    await help.TutoNavHandler(HeroData, this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    await this.DealWithTutoState(HeroData);

    await this.RefreshImageToCheck();

    this.setState({ loading: false });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    if (this.state.ImageToCheck[0].IDTaskCheck == "aaaa") {
      return (
        <Container style={this.BackgroundStyle(this.state.BackgroundColor)}>
          <View
            style={{
              height: 84,
              marginTop: 40,
              backgroundColor: "transparent"
            }}
          >
            <TopScreenStat HeroData={this.state.HeroData} Screen={"judge"} />
          </View>

          <Card
            style={{
              flex: 1,
              marginBottom: 15,
              marginTop: 10,
              width: "90%",
              alignSelf: "center"
            }}
          >
            <CardItem style={{}}>
              <Content
                style={{ height: 60, marginTop: 5 }}
                contentContainerStyle={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: -10
                }}
              >
                <Text>
                  {this.FormatTaskTitle(this.state.ImageToCheck[0].Title)}{" "}
                </Text>
              </Content>
            </CardItem>

            <CardItem cardBody style={{ flex: 1, marginBottom: 10 }}>
              <Image
                style={{
                  minHeight: 300,
                  resizeMode: "contain",
                  flex: 1,
                  marginTop: 5,
                  marginBottom: -10
                }}
                source={{ uri: this.state.ImageToCheck[0].URL }}
              />
            </CardItem>

            <Button
              full
              info
              small
              style={{ marginTop: 20 }}
              onPress={this.RefreshImageToCheck}
            >
              <Text>Refresh</Text>
            </Button>
          </Card>
          {this.RenderIsNotAvailableMode()}
        </Container>
      );
    }
    return (
      <Container style={this.BackgroundStyle(this.state.BackgroundColor)}>
        <View
          style={{
            height: 84,
            marginTop: 40,
            backgroundColor: "transparent"
          }}
        >
          <TopScreenStat HeroData={this.state.HeroData} Screen={"judge"} />
        </View>

        <View
          style={{
            flex: 1,
            width: "100%",
            alignSelf: "center",
            marginTop: 0,
            marginBottom: 0
          }}
        >
          <Swiper
            style={{ justifyContent: "center" }}
            cards={this.state.ImageToCheck}
            renderCard={card => {
              return (
                <Card
                  style={{
                    flex: 1,
                    marginBottom: 135,
                    marginRight: -10,
                    marginLeft: -10,
                    marginTop: -50
                  }}
                >
                  <CardItem>
                    <Content
                      style={{ height: 60, marginTop: 5 }}
                      contentContainerStyle={{
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: -10
                      }}
                    >
                      <Text>{this.FormatTaskTitle(card.Title)} </Text>
                    </Content>
                  </CardItem>

                  <CardItem
                    cardBody
                    style={{
                      flex: 1,
                      marginBottom: 10,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Image
                      style={{
                        minHeight: 300,
                        maxWidth: 600,
                        flex: 1,
                        marginTop: 5,
                        marginBottom: -10
                      }}
                      source={{ uri: card.URL }}
                    />
                  </CardItem>
                  <CardItem>
                    <Body
                      style={{
                        height: 25,
                        flex: 1,
                        justifyContent: "space-evenly",
                        alignItems: "flex-start",
                        flexDirection: "row",
                        marginTop: -5
                      }}
                    >
                      <Icon
                        style={{ color: "rgb(234, 157, 64)" }}
                        name={
                          Platform.OS === "ios"
                            ? "ios-arrow-down"
                            : "md-arrow-down"
                        }
                      />

                      <Icon
                        style={{ color: "rgba(234, 157, 64, 0.5)" }}
                        name={
                          Platform.OS === "ios"
                            ? "ios-arrow-back"
                            : "md-arrow-back"
                        }
                      />

                      <Icon
                        style={{ color: "rgba(79, 171, 75, 0.5)" }}
                        name={
                          Platform.OS === "ios"
                            ? "ios-arrow-forward"
                            : "md-arrow-forward"
                        }
                      />

                      <Icon
                        style={{ color: "rgb(79, 171, 75)" }}
                        name={
                          Platform.OS === "ios" ? "ios-arrow-up" : "md-arrow-up"
                        }
                      />
                    </Body>
                  </CardItem>
                </Card>
              );
            }}
            //onSwiped={(cardIndex) => { console.log(cardIndex) }}
            onSwipedBottom={cardIndex => this.OnSwipe(cardIndex, 1)}
            onSwipedLeft={cardIndex => this.OnSwipe(cardIndex, 1)}
            onSwipedRight={cardIndex => this.OnSwipe(cardIndex, 2)}
            onSwipedTop={cardIndex => this.OnSwipe(cardIndex, 3)}
            onTapCard={() => this.AddBonus()}
            onSwipedAll={async () => {
              await help.Wait(1500);
              this.RefreshImageToCheck();
            }}
            cardIndex={0}
            backgroundColor={"transparent"}
            stackSize={3}
          />
        </View>
        {this.RenderIsNotAvailableMode()}
      </Container>
    );
  }

  RenderIsNotAvailableMode() {
    if (!this.state.IsAvailableMode) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            this.props.navigation.navigate("Hero");
            this._onRefresh();
          }}
          style={{
            backgroundColor: "rgba(255,255,255,0.7)",
            position: "absolute",
            width: "100%",
            height: "100%",
            zIndex: 3,
            alignSelf: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              zIndex: 4,
              alignSelf: "center",
              justifyContent: "center",
              padding: 25,
              paddingTop: 15,
              paddingBottom: 15,
              backgroundColor: "rgba(142, 176, 211, 0.8)",
              width: "100%",
              borderColor: "rgba(255, 255, 255, 0.7)",
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderWidth: 1
            }}
          >
            <Text
              style={{
                zIndex: 5,
                color: "white",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center"
              }}
            >
              Take at least 3 pictures of your tasks to access the Temple of
              Truth.
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }
}

export default withNavigation(Judge);

const styles = StyleSheet.create({
  title: {
    marginBottom: 0,
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  },
  containerRow: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "center"
  }
});
