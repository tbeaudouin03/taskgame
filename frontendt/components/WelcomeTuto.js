import React from "react";
import { Button, Text, Content, Container } from "native-base";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Text as Txt,
  ScrollView,
  Linking,
  Platform,
  Image
} from "react-native";
import Loader from "../components/Loader";

import TypeWriter from "react-native-typewriter";

const help = require(`./HelperFunction`);
const config = require("../constants/config.json");

export default class WelcomeTuto extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      IntroShow: "",
      refreshing: false,
      loading: true,
      GivenName: "",
      ShopType: "hero",
      heroGreeting: "Hello!",
      heroMessage:
        "I am here to help! I will give you different tips each time you tap me. Different heros might give you different tips. When you level-up, you will also unlock new tips!",
      ShopData: {
        ShopObjects: [
          {
            key: "aaa",
            Title: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Description: "",
            Price: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            IsUnique: true,
            ObjectCount: 0,
            ShopType: "hero"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0
      }
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    let HeroData = await help.RefreshHeroDataSimple(this.props.navigation);

    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    const GivenName = await AsyncStorage.getItem("givenName");

    IntroShow =
      `This is the forest of dreams. 
  
You look at all these dreams and you think: "Where are my dreams?". The forest explodes into a thousand stars and you understand: these are your dreams!

"You are not alone ` +
      GivenName +
      `, spirits will guide you!". "Choose me...", one voice whispers. And another one. And another until the whole world becomes a waterfall of sounds.
	
Quick ` +
      GivenName +
      `! Choose a spirit to begin the adventure!`;

    this.setState({ loading: false });

    this.setState({ IntroShow });
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Container>
        <Image
          blurRadius={100}
          source={require("../assets/images/map_background.jpg")}
          style={{
            width: "100%",
            height: "100%",
            resizeMode: "stretch",
            opacity: 1,
            position: "absolute",
            zIndex: 0
          }}
        />
        <View
          style={{
            flex: 1,
            alignSelf: "center"
          }}
        >
          <Content>
            <View
              style={{
                position: "absolute",
                top: 100,
                alignSelf: "center",
                justifyContent: "center",
                zIndex: 2,
                height: 410,
                width: 300,
                borderRadius: 8,
                backgroundColor: "rgba(0,0,0,0.3)"
              }}
            >
              <TypeWriter
                typing={1}
                style={{
                  color: "white",
                  alignSelf: "center",
                  margin: 8,
                  textAlign: "center"
                }}
              >
                {this.state.IntroShow}
              </TypeWriter>
              <Button
                //small
                info
                onPress={async () => {
                  help.PostTutorialLevel(1);
                  this.props.navigation.navigate("ShopTuto");
                }}
                style={{
                  alignSelf: "center",
                  //width: "105%",
                  justifyContent: "center",
                  marginTop: 15
                }}
              >
                <Text>Choose Spirit</Text>
              </Button>
            </View>
            <Image
              source={require("../assets/images/forest_of_dreams.gif")}
              style={{
                width: 1736,
                height: 894,
                resizeMode: "stretch",
                //borderRadius: 30,
                //marginTop: -1,
                opacity: 0.85,
                alignSelf: "center",
                zIndex: 1
              }}
            />
          </Content>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
