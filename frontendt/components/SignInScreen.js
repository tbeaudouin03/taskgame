import React from "react";
import {
  StyleSheet,
  View,
  Image,
} from "react-native";
import { Button, Body, Text as Txt, Content, Root } from "native-base";
import { LinearGradient } from "expo";

import Loader from "./Loader";

const help = require(`../components/HelperFunction`);

export default class SignInScreen extends React.Component {
  static navigationOptions = {
    //title: "Please sign in"
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      PressStatus: false,
      URL: "aaaa"
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    await this.chooseURL();
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Body style={{ flex: 1 }}>
        <LinearGradient
          colors={["rgb(142, 176, 211)", "rgb(188, 226, 235)"]}
          style={{
            flex: 1,
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            marginRight: -1000,
            marginLeft: -1000
          }}
        />
        <Content contentContainerStyle={styles.container}>
          <Image
            source={require("../assets/images/cloud.png")}
            style={{
              width: 300,
              height: 120,
              resizeMode: "contain",
              opacity: 1
            }}
          />
        </Content>

        <Content contentContainerStyle={styles.container}>
          <Button
            info
            style={this.state.PressStatus ? { opacity: 0.5 } : {}}
            onPress={() => help.signInWithGoogleAsync(this.props.navigation)}
          >
            <Txt>Sign in</Txt>
          </Button>
        </Content>

        <Content contentContainerStyle={styles.container}>
          <Image
            source={require("../assets/images/ground.png")}
            style={{
              position: "absolute",
              bottom: 0,
              left: 15,
              right: 0,
              width: 200,
              height: 200,
              resizeMode: "stretch",
              opacity: 1,
              marginBottom: -100
            }}
          />
          <Image
            source={{ uri: this.state.URL }}
            style={{
              width: 200,
              height: 120,
              resizeMode: "contain",
              marginTop: -10,
              marginRight: 0,
              marginLeft: 0,
              opacity: 1
            }}
          />
        </Content>
      </Body>
    );
  }
  chooseURL = async () => {
    let URLArray = [
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Funicorn_spirit.png?alt=media&token=4e636195-383c-4a56-b393-40e7adf3afa5",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcollectible_coin.gif?alt=media&token=5224bcaf-37a4-4e34-95d5-bc4c63ad4747",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcute_cat.png?alt=media&token=fd89f805-601a-4a7b-bc6a-136367001323",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fdog.gif?alt=media&token=f1534bfd-1f7c-4d7f-bc4a-67ab9d265c3c",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Ffire_spirit.png?alt=media&token=10aae99d-63e6-4851-954b-e8baa975e0b9",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fwater_spirit.png?alt=media&token=89aa487b-67d5-4a19-b273-d539c30b675d",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fteddy_bear.gif?alt=media&token=dcfd2c73-f609-4698-a83c-e8a7ba1774b1",
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fshiny_ruby.gif?alt=media&token=e34a065c-a010-4c11-8123-66ade91694da"
    ];

    let randIndex = Math.round(Math.random() * URLArray.length);

    this.setState({ URL: URLArray[randIndex] });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
