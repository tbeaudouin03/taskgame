import React, { Component } from "react";
import {
  StyleSheet,
  Text as Txt,
  View,
  Image,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
//import { CheckBox } from "react-native-elements";
import {
  ListItem,
  CheckBox,
  Text,
  Body,
  Container,
  Content
} from "native-base";
import Popover from "react-native-popover-view";
import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

const config = require("../../constants/config.json");
const help = require(`../HelperFunction`);

class Task extends Component {
  state = {
    PressStatus: false
  };

  async componentDidMount() {
    this.props.copilotEvents.on("stop", async () => {
      help.PostHeroTutorialLevel(1);
      HeroData = await this.props.RefreshHeroData(this.props.navigation);
      this.props.SetHeroDataState(HeroData);
    });
  }

  componentWillUnmount() {
    // Don't forget to disable event handlers to prevent errors
    this.props.copilotEvents.off("stop");
  }

  render() {
    var Coins = Math.round(this.props.Task.Coins / 10) / 10;
    var Title = this.props.Task.Title ? this.props.Task.Title : "No Title";
    TitleF = Title.trim();
    TitleF2 = TitleF[0].toUpperCase() + TitleF.slice(1, TitleF.length);

    return (
      <ListItem style={{ justifyContent: "center" }}>
        <CView>
          <CheckBox
            color={this.props.Task.IsVerified ? "#32AC23" : "#1B88DD"}
            checked={this.props.Task.Checked}
            onPress={() => this.props.onCheckPress(this.props.taskIndex)}
          />
        </CView>

        <CView style={{ flex: 1 }}>
          <Body style={{ justifyContent: "center" }}>
            <Text>{TitleF2}</Text>
          </Body>
        </CView>

        <View
          style={{
            maxWidth: 140,
            minWidth: 110,
            marginRight: 0,
            borderWidth: 1,
            borderRightWidth: 0,
            borderTopWidth: 0,
            borderBottomWidth: 0,
            borderStyle: "dotted",
            borderColor: "rgba(0,0,0,0.05)",
            flex: 1,
            justifyContent: "center"
          }}
        >
          <Body
            style={{
              flexDirection: "column",
              marginLeft: 25,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              ref="TaskCoins"
              onPress={() => this.setState({ isTaskCoinsVisible: true })}
              style={{ flexDirection: "row" }}
            >
              <Image
                source={require("../../assets/images/coin.png")}
                style={{ height: 30, width: 30, marginTop: -2 }}
              />
              <Text style={{ marginTop: 2.5 }}>{Coins}</Text>
            </TouchableOpacity>

            {this.RenderLoveEnergy()}

            {this.RenderDarkEnergy()}

            <TouchableOpacity
              ref="TaskSkills"
              onPress={() => this.setState({ isTaskSkillsVisible: true })}
              style={{ flexDirection: "row", marginTop: 5 }}
            >
              <Image
                source={require("../../assets/images/plus.png")}
                style={{
                  height: 10,
                  width: 10,
                  marginLeft: -10,
                  marginTop: 5
                }}
              />
              <Text>
                {this.props.Task.Reputation} - {this.props.Task.Intelligence} -{" "}
                {this.props.Task.Fitness}
              </Text>
            </TouchableOpacity>

            {this.RenderJudgement()}
          </Body>
        </View>
        <Popover
          isVisible={this.state.isTaskCoinsVisible}
          fromView={this.refs.TaskCoins}
          onClose={() => this.setState({ isTaskCoinsVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Number of Soul Coins you earned doing this task.`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isTaskSkillsVisible}
          fromView={this.refs.TaskSkills}
          onClose={() => this.setState({ isTaskSkillsVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Reputation-Intelligence-Fitness you earned doing this task.
You need 100 points here to go up one skill level.`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isJudgementVisible}
          fromView={this.refs.Judgement}
          onClose={() => this.setState({ isJudgementVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Number of other players who gave their opinions about your task.
You should first unlock the power to verify your task before the community can help you verifying your tasks`}</Text>
        </Popover>
      </ListItem>
    );
  }

  RenderJudgement() {
    if (this.props.Task.JudgementCount > 0) {
      return (
        <TouchableOpacity
          ref="Judgement"
          onPress={() => this.setState({ isJudgementVisible: true })}
          style={{ flexDirection: "row", marginTop: 10 }}
        >
          <Image
            source={require("../../assets/images/judge_hammer.png")}
            style={{ height: 22, width: 22, marginLeft: 5, marginTop: 2 }}
          />
          <Text style={{ marginLeft: 20, marginTop: 3 }}>
            {this.props.Task.JudgementCount}
          </Text>
        </TouchableOpacity>
      );
    }
  }
  RenderLoveEnergy() {
    if (this.props.Task.LoveEnergy) {
      return (
        <CView
          style={{
            flexDirection: "row",
            marginTop: 5,
            marginBottom: 3,
            marginLeft: -3
          }}
        >
          <Image
            source={require("../../assets/images/love_energy.png")}
            style={{ height: 27, width: 27, marginLeft: 0, marginTop: 0 }}
          />
          <Text style={{ marginLeft: 20, marginTop: 5 }}>
            {this.props.Task.LoveEnergy}
          </Text>
        </CView>
      );
    }
  }

  RenderDarkEnergy() {
    if (this.props.Task.DarkEnergy) {
      return (
        <CView style={{ flexDirection: "row", marginLeft: -11, marginTop: 3 }}>
          <Image
            source={require("../../assets/images/dark_energy.png")}
            style={{ height: 40, width: 40, marginLeft: 2.5, marginTop: 1 }}
          />
          <Text style={{ marginLeft: 14.5, marginTop: 10 }}>
            {this.props.Task.DarkEnergy}
          </Text>
        </CView>
      );
    }
  }
}

export default copilot()(Task);

const styles = StyleSheet.create({});
