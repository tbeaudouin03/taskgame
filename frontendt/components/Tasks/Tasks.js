import React, { Component } from "react";
import { StyleSheet, FlatList, View } from "react-native";
import Task1 from "./Task.js";

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    };
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true });
    await this.props.RefreshHeroData();
    this.setState({ refreshing: false });
  };

  async componentDidMount() {
    this.props.copilotEvents.on("stop", () => {
      this._Task1.start();
    });
  }

  componentWillUnmount() {
    // Don't forget to disable event handlers to prevent errors
    this.props.copilotEvents.off("stop");
  }

  render() {
    return (
      <CopilotStep text="Here are your completed tasks" order={1} name="Tasks">
        <CView style={{ flex: 1 }}>
          <FlatList
            style={{ marginLeft: 10 }}
            contentContainerStyle={{ flex: 1 }}
            data={this.props.Tasks}
            keyExtractor={(item, index) => item.IDTask1 + item.IDTask2}
            renderItem={({ item, index }) => (
              <Task1
                ref={child => {
                  this._Task1 = child;
                }}
                RefreshHeroData={this.props.RefreshHeroData}
                SetHeroDataState={this.props.SetHeroDataState}
                Task={item}
                navigation={this.props.navigation}
                taskIndex={index}
                onCheckPress={this.props.onCheckPress}
              />
            )}
          />
        </CView>
      </CopilotStep>
    );
  }
}

export default copilot()(Tasks);

const styles = StyleSheet.create({});
