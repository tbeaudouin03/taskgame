import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Header,
  Left,
  List,
  ListItem,
  Icon,
  Right,
  Switch,
  Title
} from "native-base";

import {
  ScrollView,
  View,
  StyleSheet,
  Image,
  Text as Txt,
  AsyncStorage,
  RefreshControl,
  Platform,
  Linking
} from "react-native";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

import Loader from "../components/Loader";

export default class Menu extends React.Component {
  static navigationOptions = {
    //header:null
    title: "Menu",
    headerTitleStyle: {
      textAlign: "center",
      flex: 1
    }
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <Container>
        <Content style={{ marginTop: 0 }}>
          <Button
            full
            info
            style={{ marginBottom: 15 }}
            onPress={this.props.SetConnectToOtherAppFalse}
          >
            <Icon
              style={{ textAlign: "left" }}
              name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            />
            <Text style={{ marginRight: 50 }}>Back</Text>
          </Button>

          <Button
            info
            onPress={help.OnGoogleTaskPress}
            style={{
              alignSelf: "center",
              marginBottom: 30,
              marginTop: 20
            }}
          >
            <Text>Get Google Tasks</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
