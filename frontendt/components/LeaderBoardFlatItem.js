import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert
} from "react-native";
import Loader from "../components/Loader";

const config = require("../constants/config.json");

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

class LeaderBoardFlatItem extends React.PureComponent {
  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });

    // on copilot stop
    /*this.props.copilotEvents.on('stop', async () => {
      await this.props.PostLeaderBoardTutorialLevel(1)
      this.props._signOutAsync()
    });*/
  }

  componentWillUnmount() {
    // Don't forget to disable event handlers to prevent errors
    //this.props.copilotEvents.off('stop');
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    item = this.props.item;
    index = this.props.index;
    return (
      <Content contentContainerStyle={{ alignItems: "center" }}>
        <Card
          style={{
            width: "90%",
            marginBottom: 30
          }}
        >
          <CardItem
            style={{
              flex: 1,
              margin: -1
            }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: "#fff",
                flexDirection: "column",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  flex: 1,
                  marginTop: -13,
                  marginBottom: -13,
                  marginLeft: -17,
                  marginRight: -17,
                  borderWidth: 1,
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  borderColor: "rgba(142, 176, 211, 0.8)",
                  backgroundColor: "rgba(142, 176, 211, 0.2)"
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <CopilotStep
                    text="Current avatar of the player"
                    order={1}
                    name="player"
                  >
                    <CView
                      style={{
                        flex: 1,
                        backgroundColor: "rgba(142, 176, 211, 0.3)",
                        alignItems: "center",
                        minWidth: 60,
                        minHeight: 120,
                        borderWidth: 1,
                        borderLeftWidth: 0,
                        borderBottomWidth: 0,
                        borderTopWidth: 0,
                        borderColor: "rgba(142, 176, 211, 0.2)"
                      }}
                    >
                      <Image
                        source={{ uri: item.URL }}
                        style={{
                          width: 100,
                          height: 100,
                          resizeMode: "contain",
                          margin: 10,
                          opacity: 1
                        }}
                      />
                    </CView>
                  </CopilotStep>

                  <View
                    style={{
                      flex: 1,
                      minWidth: 130,
                      backgroundColor: "transparent",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <CopilotStep
                      text="Name of the player"
                      order={2}
                      name="GivenName"
                    >
                      <CView
                        style={{
                          backgroundColor: "rgba(255,255,255,0.4)",
                          width: "100%"
                        }}
                      >
                        <Text
                          style={{
                            marginTop: 10,
                            marginBottom: 5,
                            color: "rgba(0,0,0,0.8)",
                            fontSize: 24,
                            lineHeight: 24,
                            textAlign: "center",
                            fontWeight: "bold"
                          }}
                        >
                          {item.GivenName}
                        </Text>
                      </CView>
                    </CopilotStep>

                    <CopilotStep
                      text="Rank of the player"
                      order={3}
                      name="rank"
                    >
                      <CView style={{ flex: 1, alignItems: "flex-start" }}>
                        <Text
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontSize: 18,
                            marginRight: 10,
                            fontWeight: "bold"
                          }}
                        >
                          {item.LeaderBoardType == "hero"
                            ? "Rank: " + item.Rank
                            : "Rank: " + item.JudgeRank}
                        </Text>
                      </CView>
                    </CopilotStep>

                    <CopilotStep
                      text="Rank of the player"
                      order={3}
                      name="rank"
                    >
                      <CView style={{ flex: 1, alignItems: "flex-start" }}>
                        <Text
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontSize: 16,
                            marginRight: 10,
                            fontWeight: "bold"
                          }}
                        >
                          {item.LeaderBoardType == "hero"
                            ? "Score: " + item.Score
                            : "Score: " + item.JudgeScore}
                        </Text>
                      </CView>
                    </CopilotStep>

                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignItems: "flex-start",
                        marginTop: -8
                      }}
                    >
                      <CopilotStep
                        text="Number of tasks completed by the player"
                        order={4}
                        name="Tasks"
                      >
                        <CView
                          style={{
                            marginTop: 9,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: "rgba(0,0,0,0.5)",
                              fontSize: 16,
                              marginLeft: 15
                            }}
                          >
                            {" "}
                            {item.LeaderBoardType == "hero"
                              ? "Tasks: " + item.TaskCount
                              : "Opinions: " + item.JudgementCount}{" "}
                          </Text>
                        </CView>
                      </CopilotStep>

                      <CopilotStep
                        text="Number of tasks verified by judges"
                        order={5}
                        name="Verified Tasks"
                      >
                        <CView
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            marginRight: 10
                          }}
                        >
                          <View style={{ marginTop: 8, marginLeft: 0 }}>
                            <CheckBox color="#32AC23" checked={true} />
                          </View>
                          <View style={{ marginLeft: 13, marginTop: 9 }}>
                            <Text
                              style={{ color: "rgba(0,0,0,0.5)", fontSize: 16 }}
                            >
                              :{" "}
                              {item.LeaderBoardType == "hero"
                                ? item.TaskVerifyCount
                                : item.JudgementVerifyCount}{" "}
                            </Text>
                          </View>
                        </CView>
                      </CopilotStep>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </CardItem>
        </Card>
      </Content>
    );
  }
}

export default copilot()(LeaderBoardFlatItem);

const styles = StyleSheet.create({});
