import React from "react";
import {
  StyleSheet,
  View,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";
import Popover from "react-native-popover-view";

import TopScreenHero from "../components/TopScreenHero";
import Loader from "../components/Loader";
import Tasks1 from "../components/Tasks/Tasks";
import * as firebase from "firebase";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

class Hero extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      loading: true,
      refreshing: false,
      screen: "hero",
      GivenName: "",
      heroGreeting: "Hello",
      heroPunctuation: "!",
      heroMessage: "Verify your Tasks and get more coins!",
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],

        Tasks: [
          {
            IDTask1: "aaaa",
            IDTask2: "bbbb",
            Title: "Please wait or drag down to refresh...",
            Checked: true,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        URL: `aaaa`
      }
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <Content
        style={{ backgroundColor: "transparent" }}
        contentContainerStyle={{
          alignItems: "center",
          backgroundColor: "transparent",
          justifyContent: "space-evenly"
        }}
      >
        <Card
          style={{
            width: "90%",
            margin: 0,
            backgroundColor: "white",
            justifyContent: "space-evenly"
          }}
        >
          <CardItem
            style={{
              backgroundColor: "transparent",
              justifyContent: "space-evenly"
            }}
          >
            <Body
              style={{
                flex: 1,
                backgroundColor: "transparent",
                flexDirection: "row",
                justifyContent: "space-around"
              }}
            >
              <View
                style={{
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop: 5
                }}
              >
                <TouchableOpacity
                  ref="Coins"
                  onPress={() => this.setState({ isCoinsVisible: true })}
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignSelf: "center"
                  }}
                >
                  <Image
                    source={require("../assets/images/coin.gif")}
                    style={{
                      height: 30,
                      width: 30,
                      marginTop: -10,
                      marginRight: 0,
                      marginLeft: 0
                    }}
                  />
                  <View>
                    <Text style={styles.title}>
                      {" "}
                      {this.props.HeroData.Coins}{" "}
                    </Text>
                  </View>
                </TouchableOpacity>

                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    minWidth: 80,
                    marginBottom: -12,
                    marginRight: 10
                  }}
                >
                  <TouchableOpacity
                    ref="Love"
                    onPress={() => this.setState({ isLoveVisible: true })}
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      marginRight: 20
                    }}
                  >
                    <Image
                      source={require("../assets/images/love_energy.png")}
                      style={{
                        height: 30,
                        width: 30,
                        marginTop: 2,
                        marginRight: 3,
                        marginLeft: 0
                      }}
                    />
                    <View>
                      <Text style={[styles.title, { marginTop: 12 }]}>
                        {" "}
                        {this.props.HeroData.LoveEnergy}{" "}
                      </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    ref="Dark"
                    onPress={() => this.setState({ isDarkVisible: true })}
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      marginLeft: 20
                    }}
                  >
                    <Image
                      source={require("../assets/images/dark_energy.png")}
                      style={{
                        height: 40,
                        width: 40,
                        marginTop: -2
                      }}
                    />
                    <View>
                      <Text style={[styles.title, { marginTop: 12 }]}>
                        {" "}
                        {this.props.HeroData.DarkEnergy}{" "}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={{
                  alignContent: "center",
                  justifyContent: "center",
                  marginTop: 5
                }}
              >
                <TouchableOpacity
                  ref="Task"
                  onPress={() => this.setState({ isTaskVisible: true })}
                  style={{ marginLeft: -10 }}
                >
                  <Text style={styles.title}>
                    {this.props.Screen == "hero"
                      ? "Tasks: " + this.props.HeroData.TaskCount
                      : "Opinions: " + this.props.HeroData.JudgementCount}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  ref="Verified"
                  onPress={() => this.setState({ isVerifiedVisible: true })}
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignSelf: "center",
                    marginBottom: -4
                  }}
                >
                  <View style={{ marginTop: 12, marginLeft: 0 }}>
                    <CheckBox color="#32AC23" checked={true} />
                  </View>
                  <View
                    style={{
                      marginTop: 16,
                      marginLeft: 12,
                      marginRight: 5
                    }}
                  >
                    <Text style={styles.title}>
                      :{" "}
                      {this.props.Screen == "hero"
                        ? this.props.HeroData.TaskVerifyCount
                        : this.props.HeroData.JudgementVerifyCount}{" "}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </Body>
          </CardItem>
        </Card>
        <Popover
          isVisible={this.state.isCoinsVisible}
          fromView={this.refs.Coins}
          onClose={() => this.setState({ isCoinsVisible: false })}
        >
          <Text style={{ margin: 10 }}>{`Soul Coins:
- earned by accomplishing tasks in real life
- useful to:
  - fight monsters
  - buy items in the Magical Shop!`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isLoveVisible}
          fromView={this.refs.Love}
          onClose={() => this.setState({ isLoveVisible: false })}
        >
          <Text style={{ margin: 10 }}>{`Love Energy:
- regularly given by the Gods of Love in adventure mode
- can be earned performing certain special actions
- useful to:
  - progress in the game
  - unleash your magical powers!`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isDarkVisible}
          fromView={this.refs.Dark}
          onClose={() => this.setState({ isDarkVisible: false })}
        >
          <Text style={{ margin: 10 }}>
            {`Dark Energy:
- can only be earned performing secret actions
- dark powers requiring dark energy are unlocked further in the game`}
          </Text>
        </Popover>
        <Popover
          isVisible={this.state.isTaskVisible}
          fromView={this.refs.Task}
          onClose={() => this.setState({ isTaskVisible: false })}
        >
          <Text style={{ margin: 10 }}>
            {this.props.Screen == "hero"
              ? `Number of Tasks you say you accomplished in real life.
Beware! The Gods of Truth will need more than your word to trust that you indeed accomplished your tasks!
Lie too many times and your Soul Coins will be turned to ashes to feed the Demons of Despair!         
`
              : `Number of times you swiped to give your opinion about other people's tasks.
Be careful! The Gods of Truth will examine your opinion: 
  - if they think you have been unfair, you may lose Soul Coins!
  - if they think you judged well, they will give you treasures!`}
          </Text>
        </Popover>
        <Popover
          isVisible={this.state.isVerifiedVisible}
          fromView={this.refs.Verified}
          onClose={() => this.setState({ isVerifiedVisible: false })}
        >
          <Text style={{ margin: 10 }}>
            {this.props.Screen == "hero"
              ? `Number of Tasks verified by the Gods of Truth. 
A verified task gives you much more coins and skills than a normal task.
You need to unlock the power to verify your tasks: you cannot do so at the very beginning of the game.`
              : `Number of times your opinions were assessed by the Gods of Truth.
Each time one of your opinions was assessed, you may gain or lose Soul Coins depending on the fairness of your judgement`}
          </Text>
        </Popover>
      </Content>
    );
  }
}

export default copilot()(Hero);

const styles = StyleSheet.create({
  title: {
    marginBottom: 0,
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});
