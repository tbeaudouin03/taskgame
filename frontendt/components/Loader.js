import React from "react";
import { Image, StyleSheet, View, AsyncStorage } from "react-native";

const config = require("../constants/config.json");

export default class HeroTuto extends React.Component {
  state = {
    LoaderURL:
      "https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcollectible_coin.gif?alt=media&token=5224bcaf-37a4-4e34-95d5-bc4c63ad4747"
  };

  async componentDidMount() {
    var LoaderURL = await AsyncStorage.getItem("LoaderURL");
    if (LoaderURL) {
      this.setState({ LoaderURL });
    }
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff"
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            marginTop: 0,
            marginBottom: -30
          }}
        >
          <Image
            source={{ uri: this.state.LoaderURL }}
            style={{
              width: 110,
              height: 130,
              resizeMode: "contain",
              marginBottom: -20,
              opacity: 1
            }}
          />
          <Image
            source={require("../assets/images/loading.gif")}
            style={{
              width: 110,
              height: 130,
              resizeMode: "contain",
              marginBottom: -20,
              opacity: 1
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
