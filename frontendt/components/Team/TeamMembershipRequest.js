import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast,
  Form,
  Item,
  Input,
  Label,
  Icon
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity,
  Platform
} from "react-native";

import Dialog, { DialogContent } from "react-native-popup-dialog";

import TeamMembershipRequestFlatItem from "./TeamMembershipRequestFlatItem";

const help = require(`../HelperFunction`);

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

export default class TeamMembershipRequest extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <Container style={{ backgroundColor: "transparent" }}>
        <Button
          full
          info
          style={{ marginBottom: 28, marginTop: 40 }}
          onPress={this.props.HideMembershipRequest}
        >
          <Icon
            style={{ textAlign: "left" }}
            name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
          />
          <Text style={{ marginRight: 50 }}>Back</Text>
        </Button>
        <Text
          style={{
            marginBottom: -5,
            color: "rgba(0,0,0,0.7)",
            fontSize: 18,
            fontWeight: "bold",
            textAlign: "center"
          }}
        >
          Other Heros want to join your team!
        </Text>
        <Content
          style={{
            height: 66.5,
            marginTop: 0,
            backgroundColor: "transparent"
          }}
        >
          <FlatList
            initialNumToRender={10}
            windowSize={3}
            removeClippedSubviews={true}
            style={{ flex: 1, backgroundColor: "transparent" }}
            contentContainerStyle={{
              justifyContent: "flex-start",
              backgroundColor: "transparent",
              marginTop: 20
            }}
            data={this.props.TeamMembershipRequest}
            renderItem={({ item, index }) => (
              <TeamMembershipRequestFlatItem item={item} index={index} />
            )}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
