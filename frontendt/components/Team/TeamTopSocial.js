import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast,
  Form,
  Item,
  Input
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity,
  Clipboard
} from "react-native";

import Popover from "react-native-popover-view";

import Dialog, { DialogContent } from "react-native-popup-dialog";

const config = require("../../constants/config.json");
const help = require(`../../components/HelperFunction`);

export default class TeamTopSocial extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      LeaderBoardType: "hero",
      BuyPressStatus: [],
      UsePressStatus: [],
      LeaderBoardDataHero: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "hero"
          }
        ]
      },
      LeaderBoardDataJudge: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "judge"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0
      }
    };
  }

  GetIDTeam = async () => {
    Clipboard.setString(this.props.HeroData.IDTeam);
    Toast.show({
      text: "Team ID copied to clipboard",
      buttonText: "Okay",
      position: "bottom",
      duration: 5000
    });
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false,
      TeamInfo:
        !this.props.LeaderBoardDataHero.LeaderBoardObjects[1] ||
        this.props.HeroData.TutorialLevel <= 19
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <Content
        style={{ backgroundColor: "transparent" }}
        contentContainerStyle={{
          alignItems: "center",
          backgroundColor: "transparent",
          justifyContent: "center"
        }}
      >
        <Card
          style={{
            width: "91%",
            marginBottom: 5,
            backgroundColor: "white",
            justifyContent: "center"
          }}
        >
          <CardItem
            style={{
              backgroundColor: "transparent",
              justifyContent: "center"
            }}
          >
            {this.RenderCardItem()}
          </CardItem>
        </Card>
      </Content>
    );
  }

  RenderCardItem() {
    if (!this.state.TeamInfo) {
      return (
        <Body
          style={{
            flex: 1,
            backgroundColor: "#fff",
            flexDirection: "column",
            justifyContent: "center"
          }}
        >
          <Button
            small
            info
            onPress={() => this.setState({ TeamInfo: true })}
            style={{
              //margin: 10,
              alignSelf: "center",
              width: "100%",
              justifyContent: "center"
            }}
          >
            <Text>Show Team Info</Text>
          </Button>
        </Body>
      );
    }
    return (
      <Body
        style={{
          flex: 1,
          backgroundColor: "#fff",
          flexDirection: "column",
          justifyContent: "center"
        }}
      >
        {this.RenderFindTeammate()}
        <Button
          small
          info
          onPress={this.GetIDTeam}
          style={{
            alignSelf: "center",
            width: "100%",
            justifyContent: "center"
          }}
        >
          <Text>Get your Team ID</Text>
        </Button>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Form style={{ width: "60%", marginRight: 10 }}>
            <Item>
              <Input
                placeholder="Team ID"
                onChangeText={text => this.setState({ IDTeam: text })}
              />
            </Item>
          </Form>
          <Button
            info
            small
            onPress={() =>
              help.AlertOnJoinTeam(
                this.state.IDTeam,
                this.props.HeroData.Level,
                this.props.HeroData.LoveEnergy,
                this.props.HeroData.URL
              )
            }
            style={{ marginTop: 17, marginLeft: 7, marginRight: 10 }}
          >
            <Text> Join Team </Text>
          </Button>
        </View>
        {this.RenderMembershipRequest()}
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 10
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "flex-start"
            }}
          >
            <Text style={{ marginTop: 18, marginBottom: 0, margin: 13 }}>
              Team Group(s):
            </Text>
            {this.RenderFacebookGroup()}
            {this.RenderLinkedinGroup()}
          </View>
          <Button
            info
            small
            onPress={this.props.ShowEditTeamGroup}
            style={{
              marginLeft: 10,
              marginRight: 10,
              marginTop: 14.5,
              alignSelf: "flex-end"
            }}
          >
            <Text>
              {this.props.HeroData.TeamFacebookGroup ||
              this.props.HeroData.TeamLinkedinGroup
                ? "Edit"
                : "Add Group"}
            </Text>
          </Button>
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <Text style={{ marginTop: 18, marginBottom: 0, margin: 13 }}>
            Team Leader:{" "}
          </Text>
          <Text
            style={{
              marginTop: 18,
              marginBottom: 0,
              margin: 13,
              fontWeight: "bold"
            }}
          >
            {this.props.TeamLeaderName ? this.props.TeamLeaderName : ""}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignSelf: "center",
            marginTop: 5
          }}
        >
          <Button
            info
            small
            onPress={async () => {
              await help.PostHelpTeamLeader(true, this.props.HeroData.IDTeam);
              this.props._onRefresh();
            }}
            style={{ margin: 10, marginBottom: 0 }}
          >
            <Text>Follow</Text>
          </Button>
          <Button
            info
            small
            onPress={async () => {
              await help.PostHelpTeamLeader(false, this.props.HeroData.IDTeam);
              this.props._onRefresh();
            }}
            style={{ margin: 10, marginBottom: 0 }}
          >
            <Text> Unfollow</Text>
          </Button>
        </View>
        <Button
          small
          // info
          onPress={() => this.setState({ TeamInfo: false })}
          style={{
            marginTop: 15,
            alignSelf: "center",
            width: "100%",
            justifyContent: "center"
          }}
        >
          <Text>Hide Team Info</Text>
        </Button>
      </Body>
    );
  }

  RenderMembershipRequest() {
    if (
      this.props.TeamMembershipRequest ||
      this.props.HeroData.TutorialLevel <= 19
    ) {
      return (
        <Button
          small
          info
          onPress={this.props.ShowMembershipRequest}
          style={{
            alignSelf: "center",
            width: "105%",
            justifyContent: "center",
            marginTop: 20
          }}
        >
          <Text>See Membership Requests</Text>
        </Button>
      );
    }
  }

  RenderFacebookGroup() {
    if (this.props.HeroData.TeamFacebookGroup) {
      return (
        <TouchableOpacity
          onPress={() =>
            help.OnLinkPress(this.props.HeroData.TeamFacebookGroup)
          }
        >
          <Image
            source={require("../../assets/images/facebook.png")}
            style={{
              height: 27.5,
              width: 27.5,
              marginTop: 15,
              marginBottom: 0,
              margin: 10
            }}
          />
        </TouchableOpacity>
      );
    }
  }

  RenderLinkedinGroup() {
    if (this.props.HeroData.TeamLinkedinGroup) {
      return (
        <TouchableOpacity
          onPress={() =>
            help.OnLinkPress(this.props.HeroData.TeamLinkedinGroup)
          }
        >
          <Image
            source={require("../../assets/images/linkedin.png")}
            style={{
              height: 28,
              width: 28,
              marginTop: 15.2,
              marginBottom: 0,
              margin: 10
            }}
          />
        </TouchableOpacity>
      );
    }
  }

  RenderFindTeammate() {
    if (
      !this.props.LeaderBoardDataHero.LeaderBoardObjects[2] ||
      this.props.HeroData.TutorialLevel <= 19
    ) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "flex-start",
            marginBottom: 10,
            marginTop: 6
          }}
        >
          <Text style={{ marginTop: 3, marginBottom: 10 }}>
            Find teammates on:
          </Text>
          <TouchableOpacity
            ref="Facebook"
            onPress={() =>
              help.OnLinkPress(
                `https://www.facebook.com/TaskGame-999122266926628/`
              )
            }
          >
            <Image
              source={require("../../assets/images/facebook.png")}
              style={{
                height: 27.5,
                width: 27.5,
                marginTop: 0,
                margin: 10
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              help.OnLinkPress(`https://www.linkedin.com/groups/12224102/`)
            }
          >
            <Image
              source={require("../../assets/images/linkedin.png")}
              style={{
                height: 28,
                width: 28,
                marginTop: 0.2,
                margin: 10
              }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  segment: {
    height: "80%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "80%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
