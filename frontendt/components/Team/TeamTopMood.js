import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast,
  Form,
  Item,
  Input
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import Popover from "react-native-popover-view";

const config = require("../../constants/config.json");
const help = require(`../../components/HelperFunction`);

export default class TeamTopMood extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      LeaderBoardType: "hero",
      BuyPressStatus: [],
      UsePressStatus: [],
      LeaderBoardDataHero: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "hero"
          }
        ]
      },
      LeaderBoardDataJudge: {
        LeaderBoardObjects: [
          {
            key: "aaa",
            GivenName: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Level: 0,
            JudgeLevel: 0,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            TaskCount: 0,
            TaskVerifyCount: 0,
            JudgementCount: 0,
            JudgementVerifyCount: 0,
            LeaderBoardType: "judge"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait or drag down to refresh...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0
      }
    };
  }

  OnMoodPress = async Mood => {
    this.props.SetMood(Mood);
    await help.PostMoodStatus(Mood, ``);
    this.props._onRefresh();
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false, Status: this.props.HeroData.Status });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <Content
        style={{ backgroundColor: "transparent" }}
        contentContainerStyle={{
          alignItems: "center",
          backgroundColor: "transparent",
          justifyContent: "center"
        }}
      >
        <Card
          style={{
            width: "91%",
            marginBottom: 5,
            backgroundColor: "white",
            justifyContent: "center"
          }}
        >
          <CardItem
            style={{
              backgroundColor: "transparent",
              justifyContent: "center"
            }}
          >
            <Body
              style={{
                flex: 1,
                backgroundColor: "#fff",
                flexDirection: "column",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-around"
                }}
              >
                <TouchableOpacity
                  onPress={() => this.OnMoodPress(`very_happy`)}
                >
                  <Image
                    source={require("../../assets/images/very_happy.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        margin: 1.5
                      },
                      this.props.HeroData.Mood == "very_happy"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`happy`)}>
                  <Image
                    source={require("../../assets/images/happy.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 1,
                        margin: 1.5,
                        marginRight: 1.4,
                        marginBottom: -1.5
                      },
                      this.props.HeroData.Mood == "happy"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`in_love`)}>
                  <Image
                    source={require("../../assets/images/in_love.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: -1,
                        margin: 1.5,
                        marginBottom: 0
                      },
                      this.props.HeroData.Mood == "in_love"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`combative`)}>
                  <Image
                    source={require("../../assets/images/combative.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 0,
                        margin: 1.5,
                        marginBottom: 0
                      },
                      this.props.HeroData.Mood == "combative"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`awkward`)}>
                  <Image
                    source={require("../../assets/images/awkward.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 0,
                        margin: 1.5,
                        marginBottom: 0
                      },
                      this.props.HeroData.Mood == "awkward"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`unsure`)}>
                  <Image
                    source={require("../../assets/images/unsure.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 0,
                        margin: 1.5,
                        marginBottom: 0
                      },
                      this.props.HeroData.Mood == "unsure"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`sad`)}>
                  <Image
                    source={require("../../assets/images/sad.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 0,
                        margin: 1.5,
                        marginBottom: -1.5
                      },
                      this.props.HeroData.Mood == "sad"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`angry`)}>
                  <Image
                    source={require("../../assets/images/angry.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 0,
                        margin: 1.5,
                        marginBottom: -1
                      },
                      this.props.HeroData.Mood == "angry"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.OnMoodPress(`dead`)}>
                  <Image
                    source={require("../../assets/images/dead.png")}
                    style={[
                      {
                        height: 35,
                        width: 35,
                        marginTop: 0,
                        margin: 1.5,
                        marginBottom: 0
                      },
                      this.props.HeroData.Mood == "dead"
                        ? styles.emojiP
                        : { opacity: 0.6 }
                    ]}
                  />
                </TouchableOpacity>
              </View>

              <TouchableOpacity
                style={{ alignSelf: "center" }}
                onPress={this.props.ShowEditStatus}
              >
                <Text
                  style={{
                    margin: 10,
                    color: "rgba(0,0,0,0.95)",
                    fontWeight: "bold",
                    fontSize: 17,
                    textAlign: "center"
                  }}
                >
                  {this.props.HeroData.Status
                    ? this.props.HeroData.Status
                    : "Tap here to change status..."}
                </Text>
              </TouchableOpacity>
            </Body>
          </CardItem>
        </Card>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  emojiP: {
    opacity: 1,
    backgroundColor: "rgba(142, 176, 211, 0.2)",
    borderRadius: 10
  }
});
