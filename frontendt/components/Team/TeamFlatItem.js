import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";

const config = require("../../constants/config.json");
const help = require(`../../components/HelperFunction`);

export default class TeamFlatItem extends React.PureComponent {
  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  RenderLeaderStar() {
    if (item.IsBondedTeamLeader) {
      return (
        <Image
          source={require("../../assets/images/star.png")}
          style={{
            marginTop: 2,
            height: 20,
            width: 20
          }}
        />
      );
    } else if (item.IsBondedToTeam) {
      return (
        <Image
          source={require("../../assets/images/sheep.png")}
          style={{
            marginTop: 0,
            marginRight: -2,
            height: 30,
            width: 30
          }}
        />
      );
    }
    return (
      <Image
        source={require("../../assets/images/red_panda.png")}
        style={{
          marginTop: -2,
          marginRight: -2,
          height: 30,
          width: 30
        }}
      />
    );
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    item = this.props.item;
    index = this.props.index;
    moods = {
      very_happy: require("../../assets/images/very_happy.png"),
      happy: require("../../assets/images/happy.png"),
      in_love: require("../../assets/images/in_love.png"),
      unsure: require("../../assets/images/unsure.png"),
      combative: require("../../assets/images/combative.png"),
      awkward: require("../../assets/images/awkward.png"),
      sad: require("../../assets/images/sad.png"),
      angry: require("../../assets/images/angry.png"),
      dead: require("../../assets/images/dead.png")
    };

    mood = moods[item.Mood];

    return (
      <Content contentContainerStyle={{ alignItems: "center" }}>
        <Card
          style={{
            width: "90%",
            marginBottom: 30
          }}
        >
          <CardItem
            style={{
              flex: 1,
              margin: -1
            }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: "#fff",
                flexDirection: "column",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  flex: 1,
                  marginTop: -13,
                  marginBottom: -13,
                  marginLeft: -17,
                  marginRight: -17,
                  borderWidth: 1,
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  borderColor: "rgba(142, 176, 211, 0.8)",
                  backgroundColor: "rgba(142, 176, 211, 0.2)"
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: "rgba(142, 176, 211, 0.3)",
                      alignItems: "center",
                      minWidth: 60,
                      minHeight: 160,
                      borderWidth: 1,
                      borderLeftWidth: 0,
                      borderBottomWidth: 0,
                      borderTopWidth: 0,
                      borderColor: "rgba(142, 176, 211, 0.2)"
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        minHeight: 57.3,
                        width: "100%",
                        backgroundColor: "rgba(142, 176, 211, 0.3)",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text
                        style={{
                          margin: 8.5,
                          color: "rgba(255,255,255,0.8)",
                          fontSize: 14,
                          textAlign: "center"
                        }}
                      >
                        {item.Status ? item.Status : "Nothing to say!"}
                      </Text>
                    </View>

                    <Image
                      source={{ uri: item.URL }}
                      style={{
                        width: 130,
                        height: 130,
                        resizeMode: "contain",
                        margin: 10,
                        opacity: 1
                      }}
                    />
                  </View>

                  <View
                    style={{
                      flex: 1,
                      minWidth: 130,
                      backgroundColor: "transparent",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "rgba(255,255,255,0.4)",
                        width: "100%",
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      {this.RenderLeaderStar()}
                      <Text
                        style={{
                          marginTop: 10,
                          marginBottom: 5,
                          marginLeft: 15,
                          marginRight: 10,
                          color: "rgba(0,0,0,0.8)",
                          fontSize: 24,
                          lineHeight: 24,
                          textAlign: "center",
                          fontWeight: "bold"
                        }}
                      >
                        {item.GivenName}
                      </Text>
                      <Image
                        source={mood}
                        style={{
                          marginTop: 2,
                          height: 30,
                          width: 30
                        }}
                      />
                    </View>

                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignSelf: "center",
                        alignItems: "center",
                        marginTop: 5
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          marginLeft: 5,
                          maxWidth: 40
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            alignSelf: "center",
                            marginTop: 0,
                            marginRight: 0,
                            backgroundColor: "rgba(142, 176, 211, 1)",
                            borderColor: "rgba(255,255,255,0.5)",
                            borderRadius: 10,
                            maxHeight: 34,
                            minWidth: 27
                          }}
                        >
                          <Text
                            style={{
                              margin: 5,
                              color: "rgba(255,255,255,1)",
                              fontSize: 16,
                              textAlign: "center",
                              fontWeight: "bold"
                            }}
                          >
                            {item.LeaderBoardType == "hero"
                              ? item.Rank
                              : item.JudgeRank}
                          </Text>
                        </View>
                      </View>

                      <View style={{ flex: 1, marginLeft: -20 }}>
                        <Text
                          style={{
                            margin: 5,
                            color: "rgba(0,0,0,0.8)",
                            fontSize: 16,
                            textAlign: "center",
                            fontWeight: "bold"
                          }}
                        >
                          {item.LeaderBoardType == "hero"
                            ? item.Score
                            : item.JudgeScore}
                        </Text>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          marginLeft: -15
                        }}
                      >
                        <Image
                          source={require("../../assets/images/coin.png")}
                          style={{
                            height: 30,
                            width: 30
                          }}
                        />
                        <Text
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontSize: 16,
                            margin: 5,
                            marginTop: 3,
                            marginLeft: 0
                          }}
                        >
                          {item.Coins}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        marginTop: 5,
                        marginBottom: 5
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-start"
                        }}
                      >
                        <Image
                          source={require("../../assets/images/love_energy.png")}
                          style={{
                            height: 30,
                            width: 30,
                            marginLeft: 9,
                            marginRight: 9
                          }}
                        />
                        <Text
                          style={{
                            color: "rgba(0,0,0,0.4)",
                            fontSize: 16,
                            textAlign: "left",
                            marginTop: 5
                          }}
                        >
                          {item.LoveEnergy}
                        </Text>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-start"
                        }}
                      >
                        <Image
                          source={require("../../assets/images/dark_energy.png")}
                          style={{
                            height: 45,
                            width: 45,
                            marginTop: -6.5,
                            marginLeft: 0,
                            marginRight: 0
                          }}
                        />
                        <Text
                          style={{
                            color: "rgba(0,0,0,0.4)",
                            fontSize: 16,
                            textAlign: "left",
                            marginTop: 5,
                            marginRight: 10
                          }}
                        >
                          {item.DarkEnergy}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignItems: "flex-start",
                        marginTop: -8
                      }}
                    >
                      <View
                        style={{
                          marginTop: 0,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontSize: 16,
                            marginLeft: 5
                          }}
                        >
                          {" "}
                          {item.LeaderBoardType == "hero"
                            ? "Tasks: " + item.TaskCount
                            : "Opinions: " + item.JudgementCount}{" "}
                        </Text>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                          marginRight: 10
                        }}
                      >
                        <View style={{}}>
                          <CheckBox color="#32AC23" checked={true} />
                        </View>

                        <Text
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontSize: 16,
                            marginLeft: 13
                          }}
                        >
                          {item.LeaderBoardType == "hero"
                            ? ": " + item.TaskVerifyCount
                            : ": " + item.JudgementVerifyCount}
                        </Text>
                      </View>
                    </View>
                    <Button
                      small
                      info
                      onPress={this.props.PostTeamLeaderChoice}
                      style={{
                        margin: 8,
                        alignSelf: "center",
                        width: "90%",
                        justifyContent: "center"
                      }}
                    >
                      <Text>Elect as leader</Text>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </CardItem>
        </Card>
      </Content>
    );
  }
}

const styles = StyleSheet.create({});
