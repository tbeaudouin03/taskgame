import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast,
  Form,
  Item,
  Input,
  Label
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import Dialog, { DialogContent } from "react-native-popup-dialog";


const config = require("../../constants/config.json");
const help = require(`../../components/HelperFunction`);

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

export default class TeamAddGroup extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  OnUpdateStatus = async () => {
    this.props.SetStatus(this.state.Status);
    await help.PostMoodStatus(``, this.state.Status);
    this.props.HideEditStatus();
    if (this.props.HeroData.TutorialLevel == 9) {
      await help.PostTutorialLevel(this.props.HeroData.TutorialLevel + 1);
      await help.Wait(500);
      await this.props._onRefresh();
      this.props.SetIsMoodTutoVisible(false);
    } else {
      this.props._onRefresh();
    }
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View>
        <Dialog
          visible={!!this.props.visibleStatus}
          onTouchOutside={this.props.HideEditStatus}
        >
          <DialogContent style={{ width: "100%", height: 200 }}>
            <Text
              style={{
                marginTop: 18,
                marginBottom: 10,
                color: "rgba(0,0,0,0.7)",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center"
              }}
            >
              Update Status
            </Text>
            <View
              style={{
                flex: 1,
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <Form style={{ width: "100%" }}>
                <Item stackedLabel>
                  <Label>Your status</Label>
                  <Input
                    style={{ maxWidth: 250 }}
                    placeholder={
                      this.props.HeroData.Status
                        ? this.props.HeroData.Status
                        : "Write status here"
                    }
                    onChangeText={text => this.setState({ Status: text })}
                  />
                </Item>
              </Form>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Button
                  onPress={this.props.HideEditStatus}
                  info
                  small
                  style={{ margin: 10 }}
                >
                  <Text> Cancel </Text>
                </Button>
                <Button
                  onPress={this.OnUpdateStatus}
                  info
                  small
                  style={{ margin: 10 }}
                >
                  <Text> Update </Text>
                </Button>
              </View>
            </View>
          </DialogContent>
        </Dialog>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  segment: {
    height: "80%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "80%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
