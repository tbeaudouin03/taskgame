import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast,
  Form,
  Item,
  Input,
  Label
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import Dialog, { DialogContent } from "react-native-popup-dialog";
import TeamFlatItem from "./TeamFlatItem";

const config = require("../../constants/config.json");
const help = require(`../../components/HelperFunction`);

export default class TeamAddGroup extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  OnUpdateFacebookGroup = async () => {
    this.props.SetTeamFacebookGroup(this.state.TeamFacebookURL);
    help.PostTeamInfo(
      this.props.HeroData.IDTeam,
      this.state.TeamFacebookURL,
      ``,
      ``
    );
    this.props.HideEditTeamGroup();
  };

  OnUpdateLinkedinGroup = async () => {
    this.props.SetTeamLinkedinGroup(this.state.TeamLinkedinURL);
    help.PostTeamInfo(
      this.props.HeroData.IDTeam,
      ``,
      this.state.TeamLinkedinURL,
      ``
    );
    this.props.HideEditTeamGroup();
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View>
        <Dialog
          visible={!!this.props.visible}
          onTouchOutside={this.props.HideEditTeamGroup}
        >
          <DialogContent style={{ width: "90%", height: 300 }}>
            <Text
              style={{
                marginTop: 18,
                marginBottom: 10,
                color: "rgba(0,0,0,0.7)",
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center"
              }}
            >
              Groups of your Team
            </Text>
            <View
              style={{
                flex: 1,
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <Form style={{ width: "65%" }}>
                  <Item stackedLabel>
                    <Label>Facebook Group URL</Label>
                    <Input
                      style={{ width: 190 }}
                      placeholder={"https://www.facebook.com/groups/..."}
                      onChangeText={text =>
                        this.setState({ TeamFacebookURL: text })
                      }
                    />
                  </Item>
                </Form>
                <Button
                  onPress={this.OnUpdateFacebookGroup}
                  info
                  small
                  style={{ marginTop: 35, marginLeft: 15 }}
                >
                  <Text> Update </Text>
                </Button>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <Form style={{ width: "65%" }}>
                  <Item stackedLabel style={{ marginTop: -20 }}>
                    <Label>LinkedIn Group URL</Label>
                    <Input
                      style={{ width: 190 }}
                      placeholder={"https://www.linkedin.com/groups/..."}
                      onChangeText={text =>
                        this.setState({ TeamLinkedinURL: text })
                      }
                    />
                  </Item>
                </Form>
                <Button
                  onPress={this.OnUpdateLinkedinGroup}
                  info
                  small
                  style={{ marginLeft: 15 }}
                >
                  <Text> Update </Text>
                </Button>
              </View>
              <Button
                onPress={this.props.HideEditTeamGroup}
                info
                small
                style={{ marginLeft: 13 }}
              >
                <Text> Cancel </Text>
              </Button>
            </View>
          </DialogContent>
        </Dialog>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  segment: {
    height: "80%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "80%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
