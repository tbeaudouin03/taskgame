import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

import Popover from "react-native-popover-view";

import TypeWriter from "react-native-typewriter";

import ShieldBottomMonster from "./MonsterAnimations/ShieldBottomMonster";
import SwordBottomMonster from "./MonsterAnimations/SwordBottomMonster";
import BookBottomMonster from "./MonsterAnimations/BookBottomMonster";

import InvisibleButtonMonster from "./MonsterAnimations/InvisibleButtonMonster";
import MonsterLifeBar from "./MonsterAnimations/MonsterLifeBar";

const help = require(`../HelperFunction`);

export default class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    let MonsterSpeech,
      FightStatus = this.props.FightStatus;
    if (
      (FightStatus.MonsterRemainingLife > 0 && !this.props.IsFriend) ||
      (this.props.IsFriend && !FightStatus.MonsterIsIntroduced)
    ) {
      MonsterSpeech =
        this.props.MonsterTalkCount % 2 == 0
          ? FightStatus.OnBuyMessage
          : FightStatus.Description;
    } else {
      MonsterSpeech = FightStatus.OnSellMessage;
    }

    if (this.state.loading) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <InvisibleButtonMonster
          MonsterMove={this.props.MonsterMove}
          HandleTapMonster={this.props.HandleTapMonster}
          OnAttack={this.props.OnAttack}
        />

        <Animated.View
          style={[
            {
              flex: 1,
              position: "absolute",
              alignItems: "center",
              alignSelf: "center",
              opacity: 1,
              zIndex: 2
            },
            this.props.MonsterMove.getLayout()
          ]}
        >
          <ShieldBottomMonster
            FightStatus={this.props.FightStatus}
            MonsterShieldOpacity={this.props.MonsterShieldOpacity}
          />

          <SwordBottomMonster
            FightStatus={this.props.FightStatus}
            MonsterSwordOpacity={this.props.MonsterSwordOpacity}
          />

          <BookBottomMonster
            FightStatus={this.props.FightStatus}
            MonsterBookOpacity={this.props.MonsterBookOpacity}
          />

          {this.RenderMonsterLifeBar()}

          <Image
            ref="Monster"
            source={{
              uri: this.props.FightStatus.CharacterURL
            }}
            style={[
              {
                width: this.props.IsFriend ? 85 : 120,
                height: this.props.IsFriend ? 85 : 120,
                resizeMode: "contain",
                position: "absolute",
                opacity: 1,
                zIndex: 2
              }
            ]}
          />
        </Animated.View>
        <Popover
          isVisible={this.props.IsTalkingMonsterVisible}
          placement={"top"}
          fromView={this.refs.Monster}
          popoverStyle={{ width: "90%" }}
          onClose={async () => {
            this.props.SetIsTalkingMonsterVisible(false);

            let TutorialLevel = this.props.HeroData.TutorialLevel;
            if (
              TutorialLevel == 3 ||
              TutorialLevel == 4 ||
              TutorialLevel == 5 ||
              TutorialLevel == 6
            ) {
              await help.Wait(800);
              this.props._onRefresh(false, false, true);
            }
            if (
              (FightStatus.MonsterRemainingLife <= 0 && !this.props.IsFriend) ||
              (this.props.IsFriend && FightStatus.MonsterIsIntroduced)
            ) {
              await help.PostMonsterIsDefeated();

              if (TutorialLevel == 8) {
                await help.PostTutorialLevel(TutorialLevel + 1);
                await help.PostChooseRandomTeam();
                await help.Wait(500);
                this.props.navigation.navigate("Team");
              }
              if (FightStatus.IDShopObject == "i1004") {
                await help.PostTutorialLevel(15)
                await help.Wait(500)
                this.props.navigation.navigate("Shop");
              }
              await help.Wait(500);
              this.props._onRefresh(false, true, false);
            }
          }}
        >
          <Text
            style={{
              margin: 10,
              textAlign: `center`,
              alignSelf: "center",
              fontWeight: "bold",
              marginBottom: 0,
              fontSize: 18,
              marginRight: 15,
              marginLeft: 15
            }}
          >
            {this.props.FightStatus.Title}
          </Text>
          <TypeWriter
            typing={1}
            fixed={true}
            style={{
              margin: 10,
              textAlign: `center`,
              alignSelf: "center",
              fontSize: 16,
              marginRight: 15,
              marginLeft: 15
            }}
          >
            {MonsterSpeech}
          </TypeWriter>
        </Popover>
      </View>
    );
  }

  RenderMonsterLifeBar() {
    if (!this.props.IsFriend) {
      return (
        <MonsterLifeBar
          FightStatus={this.props.FightStatus}
          OpacityMonsterDamage={this.props.OpacityMonsterDamage}
          NewDamageToMonster={this.props.NewDamageToMonster}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
