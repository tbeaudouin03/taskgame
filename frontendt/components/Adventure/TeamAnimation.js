import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

import ShieldTopTeam from "./TeamAnimations/ShieldTopTeam";
import SwordTopTeam from "./TeamAnimations/SwordTopTeam";
import BookTopTeam from "./TeamAnimations/BookTopTeam";

import InvisibleButtonTeam from "./TeamAnimations/InvisibleButtonTeam";
import TeamLifeBar from "./TeamAnimations/TeamLifeBar";
import TopCompanionCount from "./TeamAnimations/TopCompanionCount";

const help = require(`../HelperFunction`);

export default class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading || !this.props.FightStatusTeam) {
      return <View />;
    }
    return (
      <View
        style={{
          flex: 1,
          width: "100%",
          height: "100%"
        }}
      >
        <InvisibleButtonTeam
          TeamMove={this.props.TeamMove}
          HandleTapTeam={this.props.HandleTapTeam}
        />

        <Animated.View
          style={[
            {
              flex: 1,
              position: "absolute",
              alignItems: "center",
              alignSelf: "center",
              opacity: 1,
              zIndex: 2
            },
            this.props.TeamMove.getLayout()
          ]}
        >
          <ShieldTopTeam
            TeamShieldOpacity={this.props.TeamShieldOpacity}
            TeamReputation={this.props.TeamReputation}
          />

          <SwordTopTeam
            TeamSwordOpacity={this.props.TeamSwordOpacity}
            TeamFitness={this.props.TeamFitness}
          />

          <BookTopTeam
            TeamBookOpacity={this.props.TeamBookOpacity}
            TeamIntelligence={this.props.TeamIntelligence}
          />

          {this.RenderTeamLifeBar()}

          {this.RenderTeammates()}
        </Animated.View>
      </View>
    );
  }

  RenderTopCompanionCount() {
    if (!this.props.IsFriend) {
      return (
        <TopCompanionCount
          OpacityTopCompanionCount={this.props.OpacityTopCompanionCount}
          FightStatusTeam={this.props.FightStatusTeam}
        />
      );
    }
  }

  RenderTeamLifeBar() {
    if (!this.props.IsFriend) {
      return (
        <TeamLifeBar
          TeamLifeBarOpacity={this.props.TeamLifeBarOpacity}
          OpacityTeamDamage={this.props.OpacityTeamDamage}
          WeakestCompanionLifeRatio={this.props.WeakestCompanionLifeRatio}
          NewDamageToTeam={this.props.NewDamageToTeam}
        />
      );
    }
  }
  RenderTeammates = () => {
    let ShuffledFightStatusTeam = this.props.FightStatusTeam;
    if (this.props.FightStatusTeam.length == 1) {
      return (
        <Animated.Image
          source={{
            uri: ShuffledFightStatusTeam[0].HeroURL
          }}
          style={[
            {
              width: 60,
              height: 60,
              resizeMode: "contain",
              opacity: 1,
              zIndex: 2,
              position: "absolute",
              alignSelf: "center",
              top: 5,
              opacity: this.props.OpacityTeammate2
            }
          ]}
        />
      );
    }
    if (this.props.FightStatusTeam.length == 2) {
      return (
        <View
          style={{
            flex: 1,
            width: "100%",
            height: "100%"
          }}
        >
          <Animated.Image
            source={{
              uri: ShuffledFightStatusTeam[0].HeroURL
            }}
            style={[
              {
                width: 60,
                height: 60,
                resizeMode: "contain",
                opacity: 1,
                zIndex: 2,
                position: "absolute",
                alignSelf: "center",
                left: 5 + Math.random() * 30,
                opacity: this.props.OpacityTeammate1
              }
            ]}
          />

          <Animated.Image
            source={{
              uri: ShuffledFightStatusTeam[1].HeroURL
            }}
            style={[
              {
                width: 60,
                height: 60,
                resizeMode: "contain",
                opacity: 1,
                zIndex: 2,
                position: "absolute",
                alignSelf: "center",
                right: 5 + Math.random() * 30,
                opacity: this.props.OpacityTeammate3
              }
            ]}
          />
        </View>
      );
    }
    return (
      <View
        style={{
          flex: 1,
          width: "100%",
          height: "100%"
        }}
      >
        <Animated.Image
          source={{
            uri: ShuffledFightStatusTeam[0].HeroURL
          }}
          style={[
            {
              width: 60,
              height: 60,
              resizeMode: "contain",
              opacity: 1,
              zIndex: 2,
              position: "absolute",
              alignSelf: "center",
              left: 5 + Math.random() * 30,
              opacity: this.props.OpacityTeammate1
            }
          ]}
        />

        <Animated.Image
          source={{
            uri: ShuffledFightStatusTeam[1].HeroURL
          }}
          style={[
            {
              width: 60,
              height: 60,
              resizeMode: "contain",
              opacity: 1,
              zIndex: 2,
              position: "absolute",
              alignSelf: "center",
              top: 5 + Math.random() * 30,
              opacity: this.props.OpacityTeammate2
            }
          ]}
        />

        <Animated.Image
          source={{
            uri: ShuffledFightStatusTeam[2].HeroURL
          }}
          style={[
            {
              width: 60,
              height: 60,
              resizeMode: "contain",
              opacity: 1,
              zIndex: 2,
              position: "absolute",
              alignSelf: "center",
              right: 5 + Math.random() * 30,
              opacity: this.props.OpacityTeammate3
            }
          ]}
        />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
