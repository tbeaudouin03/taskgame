import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

export default class BlueArrowUpTuto extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    let TextTuto = "";
    if (
      this.props.HeroData.TutorialLevel == 3 ||
      this.props.HeroData.TutorialLevel == 5 ||
      this.props.HeroData.TutorialLevel == 6
    ) {
      TextTuto = `Tap here`;
    } else {
      TextTuto = `Press longer`;
    }

    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <Animated.View
          style={[
            {
              position: "absolute",
              flex: 1,
              zIndex: 3,
              backgroundColor: `transparent`,
              alignItems: "center",
              justifyContent: "center",
              opacity: this.props.OpacityBlueArrowUpTuto
            },
            this.props.BlueArrowUpTutoMove.getLayout()
          ]}
        >
          <Image
            source={require("../../assets/images/short_blue_arrow_up.png")}
            style={{
              height: 60,
              width: 30,
              resizeMode: "stretch"
            }}
          />
          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 18,
              textAlign: "center",
              fontWeight: "bold",
              padding: 5,
              paddingRight: 10,
              paddingLeft: 10,
              marginTop: 10,
              borderRadius: 10,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {TextTuto}
          </Text>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66


