import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

export default class TeamHitAnimation extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <Animated.Image
          source={this.props.HitImage5}
          style={[
            {
              width: 85,
              height: 85,
              resizeMode: "stretch",
              position: "absolute",
              zIndex: 3,
              opacity: this.props.OpacityHitImage5
            },
            this.props.TeamHitMove.getLayout()
          ]}
        />
        <Animated.Image
          source={this.props.HitImage6}
          style={[
            {
              width: 85,
              height: 85,
              resizeMode: "stretch",
              position: "absolute",
              zIndex: 3,
              opacity: this.props.OpacityHitImage6
            },
            this.props.TeamHitMove.getLayout()
          ]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
