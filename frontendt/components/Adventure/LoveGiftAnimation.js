import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

export default class LoveGiftAnimation extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <Animated.View
          style={[
            {
              position: "absolute",
              width: 200,
              height: 200,
              zIndex: 3,
              borderRadius: 100,
              backgroundColor: `rgba(255,255,255,0.85)`,
              alignItems: "center",
              justifyContent: "center",
              opacity: this.props.LoveGiftOpacity
            },
            this.props.LoveGiftMove.getLayout()
          ]}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              marginTop: 25
            }}
          >
            <Text
              style={{
                color: "rgba(0,0,0,1)",
                fontSize: 18,
                textAlign: "center",
                fontWeight: "bold",
                marginBottom: -5
              }}
            >
              {this.props.LoveGift.LoveEnergyGift}
            </Text>
            <Image
              source={require("../../assets/images/love_energy.gif")}
              style={{
                height: 40,
                width: 40,
                marginRight: 8,
                marginLeft: 8
              }}
            />
            <Text
              style={{
                color: "rgba(0,0,0,1)",
                fontSize: 18,
                textAlign: "center",
                fontWeight: "bold",
                marginBottom: -5
              }}
            >
              gift!
            </Text>
          </View>
          <Image
            source={this.props.LoveImage}
            style={{
              resizeMode: "contain",
              height: 120,
              width: 120,
              margin: 10
            }}
          />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
