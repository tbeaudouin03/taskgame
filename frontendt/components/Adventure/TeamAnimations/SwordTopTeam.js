import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

export default class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }



  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <Animated.View
          style={{
            flex: 1,
            flexDirection: "row",
            minWidth: 50,
            position: "absolute",
            marginTop: -50,
            justifyContent: "center",
            alignSelf: "center",
            opacity: this.props.TeamSwordOpacity
          }}
        >
          <Image
            source={require("../../../assets/images/sword.png")}
            style={{
              height: 30,
              width: 30,
              marginLeft: -3,
              marginRight: 5,
              marginTop: -2.5
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              marginLeft: -2.5,
              height: 23,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {this.props.TeamFitness}
          </Text>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
