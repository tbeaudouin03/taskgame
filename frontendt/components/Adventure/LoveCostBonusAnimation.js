import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

export default class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <Animated.View
          style={[
            {
              flex: 1,
              position: "absolute",
              width: 80,
              height: 80,
              zIndex: 3,
              borderRadius: 50,
              backgroundColor: `rgba(255,255,255,0.5)`,
              alignItems: "center",
              justifyContent: "center",
              opacity: this.props.LoveCostBonusOpacity
            },
            this.props.LoveCostBonusMove.getLayout()
          ]}
        >
          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 18,
              textAlign: "left",
              fontWeight: "bold",
              marginBottom: -5
            }}
          >
            {this.props.LoveCostBonus}
          </Text>
          <Image
            source={require("../../assets/images/love_energy.gif")}
            style={{
              height: 40,
              width: 40
            }}
          />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
