import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

import Popover from "react-native-popover-view";

import Dialog, { DialogContent } from "react-native-popup-dialog";

import TypeWriter from "react-native-typewriter";

import ChooseCombatCompanion from "./HeroAnimations/ChooseCombatCompanion";

import LoveTopHero from "./HeroAnimations/LoveTopHero";
import CoinsTopHero from "./HeroAnimations/CoinsTopHero";
import FollowerTopHero from "./HeroAnimations/FollowerTopHero";

import ShieldTopHero from "./HeroAnimations/ShieldTopHero";
import SwordTopHero from "./HeroAnimations/SwordTopHero";
import BookTopHero from "./HeroAnimations/BookTopHero";

import InvisibleButtonHero from "./HeroAnimations/InvisibleButtonHero";
import HeroLifeBar from "./HeroAnimations/HeroLifeBar";

const help = require(`../HelperFunction`);

export default class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <View
        style={{
          flex: 1,
          width: "100%",
          height: "100%"
        }}
      >
        <InvisibleButtonHero
          HeroMove={this.props.HeroMove}
          HandleTapHero={this.props.HandleTapHero}
          SetIsChoosingCompanionVisible={
            this.props.SetIsChoosingCompanionVisible
          }
        />

        <Animated.View
          style={[
            {
              flex: 1,
              position: "absolute",
              alignItems: "center",
              alignSelf: "center",
              opacity: 1,
              zIndex: 2
            },
            this.props.HeroMove.getLayout()
          ]}
        >
          <LoveTopHero
            HeroData={this.props.HeroData}
            LoveGift={this.props.LoveGift}
            HeroLoveOpacity={this.props.HeroLoveOpacity}
          />

          <CoinsTopHero
            HeroData={this.props.HeroData}
            HeroCoinsOpacity={this.props.HeroCoinsOpacity}
          />

          <FollowerTopHero
            HeroData={this.props.HeroData}
            FightStatus={this.props.FightStatus}
            HeroFollowerOpacity={this.props.HeroFollowerOpacity}
          />

          <ShieldTopHero
            HeroData={this.props.HeroData}
            HeroShieldOpacity={this.props.HeroShieldOpacity}
          />

          <SwordTopHero
            HeroData={this.props.HeroData}
            HeroSwordOpacity={this.props.HeroSwordOpacity}
          />

          <BookTopHero
            HeroData={this.props.HeroData}
            HeroBookOpacity={this.props.HeroBookOpacity}
          />

          {this.RenderHeroLifeBar()}

          <Image
            ref="Hero"
            source={{
              uri: this.props.HeroData.URL
            }}
            style={[
              {
                width: 85,
                height: 85,
                resizeMode: "contain",
                opacity: 1,
                zIndex: 2
              }
            ]}
          />
        </Animated.View>
        <Popover
          isVisible={this.props.IsTutoVisible}
          fromView={this.refs.Hero}
          //placement={"bottom"}
          popoverStyle={{ width: "90%" }}
          onClose={async () => {
            count = this.state.count ? this.state.count + 1 : 1;
            this.setState({ count });
            if (count > 2) {
              await help.Wait(500);
              await this.props.SetIsTutoVisible(false);
              if (this.props.HeroData.TutorialLevel == 11) {
                await this.props.BlueArrowUpTutoAnimHero();
              } else {
                await this.props.BlueArrowUpTutoAnim();
              }
            }
          }}
        >
          <TypeWriter
            typing={1}
            fixed={true}
            onTypingEnd={async () => {
              await help.Wait(500);
              await this.props.SetIsTutoVisible(false);
              if (this.props.HeroData.TutorialLevel == 11) {
                await this.props.BlueArrowUpTutoAnimHero();
              } else {
                await this.props.BlueArrowUpTutoAnim();
              }
            }}
            style={{
              margin: 10,
              fontSize: 18,
              textAlign: "center",
              fontWeight: "bold",
              padding: 5
            }}
          >
            {this.props.TutorialTextShow}
          </TypeWriter>
        </Popover>
        <Dialog
          visible={!!this.props.IsChoosingCompanionVisible}
          onTouchOutside={async () => {
            this.props.SetIsChoosingCompanionVisible(false);
            await this.refs.ChooseCombatCompanion.PostHeroCombatParty(); // TutorialLevel logic in this function
            await help.Wait(800)
            this.props._onRefresh(false, false, false);
            
          }}
        >
          <DialogContent style={{ width: 300, height: 300 }}>
            <ChooseCombatCompanion
              ref="ChooseCombatCompanion"
              HeroData={this.props.HeroData}
              FightStatus={this.props.FightStatus}
              BlueArrowUpTutoAnim={this.props.BlueArrowUpTutoAnim}
              BlueArrowUpTutoDisappearHero={
                this.props.BlueArrowUpTutoDisappearHero
              }
              _onRefresh={this.props._onRefresh}
            />
          </DialogContent>
        </Dialog>
      </View>
    );
  }

  RenderHeroLifeBar() {
    if (this.props.IsOnQuest && !this.props.IsFriend) {
      return (
        <HeroLifeBar
          HeroLifeBarOpacity={this.props.HeroLifeBarOpacity}
          OpacityHeroDamage={this.props.OpacityHeroDamage}
          FightStatus={this.props.FightStatus}
          HeroData={this.props.HeroData}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
