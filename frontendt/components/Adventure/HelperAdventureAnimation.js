import uuid from "uuid";
import {
  Linking,
  Platform,
  AsyncStorage,
  Alert,
  Clipboard,
  Animated
} from "react-native";
import { ImagePicker, Permissions, Constants } from "expo";
import { Toast } from "native-base";
import * as firebase from "firebase";
const helpAdvAnim = require(`./HelperAdventureAnimation`);
const help = require(`../HelperFunction`);

exports.SendHeroLoveToMonster = async (
  HeroX3Position,
  HeroY3Position,
  MonsterXPosition,
  MonsterYPosition,
  LoveCostBonusMove,
  LoveCostBonusOpacity
) => {
  Animated.sequence([
    // make sure love is at the right start position
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      },
      duration: 1
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    // send love to Monster
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: MonsterXPosition,
        y: MonsterYPosition
      },
      duration: 1000
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.MakeAttackLoveCostAppear = async (
  LoveCostBonusMove,
  LoveCostBonusOpacity
) => {
  Animated.sequence([
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      },
      duration: 1
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 3000
    })
  ]).start();
};

exports.OneBamAttackOnMonster = async (
  MonsterXPosition,
  MonsterYPosition,
  OpacityHitImage,
  MonsterHitMove
) => {
  Animated.sequence([
    // fight Monster
    Animated.timing(MonsterHitMove, {
      toValue: {
        x: MonsterXPosition + 100 * (Math.random() - 0.5),
        y: MonsterYPosition + 100 * (Math.random() - 0.5)
      },
      duration: 1
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 1,
      duration: 1
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 1,
      duration: 400
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 0,
      duration: 1
    })
  ]).start();
};

exports.OneBamAttackOnHero = async (
  HeroX3Position,
  HeroY3Position,
  OpacityHitImage,
  HeroHitMove
) => {
  Animated.sequence([
    // fight Monster
    Animated.timing(HeroHitMove, {
      toValue: {
        x: HeroX3Position + 100 * (Math.random() - 0.5),
        y: HeroY3Position + 100 * (Math.random() - 0.5)
      },
      duration: 1
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 1,
      duration: 1
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 1,
      duration: 400
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 0,
      duration: 1
    })
  ]).start();
};

exports.OneBamAttackOnTeam = async (
  TeamXPosition,
  TeamYPosition,
  OpacityHitImage,
  TeamHitMove
) => {
  Animated.sequence([
    // fight Team
    Animated.timing(TeamHitMove, {
      toValue: {
        x: TeamXPosition + 100 * (Math.random() - 0.5),
        y: TeamYPosition + 100 * (Math.random() - 0.5)
      },
      duration: 1
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 1,
      duration: 1
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 1,
      duration: 400
    }),
    Animated.timing(OpacityHitImage, {
      toValue: 0,
      duration: 1
    })
  ]).start();
};

exports.SendHeroCoinsToMonster = async (
  CoinsCostBonusOpacity,
  CoinsCostBonusMove,
  HeroX3Position,
  HeroY3Position,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.sequence([
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      },
      duration: 1
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: MonsterXPosition,
        y: MonsterYPosition
      },
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.SendTeamCoinsToMonster = async (
  CoinsCostBonusOpacity,
  CoinsCostBonusMove,
  TeamXPosition,
  TeamYPosition,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.sequence([
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: TeamXPosition,
        y: TeamYPosition - 90
      },
      duration: 1
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: MonsterXPosition,
        y: MonsterYPosition
      },
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.SendMonsterCoinsToHero = async (
  CoinsCostBonusOpacity,
  CoinsCostBonusMove,
  HeroX3Position,
  HeroY3Position,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.sequence([
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: MonsterXPosition - 80,
        y: MonsterYPosition - 20
      },
      duration: 1
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: HeroX3Position,
        y: HeroY3Position
      },
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.SendMonsterLoveToHero = async (
  LoveCostBonusOpacity,
  LoveCostBonusMove,
  HeroX3Position,
  HeroY3Position,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.sequence([
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: MonsterXPosition - 80,
        y: MonsterYPosition - 20
      },
      duration: 1
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: HeroX3Position,
        y: HeroY3Position
      },
      duration: 1000
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.SendMonsterCoinsToTeam = async (
  CoinsCostBonusOpacity,
  CoinsCostBonusMove,
  TeamXPosition,
  TeamYPosition,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.sequence([
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: MonsterXPosition - 80,
        y: MonsterYPosition - 20
      },
      duration: 1
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: TeamXPosition,
        y: TeamYPosition
      },
      duration: 1000
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.SendMonsterLoveToTeam = async (
  LoveCostBonusOpacity,
  LoveCostBonusMove,
  TeamXPosition,
  TeamYPosition,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.sequence([
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: MonsterXPosition - 80,
        y: MonsterYPosition - 20
      },
      duration: 1
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 1000
    }),
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: TeamXPosition,
        y: TeamYPosition
      },
      duration: 1000
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0,
      duration: 1000
    })
  ]).start();
};

exports.MonsterMoveLoop = async (
  MonsterMove,
  MonsterXPosition,
  MonsterYPosition
) => {
  Animated.loop(
    Animated.sequence([
      Animated.spring(MonsterMove, {
        toValue: { x: MonsterXPosition, y: MonsterYPosition - 30 },
        speed: 0.0001,
        delay: 2000
      }),
      Animated.timing(MonsterMove, {
        toValue: {
          x: MonsterXPosition + 50 * (Math.random() - 0.5),
          y: MonsterYPosition + 50 * (Math.random() - 0.5)
        },
        duration: 5000
      }),
      Animated.timing(MonsterMove, {
        toValue: {
          x: MonsterXPosition + 50 * (Math.random() - 0.5),
          y: MonsterYPosition + 50 * (Math.random() - 0.5)
        },
        duration: 5000
      }),
      Animated.timing(MonsterMove, {
        toValue: {
          x: MonsterXPosition + 50 * (Math.random() - 0.5),
          y: MonsterYPosition + 50 * (Math.random() - 0.5)
        },
        duration: 5000
      }),
      Animated.timing(MonsterMove, {
        toValue: {
          x: MonsterXPosition,
          y: MonsterYPosition
        },
        duration: 5000
      })
    ])
  ).start();
};

exports.MakeHeroStatAppear = async (
  HeroLifeBarOpacity,
  HeroFollowerOpacity,
  HeroLoveOpacity,
  HeroCoinsOpacity,
  HeroShieldOpacity,
  HeroSwordOpacity,
  HeroBookOpacity
) => {
  // life bar disappear
  Animated.sequence([
    Animated.timing(HeroLifeBarOpacity, {
      toValue: 0,
      duration: 100
    }),
    // if needed, hero fight stat disappear
    Animated.timing(HeroBookOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroSwordOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroShieldOpacity, {
      toValue: 0,
      duration: 100
    }),
    // hero stat appear
    Animated.timing(HeroFollowerOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 1,
      duration: 100
    }),
    // remain visible
    Animated.timing(HeroLifeBarOpacity, {
      toValue: 0,
      duration: 5000
    }),
    // hero stat disapear
    Animated.timing(HeroCoinsOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroFollowerOpacity, {
      toValue: 0,
      duration: 100
    }),
    // life bar appear
    Animated.timing(HeroLifeBarOpacity, {
      toValue: 1,
      duration: 100
    })
  ]).start();
};

exports.MakeHeroFightStatAppear = async (
  HeroLifeBarOpacity,
  HeroFollowerOpacity,
  HeroLoveOpacity,
  HeroCoinsOpacity,
  HeroShieldOpacity,
  HeroSwordOpacity,
  HeroBookOpacity
) => {
  // life bar disappear
  Animated.sequence([
    Animated.timing(HeroLifeBarOpacity, {
      toValue: 0,
      duration: 100
    }),
    // if needed, hero stat disappear
    Animated.timing(HeroCoinsOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroFollowerOpacity, {
      toValue: 0,
      duration: 100
    }),
    // hero fight stat appear
    Animated.timing(HeroShieldOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(HeroSwordOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(HeroBookOpacity, {
      toValue: 1,
      duration: 100
    }),
    // remain visible
    Animated.timing(HeroBookOpacity, {
      toValue: 1,
      duration: 5000
    }),
    // hero fight stat disappear
    Animated.timing(HeroBookOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroSwordOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(HeroShieldOpacity, {
      toValue: 0,
      duration: 100
    }),
    // life bar appear
    Animated.timing(HeroLifeBarOpacity, {
      toValue: 1,
      duration: 100
    })
  ]).start();
};

exports.MakeMonsterFightStatAppear = async (
  MonsterShieldOpacity,
  MonsterSwordOpacity,
  MonsterBookOpacity
) => {
  Animated.sequence([
    // monster fight stat appear
    Animated.timing(MonsterShieldOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(MonsterSwordOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(MonsterBookOpacity, {
      toValue: 1,
      duration: 100
    }),
    // remain visible
    Animated.timing(MonsterBookOpacity, {
      toValue: 1,
      duration: 5000
    }),
    // monster fight stat disappear
    Animated.timing(MonsterBookOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(MonsterSwordOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(MonsterShieldOpacity, {
      toValue: 0,
      duration: 100
    })
  ]).start();
};

exports.MakeTeamAppear = async (
  OpacityTeammate1,
  OpacityTeammate2,
  OpacityTeammate3,
  TeamLifeBarOpacity,
  OpacityTopCompanionCount
) => {
  Animated.sequence([
    // monster fight stat appear
    Animated.parallel([
      Animated.timing(OpacityTeammate1, {
        toValue: 1,
        duration: 300
      }),
      Animated.timing(TeamLifeBarOpacity, {
        toValue: 1,
        duration: 300
      }),
      Animated.timing(OpacityTopCompanionCount, {
        toValue: 1,
        duration: 300
      })
    ]),
    Animated.timing(OpacityTeammate2, {
      toValue: 1,
      duration: 300 + Math.random() * 300
    }),
    Animated.timing(OpacityTeammate3, {
      toValue: 1,
      duration: 300 + Math.random() * 300
    })
  ]).start();
};

exports.MakeTeamFightStatAppear = async (
  TeamShieldOpacity,
  TeamSwordOpacity,
  TeamBookOpacity,
  TeamLifeBarOpacity,
  OpacityTopCompanionCount
) => {
  Animated.sequence([
    Animated.timing(OpacityTopCompanionCount, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(TeamLifeBarOpacity, {
      toValue: 0,
      duration: 100
    }),
    // Team fight stat appear
    Animated.timing(TeamShieldOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(TeamSwordOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(TeamBookOpacity, {
      toValue: 1,
      duration: 100
    }),
    // remain visible
    Animated.timing(TeamBookOpacity, {
      toValue: 1,
      duration: 5000
    }),
    // Team fight stat disappear
    Animated.timing(TeamBookOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(TeamSwordOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(TeamShieldOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(TeamLifeBarOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(OpacityTopCompanionCount, {
      toValue: 1,
      duration: 100
    })
  ]).start();
};

exports.NewDamageToHero = async OpacityHeroDamage => {
  Animated.sequence([
    Animated.timing(OpacityHeroDamage, {
      toValue: 1,
      duration: 1500
    }),
    Animated.timing(OpacityHeroDamage, {
      toValue: 0,
      duration: 6500
    })
  ]).start();
};

exports.NewDamageToMonster = async OpacityMonsterDamage => {
  Animated.sequence([
    Animated.timing(OpacityMonsterDamage, {
      toValue: 1,
      duration: 1500
    }),
    Animated.timing(OpacityMonsterDamage, {
      toValue: 0,
      duration: 6500
    })
  ]).start();
};

exports.NewDamageToTeam = async (
  OpacityTeamDamage,
  OpacityTopCompanionCount
) => {
  Animated.sequence([
    Animated.timing(OpacityTopCompanionCount, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(OpacityTeamDamage, {
      toValue: 1,
      duration: 1500
    }),
    Animated.timing(OpacityTeamDamage, {
      toValue: 0,
      duration: 6500
    }),
    Animated.timing(OpacityTopCompanionCount, {
      toValue: 1,
      duration: 100
    })
  ]).start();
};

exports.HeroMove = async (
  HeroMove,
  HeroX2Position,
  HeroY2Position,
  HeroX3Position,
  HeroY3Position,
  IsOnQuest
) => {
  if (IsOnQuest) {
    Animated.sequence([
      Animated.timing(HeroMove, {
        toValue: {
          x: HeroX2Position,
          y: HeroY2Position
        },
        duration: 3000
      }),
      Animated.timing(HeroMove, {
        toValue: {
          x: HeroX3Position,
          y: HeroY3Position
        },
        duration: 3000
      })
    ]).start();
  }
};

exports.NotEnoughLoveAnim = async (
  LoveCostBonusMove,
  LoveCostBonusOpacity,
  HeroLoveOpacity,
  HeroX3Position,
  HeroY3Position
) => {
  Animated.sequence([
    Animated.timing(LoveCostBonusMove, {
      toValue: {
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      },
      duration: 1
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 1,
      duration: 5000
    }),
    Animated.timing(HeroLoveOpacity, {
      toValue: 0,
      duration: 600
    })
  ]).start();
};

exports.NotEnoughCoinsAnim = async (
  LoveCostBonusOpacity,
  CoinsCostBonusMove,
  CoinsCostBonusOpacity,
  HeroCoinsOpacity,
  HeroX3Position,
  HeroY3Position
) => {
  Animated.sequence([
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(CoinsCostBonusMove, {
      toValue: {
        x: HeroX3Position + 90,
        y: HeroY3Position - 105
      },
      duration: 1
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 100
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 0.5,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 1,
      duration: 600
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 1,
      duration: 5000
    }),
    Animated.timing(HeroCoinsOpacity, {
      toValue: 0,
      duration: 600
    }),
    Animated.timing(CoinsCostBonusOpacity, {
      toValue: 0,
      duration: 600
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 100
    })
  ]).start();
};

exports.LoveGiftAnim = async (
  HeroX3Position,
  HeroY3Position,
  LoveGiftMove,
  LoveGiftOpacity,
  LoveCostBonusOpacity
) => {
  Animated.sequence([
    // make sure love is at the right start position
    Animated.timing(LoveGiftMove, {
      toValue: {
        x: HeroX3Position,
        y: 10
      },
      duration: 1
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 0,
      duration: 100
    }),
    Animated.timing(LoveGiftOpacity, {
      toValue: 1,
      duration: 300
    }),
    Animated.spring(LoveGiftMove, {
      toValue: { x: HeroX3Position, y: HeroY3Position },
      tension: 0.001,
      friction: 1
      //speed: 0.0001
    }),
    Animated.timing(LoveGiftOpacity, {
      toValue: 1,
      duration: 500
    }),
    Animated.timing(LoveGiftOpacity, {
      toValue: 0,
      duration: 2000
    }),
    Animated.timing(LoveCostBonusOpacity, {
      toValue: 1,
      duration: 100
    })
  ]).start();
};

exports.BlueArrowUpTutoAnim = async (
  MonsterXPosition,
  MonsterYPosition,
  BlueArrowUpTutoMove,
  OpacityBlueArrowUpTuto
) => {
  Animated.sequence([
    Animated.timing(OpacityBlueArrowUpTuto, {
      toValue: 1,
      duration: 100
    }),
    Animated.loop(
      Animated.sequence([
        Animated.timing(BlueArrowUpTutoMove, {
          toValue: { x: MonsterXPosition - 40, y: MonsterYPosition + 80 },
          duration: 800
        }),
        Animated.timing(BlueArrowUpTutoMove, {
          toValue: { x: MonsterXPosition - 40, y: MonsterYPosition + 110 },
          duration: 800
        })
      ])
    )
  ]).start();
};

exports.BlueArrowUpTutoDisappear = async OpacityBlueArrowUpTuto => {
  Animated.timing(OpacityBlueArrowUpTuto, {
    toValue: 0,
    duration: 100
  }).start();
};

exports.BlueArrowUpTutoAnimHero = async (
  HeroX3Position,
  HeroY3Position,
  BlueArrowUpTutoMove,
  OpacityBlueArrowUpTuto
) => {
  Animated.sequence([
    Animated.timing(OpacityBlueArrowUpTuto, {
      toValue: 1,
      duration: 100
    }),
    Animated.loop(
      Animated.sequence([
        Animated.timing(BlueArrowUpTutoMove, {
          toValue: { x: HeroX3Position - 20, y: HeroY3Position + 80 },
          duration: 800
        }),
        Animated.timing(BlueArrowUpTutoMove, {
          toValue: { x: HeroX3Position - 20, y: HeroY3Position + 110 },
          duration: 800
        })
      ])
    )
  ]).start();
};
