import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  AsyncStorage,
  Text,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity,
  Animated
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text as Txt
} from "native-base";

export default class Adventure extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, width: "100%", height: "100%" }}>
        <Animated.View
          style={[
            {
              position: "absolute",
              zIndex: 4,
              alignSelf: "center",
              width: 85,
              height: 10,
              marginTop: -10,
              opacity: this.props.HeroLifeBarOpacity
            }
          ]}
        >
          <Animated.View
            style={[
              {
                position: "absolute",
                zIndex: 4,
                alignSelf: "center",
                alignItems: "center",
                justifyContent: "center",
                minWidth: 30,
                height: 30,
                borderRadius: 20,
                marginTop: -40,
                backgroundColor: "rgba(210, 92, 83,1)",
                opacity: this.props.OpacityHeroDamage
              }
            ]}
          >
            <Text
              style={{
                margin: 7,
                fontWeight: "bold",
                color: "white",
                fontSize: 14
              }}
            >
              {-this.props.FightStatus.NewDamageToHero}
            </Text>
          </Animated.View>
          <View
            style={{
              position: "absolute",
              width: 85,
              height: 10,
              backgroundColor: "rgba(255,255,255,0.6)",
              borderRadius: 5,
              borderWidth: 1.5,
              borderColor: "rgba(0,0,0,0.8)"
            }}
          />
          <View
            style={{
              position: "absolute",
              width:
                (83.5 * this.props.FightStatus.HeroRemainingLife) /
                this.props.HeroData.Reputation,
              height: 10,
              borderWidth: 1.5,
              borderRightWidth: 0,
              borderRadius: 5,
              borderBottomLeftRadius: 5,
              borderTopLeftRadius: 5,
              backgroundColor: "rgba(50, 172, 35, 0.7)"
            }}
          />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: "rgba(0,0,0,0.4)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left"
  }
});

//https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Ft_rahk_enox%2Ft_rahk_enox.jpg?alt=media&token=9e2c7b02-15ef-44c7-99e9-515828af8b66
