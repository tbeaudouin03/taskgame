import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Header,
  Left,
  List,
  ListItem,
  Icon,
  Right,
  Switch,
  Title
} from "native-base";

import {
  ScrollView,
  View,
  StyleSheet,
  Image,
  Text as Txt,
  AsyncStorage,
  RefreshControl,
  Platform,
  FlatList
} from "react-native";

const help = require(`../../../components/HelperFunction`);

export default class ChooseCombatCompanion extends React.Component {
  static navigationOptions = {
    //header:null
    title: "Menu",
    headerTitleStyle: {
      textAlign: "center",
      flex: 1
    }
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      WelcomeTuto: this.props.WelcomeTuto,
      HeroTuto: this.props.HeroTuto,
      JudgeTuto: this.props.JudgeTuto,
      ShopTuto: this.props.ShopTuto,
      LeaderBoardTuto: this.props.LeaderBoardTuto,
      Test: [{ yap: "test" }, { yap: "test" }]
    };
  }

  SetAllIsFightCompanion = async bool => {
    let LeaderBoardDataHeroWithoutMe = this.state.LeaderBoardDataHeroWithoutMe,
      i;
    for (
      i = 0;
      i < LeaderBoardDataHeroWithoutMe.LeaderBoardObjects.length;
      i++
    ) {
      LeaderBoardDataHeroWithoutMe.LeaderBoardObjects[
        i
      ].IsFightCompanion = bool;
    }

    await this.setState({ LeaderBoardDataHeroWithoutMe });
  };

  SetIsFightCompanion = async (index, bool) => {
    let LeaderBoardDataHeroWithoutMe = this.state.LeaderBoardDataHeroWithoutMe;
    LeaderBoardDataHeroWithoutMe.LeaderBoardObjects[
      index
    ].IsFightCompanion = bool;

    await this.setState({ LeaderBoardDataHeroWithoutMe });
  };

  PostHeroCombatParty = async () => {
    let LeaderBoardDataHeroWithoutMe = this.state.LeaderBoardDataHeroWithoutMe,
      HeroCombatParty = [];

    LeaderBoardObjectsChosen = LeaderBoardDataHeroWithoutMe.LeaderBoardObjects.filter(
      LeaderBoardObject => LeaderBoardObject.IsFightCompanion == true
    );

    HeroCombatParty = HeroCombatParty.concat(
      LeaderBoardObjectsChosen.map(LeaderBoardObject => {
        // NB: key = IDHero
        return LeaderBoardObject.key;
      })
    );
    if (!HeroCombatParty) {
      HeroCombatParty = [];
    }
    if (this.props.HeroData.TutorialLevel == 11) {
      if (HeroCombatParty.length > 0) {
        this.props.BlueArrowUpTutoDisappearHero();
        this.props.BlueArrowUpTutoAnim();
        await help.PostTutorialLevel(this.props.HeroData.TutorialLevel + 1);
        await help.Wait(800);
        this.props._onRefresh(false, false, false);
      }
    } else {
      help.PostHeroCombatParty(HeroCombatParty);
    }
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    let LeaderBoardData = {
      LeaderBoardDataHeroWithoutMe: {
        LeaderBoardObjects: [
          {
            key: "aaaa",
            Reputation: 1,
            Intelligence: 1,
            Fitness: 1,
            GivenName: "Kickass",
            Coins: 100,
            IsBondedToTeam: true,
            IsBondedTeamLeader: true,
            IsFightCompanion: false
          }
        ]
      }
    };
    if (
      this.props.HeroData.TutorialLevel != 11 &&
      this.props.HeroData.TutorialLevel != 12
    ) {
      LeaderBoardData = await help.GetLeaderBoardData(
        this.props.HeroData.IDTeam,
        this.props.HeroData.IDHero
      );
    }

    this.setState({
      loading: false,
      LeaderBoardDataHeroWithoutMe: LeaderBoardData.LeaderBoardDataHeroWithoutMe
    });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <Container
        style={{ width: 300, height: 300, marginTop: 8, marginBottom: 3 }}
      >
        <Content style={{}}>
          <Text
            style={{
              alignSelf: "center",
              fontSize: 18,
              fontWeight: "bold",
              marginTop: 5,
              marginBottom: 15
            }}
          >
            Select Fight Companions
          </Text>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "flex-start",
              marginTop: 1,
              marginBottom: 18
            }}
          >
            <Button
              onPress={() => this.SetAllIsFightCompanion(true)}
              small
              info
              style={{ marginLeft: 20 }}
            >
              <Text> All</Text>
            </Button>
            <Button
              onPress={() => this.SetAllIsFightCompanion(false)}
              small
              info
              style={{ marginLeft: 8 }}
            >
              <Text> None</Text>
            </Button>
          </View>
          <FlatList
            style={{ marginTop: 0 }}
            data={this.state.LeaderBoardDataHeroWithoutMe.LeaderBoardObjects}
            extraData={this.state}
            renderItem={({ item, index }) => (
              <ListItem icon style={{ height: 100 }}>
                <Body>
                  <Text style={{ marginTop: -30 }}>{item.GivenName}</Text>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "space-between"
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        marginRight: 0
                      }}
                    >
                      <Image
                        source={require("../../../assets/images/shield.png")}
                        style={{
                          height: 25,
                          width: 25,
                          marginLeft: 0,
                          marginRight: 8,
                          marginTop: 5,
                          marginBottom: 5
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 13,
                          marginTop: 8
                        }}
                      >
                        {item.Reputation}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        marginRight: 5,
                        marginLeft: 10
                      }}
                    >
                      <Image
                        source={require("../../../assets/images/sword.png")}
                        style={{
                          height: 25,
                          width: 25,
                          marginLeft: 0,
                          marginRight: 6,
                          marginTop: 5,
                          marginBottom: 5
                        }}
                      />
                      <Text
                        style={{ fontSize: 13, marginTop: 8, marginLeft: -2 }}
                      >
                        {item.Fitness}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        marginLeft: -3
                      }}
                    >
                      <Image
                        source={require("../../../assets/images/book.png")}
                        style={{
                          height: 25,
                          width: 25,
                          marginLeft: 0,
                          marginRight: 8,
                          marginTop: 5,
                          marginBottom: 5
                        }}
                      />
                      <Text style={{ fontSize: 13, marginTop: 8 }}>
                        {item.Intelligence}
                      </Text>
                    </View>
                  </View>
                </Body>
                {this.RenderRightItem(item, index)}
              </ListItem>
            )}
          />
        </Content>
      </Container>
    );
  }

  RenderRightItem = (item, index) => {
    if (!item.IsBondedTeamLeader && !item.IsBondedToTeam) {
      return (
        <Right style={{ height: 72, flexDirection: "column" }}>
          <Text style={{ fontSize: 12 }}>Not</Text>
          <Image
            source={require("../../../assets/images/sheep.png")}
            style={{
              height: 23,
              width: 23,
              marginBottom: 12,
              marginLeft: 1.63
            }}
          />
        </Right>
      );
    }

    if (
      2 * this.props.FightStatus.CharacterFitness >
      item.Reputation + item.Coins
    ) {
      return (
        <Right style={{ height: 72, flexDirection: "column" }}>
          <Text style={{ fontSize: 12 }}>Need</Text>
          <Text style={{ fontSize: 12 }}>
            {2 * this.props.FightStatus.CharacterFitness - item.Reputation}
          </Text>
          <Image
            source={require("../../../assets/images/coin.png")}
            style={{
              height: 23,
              width: 23,
              marginBottom: 1,
              marginLeft: 1.63
            }}
          />
        </Right>
      );
    }

    return (
      <Right style={{ height: 62, marginTop: 10, marginRight: 10 }}>
        <Switch
          value={item.IsFightCompanion}
          onValueChange={async () =>
            this.SetIsFightCompanion(index, !item.IsFightCompanion)
          }
        />
      </Right>
    );
  };
}

const styles = StyleSheet.create({});
