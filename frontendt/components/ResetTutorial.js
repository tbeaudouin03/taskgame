import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Header,
  Left,
  List,
  ListItem,
  Icon,
  Right,
  Switch,
  Title
} from "native-base";

import {
  ScrollView,
  View,
  StyleSheet,
  Image,
  Text as Txt,
  AsyncStorage,
  RefreshControl,
  Platform
} from "react-native";

const config = require("../constants/config.json");
const help = require(`../components/HelperFunction`);

import Loader from "../components/Loader";

export default class Menu extends React.Component {
  static navigationOptions = {
    //header:null
    title: "Menu",
    headerTitleStyle: {
      textAlign: "center",
      flex: 1
    }
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      WelcomeTuto: this.props.WelcomeTuto,
      HeroTuto: this.props.HeroTuto,
      JudgeTuto: this.props.JudgeTuto,
      ShopTuto: this.props.ShopTuto,
      LeaderBoardTuto: this.props.LeaderBoardTuto
    };
  }

  PostTutorialLevel = async () => {
    await this.setState({ WelcomeTuto: !this.state.WelcomeTuto });
    await help.PostTutorialLevel(this.state.WelcomeTuto ? 1 : 3);
    await new Promise(res => setTimeout(res, 3000));
    this.props._signOutAsync();
  };

  PostHeroTutorialLevel = async () => {
    await this.setState({ HeroTuto: !this.state.HeroTuto });
    await help.PostHeroTutorialLevel(this.state.HeroTuto ? 0 : 1);
    await new Promise(res => setTimeout(res, 3000));
    this.props._signOutAsync();
  };

  PostJudgeTutorialLevel = async () => {
    await this.setState({ JudgeTuto: !this.state.JudgeTuto });
    await help.PostJudgeTutorialLevel(this.state.JudgeTuto ? 0 : 1);
    await new Promise(res => setTimeout(res, 3000));
    this.props._signOutAsync();
  };

  PostShopTutorialLevel = async () => {
    await this.setState({ ShopTuto: !this.state.ShopTuto });
    await help.PostShopTutorialLevel(this.state.ShopTuto ? 0 : 1);
    await new Promise(res => setTimeout(res, 3000));
    this.props._signOutAsync();
  };

  PostLeaderBoardTutorialLevel = async () => {
    await this.setState({ LeaderBoardTuto: !this.state.LeaderBoardTuto });
    await help.PostLeaderBoardTutorialLevel(this.state.LeaderBoardTuto ? 0 : 1);
    await new Promise(res => setTimeout(res, 3000));
    this.props._signOutAsync();
  };

  PostAllTutorialLevel = () => {
    this.PostTutorialLevel();
    this.PostHeroTutorialLevel();
    this.PostJudgeTutorialLevel();
    this.PostShopTutorialLevel();
    this.PostLeaderBoardTutorialLevel();
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    var AllTuto =
      this.state.WelcomeTuto &&
      this.state.HeroTuto &&
      this.state.JudgeTuto &&
      this.state.ShopTuto &&
      this.state.LeaderBoardTuto;

    if (this.state.loading) {
      return <View />;
    }

    return (
      <Container>
        <Content style={{ marginTop: 0 }}>
          <Button
            full
            info
            style={{ marginBottom: 15 }}
            onPress={this.props.SetResetTutorialFalse}
          >
            <Icon
              style={{ textAlign: "left" }}
              name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            />
            <Text style={{ marginRight: 50 }}>Back</Text>
          </Button>

          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>
            <Body>
              <Text>Reset All Tutorials</Text>
            </Body>
            <Right>
              <Switch
                value={AllTuto}
                onValueChange={this.PostAllTutorialLevel}
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>
            <Body>
              <Text>Reset Welcome Tutorial</Text>
            </Body>
            <Right>
              <Switch
                value={this.state.WelcomeTuto}
                onValueChange={this.PostTutorialLevel}
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>
            <Body>
              <Text>Reset Hero Tutorial</Text>
            </Body>
            <Right>
              <Switch
                value={this.state.HeroTuto}
                onValueChange={this.PostHeroTutorialLevel}
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>
            <Body>
              <Text>Reset Judge Tutorial</Text>
            </Body>
            <Right>
              <Switch
                value={this.state.JudgeTuto}
                onValueChange={this.PostJudgeTutorialLevel}
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>
            <Body>
              <Text>Reset Shop Tutorial</Text>
            </Body>
            <Right>
              <Switch
                value={this.state.ShopTuto}
                onValueChange={this.PostShopTutorialLevel}
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "rgba(142, 176, 211, 1)" }}>
                <Icon
                  active
                  name={Platform.OS === "ios" ? "ios-refresh" : "md-refresh"}
                />
              </Button>
            </Left>
            <Body>
              <Text>Reset LeaderBoard Tutorial</Text>
            </Body>
            <Right>
              <Switch
                value={this.state.LeaderBoardTuto}
                onValueChange={this.PostLeaderBoardTutorialLevel}
              />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
