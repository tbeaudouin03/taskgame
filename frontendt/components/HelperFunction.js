const help = require(`./HelperFunction`);
import uuid from "uuid";
import {
  Linking,
  Platform,
  AsyncStorage,
  Alert,
  Clipboard
} from "react-native";
import { ImagePicker, Permissions, Constants } from "expo";
import { Toast } from "native-base";
import * as firebase from "firebase";
const config = require("../constants/config.json");
const webClientId =
  "943300155553-m3497vbgo17jlkkibe42q2hm1devghuj.apps.googleusercontent.com";
androidStandaloneAppClientId =
  "943300155553-al0mt3ck5nuik3tg3s26ki67eavtujrp.apps.googleusercontent.com";
androidClientId =
  "943300155553-p9njl33o1svla53bmqo7cqabrsp932fe.apps.googleusercontent.com";
iosClientId =
  "943300155553-hllfc41pov0ngd1bt2g05hvn5ft15l7i.apps.googleusercontent.com";

// sign in ----------------------------------------------------------------------------------------------------------------------------------

exports.ShuffleArray = async array => {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
};

exports.signInWithGoogleAsync = async navigation => {
  try {
    const result = await Expo.Google.logInAsync({
      behavior: "web",
      webClientId: webClientId,
      androidStandaloneAppClientId: androidStandaloneAppClientId,
      androidClientId: androidClientId,
      iosClientId: iosClientId,
      scopes: [
        "profile",
        "email",
        //"https://www.googleapis.com/auth/calendar.events.readonly",
        "https://www.googleapis.com/auth/tasks.readonly"
      ]
    });

    if (result.type === "success") {
      userInfoResponse = await fetch(
        "https://www.googleapis.com/userinfo/v2/me",
        {
          headers: { Authorization: `Bearer ${result.accessToken}` }
        }
      );

      now = new Date();
      AsyncStorage.setItem("refreshTokensTime", now);
      await AsyncStorage.setItem("accessToken", result.accessToken);
      await AsyncStorage.setItem("refreshToken", result.refreshToken);
      await AsyncStorage.setItem("idToken", result.idToken);
      await AsyncStorage.setItem(
        "givenName",
        JSON.parse(userInfoResponse._bodyInit).given_name
      );
      await AsyncStorage.setItem(
        "familyName",
        JSON.parse(userInfoResponse._bodyInit).family_name
      );
      await AsyncStorage.setItem(
        "email",
        JSON.parse(userInfoResponse._bodyInit).email
      );

      navigation.navigate("App");
    } else {
      return { cancelled: true };
    }
  } catch (e) {
    return { error: true };
  }
};

refreshTokens = async navigation => {
  try {
    now = new Date();
    AsyncStorage.setItem("refreshTokensTime", now);

    RefreshToken = await AsyncStorage.getItem("refreshToken");

    var client_id;
    if (Platform.OS === "ios") {
      client_id = iosClientId;
    } else {
      if (Constants.appOwnership == "standalone") {
        client_id = androidStandaloneAppClientId;
      } else {
        client_id = androidClientId;
      }
    }

    refresh = await fetch(`https://www.googleapis.com/oauth2/v4/token`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        client_id: client_id,
        refresh_token: RefreshToken,
        grant_type: `refresh_token`
      })
    });
    await AsyncStorage.setItem(
      "accessToken",
      JSON.parse(refresh._bodyInit).access_token
    );
    await AsyncStorage.setItem(
      "iDToken",
      JSON.parse(refresh._bodyInit).id_token
    );
  } catch (e) {
    console.log(e);
    await help._signOutAsync(navigation);
  }
};

// ShopTuto = first spirit selection screen ---------------------------------------------------------------------------------------------------------------------
exports.GetShopTutoData = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");

  ShopData = await fetch(config.APIHandlerIP + `GetShopData`, {
    headers: {
      Authorization: `${IDToken}`
    }
  });

  // if you use `json: "whatever"' in golang struct then you have to do this below
  let ShopDataJSON = await JSON.parse(ShopData._bodyText);

  if (!!ShopDataJSON.ShopObjects) {
    // filter only for the appropriate ShopType
    ShopObjectsF = ShopDataJSON.ShopObjects.filter(
      ShopObject =>
        ShopObject.ShopType == "hero" &&
        (ShopObject.Title == "Al" ||
          ShopObject.Title == "Eric" ||
          ShopObject.Title == "Jess" ||
          ShopObject.Title == "Ada" ||
          ShopObject.Title == "Jimmy" ||
          ShopObject.Title == "Stephan")
    );
    ShopObjectsF = await putShopObjectTwoByTwo(ShopObjectsF);
    ShopDataF = { ShopObjects: [] };
    ShopDataF.ShopObjects = ShopObjectsF;

    return ShopDataF;
  }
};

exports.AlertOnBuyTuto = (
  index,
  ShopData,
  navigation,
  ItemNumber,
  SetIsShopItemDialog
) => {
  Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()];
  Alert.alert(
    "",
    `Are you sure you want to choose ` + Item.Title + `?`,
    [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      {
        text: "Choose",
        onPress: () =>
          help.OnBuyTutoPress(
            index,
            ShopData,
            navigation,
            ItemNumber,
            SetIsShopItemDialog
          )
      }
    ],
    { cancelable: false }
  );
};

exports.OnBuyTutoPress = async (
  index,
  ShopData,
  navigation,
  ItemNumber,
  SetIsShopItemDialog
) => {
  let HeroData = await help.RefreshHeroData(navigation);
  Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()];

  if (Item.IsUnique && Item.ObjectCount >= 1) {
  } else if (HeroData.Coins - Item.Price < 0) {
    Toast.show({
      text:
        "Press CHOOSE harder! " + Item.Title + " is reluctant to join you!...",
      buttonText: "Okay",
      position: "bottom",
      duration: 5000
    });
  } else {
    AsyncStorage.setItem("LoaderURL", Item.URL);
    await SetIsShopItemDialog(false);
    await help.PostTutorialLevel(2);
    await help.PostPurchasedShopObject(Item);
    await help.PostUsedShopObject(Item);

    navigation.navigate("App");
  }
};

// other ---------------------------------------------------------------------------------------------------------------------
exports._signOutAsync = async navigation => {
  await AsyncStorage.clear();
  navigation.navigate("Auth");
};

// send to right tuto level
exports.TutoNavHandler = async (HeroData, navigation) => {
  if (HeroData.TutorialLevel == 0) {
    await navigation.navigate("WelcomeTuto");
    return;
  } else if (HeroData.TutorialLevel == 1) {
    await navigation.navigate("ShopTuto");
    return;
  }
};

exports.GetTasksViaAPI = async (AccessToken, navigation) => {
  try {
    var i;
    var Tasks = [];
    today = new Date();
    today.setDate(today.getDate() - 90);
    minDateISO = today.toISOString();
    let taskListResponse = await fetch(
      "https://www.googleapis.com/tasks/v1/users/@me/lists",
      {
        headers: { Authorization: `Bearer ${AccessToken}` }
      }
    );

    if (!!taskListResponse) {
      var items0 = JSON.parse(taskListResponse._bodyInit).items;

      if (!items0) {
        await help._signOutAsync(navigation);
      }

      for (i = 0; i < items0.length; i++) {
        let taskResponse = await fetch(
          "https://www.googleapis.com/tasks/v1/lists/" +
            items0[i].id +
            "/tasks?showCompleted=true&showHidden=true&completedMin=" +
            minDateISO +
            "&updatedMin=" +
            minDateISO,
          {
            headers: {
              Authorization: `Bearer ${AccessToken}`
            }
          }
        );

        var items = JSON.parse(taskResponse._bodyInit).items;

        if (!!items) {
          Tasks = Tasks.concat(
            items.map(item => {
              return {
                ID: item.id,
                Title: item.title,
                Updated: item.updated,
                Status: item.status,
                Completed: item.completed
              };
            })
          );
        }
      }
    }
    return Tasks;
  } catch (e) {
    console.log(e);
    await help._signOutAsync(navigation);
  }
};

exports.PostTasks = async (Tasks, IDToken) => {
  fetch(config.APIHandlerIP + `PostTasks`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      Tasks: Tasks,
      IDToken: IDToken
    })
  });
};

exports.CalculateHeroStat = async IDToken => {
  fetch(config.APIHandlerIP + `CalculateHeroStat`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken
    })
  });
};

exports.RefreshHeroData = async navigation => {
  // Fetch access token
  var AccessToken, IDToken;
  var refreshTokensTime = await AsyncStorage.getItem("refreshTokensTime");
  refreshTokensTime = Date.parse(refreshTokensTime);
  today = new Date();
  minuteSinceRefreshAccess = (today - refreshTokensTime) / 60000;

  if (minuteSinceRefreshAccess > 45) {
    await this.refreshTokens(navigation);
  }

  AccessToken = await AsyncStorage.getItem("accessToken");
  IDToken = await AsyncStorage.getItem("idToken");

  if (!IDToken || !AccessToken) {
    await help._signOutAsync(navigation);
  }

  Tasks = await help.GetTasksViaAPI(AccessToken, navigation);

  if (!!Tasks) {
    await help.PostTasks(Tasks, IDToken);
    await help.CalculateHeroStat(IDToken);
  }

  HeroData = await fetch(config.APIHandlerIP + `GetHeroData`, {
    headers: {
      Authorization: `${IDToken}`
    }
  });

  let HeroDataJSON = await HeroData.json();

  return HeroDataJSON;
};

exports.RefreshHeroDataSimple = async navigation => {
  const IDToken = await AsyncStorage.getItem("idToken");
  const GivenName = await AsyncStorage.getItem("givenName");

  if (!IDToken) {
    await help._signOutAsync(navigation);
  }

  await help.CalculateHeroStat(IDToken);

  HeroData = await fetch(config.APIHandlerIP + `GetHeroData`, {
    headers: {
      Authorization: `${IDToken}`
    }
  });

  let HeroDataJSON = await HeroData.json();

  if (HeroDataJSON.Tasks == null) {
    HeroDataJSON.Tasks = [
      {
        IDTask1: "aaaa",
        IDTask2: "bbbb",
        Title: "You should at least COMPLETE one task in Google Tasks",
        Checked: true,
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0
      }
    ];
  }

  return HeroDataJSON;
};

exports.PostPurchasedShopObject = async ShopObject => {
  const IDToken = await AsyncStorage.getItem("idToken");
  IDHeroObject = uuid.v4();
  ShopObject.IDToken = IDToken;
  ShopObject.IDHeroObject = IDHeroObject;

  fetch(config.APIHandlerIP + `PostPurchasedShopObject`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(ShopObject)
  });
};

exports.PostUsedShopObject = async ShopObject => {
  const IDToken = await AsyncStorage.getItem("idToken");

  ShopObject.IDToken = IDToken;

  fetch(config.APIHandlerIP + `PostUsedShopObject`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(ShopObject)
  });
};

exports.PostTutorialLevel = async TutorialLevel => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostTutorialLevel`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      TutorialLevel: TutorialLevel
    })
  });
};

exports.GetAdvice = async (Screen, TaskCount) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  Advice = await fetch(config.APIHandlerIP + `GetAdvice`, {
    headers: {
      Authorization: `${IDToken}`,
      Screen: `${Screen}`,
      TaskCount: `${TaskCount}`
    }
  });

  let AdviceJSON = await Advice.json();

  return AdviceJSON;
};

exports.PostImageAndTaskCheck = async (URL, IDImage, IDToken, Task) => {
  fetch(config.APIHandlerIP + `PostImageAndTaskCheck`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDTask1: Task.IDTask1,
      IDTask2: Task.IDTask2,
      Title: Task.Title,
      Checked: true,
      URL: URL,
      IDImage: IDImage,
      IDToken: IDToken
    })
  });
};

exports.TakeAndStorePicture = async Task => {
  await Permissions.askAsync(Permissions.CAMERA);
  await Permissions.askAsync(Permissions.CAMERA_ROLL);
  let result = await ImagePicker.launchCameraAsync(
    (options = {
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.3
    })
  );
  //let result = await ImagePicker.launchImageLibraryAsync();
  if (!result.cancelled) {
    ok = this.uploadImage(result.uri, Task);
    return ok;
  }
};

uploadImage = async (uri, Task) => {
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      console.log(e);
      reject(new TypeError("Network request failed"));
    };
    xhr.responseType = "blob";
    xhr.open("GET", uri, true);
    xhr.send(null);
  });

  IDImage = uuid.v4();
  const ref = firebase
    .storage()
    .ref()
    .child(`verify/` + IDImage);
  const snapshot = await ref.put(blob);

  // We're done with the blob, close and release it
  blob.close();
  URL = await snapshot.ref.getDownloadURL();

  const IDToken = await AsyncStorage.getItem("idToken");

  try {
    await help.PostImageAndTaskCheck(URL, IDImage, IDToken, Task);
  } catch (err) {
    console.log(err);
    return false;
  }

  return true;
};

exports.OnGoogleTaskPress = async () => {
  let link =
    Platform.OS === "ios"
      ? `https://itunes.apple.com/fr/app/google-tasks/id1353634006?mt=8`
      : `https://play.google.com/store/apps/details?id=com.google.android.apps.tasks`;

  Linking.canOpenURL(link).then(
    supported => {
      supported && Linking.openURL(link);
    },
    err => console.log(err)
  );
};

exports.OnGmailPress = async email => {
  let link = Platform.OS === "ios" ? `mailto:${email}` : `mailto:${email}`;

  Linking.canOpenURL(link).then(
    supported => {
      supported && Linking.openURL(link);
    },
    err => console.log(err)
  );
};

exports.OnLinkPress = async link0 => {
  let link = Platform.OS === "ios" ? link0 : link0;

  Linking.canOpenURL(link).then(
    supported => {
      supported && Linking.openURL(link);
    },
    err => console.log(err)
  );
};

exports.PostJudgeTutorialLevel = async JudgeTutorialLevel => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostJudgeTutorialLevel`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      JudgeTutorialLevel: JudgeTutorialLevel
    })
  });
};

exports.PostJudgement = async (
  ImageToCheck,
  CoinValueChosen,
  SkillChosen,
  IDToken
) => {
  await fetch(config.APIHandlerIP + `PostJudgement`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      CoinValueChosen: CoinValueChosen,
      SkillChosen: SkillChosen,
      Title: ImageToCheck.Title,
      IDTaskCheck: ImageToCheck.IDTaskCheck,
      IDTask1: ImageToCheck.IDTask1,
      IDTask2: ImageToCheck.IDTask2,
      IDHero: ImageToCheck.IDHero,
      IDToken: IDToken
    })
  });
};

exports.GetImageToCheck = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");
  ImageToCheck = await fetch(config.APIHandlerIP + `GetImageToCheck`, {
    headers: {
      Authorization: `${IDToken}`
    }
  });

  let ImageToCheckJSON = await ImageToCheck.json();

  return ImageToCheckJSON;
};

exports.GetLeaderBoardData = async (IDTeam, IDHero) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  LeaderBoardData = await fetch(config.APIHandlerIP + `GetLeaderBoardData`, {
    headers: {
      Authorization: `${IDToken}`,
      IDTeam: `${IDTeam}`
    }
  });

  // if you use `json: "whatever"' in golang struct then you have to do this below
  let LeaderBoardDataJSON = await JSON.parse(LeaderBoardData._bodyText);

  var LeaderBoardDataHero = { LeaderBoardObjects: [] },
    LeaderBoardDataHeroBonded = { LeaderBoardObjects: [] },
    LeaderBoardDataHeroWithoutMe = { LeaderBoardObjects: [] },
    LeaderBoardDataJudge = { LeaderBoardObjects: [] };
  if (!!LeaderBoardDataJSON.LeaderBoardObjects) {
    // hero
    LeaderBoardObjectsHero = LeaderBoardDataJSON.LeaderBoardObjects.filter(
      LeaderBoardObject => LeaderBoardObject.LeaderBoardType == "hero"
    );
    try {
      LeaderBoardObjectsHero = await this.SetRank(LeaderBoardObjectsHero);
    } catch (e) {
      console.log(e);
    }

    LeaderBoardDataHero.LeaderBoardObjects = LeaderBoardObjectsHero;

    // hero without me
    LeaderBoardObjectsHeroWithoutMe = LeaderBoardDataJSON.LeaderBoardObjects.filter(
      LeaderBoardObject =>
        LeaderBoardObject.LeaderBoardType == "hero" &&
        LeaderBoardObject.key != IDHero
    );
    try {
      LeaderBoardObjectsHeroWithoutMe = await this.SetRank(
        LeaderBoardObjectsHeroWithoutMe
      );
    } catch (e) {
      console.log(e);
    }
    LeaderBoardDataHeroWithoutMe.LeaderBoardObjects = LeaderBoardObjectsHeroWithoutMe;

    // only team leader and team followers
    LeaderBoardObjectsHeroBonded = LeaderBoardDataJSON.LeaderBoardObjects.filter(
      LeaderBoardObject =>
        LeaderBoardObject.LeaderBoardType == "hero" &&
        LeaderBoardObject.key != IDHero &&
        (LeaderBoardObject.IsBondedTeamLeader == true ||
          LeaderBoardObject.IsBondedToTeam == true)
    );
    try {
      LeaderBoardObjectsHeroBonded = await this.SetRank(
        LeaderBoardObjectsHeroBonded
      );
    } catch (e) {
      console.log(e);
    }
    LeaderBoardDataHeroBonded.LeaderBoardObjects = LeaderBoardObjectsHeroBonded;

    // judge
    LeaderBoardObjectsJudge = LeaderBoardDataJSON.LeaderBoardObjects.filter(
      LeaderBoardObject => LeaderBoardObject.LeaderBoardType == "judge"
    );
    try {
      LeaderBoardObjectsJudge = await this.SetRank(LeaderBoardObjectsJudge);
    } catch (e) {
      console.log(e);
    }
    LeaderBoardDataJudge.LeaderBoardObjects = LeaderBoardObjectsJudge;
  }

  return {
    LeaderBoardDataHero: LeaderBoardDataHero,
    LeaderBoardDataJudge: LeaderBoardDataJudge,
    LeaderBoardDataHeroBonded: LeaderBoardDataHeroBonded,
    LeaderBoardDataHeroWithoutMe: LeaderBoardDataHeroWithoutMe
  };
};

SetRank = async LeaderBoardObjects => {
  var i,
    k = 1,
    l = 1;
  LeaderBoardObjects[0].Rank = k;
  LeaderBoardObjects[0].JudgeRank = l;
  for (i = 1; i < LeaderBoardObjects.length; i++) {
    if (LeaderBoardObjects[i].Score < LeaderBoardObjects[i - 1].Score) {
      k++;
    }
    if (
      LeaderBoardObjects[i].JudgeScore < LeaderBoardObjects[i - 1].JudgeScore
    ) {
      l++;
    }
    LeaderBoardObjects[i].Rank = k;
    LeaderBoardObjects[i].JudgeRank = l;
  }

  return LeaderBoardObjects;
};

exports.PostLeaderBoardTutorialLevel = async LeaderBoardTutorialLevel => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostLeaderBoardTutorialLevel`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      LeaderBoardTutorialLevel: LeaderBoardTutorialLevel
    })
  });
};

exports.GetShopData = async HeroData => {
  const IDToken = await AsyncStorage.getItem("idToken");

  ShopData = await fetch(config.APIHandlerIP + `GetShopData`, {
    headers: {
      Authorization: `${IDToken}`
    }
  });

  // if you use `json: "whatever"' in golang struct then you have to do this below
  let ShopDataJSON = await JSON.parse(ShopData._bodyText);

  if (!!ShopDataJSON.ShopObjects) {
    // random change LoaderURL
    randIndex = Math.round(Math.random() * ShopDataJSON.ShopObjects.length);
    ShopDataJSON.ShopObjects[randIndex]
      ? AsyncStorage.setItem(
          "LoaderURL",
          ShopDataJSON.ShopObjects[randIndex].URL
        )
      : console.log(`ShopObjects[randIndex] undefined`);

    var ShopDataSpirit = { ShopObjects: [] },
      ShopDataArtefact = { ShopObjects: [] },
      ShopDataMap = { ShopObjects: [] };

    // filter Spirit, Artefact & Map
    ShopObjectSpirit = ShopDataJSON.ShopObjects.filter(
      ShopObject => ShopObject.ShopType == "hero"
    );
    ShopObjectSpirit2 = await putShopObjectTwoByTwo(ShopObjectSpirit);
    ShopDataSpirit.ShopObjects = ShopObjectSpirit2;

    ShopObjectArtefact = ShopDataJSON.ShopObjects.filter(
      ShopObject => ShopObject.ShopType == "artefact"
    );
    ShopObjectArtefact = await this.filterShopObjectArtefact(
      ShopObjectArtefact,
      HeroData
    );
    ShopObjectArtefact2 = await putShopObjectTwoByTwo(ShopObjectArtefact);
    ShopDataArtefact.ShopObjects = ShopObjectArtefact2;

    ShopObjectMap = ShopDataJSON.ShopObjects.filter(
      ShopObject => ShopObject.ShopType == "map"
    );
    ShopObjectMap2 = await putShopObjectTwoByTwo(ShopObjectMap);
    ShopDataMap.ShopObjects = ShopObjectMap2;

    return {
      ShopDataArtefact: ShopDataArtefact,
      ShopDataSpirit: ShopDataSpirit,
      ShopDataMap: ShopDataMap,
      ShopObjectArtefact: ShopObjectArtefact,
      ShopObjectSpirit: ShopObjectSpirit,
      ShopObjectMap: ShopObjectMap
    };
  }
};

putShopObjectTwoByTwo = async ShopObjects => {
  let loopLength = Math.floor(ShopObjects.length / 2),
    extraLoop = ShopObjects.length % 2,
    i,
    ShopObjects2by2 = [];

  for (i = 0; i < 2 * loopLength; i = i + 2) {
    ShopObjects2by2.push({
      key: ShopObjects[i].key,
      Item1: ShopObjects[i],
      Item2: ShopObjects[i + 1]
    });
  }

  if (extraLoop == 1) {
    ShopObjects2by2.push({
      key: ShopObjects[ShopObjects.length - 1].key,
      Item1: ShopObjects[ShopObjects.length - 1],
      Item2: "NA"
    });
  }
  return ShopObjects2by2;
};

filterShopObjectArtefact = async (ShopObjectArtefact, HeroData) => {
  if (HeroData.TutorialLevel <= 17) {
    ShopObjectArtefact = ShopObjectArtefact.filter(
      ShopObject => ShopObject.key == "i934"
    );
    return ShopObjectArtefact;
  } else {
    return ShopObjectArtefact;
  }
};

exports.PostShopTutorialLevel = async ShopTutorialLevel => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostShopTutorialLevel`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      ShopTutorialLevel: ShopTutorialLevel
    })
  });
};

exports.AlertOnBuy = (
  index,
  ShopData,
  HeroData,
  SetHeroDataState,
  SetShopDataArtefactState,
  SetShopDataSpiritState,
  SetShopDataMapState,
  ItemNumber
) => {
  Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()];
  if (Item.IsUnique && Item.ObjectCount >= 1) {
    Toast.show({
      text: "You already bought " + Item.Title,
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else if (
    HeroData.Coins - Item.Price < 0 ||
    HeroData.LoveEnergy - Item.LovePrice < 0 ||
    HeroData.DarkEnergy - Item.DarkPrice < 0
  ) {
    Toast.show({
      text: "Not enough coins, love or dark energy",
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else {
    Alert.alert(
      "",
      `Are you sure you want to buy ` + Item.Title + `?`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Buy",
          onPress: () =>
            help.OnBuyPress(
              index,
              ShopData,
              HeroData,
              SetHeroDataState,
              SetShopDataArtefactState,
              SetShopDataSpiritState,
              SetShopDataMapState,
              ItemNumber
            )
        }
      ],
      { cancelable: false }
    );
  }
};

exports.OnBuyPress = async (
  index,
  ShopData,
  HeroData,
  SetHeroDataState,
  SetShopDataArtefactState,
  SetShopDataSpiritState,
  SetShopDataMapState,
  ItemNumber
) => {
  Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()];

  Item.ObjectCount = Item.ObjectCount + 1;
  HeroData.Coins = HeroData.Coins - Item.Price;
  HeroData.LoveEnergy = HeroData.LoveEnergy - Item.LovePrice;
  HeroData.DarkEnergy = HeroData.DarkEnergy - Item.DarkPrice;

  SetHeroDataState(HeroData);
  await help.PostPurchasedShopObject(Item);

  if (Item.ShopType == "hero") {
    ShopData.ShopObjects[index]["Item" + ItemNumber.toString()] = Item;
    SetShopDataSpiritState(ShopData);
    SetHeroDataState(HeroData);
    Toast.show({
      text: Item.Title + " joined your team!",
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else if (Item.ShopType == "artefact") {
    ShopData.ShopObjects[index]["Item" + ItemNumber.toString()] = Item;
    SetShopDataArtefactState(ShopData);
    SetHeroDataState(HeroData);
    Toast.show({
      text: "You now have " + Item.ObjectCount + " " + Item.Title,
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else if (Item.ShopType == "map") {
    ShopData.ShopObjects[index]["Item" + ItemNumber.toString()] = Item;
    SetShopDataMapState(ShopData);
    SetHeroDataState(HeroData);
    Toast.show({
      text: "You now have " + Item.Title,
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  }
};

exports.AlertOnUse = (
  index,
  ShopData,
  SetShopDataArtefactState,
  SetShopDataSpiritState,
  SetShopDataMapState,
  ItemNumber,
  navigation
) => {
  Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()];
  if (Item.ObjectCount < 1) {
    Toast.show({
      text: "You do not have " + Item.Title,
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else if (
    Item.IsUnique &&
    Item.ShopType != "hero" &&
    Item.ShopType != "map"
  ) {
    Toast.show({
      text: Item.Title + " is already active",
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else {
    Alert.alert(
      "",
      `Are you sure you want to use me?`,
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Yes",
          onPress: () =>
            help.OnUsePress(
              index,
              ShopData,
              SetShopDataArtefactState,
              SetShopDataSpiritState,
              SetShopDataMapState,
              ItemNumber,
              navigation
            )
        }
      ],
      { cancelable: false }
    );
  }
};

exports.OnUsePress = async (
  index,
  ShopData,
  SetShopDataArtefactState,
  SetShopDataSpiritState,
  SetShopDataMapState,
  ItemNumber,
  navigation
) => {
  Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()];
  if (Item.ShopType == "artefact") {
    Item.ObjectCount = Item.ObjectCount - 1;
    Item = ShopData.ShopObjects[index]["Item" + ItemNumber.toString()] = Item;
    SetShopDataArtefactState(ShopData);
    Toast.show({
      text: "You now have " + Item.ObjectCount + " " + Item.Title,
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else if (Item.ShopType == "hero") {
    Toast.show({
      text: Item.Title + " is your Avatar now",
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
  } else if (Item.ShopType == "map") {
    Toast.show({
      text: Item.Title + " is your Quest now",
      buttonText: "Okay",
      position: "bottom",
      //type:"warning",
      duration: 5000
    });
    navigation.navigate("Adventure");
  }

  await help.PostUsedShopObject(Item);
};

exports.PostHeroTutorialLevel = async HeroTutorialLevel => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostHeroTutorialLevel`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      HeroTutorialLevel: HeroTutorialLevel
    })
  });
};

exports.OnGoogleTaskPress = async () => {
  let link =
    Platform.OS === "ios"
      ? `https://itunes.apple.com/fr/app/google-tasks/id1353634006?mt=8`
      : `https://play.google.com/store/apps/details?id=com.google.android.apps.tasks`;

  Linking.canOpenURL(link).then(
    supported => {
      supported && Linking.openURL(link);
    },
    err => console.log(err)
  );
};

exports.PostMoodStatus = async (Mood, Status) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostMoodStatus`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      Mood: Mood,
      Status: Status
    })
  });
};

exports.PostTeamInfo = async (IDTeam, FacebookURL, LinkedinURL, Name) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostTeamInfo`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      IDTeam: IDTeam,
      FacebookURL: FacebookURL,
      LinkedinURL: LinkedinURL,
      Name: Name
    })
  });
};

exports.GetCostTeamMembership = async (IDTeam, Level) => {
  const IDToken = await AsyncStorage.getItem("idToken");
  CostTeamMembership = await fetch(
    config.APIHandlerIP + `GetCostTeamMembership`,
    {
      headers: {
        Authorization: `${IDToken}`,
        IDTeam: `${IDTeam}`,
        Level: `${Level}`
      }
    }
  );

  let CostTeamMembershipJSON = await CostTeamMembership.json();

  return CostTeamMembershipJSON;
};

exports.AlertOnJoinTeam = async (IDTeam, Level, LoveEnergy, URL) => {
  CostTeamMembership = await help.GetCostTeamMembership(IDTeam, Level);
  CostTeamMembership = CostTeamMembership.CostTeamMembership;
  let Title = "Joining Bonus",
    Body =
      `You will get a ` +
      -CostTeamMembership +
      ` Love Energy bonus.
Join the team?`;

  if (CostTeamMembership > 0) {
    Title = "Joining Cost";
    Body =
      `The cost for joining is ` +
      CostTeamMembership +
      ` Love Energy.
Are you sure you want to join this team?`;
  }

  if (CostTeamMembership == 0) {
    Title = "Illegal Team";
    Body = `Seems like this team does not exist or is your current team.
Did you paste the correct Team ID?`;
    Alert.alert(
      Title,
      Body,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  } else {
    Alert.alert(
      Title,
      Body,
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Yes",
          onPress: () => OnJoinTeamPress(IDTeam, URL, LoveEnergy, Level)
        }
      ],
      { cancelable: false }
    );
  }
};

OnJoinTeamPress = async (IDTeam, URL, LoveEnergy, Level) => {
  if (LoveEnergy - CostTeamMembership < 0) {
    Toast.show({
      text: "You need more Love Energy to join this team!",
      buttonText: "Okay",
      position: "bottom",
      duration: 5000
    });
  } else {
    const IDToken = await AsyncStorage.getItem("idToken");

    fetch(config.APIHandlerIP + `PostTeamMembershipRequest`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        IDToken: IDToken,
        IDTeam: IDTeam,
        URL: URL,
        Level: Level
      })
    });
    Toast.show({
      text: "Request sent. Wait for approval...",
      buttonText: "Okay",
      position: "bottom",
      duration: 5000
    });
  }
};

exports.GetTeamMembershipRequest = async IDTeam => {
  const IDToken = await AsyncStorage.getItem("idToken");
  TeamMembershipRequest = await fetch(
    config.APIHandlerIP + `GetTeamMembershipRequest`,
    {
      headers: {
        Authorization: `${IDToken}`,
        IDTeam: `${IDTeam}`
      }
    }
  );

  let TeamMembershipRequestJSON = await TeamMembershipRequest.json();

  return TeamMembershipRequestJSON;
};

exports.PostAcceptTeamMembership = async (IDHero, IDTeam, GivenName) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostAcceptTeamMembership`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      IDTeam: IDTeam,
      IDHero: IDHero
    })
  });

  Toast.show({
    text: GivenName + " joined your team!",
    buttonText: "Okay",
    position: "bottom",
    duration: 5000
  });
};

exports.PostTeamLeaderChoice = async (GivenName, IDHero, IDTeam) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostTeamLeaderChoice`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      IDHero: IDHero,
      IDTeam: IDTeam
    })
  });

  Toast.show({
    text: GivenName + " was elected team leader!",
    buttonText: "Okay",
    position: "bottom",
    duration: 5000
  });
};

exports.PostHelpTeamLeader = async (Help, IDTeam) => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostHelpTeamLeader`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      Help: Help,
      IDTeam: IDTeam
    })
  });

  Toast.show({
    text: Help
      ? "You will now follow team leader!"
      : "You will now follow your own path!",
    buttonText: "Okay",
    position: "bottom",
    duration: 5000
  });
};

exports.AlertTapMonster = async (
  FightStatus,
  HeroData,
  _onRefresh,
  SetVisibleTeamAttackState
) => {
  if (!FightStatus.IDShopObject) {
    Alert.alert(
      "Congratulations!",
      `You finished your quest.

You are back in TaskTown safe and sound.

To start a new quest, go to Shop > Quest then Buy and Use a new quest.`,
      [
        {
          text: "OK",
          onPress: () => console.log("OK"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  } else if (
    2 * FightStatus.CharacterFitness - HeroData.Reputation >
    HeroData.Coins
  ) {
    Alert.alert(
      "Not Enough Soul Coins",
      `Accomplish more tasks to earn soul coins and beat this monster! 

You need at least ` +
        (2 * FightStatus.CharacterFitness - HeroData.Reputation) +
        ` coins to engage fight with this monster.

Hurry up! Without these coins, your soul will soon get captured by the demons and you will be taken to hell for eternity!
`,
      [
        {
          text: "OK",
          onPress: () => console.log("OK"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  } else if (FightStatus.LovePrice > HeroData.LoveEnergy) {
    Alert.alert(
      "Not Enough Love",
      `Wait for a love gift from the Gods of motivation. 

You can see how long you should wait until next Love-up by tapping on the Heart icon.
In the meantime, try to collect more coins and gain strength by accomplishing your tasks and getting closer to your dreams!

NB: you should open the app and tap on the Heart icon to collect your love gift
`,
      [
        {
          text: "OK",
          onPress: () => console.log("OK"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  } else {
    Alert.alert(
      "Attack!",
      "Solo or Team attack?",
      [
        {
          text: "Solo Attack",
          onPress: () => this.OnSoloAttack(FightStatus, _onRefresh),
          style: "cancel"
        },
        {
          text: "Team Attack",
          onPress: () => SetVisibleTeamAttackState(true)
        }
      ],
      { cancelable: false }
    );
  }
};

OnSoloAttack = async (FightStatus, _onRefresh) => {
  Alert.alert(
    "All you need is love",
    "We need " +
      FightStatus.LovePrice +
      " love energies to attack this disheartening ennemy!",
    [
      {
        text: "Retreat...",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      {
        text: "Engage!",
        onPress: async () => {
          await help.PostIsFightStarted(FightStatus.IDShopObject);
          await help.Wait(500);
          FightStatus = await help.GetFightStatus(true);
          this.OnFight(FightStatus, _onRefresh);
        }
      }
    ],
    { cancelable: false }
  );
};

exports.OnTeamAttack = async (FightStatus, _onRefresh, ArrayOfIDHero) => {
  Alert.alert(
    "All you need is love",
    "We need " +
      FightStatus.LovePrice +
      " love energies to attack this heartless ennemy!",
    [
      {
        text: "Retreat...",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      {
        text: "Engage!",
        onPress: async () => {
          await help.PostIsFightStarted(FightStatus.IDShopObject);
          await help.Wait(500);
          FightStatus = await help.GetFightStatus(true, ArrayOfIDHero);
          this.OnFight(FightStatus, _onRefresh);
        }
      }
    ],
    { cancelable: false }
  );
};

OnFight = async (FightStatus, _onRefresh) => {
  var message,
    MonsterRemainingLife =
      FightStatus.MonsterRemainingLife <= 0
        ? 0
        : FightStatus.MonsterRemainingLife,
    HeroRemainingLife =
      FightStatus.HeroRemainingLife <= 0 ? 0 : FightStatus.HeroRemainingLife,
    CoinsStolen =
      FightStatus.HeroRemainingLife < 0 ? -FightStatus.HeroRemainingLife : 0;

  Alert.alert(
    "Bam! Bam! Ouch!",
    `Bards shall write songs about us! 
    
Meanwhile, here is a brief summary:

- Monster remaining life: ` +
      MonsterRemainingLife +
      `
- Your life: ` +
      HeroRemainingLife +
      `
- Monster stole ` +
      CoinsStolen +
      ` soul coins from you!
  `,
    [
      {
        text: "OK",
        onPress: () => _onRefresh(false)
      }
    ],
    { cancelable: false }
  );
};

exports.GetFightStatus = async IsFight => {
  const IDToken = await AsyncStorage.getItem("idToken");
  FightStatus = await fetch(config.APIHandlerIP + `GetFightStatus`, {
    headers: {
      Authorization: `${IDToken}`,
      IsFight: `${IsFight}`
    }
  });

  let FightStatusJSON = await FightStatus.json();

  return FightStatusJSON;
};

exports.Wait = async Time => {
  await new Promise(res => setTimeout(res, Time));
};

exports.PostMonsterIsIntroduced = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostMonsterIsIntroduced`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken
    })
  });
};

exports.PostIsFightAccepted = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostIsFightAccepted`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken
    })
  });
};

exports.PostIsFightStarted = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostIsFightStarted`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken
    })
  });
};

exports.PostMonsterIsDefeated = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostMonsterIsDefeated`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken
    })
  });
};

exports.PostHeroCombatParty = async HeroCombatParty => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostHeroCombatParty`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken,
      HeroCombatParty: HeroCombatParty
    })
  });
};

exports.GetTeamFitness = async FightStatusTeam => {
  if (FightStatusTeam) {
    let i;
    let TeamFitness = FightStatusTeam[0].HeroFitness;
    for (i = 1; i < FightStatusTeam.length; i++) {
      TeamFitness += FightStatusTeam[i].HeroFitness;
    }

    return TeamFitness;
  }
};

exports.GetTeamReputation = async FightStatusTeam => {
  if (FightStatusTeam) {
    let i;
    let TeamReputation = FightStatusTeam[0].HeroReputation;
    for (i = 1; i < FightStatusTeam.length; i++) {
      TeamReputation += FightStatusTeam[i].HeroReputation;
    }

    return TeamReputation;
  }
};

exports.GetTeamIntelligence = async FightStatusTeam => {
  if (FightStatusTeam) {
    let i;
    let TeamIntelligence = FightStatusTeam[0].HeroIntelligence;
    for (i = 1; i < FightStatusTeam.length; i++) {
      TeamIntelligence += FightStatusTeam[i].HeroIntelligence;
    }

    return TeamIntelligence;
  }
};

exports.GetWeakestCompanionLifeRatio = async FightStatusTeam => {
  if (FightStatusTeam) {
    let i;
    let WeakestCompanionRemainingLife = FightStatusTeam[0].HeroRemainingLife,
      WeakestCompanionReputation = FightStatusTeam[0].HeroReputation;
    for (i = 1; i < FightStatusTeam.length; i++) {
      if (
        FightStatusTeam[i].HeroRemainingLife < WeakestCompanionRemainingLife
      ) {
        WeakestCompanionRemainingLife = FightStatusTeam[i].HeroRemainingLife;
        WeakestCompanionReputation = FightStatusTeam[i].HeroReputation;
      }
    }

    return WeakestCompanionRemainingLife / WeakestCompanionReputation;
  }
};

exports.GetLoveGift = async LoveGift => {
  now = new Date();
  let LoveGiftRequestTime = await AsyncStorage.getItem("LoveGiftRequestTime");
  LoveGiftRequestTime = Date.parse(LoveGiftRequestTime);
  // only request gift if last request was more than 60 seconds ago to make sure backend can synchronize before user can get several gifts
  if (!LoveGiftRequestTime || now - LoveGiftRequestTime > 60000 || !LoveGift) {
    AsyncStorage.setItem("LoveGiftRequestTime", now);

    const IDToken = await AsyncStorage.getItem("idToken");
    LoveGift = await fetch(config.APIHandlerIP + `GetLoveGift`, {
      headers: {
        Authorization: `${IDToken}`
      }
    });

    let LoveGiftJSON = await LoveGift.json();

    return LoveGiftJSON;
  }
};

exports.PostInAppTask = async (
  Text,
  _onRefresh,
  HeroData,
  navigation,
  SetIsTuto
) => {
  if (!!Text) {
    const IDToken = await AsyncStorage.getItem("idToken");
    if (!IDToken) {
      await help._signOutAsync();
    }
    await fetch(config.APIHandlerIP + `PostInAppTask`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        IDToken: IDToken,
        Text: Text
      })
    });

    if (HeroData.TutorialLevel == 2) {
      await help.PostTutorialLevel(3);
      ShopData = await help.GetShopData(HeroData);
      ShopObjectMap = ShopData.ShopObjectMap;

      FirstMap = ShopObjectMap.filter(ShopObject => ShopObject.key == "i101");

      await help.PostPurchasedShopObject(FirstMap[0]);
      await help.Wait(800);
      await help.PostUsedShopObject(FirstMap[0]);
      await help.Wait(800);
      navigation.navigate("Adventure");
      SetIsTuto(false);
    }

    await _onRefresh();
    _onRefresh();
  }
};

exports.PostChooseRandomTeam = async () => {
  const IDToken = await AsyncStorage.getItem("idToken");

  fetch(config.APIHandlerIP + `PostChooseRandomTeam`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      IDToken: IDToken
    })
  });
};
