import React from "react";
import {
  Button,
  Text as Txt,
  Content,
  Card,
  CardItem,
  Body,
  Toast,
  Container
} from "native-base";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Text,
  Image,
  RefreshControl,
  Alert
} from "react-native";
import Loader from "../../components/Loader";
import ShopFlatItem from "./ShopFlatItem";

const help = require(`../HelperFunction`);

export default class Shop extends React.Component {
  static navigationOptions = {
    title: "Choose your first spirit!"
    // header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      ShopType: "hero",
      PressStatus: [],
      ShopData: {
        ShopObjects: [
          {
            key: "aaa",
            Title: "Please wait or drag down to refresh...",
            URL: "aaaa",
            Description: "",
            Price: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0,
            IsUnique: true,
            ObjectCount: 0,
            ShopType: "hero"
          }
        ]
      },
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],
        Tasks: [
          {
            Title: "Please wait...",
            Checked: false,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0
      }
    };
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true });

    let HeroData = await help.RefreshHeroData(this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    ShopData = await help.GetShopTutoData(this.state.ShopType);

    this.setState({ refreshing: false, ShopData: ShopData });
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    ShopData = await help.GetShopTutoData();
    this.setState({ ShopData: ShopData });
    let HeroData = await help.RefreshHeroData(this.props.navigation);
    if (!!HeroData.Coins || !!HeroData.Intelligence || !!HeroData.Fitness) {
      this.setState({ HeroData });
    }

    const GivenName = await AsyncStorage.getItem("givenName");

    this.setState({ loading: false, GivenName: GivenName });
  }

  /**/

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    return (
      <Container>
        <Image
          blurRadius={100}
          source={require("../../assets/images/map_background.jpg")}
          style={{
            width: "100%",
            height: "100%",
            resizeMode: "stretch",
            opacity: 1,
            position: "absolute",
            zIndex: 0
          }}
        />
        <View
          style={{
            flex: 1,
            alignSelf: "center"
          }}
        >
          <View
            style={{
              position: "absolute",
              top: 0,
              alignSelf: "center",
              zIndex: 2,
              height: "100%",
              width: "100%"
            }}
          >
            <FlatList
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
              style={{ flex: 1, backgroundColor: "transparent" }}
              contentContainerStyle={{
                justifyContent: "flex-start",
                backgroundColor: "transparent",
                marginTop: 20
              }}
              data={this.state.ShopData.ShopObjects}
              renderItem={({ item, index }) => (
                <ShopFlatItem
                  item={item}
                  IsShopTuto={true}
                  index={index}
                  ShopData={this.state.ShopData}
                  HeroData={this.state.HeroData}
                  navigation={this.props.navigation}
                />
              )}
            />
          </View>
          <Image
            source={require("../../assets/images/forest_of_dreams.gif")}
            style={{
              width: 1736,
              height: 894,
              resizeMode: "stretch",
              //borderRadius: 30,
              //marginTop: -1,
              opacity: 0.85,
              alignSelf: "center",
              zIndex: 1
            }}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
