import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import ShopFlatSingleton from "./ShopFlatSingleton";

export default class ShopFlatItem extends React.PureComponent {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      ShopType: "hero"
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <View style={{ flex: 1, flexDirection: "row", justifyContent: "center" }}>
        <ShopFlatSingleton
          item={this.props.item.Item1}
          ItemNumber={1}
          index={this.props.index}
          IsShopTuto={this.props.IsShopTuto}
          ShopData={this.props.ShopData}
          HeroData={this.props.HeroData}
          SetHeroDataState={this.props.SetHeroDataState}
          SetShopDataArtefactState={this.props.SetShopDataArtefactState}
          SetShopDataSpiritState={this.props.SetShopDataSpiritState}
          SetShopDataMapState={this.props.SetShopDataMapState}
          navigation={this.props.navigation}
        />
        {this.RenderItem2()}
      </View>
    );
  }
  RenderItem2() {
    if (this.props.item.Item2 != "NA") {
      return (
        <ShopFlatSingleton
          item={this.props.item.Item2}
          ItemNumber={2}
          index={this.props.index}
          IsShopTuto={this.props.IsShopTuto}
          ShopData={this.props.ShopData}
          HeroData={this.props.HeroData}
          SetHeroDataState={this.props.SetHeroDataState}
          SetShopDataArtefactState={this.props.SetShopDataArtefactState}
          SetShopDataSpiritState={this.props.SetShopDataSpiritState}
          SetShopDataMapState={this.props.SetShopDataMapState}
          navigation={this.props.navigation}
        />
      );
    }
  }
}

const styles = StyleSheet.create({});
