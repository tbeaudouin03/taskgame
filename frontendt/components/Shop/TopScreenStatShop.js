import React from "react";
import {
  StyleSheet,
  View,
  AsyncStorage,
  Image,
  RefreshControl,
  Linking,
  Platform,
  TouchableOpacity
} from "react-native";
import uuid from "uuid";
import {
  Container,
  Content,
  Header,
  Body,
  Card,
  CardItem,
  CheckBox,
  Fab,
  Icon,
  Button,
  Text,
  Segment
} from "native-base";
import Popover from "react-native-popover-view";

const help = require(`../HelperFunction`);

export default class Hero extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {};
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      loading: true,
      refreshing: false,
      screen: "hero",
      GivenName: "",
      heroGreeting: "Hello",
      heroPunctuation: "!",
      heroMessage: "Verify your Tasks and get more coins!",
      HeroData: {
        ServerLoggedOut: "false",

        ChartData: [
          {
            data: [
              { x: "W0 #Tasks", y: 9 },
              { x: "W1 #Tasks", y: 7 },
              { x: "W2 #Tasks", y: 6 },
              { x: "W3 #Tasks", y: 1 },
              { x: "W4 #Tasks", y: 0 }
            ],
            color: "#4285F4"
          }
        ],

        Tasks: [
          {
            IDTask1: "aaaa",
            IDTask2: "bbbb",
            Title: "Please wait or drag down to refresh...",
            Checked: true,
            Coins: 0,
            Reputation: 0,
            Intelligence: 0,
            Fitness: 0
          }
        ],
        Coins: 0,
        Reputation: 0,
        Intelligence: 0,
        Fitness: 0,
        Reliability: 0,
        JudgementCount: 0,
        TaskCount: 0,
        TaskVerifyCount: 0,
        Level: 0,
        JudgeLevel: 0,
        URL: `aaaa`
      }
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    return (
      <Content
        style={{ backgroundColor: "transparent" }}
        contentContainerStyle={{
          alignItems: "center",
          backgroundColor: "transparent",
          justifyContent: "center"
        }}
      >
        <Card
          style={{
            width: "91%",
            marginBottom: 5,
            backgroundColor: "white",
            justifyContent: "center"
          }}
        >
          <CardItem
            style={{
              backgroundColor: "transparent",
              justifyContent: "center"
            }}
          >
            <Body
              style={{
                flex: 1,
                backgroundColor: "#fff",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-around",
                  marginTop: 2,
                  marginBottom: -2
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/coin.gif")}
                    style={{
                      height: 35,
                      width: 35,
                      marginTop: -12.5
                    }}
                  />
                  <Text
                    style={{
                      color: "rgba(0,0,0,0.4)",
                      fontSize: 16,
                      textAlign: "left",
                      marginTop: -5
                    }}
                  >
                    {" "}
                    {this.props.HeroData.Coins}{" "}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/love_energy.png")}
                    style={{
                      height: 30,
                      width: 30,
                      marginTop: -10,
                      marginRight: 8.5
                    }}
                  />
                  <Text
                    style={{
                      color: "rgba(0,0,0,0.4)",
                      fontSize: 16,
                      textAlign: "left",
                      marginTop: -5
                    }}
                  >
                    {this.props.HeroData.LoveEnergy}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/dark_energy.png")}
                    style={{
                      height: 45,
                      width: 45,
                      marginTop: -16.5,
                      marginRight: 4
                    }}
                  />
                  <Text
                    style={{
                      color: "rgba(0,0,0,0.4)",
                      fontSize: 16,
                      textAlign: "left",
                      marginTop: -5
                    }}
                  >
                    {this.props.HeroData.DarkEnergy}
                  </Text>
                </View>
              </View>

              <View style={{ height: 35, alignSelf: "center" }}>
                <Segment
                  style={{
                    backgroundColor: "transparent",
                    marginLeft: 10,
                    marginRight: -5
                  }}
                >
                  <Button
                    first
                    small
                    style={
                      this.props.ShopType == "hero"
                        ? styles.segmentP
                        : styles.segment
                    }
                    onPress={() => this.props.OnSegmentPress("hero")}
                  >
                    <Text
                      style={
                        this.props.ShopType == "hero"
                          ? styles.segmentTextP
                          : styles.segmentText
                      }
                    >
                      Spirit
                    </Text>
                  </Button>

                  <Button
                    small
                    style={
                      this.props.ShopType == "artefact"
                        ? styles.segmentP
                        : styles.segment
                    }
                    onPress={() => this.props.OnSegmentPress("artefact")}
                  >
                    <Text
                      style={
                        this.props.ShopType == "artefact"
                          ? styles.segmentTextP
                          : styles.segmentText
                      }
                    >
                      Artefact
                    </Text>
                  </Button>
                  <Button
                    last
                    small
                    style={
                      this.props.ShopType == "map"
                        ? styles.segmentP
                        : styles.segment
                    }
                    onPress={() => this.props.OnSegmentPress("map")}
                  >
                    <Text
                      style={
                        this.props.ShopType == "map"
                          ? styles.segmentTextP
                          : styles.segmentText
                      }
                    >
                      Quest
                    </Text>
                  </Button>
                </Segment>
              </View>
            </Body>
          </CardItem>
        </Card>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  segment: {
    height: "60%",
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentText: {
    color: "rgba(0,0,0,0.4)"
  },
  segmentP: {
    height: "60%",
    backgroundColor: "rgba(142, 176, 211, 0.6)",
    borderWidth: 1,
    borderColor: "rgba(142, 176, 211, 0.6)",
    marginBottom: 10
  },
  segmentTextP: {
    color: "white"
  }
});
