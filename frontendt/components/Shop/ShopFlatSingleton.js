import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast
} from "native-base";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import ShopItemDialog from "./ShopItemDialog";

const help = require(`../../components/HelperFunction`);

export default class ShopFlatSingleton extends React.PureComponent {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  SetIsShopItemDialog = async IsShopItemDialog => {
    this.setState({ IsShopItemDialog });
  };

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  BackgroundColor = () => {
    let BackgroundColor = "rgba(255,255,255,0.35)";
    if (!this.props.IsShopTuto) {
      if (this.props.item.ObjectCount > 0) {
        BackgroundColor = "rgba(142, 176, 211, 0.3)";
      } else if (
        this.props.HeroData.Coins - this.props.item.Price >= 0 &&
        this.props.HeroData.LoveEnergy - this.props.item.LovePrice >= 0 &&
        this.props.HeroData.DarkEnergy - this.props.item.DarkPrice >= 0
      ) {
        BackgroundColor = "rgba(100, 219, 162, 0.3)";
      }
    }

    return BackgroundColor;
  };

  OnLongPress = () => {
    if (this.props.IsShopTuto) {
      help.AlertOnBuyTuto(
        this.props.index,
        this.props.ShopData,
        this.props.navigation,
        this.props.ItemNumber,
        this.SetIsShopItemDialog.bind(this)
      );
    } else if (this.props.item.ObjectCount > 0) {
      help.AlertOnUse(
        this.props.index,
        this.props.ShopData,
        this.props.SetShopDataArtefactState,
        this.props.SetShopDataSpiritState,
        this.props.SetShopDataMapState,
        this.props.ItemNumber,
        this.props.navigation
      );
    } else if (
      this.props.HeroData.Coins - this.props.item.Price >= 0 &&
      this.props.HeroData.LoveEnergy - this.props.item.LovePrice >= 0 &&
      this.props.HeroData.DarkEnergy - this.props.item.DarkPrice >= 0
    ) {
      help.AlertOnBuy(
        this.props.index,
        this.props.ShopData,
        this.props.HeroData,
        this.props.SetHeroDataState,
        this.props.SetShopDataArtefactState,
        this.props.SetShopDataSpiritState,
        this.props.SetShopDataMapState,
        this.props.ItemNumber
      );
    }
  };

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <View>
        <TouchableOpacity
          onPress={() => this.SetIsShopItemDialog(true)}
          onLongPress={this.OnLongPress}
          style={{
            backgroundColor: this.BackgroundColor(),
            borderRadius: 35,
            width: 150,
            height: 150,
            marginTop: 0,
            margin: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Image
            source={{ uri: this.props.item.URL }}
            style={{
              width: 120,
              height: 120,
              resizeMode: "contain",
              margin: 10
            }}
          />
        </TouchableOpacity>
        <ShopItemDialog
          SetIsShopItemDialog={this.SetIsShopItemDialog.bind(this)}
          IsShopItemDialog={this.state.IsShopItemDialog}
          item={this.props.item}
          ItemNumber={this.props.ItemNumber}
          index={this.props.index}
          IsShopTuto={this.props.IsShopTuto}
          ShopData={this.props.ShopData}
          HeroData={this.props.HeroData}
          SetHeroDataState={this.props.SetHeroDataState}
          SetShopDataArtefactState={this.props.SetShopDataArtefactState}
          SetShopDataSpiritState={this.props.SetShopDataSpiritState}
          SetShopDataMapState={this.props.SetShopDataMapState}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
