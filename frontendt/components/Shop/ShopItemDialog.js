import React from "react";
import {
  Button,
  Body,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  CheckBox,
  Badge,
  Segment,
  Toast,
  Form,
  Item,
  Input,
  Label
} from "native-base";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import uuid from "uuid";
import {
  View,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from "react-native";

import Dialog, { DialogContent } from "react-native-popup-dialog";

import TypeWriter from "react-native-typewriter";

const help = require(`../../components/HelperFunction`);

export default class ShopItemDialog extends React.Component {
  static navigationOptions = {
    //title: "Coming soon...",
    header: null
  };

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <View />;
    }
    item = this.props.item;
    return (
      <Dialog
        visible={!!this.props.IsShopItemDialog}
        onTouchOutside={() => this.props.SetIsShopItemDialog(false)}
        dialogStyle={{
          backgroundColor: "rgba(142, 176, 211, 1)",
          borderRadius: 30,
          borderColor: "rgba(255, 255, 255, 0.7)",
          borderWidth: 2
        }}
      >
        <DialogContent style={{ height: 500, width: "85%" }}>
          <Content
            style={{}}
            contentContainerStyle={{
              alignItems: "center"
            }}
          >
            <Text
              style={{
                marginTop: 18,
                marginBottom: 10,
                color: "rgba(0,0,0,0.7)",
                fontSize: 24,
                fontWeight: "bold",
                textAlign: "center"
              }}
            >
              {item.Title}
            </Text>
            {this.RenderSkill()}
            <Image
              source={{ uri: item.URL }}
              style={{
                width: 120,
                height: 120,
                resizeMode: "contain",
                margin: 10
              }}
            />

            <TypeWriter
              typing={1}
              style={{
                margin: 15,
                color: "rgba(0,0,0,0.7)",
                padding: 10,
                borderRadius: 20,
                fontSize: 16,
                fontWeight: "normal",
                textAlign: "center",
                backgroundColor: "rgba(255,255,255,0.3)"
              }}
            >
              {item.Description}
            </TypeWriter>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                width: "100%",
                alignItems: "center",
                justifyContent: "space-around"
              }}
            >
              {this.RenderPrice()}
              {this.RenderLovePrice()}
              {this.RenderDarkPrice()}
            </View>
            {this.RenderButton()}
          </Content>
        </DialogContent>
      </Dialog>
    );
  }

  RenderButton() {
    if (this.props.IsShopTuto) {
      return (
        <Button
          small
          info
          style={{ margin: 20, marginTop: 10, alignSelf: "center" }}
          onPress={() =>
            help.AlertOnBuyTuto(
              this.props.index,
              this.props.ShopData,
              this.props.navigation,
              this.props.ItemNumber,
              this.props.SetIsShopItemDialog
            )
          }
        >
          <Text>Choose</Text>
        </Button>
      );
    }
    let UseButtonText = "Use";
    if (item.IsUnique && item.ObjectCount >= 1) {
      if (item.ShopType == "hero") {
        UseButtonText = "Choose Avatar";
      } else if (item.ShopType == "map") {
        UseButtonText = "Start";
      }
      return (
        <Button
          small
          info
          style={{ margin: 10, alignSelf: "center" }}
          onPress={() =>
            help.AlertOnUse(
              this.props.index,
              this.props.ShopData,
              this.props.SetShopDataArtefactState,
              this.props.SetShopDataSpiritState,
              this.props.SetShopDataMapState,
              this.props.ItemNumber,
              this.props.navigation
            )
          }
        >
          <Text />
        </Button>
      );
    }
    if (item.ObjectCount == 0) {
      return (
        <Button
          small
          primary
          style={{ margin: 10, alignSelf: "center" }}
          onPress={() =>
            help.AlertOnBuy(
              this.props.index,
              this.props.ShopData,
              this.props.HeroData,
              this.props.SetHeroDataState,
              this.props.SetShopDataArtefactState,
              this.props.SetShopDataSpiritState,
              this.props.SetShopDataMapState,
              this.props.ItemNumber
            )
          }
        >
          <Text>Buy</Text>
        </Button>
      );
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Button
          small
          primary
          style={{ margin: 10 }}
          onPress={() =>
            help.AlertOnBuy(
              this.props.index,
              this.props.ShopData,
              this.props.HeroData,
              this.props.SetHeroDataState,
              this.props.SetShopDataArtefactState,
              this.props.SetShopDataSpiritState,
              this.props.SetShopDataMapState,
              this.props.ItemNumber
            )
          }
        >
          <Text>Buy</Text>
        </Button>

        <Button
          small
          info
          style={{ margin: 10 }}
          onPress={() =>
            help.AlertOnUse(
              this.props.index,
              this.props.ShopData,
              this.props.SetShopDataArtefactState,
              this.props.SetShopDataSpiritState,
              this.props.SetShopDataMapState,
              this.props.ItemNumber
            )
          }
        >
          <Text>Use</Text>
        </Button>
      </View>
    );
  }

  RenderSkill() {
    if (item.Reputation > 0 || item.Intelligence > 0 || item.Fitness > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            width: "100%",
            marginTop: 10,
            alignItems: "center",
            justifyContent: "space-around"
          }}
        >
          {this.RenderIntelligence()}
          {this.RenderReputation()}
          {this.RenderFitness()}
        </View>
      );
    }
  }

  RenderReputation() {
    if (item.Reputation > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../assets/images/shield.png")}
            style={{
              height: 25,
              width: 25,
              margin: 10,
              marginTop: 0
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              height: 23,
              marginRight: 20,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {item.Reputation}
          </Text>
        </View>
      );
    }
  }

  RenderIntelligence() {
    if (item.Intelligence > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../assets/images/book.png")}
            style={{
              height: 25,
              width: 25,
              margin: 10,
              marginTop: 0
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              height: 23,
              marginRight: 20,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {item.Intelligence}
          </Text>
        </View>
      );
    }
  }

  RenderFitness() {
    if (item.Fitness > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../assets/images/sword.png")}
            style={{
              height: 25,
              width: 25,
              margin: 10,
              marginTop: 0
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              height: 23,
              marginRight: 20,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {item.Fitness}
          </Text>
        </View>
      );
    }
  }

  RenderPrice() {
    if (item.Price > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../assets/images/coin.png")}
            style={{
              height: 30,
              width: 30,
              margin: 10,
              marginBottom: 7
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              height: 23,
              marginTop: 12.3,
              marginBottom: -10,
              marginRight: 20,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {item.Price}
          </Text>
        </View>
      );
    }
  }

  RenderLovePrice() {
    //
    if (item.LovePrice > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../assets/images/love_energy.png")}
            style={{
              height: 25,
              width: 25,
              margin: 10,
              marginTop: 0
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              height: 23,
              marginRight: 20,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {item.LovePrice}
          </Text>
        </View>
      );
    }
  }
  RenderDarkPrice() {
    //
    if (item.DarkPrice > 0) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../assets/images/dark_energy.png")}
            style={{
              height: 35,
              width: 35,
              margin: 10,
              marginTop: 0,
              marginBottom: 5,
              marginLeft: -5
            }}
          />

          <Text
            style={{
              color: "rgba(0,0,0,1)",
              fontSize: 16,
              textAlign: "center",
              fontWeight: "bold",
              borderRadius: 5,
              minWidth: 20,
              height: 23,
              marginBottom: -15,
              marginTop: 5,
              marginLeft: -4,
              marginRight: 20,
              backgroundColor: "rgba(255,255,255,0.3)"
            }}
          >
            {item.DarkPrice}
          </Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({});
