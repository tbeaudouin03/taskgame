import React from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Tooltip from "rn-tooltip";
import { Header } from "native-base";
import Popover from "react-native-popover-view";

import {
  copilot,
  walkthroughable,
  CopilotStep
} from "@okgrow/react-native-copilot";
const CView = walkthroughable(View);

const config = require("../constants/config.json");

class TopScreenHero extends React.Component {
  state = {
    pressStatus: false
  };

  async componentDidMount() {
    this.props.copilotEvents.on("stop", () => {
      this.props.StartTaskTutorial();
    });
  }

  componentWillUnmount() {
    // Don't forget to disable event handlers to prevent errors
    this.props.copilotEvents.off("stop");
  }

  render() {
    return (
      <Header
        style={{
          flex: 1,
          backgroundColor: "#fff",
          flexDirection: "column",
          justifyContent: "center",
          height: 190
        }}
      >
        <View
          style={{
            flex: 1,
            marginTop: -10,
            backgroundColor: "#fff",
            flexDirection: "row",
            justifyContent: "center",
            backgroundColor: "transparent"
          }}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              marginTop: 30,
              marginLeft: 19,
              marginRight: 30,
              marginBottom: 20,
              borderRadius: 4,
              borderWidth: 2,
              borderColor: "rgba(142, 176, 211, 0.6)",
              backgroundColor: "rgba(142, 176, 211,0.5)"
            }}
          >
            <TouchableOpacity
              style={{ flex: 1 }}
              ref="Spirit"
              onPress={() => this.setState({ isSpiritVisible: true })}
            >
              <Image
                source={{ uri: this.props.HeroData.URL }}
                style={{
                  width: 120,
                  height: 140,
                  resizeMode: "contain",
                  marginTop: 0,
                  marginLeft: 0,
                  opacity: 1
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              flex: 1,
              minWidth: 150,
              alignItems: "baseline",
              marginTop: 22,
              marginLeft: 0,
              marginRight: 5,
              marginBottom: 10
            }}
          >
            <TouchableOpacity
              ref="Level"
              style={styles.perfTextWrapper}
              onPress={() => this.setState({ isLevelVisible: true })}
            >
              <Text style={styles.titleBold}>Level: </Text>
              <Text style={[styles.titleBold, { textAlign: "right" }]}>
                {this.props.HeroData.Level}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              ref="JudgeLevel"
              onPress={() => this.setState({ isJudgeLevelVisible: true })}
              style={styles.perfTextWrapper}
            >
              <Text style={styles.titleBold}>Judge Level: </Text>
              <Text style={styles.titleBold}>
                {this.props.HeroData.JudgeLevel}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              ref="Reputation"
              onPress={() => this.setState({ isReputationVisible: true })}
              style={styles.perfTextWrapper}
            >
              <Text style={styles.perfText}>Reputation: </Text>
              <Text style={styles.perfText}>
                {this.props.HeroData.Reputation}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ isIntelligenceVisible: true })}
              ref="Intelligence"
              style={styles.perfTextWrapper}
            >
              <Text style={styles.perfText}>Intelligence: </Text>
              <Text style={styles.perfText}>
                {this.props.HeroData.Intelligence}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              ref="Fitness"
              onPress={() => this.setState({ isFitnessVisible: true })}
              style={styles.perfTextWrapper}
            >
              <Text style={styles.perfText}>Fitness: </Text>
              <Text style={styles.perfText}>{this.props.HeroData.Fitness}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Popover
          isVisible={this.state.isSpiritVisible}
          fromView={this.refs.Spirit}
          onClose={() => this.setState({ isSpiritVisible: false })}
        >
          <Text style={{ margin: 10 }}>{this.props.heroMessage}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isLevelVisible}
          fromView={this.refs.Level}
          onClose={() => this.setState({ isLevelVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Accomplish tasks, fight monsters and progress in the adventure with your friends to level-up and show the world what you are made of!`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isJudgeLevelVisible}
          fromView={this.refs.JudgeLevel}
          onClose={() => this.setState({ isJudgeLevelVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Help the Gods of Truth by assessing fairly the tasks of other players to level-up and grow your reputation as a fair judge!`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isReputationVisible}
          fromView={this.refs.Reputation}
          onClose={() => this.setState({ isReputationVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Reputation makes you more resistant against the attacks of the evil Demons of Despair.
Reputation also gives you more coins when verifying your tasks or when helping the Gods of Truth to assess other players' tasks.`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isIntelligenceVisible}
          fromView={this.refs.Intelligence}
          onClose={() => this.setState({ isIntelligenceVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Intelligence improves your chances of critical hit against monsters - dealing more damage than a normal blow.
Intelligence also improves all your skills faster when you accomplish tasks.`}</Text>
        </Popover>
        <Popover
          isVisible={this.state.isFitnessVisible}
          fromView={this.refs.Fitness}
          onClose={() => this.setState({ isFitnessVisible: false })}
        >
          <Text
            style={{ margin: 10 }}
          >{`Fitness increases the damage of your normal attack against enemies.
Fitness also gives you more coins when you accomplish tasks.`}</Text>
        </Popover>
      </Header>
    );
  }
}

/**/

export default copilot()(TopScreenHero);

const styles = StyleSheet.create({
  perfTextWrapper: {
    marginLeft: 10,
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch",
    justifyContent: "space-between",
    width: 130
  },
  perfText: {
    marginTop: 12,
    color: "rgba(0,0,0,0.4)",
    fontSize: 15,
    lineHeight: 15,
    textAlign: "left"
  },
  titleBold: {
    marginTop: 14,
    color: "rgba(0,0,0,0.65)",
    fontSize: 16,
    lineHeight: 16,
    textAlign: "left",
    fontWeight: "bold"
  },
  textTooltip: {
    color: "rgba(0,0,0,1)",
    fontSize: 15,
    lineHeight: 16,
    textAlign: "center"
  }
});
