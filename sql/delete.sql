-- always install ignite 2.7!! otherwise your queries with "WITH" will not work!

-- after running postman, always delete this
delete taskcheck where id_image = 'i3';
update hero set id_team = 'bi56qfgke9c790e2cpc0', id_image = 'i42' where email = 'tbeaudouin03@gmail.com';


-- to test adventure mode
update HeroUniqueObject set monster_is_defeated = null, is_used = true, damage_to_monster = null where id_image = 'i1001' and id_hero = 'i113811807988213595940'; 
update HeroUniqueObject set monster_is_defeated = null, is_used = false, damage_to_monster = null where id_image = 'i1002' and id_hero = 'i113811807988213595940';
update HeroUniqueObject set monster_is_defeated = null, is_used = false, damage_to_monster = null where id_image = 'i1003' and id_hero = 'i113811807988213595940';
update HeroUniqueObject set monster_is_defeated = null, is_used = false, damage_to_monster = null where id_image = 'i1004' and id_hero = 'i113811807988213595940';
update hero set monster_is_introduced = null, is_fight_accepted = null, is_fight_started = null where id_hero = 'i113811807988213595940';
delete taskrate where id_task_2 = 'MonsterBonus' and id_hero = 'i113811807988213595940';
update herostat set damage_to_hero = null where id_hero = 'i113811807988213595940';
update hero set is_bonded_to_team = true where id_hero = 'i113811807988213595940';
update hero set IS_BONDED_TEAM_LEADER = null where id_hero = 'i113811807988213595940';

update hero set id_team = 'bi56qfgke9c790e2cpc0', IS_BONDED_TO_TEAM = true, IS_BONDED_TEAM_LEADER = null where id_hero = 'i101260030415309478634';
update hero set id_team = 'bi56qfgke9c790e2cpc0', IS_BONDED_TO_TEAM = true, IS_BONDED_TEAM_LEADER = null where id_hero = 'i110330442578453275559';

select email from hero where IS_BONDED_TEAM_LEADER = true and id_team = 'bi56qfgke9c790e2cpc0'


-- restart tutorial
update hero set tutorial_level = 0 where id_hero = 'i113811807988213595940';
delete herouniqueobject where id_hero = 'i113811807988213595940';
update herocombatparty set is_active = false where id_hero_primary = 'i113811807988213595940';

-- don't use these deletes unless you know what you are doing!
delete image where shop_type IS NULL;
delete judgeopinion;
delete taskrate;
delete taskcheck;
delete task;
delete heroobject;
delete herouniqueobject;
delete herostat;
delete hero;


delete taskrate where id_hero IN (select id_hero from hero where email='tbeaudouin10@gmail.com');
delete taskcheck where id_hero IN (select id_hero from hero where email='tbeaudouin10@gmail.com');
delete task where id_hero IN (select id_hero from hero where email='tbeaudouin10@gmail.com');
delete heroobject where id_hero IN (select id_hero from hero where email='tbeaudouin10@gmail.com');
delete herouniqueobject where id_hero IN (select id_hero from hero where email='tbeaudouin10@gmail.com');
delete herostat where id_hero IN (select id_hero from hero where email='tbeaudouin10@gmail.com');
delete hero where email='tbeaudouin10@gmail.com';