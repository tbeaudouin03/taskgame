

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i1', 'Hello', '!', 'Tap the tick box below one of your tasks, take a picture showing you completed your task and gain lots of points!', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i2', 'Be wise', '!', 'The more intelligent you are, the faster your skills will grow: work smart my friend!', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i3', 'Be fit', '!', 'The fitter you are, the more coins you get from unverified tasks: ready? Go!', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i4', 'Did you know', '?', 'Build a team of heros. They will greatly improve your skills and help you accomplish your tasks!', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i5', 'Did you know', '?', 'Artefacts have secret powers - and from power springs power! Who knows what might be unleashed if you eat enough brocolis?', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i6', 'There you are', '!', 'I have been waiting for you! I wanted to tell you: do not be greedy! Offer objects to your friends! You may get more than you think from friendship.', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i7', 'Wait! Just a second', '!', 'Sometimes, greed is good: maybe that small collectible coin given by your great grand mother will be worth the wait after all!', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i8', 'I am hearing voices', '...', 'Build a team of heros. They will greatly improve your skills and help you accomplish your tasks!', 'hero', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i9', 'Hello again', '!', 'Below, you might see the picture that someone took to prove they did their task: scroll down, choose what you think it is worth and tap on Judge to gain points for yourself!', 'judge', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i10', 'Hold on', '!', 'Give an unfair judgement, and lose your reputation and money! The closest your judgement is from the average, the more points. The farthest, the more penalties.', 'judge', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i11', 'Be careful', '!', 'We also manually check some judgements. If we see you are making unfair judgements, you shall be sentenced to jail or directly sent to hell!', 'judge', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i12', 'Salutations', '!', 'The more reputation you have, the more points you get for judging and for being judged - and the more your judgement counts!', 'judge', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i13', 'Howdy', '!', 'The more tasks you complete in Google Tasks, the more advice I can give you: this is the rule of our Universe!', 'both', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i14', 'What is new', '?', 'We, heros, are not simple drawing of pixels on a screen. We are spirits from beyond. We inspire and motivate our masters! We also remain humble - which is an accomplishment in itself.', 'both', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i15', 'Did you know', '?', 'The tragedy of old age is not that one is old, but that one is young.', 'both', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i16', 'I just thought of something', '!', 'The only way to get rid of a temptation is to yield to it. Resist it, and your soul grows sick with longing for the things it has forbidden to itself, with desire for what its monstrous laws have made monstrous and unlawful. Or did I read it somewhere?', 'both', 0);

insert into advice(id_advice, greeting, punctuation, message, advice_rule, task_count_unlock)
values('i17', 'I read somewhere something interesting', ',', 'People often confuse their imagination with their heart and they believe themselves converted as soon as they think of converting themselves.', 'both', 0);
      