CREATE TABLE Team (
   ID_TEAM       VARCHAR,
   NAME          VARCHAR,
   FACEBOOK_GROUP      VARCHAR,
   LINKEDIN_GROUP      VARCHAR,
   CREATED_AT    TIMESTAMP,
   UPDATED_AT    TIMESTAMP,
   ROOKIE_TRAINED_COUNT INT,
  PRIMARY KEY (id_team)
) WITH "TEMPLATE=replicated, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=TeamCache, 
        DATA_REGION=TaskgameOperational, 
        KEY_TYPE=TeamKey, 
        VALUE_TYPE=TeamValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE Hero (
   ID_HERO       VARCHAR,
   ID_IMAGE      VARCHAR, -- id_shop_object
   ID_TEAM      VARCHAR,
   CREATED_AT    TIMESTAMP,
   UPDATED_AT    TIMESTAMP,
   EMAIL         VARCHAR,
   GIVEN_NAME          VARCHAR,
   FAMILY_NAME          VARCHAR,
   STATUS               VARCHAR,
   MOOD                 VARCHAR,
   FACEBOOK_PROFILE     VARCHAR,
   LINKEDIN_PROFILE     VARCHAR,
   TUTORIAL_LEVEL INT,
   HERO_TUTORIAL_LEVEL INT,
   JUDGE_TUTORIAL_LEVEL INT,
   SHOP_TUTORIAL_LEVEL INT,
   LEADERBOARD_TUTORIAL_LEVEL INT,
   MAP_TUTORIAL_LEVEL INT,
   TEAM_TUTORIAL_LEVEL INT,
   MENU_TUTORIAL_LEVEL INT,
   LAST_CONNECT_AT TIMESTAMP,
   CONNECTION_COUNT INT,
   LAST_FREE_LOVE_ENERGY_AT TIMESTAMP,
   MINUTE_UNTIL_NEXT_LOVE_GIFT INT,
   LAST_DARK_LOVE_ENERGY_AT TIMESTAMP,
   MINUTE_UNTIL_NEXT_DARK_GIFT INT,
   IS_BONDED_TEAM_LEADER BOOLEAN,
   IS_BONDED_TO_TEAM BOOLEAN,
  PRIMARY KEY (id_hero)
) WITH "TEMPLATE=replicated, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=HeroCache, 
        DATA_REGION=TaskgameOperational, 
        KEY_TYPE=HeroKey, 
        VALUE_TYPE=HeroValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE HeroStat (
   ID_HERO       VARCHAR,
   LEVEL         INTEGER,
   JUDGE_LEVEL   INTEGER,
   COINS         INTEGER,
   REPUTATION    INTEGER,
   INTELLIGENCE  INTEGER,
   FITNESS       INTEGER,
   RELIABILITY   INTEGER,
   TASK_COUNT INTEGER,
   TASK_VERIFY_COUNT INTEGER,
   JUDGEMENT_COUNT INTEGER,
   JUDGEMENT_VERIFY_COUNT INTEGER,
   W0_TASK_COUNT INTEGER,
   W1_TASK_COUNT INTEGER,
   W2_TASK_COUNT INTEGER,
   W3_TASK_COUNT INTEGER,
   W4_TASK_COUNT INTEGER,
   xRANK INTEGER,
   xJUDGE_RANK INTEGER,
   SCORE INTEGER,
   JUDGE_SCORE INTEGER,
   LOVE_ENERGY INTEGER,
   DARK_ENERGY INTEGER,
   DAMAGE_TO_HERO INTEGER,
   updated_at timestamp,
  PRIMARY KEY (id_hero)
) WITH "TEMPLATE=replicated, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=HeroStatCache, 
        DATA_REGION=TaskgameOperational, 
        KEY_TYPE=HeroStatKey, 
        VALUE_TYPE=HeroStatValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE HeroCombatParty (
   ID_HERO_PRIMARY VARCHAR,
   ID_HERO_COMPANION VARCHAR,
   IS_ACTIVE BOOLEAN,
   LAST_DAMAGE_TO_MONSTER INT,
   CREATED_AT TIMESTAMP,
   UPDATED_AT TIMESTAMP,
  PRIMARY KEY (ID_HERO_PRIMARY, ID_HERO_COMPANION)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync,
        CACHE_NAME=HeroCombatPartyCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=HeroCombatPartyKey, 
        VALUE_TYPE=HeroCombatPartyValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

-- we add URL in there because it is partitionned so cannot easily get URL from image table
CREATE TABLE TeamMembershipRequest (
   ID_HERO VARCHAR,
   ID_TEAM VARCHAR,
   URL     VARCHAR,
   IS_ACTIVE BOOLEAN,
   IS_APPROVED BOOLEAN,
   CREATED_AT TIMESTAMP,
   UPDATED_AT TIMESTAMP,
   COST       INT,
  PRIMARY KEY (ID_HERO, ID_TEAM)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync,
        CACHE_NAME=TeamMembershipRequestCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=TeamMembershipRequestKey, 
        VALUE_TYPE=TeamMembershipRequestValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

-- does not require affinity since ShopObject is a replicated table
CREATE TABLE HeroUniqueObject (
   ID_HERO VARCHAR,
   ID_IMAGE VARCHAR, -- ID_SHOP_OBJECT
   IS_USED BOOLEAN, -- is_current_map_step / is_current_map
   CREATED_AT TIMESTAMP,
   UPDATED_AT TIMESTAMP,
   is_fight_accepted BOOLEAN,
   is_fight_started BOOLEAN,
   monster_is_introduced BOOLEAN,
   monster_is_defeated BOOLEAN,
  PRIMARY KEY (ID_HERO, ID_IMAGE)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync,
        AFFINITY_KEY=id_image, 
        CACHE_NAME=HeroUniqueObjectCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=HeroUniqueObjectKey, 
        VALUE_TYPE=HeroUniqueObjectValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";


            MonsterXPosition = 420 + 10 * (Math.random() - 0.5);
    MonsterYPosition = 485 + 10 * (Math.random() - 0.5);
    TeamXPosition = 300;
    TeamYPosition = 400;
    HeroX1Position = 180;
    HeroY1Position = 100;
    HeroX2Position = 430;
    HeroY2Position = 200;
    HeroX3Position = 390;
    HeroY3Position = 350;

-- does not require affinity since ShopObject is a replicated table
CREATE TABLE HeroObject (
   ID_HERO_OBJECT VARCHAR,
   ID_HERO VARCHAR,
   ID_IMAGE VARCHAR, -- ID_SHOP_OBJECT
   IS_USED BOOLEAN,
   CREATED_AT TIMESTAMP,
   UPDATED_AT TIMESTAMP,
  PRIMARY KEY (ID_HERO_OBJECT, ID_HERO, ID_IMAGE)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync,
        AFFINITY_KEY=id_image, 
        CACHE_NAME=HeroObjectCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=HeroObjectKey, 
        VALUE_TYPE=HeroObjectValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

-- id_hero is never an affinity key because Hero table is replicated across all nodes
CREATE TABLE Task (
  id_task_1 varchar,
  id_task_2 varchar,
  id_hero varchar, 
  created_at timestamp,
  updated_at timestamp,
  title varchar,
  PRIMARY KEY (id_task_1, id_task_2, id_hero)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=TaskCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=TaskKey, 
        VALUE_TYPE=TaskValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE TaskRate (
  id_task_1 varchar,
  id_task_2 varchar,
  id_hero varchar,
  checked bool,
  coins int,
  reputation int,
  intelligence int,
  fitness int,
  is_verified bool,
  created_at timestamp,
  verified_at timestamp,
  judgement_count int,
  love_energy int,
  dark_energy int,
  PRIMARY KEY (id_task_1, id_task_2, id_hero)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=TaskRateCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=TaskRateKey, 
        VALUE_TYPE=TaskRateValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE TaskCheck (
  id_task_check varchar,
  id_task_1 varchar,
  id_task_2 varchar,
  id_hero varchar, 
  id_image varchar,
  title varchar,
  created_at timestamp,
  updated_at timestamp,
  is_verified bool,
  verified_at timestamp,
  PRIMARY KEY (id_task_check, id_task_1, id_task_2, id_image)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        AFFINITY_KEY=id_image,
        CACHE_NAME=TaskCheckCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=TaskCheckKey, 
        VALUE_TYPE=TaskCheckValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

-- id_judge as primary key in order to be able to merge on it
CREATE TABLE JudgeOpinion (
  id_judge_opinion varchar,
  id_judge varchar,
  id_task_check varchar,
  id_hero varchar, 
  id_task_1 varchar,
  id_task_2 varchar,
  created_at timestamp,
  updated_at timestamp,
  coins int,
  main_skill varchar,
  judge_coins int,
  judge_reputation int,
  judge_love int,
  judge_dark int,
  PRIMARY KEY (id_judge_opinion, id_task_check, id_task_1, id_task_2, id_judge)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        AFFINITY_KEY=id_task_check,
        CACHE_NAME=JudgeOpinionCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=JudgeOpinionKey, 
        VALUE_TYPE=JudgeOpinionValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

-- if is_friend = true then just on appear message then postNewMapStep
-- if is_friend = false then onAppearMessage then checkIfNewMapStep
CREATE TABLE ShopObject (
  id_shop_object varchar,
  url varchar,
  title varchar, 
  description varchar, -- on appear message
  price integer,
  love_price integer,
  dark_price integer,
  reputation integer, -- character reputation
  intelligence integer, -- character intelligence
  fitness integer, -- character fitness
  image_type varchar,
  shop_type varchar,
  shop_rule varchar,
  on_buy_message varchar, -- on positive answer
  on_use_message varchar,  -- on negative answer
  on_sell_message varchar,  -- on disappear message
  created_at timestamp,
  updated_at timestamp,
  is_unique boolean,
  id_map varchar,
  map_step_order int,
  character_url varchar,
  character_coins_bonus int,
  character_love_bonus int,
  character_dark_bonus int,
  damage_to_monster INT,
   monster_x_position INT,
   monster_y_position INT,
   team_x_position INT,
   team_y_position INT,
   hero_x1_position INT,
   hero_y1_position INT,
   hero_x2_position INT,
   hero_y2_position INT,
   hero_x3_position INT,
   hero_y3_position INT,
  PRIMARY KEY (id_shop_object)
) WITH "TEMPLATE=replicated, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=ShopObjectCache, 
        DATA_REGION=TaskgameOperational, 
        KEY_TYPE=ShopObjectKey, 
        VALUE_TYPE=ShopObjectValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE Image (
  id_image varchar,
  url varchar,
  title varchar, 
  description varchar,
  price integer,
  love_price integer,
  dark_price integer,
  reputation integer,
  intelligence integer,
  fitness integer,
  image_type varchar,
  shop_type varchar,
  shop_rule varchar,
  on_buy_message varchar,
  on_use_message varchar,
  created_at timestamp,
  updated_at timestamp,
  is_unique boolean,
  PRIMARY KEY (id_image)
) WITH "TEMPLATE=partitioned, 
        BACKUPS=1, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=ImageCache, 
        DATA_REGION=TaskgameOperationalBis, 
        KEY_TYPE=ImageKey, 
        VALUE_TYPE=ImageValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE Advice (
   ID_ADVICE       varchar,
   greeting varchar,
   greeting_fr varchar,
   message varchar,
   message_fr varchar,
   punctuation varchar,
   advice_rule varchar,
   task_count_unlock int,
  PRIMARY KEY (ID_ADVICE)
) WITH "TEMPLATE=replicated, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=AdviceCache, 
        DATA_REGION=TaskgameOperational, 
        KEY_TYPE=AdviceKey, 
        VALUE_TYPE=AdviceValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";

CREATE TABLE DropdownElement (
    id_dropdown_element       varchar,
    label varchar,
    label_fr varchar,
    value varchar,
    value_fr varchar,
    element_order int,
    id_dropdown varchar,
    PRIMARY KEY (id_dropdown_element)
) WITH "TEMPLATE=replicated, 
        ATOMICITY=atomic, 
        WRITE_SYNCHRONIZATION_MODE=full_sync, 
        CACHE_NAME=DropdownElementCache, 
        DATA_REGION=TaskgameOperational, 
        KEY_TYPE=DropdownElementKey, 
        VALUE_TYPE=DropdownElementValue, 
        WRAP_KEY=true, 
        WRAP_VALUE=true";


