

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique,
id_map)
VALUES('i101', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Ftree_monster.gif?alt=media&token=ce10fb95-fb12-4624-a899-f597352f73e1',
'Fighting Wretchedness',
'Your first quest!',
0,
0,
0,
CURRENT_TIMESTAMP(),
0,
0,
0
,'shop'
,'map'
,'start'
,true
,'im1');

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique,
id_map)
VALUES('i102', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdemotivational_virus.gif?alt=media&token=6f91f490-29d3-43b2-99c8-3849991387f0',
'Motivation Cure',
'Viruses and other diseases have spread to Dream Shire. If nobody finds ways to fight them, our dreams will die. The Will Mills are contaminated and people started behaving strangely in the whereabouts of the Singing Hills.',
10,
0,
0,
CURRENT_TIMESTAMP(),
0,
0,
0
,'shop'
,'map'
,'start'
,true
,'im2');


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique,
id_map)
VALUES('i103', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fburning_issue.gif?alt=media&token=2ceebd94-8e6c-4cc4-9d25-115099529ce4',
'Burning Issues',
'Demons are more agitated than ever: Bourn Watch is under attack! Our soldiers are crippled with dark thoughts and mind pollution.',
30,
0,
0,
CURRENT_TIMESTAMP(),
0,
0,
0
,'shop'
,'map'
,'start'
,true
,'im3');

