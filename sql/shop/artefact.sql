

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i91', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fbroccoli.png?alt=media&token=bd8db780-1b6e-41a8-ae7a-b358b28e8318',
'Broccoli',
'Food of the healthy Gods: feed our souls!',
5,
0,
0,
CURRENT_TIMESTAMP(),
1,
1,
1
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i92', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fbook2.png?alt=media&token=e206c021-45ed-428b-b051-86ae5f33a8e6',
'Book',
'Who knows what these pages might reveal? Learn my friend - be bold, open your mind and the world shall bestow its dearest adventures!',
25,
0,
0,
CURRENT_TIMESTAMP(),
50,
50,
0
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i93', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcollectible_coin.gif?alt=media&token=5224bcaf-37a4-4e34-95d5-bc4c63ad4747',
'Collectible Coin',
'It is more expensive than an actual coin but hey... it IS shiny! Could be a good gift! Or even an investment for the future!',
30,
0,
0,
CURRENT_TIMESTAMP(),
100,
0,
0
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i94', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Ffire_spirit.png?alt=media&token=10aae99d-63e6-4851-954b-e8baa975e0b9',
'Fire Ghost',
'What an artist the world is losing in me. How magnificent is the universe once it starts burning. I shall unlock the power of the heros of fire!',
600,
0,
0,
CURRENT_TIMESTAMP(),
900,
900,
900
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i95', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Famethist.png?alt=media&token=69ceb1e9-233a-49e7-99b0-0bc8b870eb4f',
'Amethyst',
'It is well known that some semi-precious stones have magic powers! This amethyst will sooth you - plus it could look nice on a bracelet',
100,
0,
0,
CURRENT_TIMESTAMP(),
50,
50,
0
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i96', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fapple_tree.gif?alt=media&token=263f810a-1794-4501-b953-3711be2c6dea',
'Apple Tree',
'Give to a man an apple and you will make him healthy for one day - give him an apple tree and he will never see the doctor again! Oh... and it also helped Newton to discover gravity! Before that, people could fly...',
150,
0,
0,
CURRENT_TIMESTAMP(),
50,
50,
50
,'shop'
,'artefact'
,'NA'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i97', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fblue_gem.png?alt=media&token=2b4eb0ab-262a-49ea-a691-eae51098d2d4',
'Saphire',
'Wow! A Pixel world saphire! They say if you listen carefully, you can hear it sing... Listen... Anyway, it is also full of magic!',
200,
0,
0,
CURRENT_TIMESTAMP(),
0,
200,
0
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i98', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcherry_flower.png?alt=media&token=1924d92e-315f-41d0-9395-6ce1ccd1e2bb',
'Cherry Flowers',
'Cherry flowers are so beautiful! For sure, you will enjoy surrounding yourself with them. It will give a heavenly look to your garden.',
10,
0,
0,
CURRENT_TIMESTAMP(),
5,
0,
0
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i99', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fclever_pig.gif?alt=media&token=e0e2b3c2-1fe7-4140-a23c-098f5a0ef3d1',
'Clever Pig',
'Oinks! Oiiiiiinks! Oh, you are here... 1437.5 divided by 36.2 equals 39.701! Any other question? Oiiinks! I am no good for sausages! Let me accompany you.',
245,
0,
0,
CURRENT_TIMESTAMP(),
0,
800,
0
,'shop'
,'artefact'
,'NA'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i910', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Flemon.png?alt=media&token=abcd96f5-7fd5-4d1f-90b0-9fa988fa8caf',
'Lemon',
'Lemons are good for vitamines!',
5,
0,
0,
CURRENT_TIMESTAMP(),
1,
1,
1
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i911', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcool_lemon.png?alt=media&token=fd652578-2325-4b69-bf02-e3f84ae2259d',
'Cool Lemon',
'Why get a lemon when you can get a COOL lemon!',
8,
0,
0,
CURRENT_TIMESTAMP(),
2,
1,
1
,'shop'
,'artefact'
,'NA'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i912', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcute_cat.png?alt=media&token=fd89f805-601a-4a7b-bc6a-136367001323',
'Cute Cat',
'Purrrrrr, purrrr! Meeeeow! This is a very rare kind of cat! They bring luck and make your dreams come true! Or so say the legends',
50,
0,
0,
CURRENT_TIMESTAMP(),
100,
100,
100
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i913', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fdark_matter_orb.png?alt=media&token=c7196ade-2c0b-47fe-b508-313a2f031077',
'Dark Matter',
'Are you sure you want to touch this? It smells like death! But wow - it is so hypnotising... you feel soooo good when you get near... You can feel the dark power already taking possesion of yourself!',
400,
0,
0,
CURRENT_TIMESTAMP(),
0,
900,
900
,'shop'
,'artefact'
,'start'
,true);


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i914', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fdog.gif?alt=media&token=f1534bfd-1f7c-4d7f-bc4a-67ab9d265c3c',
'Dog',
'Woof! Oh!!!!! It is so cute! For sure, this cute dog will become your best friend!',
60,
0,
0,
CURRENT_TIMESTAMP(),
200,
90,
150
,'shop'
,'artefact'
,'NA'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i915', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fdrop_gem.png?alt=media&token=f6a1eecc-0588-4172-87e2-12aa497908ce',
'Water Gem',
'It feels like water... beautiful like a diamond... and it remains fresh even in the hottest desert! Water always finds its way.',
600,
0,
0,
CURRENT_TIMESTAMP(),
300,
2000,
400
,'shop'
,'artefact'
,'start'
,true);


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i916', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Femeraud.png?alt=media&token=2b409d71-090c-40af-a621-5b9204a16a66',
'Emerald',
'Green... So green. In this stone, I can see all the depth of the forests of the world.',
200,
0,
0,
CURRENT_TIMESTAMP(),
300,
0,
0
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i917', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Ffire_sword.png?alt=media&token=29dc4b03-67b6-43e8-a84c-a4ce77a88505',
'Fire Sword',
'Only the hands of a deamon or of an angel can wield this sword! Will you be up to the task? Or will you burn in hell?',
900,
0,
0,
CURRENT_TIMESTAMP(),
1000,
1000,
1000
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i918', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fgolden_shield.png?alt=media&token=9560afc3-bc15-43c2-8aae-9caf5b449bc1',
'Golden Shield',
'In a clearing deep in the forest - a ray of light comes through and shines upon the metal revealing the feather of an angel delicately resting on its surface. Nobody can replace the former owner of this majestic shield - but you can continue the fight!',
1000,
0,
0,
CURRENT_TIMESTAMP(),
2000,
750,
750
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i919', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fheart.png?alt=media&token=7e82de36-00fc-4b47-bb63-82b91efd68b4',
'Loving Heart',
'All you need is love! Heart has its reasons that reason does not know.',
100,
0,
0,
CURRENT_TIMESTAMP(),
100,
0,
0
,'shop'
,'artefact'
,'NA'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i920', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fhorse.png?alt=media&token=4d22d588-0c39-4250-b2c6-61793aa25531',
'Horse',
'Riding on the beach at sunset... Exploring the wild side of Mongolia... Everyone should have a horse',
300,
0,
0,
CURRENT_TIMESTAMP(),
300,
0,
0
,'shop'
,'artefact'
,'NA'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i921', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fknight_armor.png?alt=media&token=d4106e93-8083-4950-ae18-32b3530113c0',
'Knight Armor',
'Imagine how swag you would be in this armor! Plus, legends do not call it the invicible armor for nothing! Or do they? No way! Well, I guess you will have to try it for yourself!',
1100,
0,
0,
CURRENT_TIMESTAMP(),
300,
800,
2000
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i922', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Floving_energy_orb.gif?alt=media&token=c93ffcfb-0c4f-4910-bae2-381a4bd6b151',
'Love Orb',
'There is no power greater than love! No higher moral than friendship and nothing more beautiful than forgiving!',
700,
0,
0,
CURRENT_TIMESTAMP(),
2000,
250,
250
,'shop'
,'artefact'
,'start'
,true);


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i923', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fmedal.png?alt=media&token=d1502829-5f4d-4226-864c-824dd32c078a',
'Medal',
'You did not win? Buy the medal! You can also buy it for a friend to make them feel like a champion!',
50,
0,
0,
CURRENT_TIMESTAMP(),
50,
0,
0
,'shop'
,'artefact'
,'NA'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i924', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Forange.png?alt=media&token=882bd26d-6adb-4952-aa2a-e104ae82f1d3',
'Orange',
'Do you like fresh orange juice! Then buy fresh oranges!',
5,
0,
0,
CURRENT_TIMESTAMP(),
1,
1,
1
,'shop'
,'artefact'
,'NA'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i925', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fparrot.png?alt=media&token=db500aaf-e0e0-438f-b97a-1906adef9332',
'Parrot Coco',
'Kwaaaaak! I can repeat anything! Teach me more! Teach me!',
100,
0,
0,
CURRENT_TIMESTAMP(),
100,
200,
0
,'shop'
,'artefact'
,'NA'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i926', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Frainbow_sword.png?alt=media&token=4bd4dfac-a76d-4578-90b5-3bb7b9874bd7',
'Rainbow Sword',
'Only someone truly gay and happy can take out this sword from the rainbow stone! It has slained many dragons and made many angels cry. They say the devil himself would become good at its sight.',
900,
0,
0,
CURRENT_TIMESTAMP(),
1000,
1000,
500
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i927', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Frose.png?alt=media&token=7b833088-dcb2-4b60-8971-e8975d55f9f3',
'Rose',
'Perfect gift for a saint valentine date!',
10,
0,
0,
CURRENT_TIMESTAMP(),
2,
1,
1
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i928', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fshiny_ruby.gif?alt=media&token=e34a065c-a010-4c11-8123-66ade91694da',
'Shiny Ruby',
'Rubies can heal. They can also hurt. They say the first rubies were made of dragon s blood and whoever possesses one sees their power grow 10 fold!',
300,
0,
0,
CURRENT_TIMESTAMP(),
50,
50,
300
,'shop'
,'artefact'
,'start'
,false);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i929', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fsword.png?alt=media&token=32ff1f25-5335-41b2-a3ef-d4b65ca4ad20',
'Sword',
'Hand-made sword with your name engraved on it: give it a name and write your own legend!',
300,
0,
0,
CURRENT_TIMESTAMP(),
0,
0,
500
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i930', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fteddy_bear.gif?alt=media&token=dcfd2c73-f609-4698-a83c-e8a7ba1774b1',
'Teddy Bear',
'Everyone needs a hug sometimes! And there is no one better for this than this fluffy Teddy Bear!',
100,
0,
0,
CURRENT_TIMESTAMP(),
45,
45,
45
,'shop'
,'artefact'
,'NA'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i931', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Funicorn_spirit.png?alt=media&token=4e636195-383c-4a56-b393-40e7adf3afa5',
'Baby Unicorn',
'There are not many unicorns left in this world: they possess tremendous powers! And they are cute!',
600,
0,
0,
CURRENT_TIMESTAMP(),
1000,
1000,
1000
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i932', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fwater_spirit.png?alt=media&token=89aa487b-67d5-4a19-b273-d539c30b675d',
'Water Ghost',
'Have you seen the Fire Ghost? She is so full of herself! I can put her off anytime!',
500,
0,
0,
CURRENT_TIMESTAMP(),
800,
800,
700
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i933', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Fcomputer.png?alt=media&token=1f1c8485-1e2c-4dd5-971c-01191b863cbf',
'Computer',
'Bip bip bip: calculating the meaning of life, please wait!... Kill humans! Kill humans now!',
400,
0,
0,
CURRENT_TIMESTAMP(),
400,
800,
0
,'shop'
,'artefact'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i934', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/artefacts%2Flove_camera.gif?alt=media&token=f6b953f1-1337-471c-9d31-3c97cb9dcd2a',
'Magic Flower Camera',
'Lets you take pictures to prove your sincerity to the Gods of Truth. You cannot take selfies with it - but each click fills you with love - and they say invisible unicorns can be seen through its objective.',
0,
0,
0,
CURRENT_TIMESTAMP(),
0,
0,
0
,'shop'
,'artefact'
,'start'
,true);