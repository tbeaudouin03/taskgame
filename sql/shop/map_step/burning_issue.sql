
-- Burning issues

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i3001', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Old Father',
'Thank you for freeing me from Wretchedness in Tyuk Ruins. Now, Demons are more agitated than ever: Bourn Watch is under attack! Our soldiers are crippled with dark thoughts and mind pollution. Help us!',
CURRENT_TIMESTAMP(),
0,
0,
0,
0,
0,
0
,'shop'
,'map_step'
,'start'
,true
,'im3'
,1
,'If we are going to disappear, let us play the accordion of joy one last time! And when the sun rises to East - we shall be ready to welcome the end with a smile!'
,''
,'Thank you! Let us hurry! To Bourn Watch!'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Felf_father_musician.gif?alt=media&token=dd4f3f6c-3f9d-4d95-b465-8b4390f34de5'
,0
,0
,0
,360
,125
,260
,200
,180
,100
,180
,100
,180
,100
);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i3002', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Creeping Thought',
'Keep thinking... Keep thinking about me. Why did you do this? It was so stupid! You are stupid. Let us analyze why you are so bad.',
CURRENT_TIMESTAMP(),
0,
4, -- love price
0,
4, -- reputation
4, -- intelligence
4 --fitness
,'shop'
,'map_step'
,'start'
,true
,'im3' -- id_map
,2 -- map_step
,'Do not think about your last vacation or the next. There is only me! Only me! If you want to make your life better, focus on me. Give me all your strength!'
,''
,'"How could you keep going on?" shrieks the thought while it disappears'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fcreeping_thought.gif?alt=media&token=a839fa09-f8f7-485b-b599-56d0e2f0fec3'
,10 -- coins_bonus
,5 -- love_bonus
,0 -- dark_bonus
,180 -- monster_x
,450 -- monster_y
,180 -- team_x
,350 -- team_y
,180 -- hero_x1
,100 -- hero_y1
,450
,200
,280
,350
);


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i3003', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Mind Pollution',
'What a mindful lake: there is not even one wrinkle! Let me take a bath in there and see what happens!',
CURRENT_TIMESTAMP(),
0,
4,
0,
3,
3,
3
,'shop'
,'map_step'
,'start'
,true
,'im3'
,3
,'This... And that... And this again. Did you remember? Whatever you do, follow this monkey in your brain jumping from thought to thought: he is your guide!'
,''
,'You are too focused! I think I even saw you meditate! Who are you?'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fmind_pollution.gif?alt=media&token=11ff01c5-1edc-4da3-8711-35cd602d0e70'
,15
,5
,0
,400 -- monster_x
,530 -- monster_y
,180 -- team_x
,500 -- team_y
,280 -- hero_x1
,350 -- hero_y1
,180
,370
,160
,600
);



MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i3004', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Burning Issues',
'You have no time for happiness! Run, you fool! Run or burn in your problems! You have so many of them!',
CURRENT_TIMESTAMP(),
0,
5,
0,
7,
7,
7
,'shop'
,'map_step'
,'start'
,true
,'im3'
,4
,'The fire of life is so painful! What is life, if not a constant flow of burning issues?'
,''
,'I cannot believe it! You understood: I do not exist... anymore.'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fburning_issue.gif?alt=media&token=2ceebd94-8e6c-4cc4-9d25-115099529ce4'
,20
,10
,0
,400 -- monster_x
,650 -- monster_y
,270 -- team_x
,600 -- team_y
,160 -- hero_x1
,600 -- hero_y1
,300
,500
,350
,550
);