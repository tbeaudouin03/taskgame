

-- Fighting Wretchedness -------------------------------------------------------------------------------------

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
    monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i1001', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'The Young Elf',
'Congratulations on your first task! You are my only hope: Demons of Despair captured my father! They took him and other Spirits to Tyuk Ruins. Please, help me!',
CURRENT_TIMESTAMP(),
0,
0,
0,
0,
0,
0
,'shop'
,'map_step'
,'start'
,true
,'im1'
,1
,'Maintain LONGER your finger on me to continue! Demons used to live far away from Dream Shire. Many thought they were legends. Let us hope more humans come help the Spirit world!'
,''
,'Thank you! May courage be with you!'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fyoung_elf.png?alt=media&token=3640f24c-b94c-4b78-b150-fbecafcaa7ae'
,0
,0
,0
,360
,125
,260
,200
,180
,100
,180
,100
,180
,100
);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
      monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i1002', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'The Bear Rabbit',
'Tap me longer if you dare! Did you bring me carrots? No? Then YOU will be my carrot! I am hungry!',
CURRENT_TIMESTAMP(),
0,
4,
0,
1,
1,
1
,'shop'
,'map_step'
,'start'
,true
,'im1'
,2
,'I said: tap LONGER and fight! I used to be cute but demons turned me into my true self! How dare you dwell in these dreadful mountains!'
,''
,'"Thank you for ending my hunger... Take my strength and beat the Evil beyond these mountains!" says the beast in a last groan'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fbear_rabbit.png?alt=media&token=d06cde4d-c909-45f3-b766-1957390faee5'
,10
,1
,0
,500
,460
,300
,400
,180
,100
,430
,200
,390
,350
);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, 
image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
      monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i1003', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Evil Tree',
'We do not speak wind for we do not drink water. We speak the cracking sound of sorrow for we quench on the blood of foolish travellers!',
CURRENT_TIMESTAMP(),
0,
4,
0,
2,
2,
2
,'shop'
,'map_step'
,'start'
,true
,'im1'
,3
,'Istan Varul was once on the commercial route to Tyuk harbour - our leaves were green and we grew happy - Nightmare Shire has long reclaimed its rights!'
,''
,'Why using my branches for a barbecue with your friends!? No matter how beautiful is the Comedy of Life: the final act is always bloody!'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Ftree_monster.gif?alt=media&token=ce10fb95-fb12-4624-a899-f597352f73e1'
,15
,2
,0
,550
,560
,370
,570
,390
,350
,420
,420
,450
,450
);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, 
image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
      monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i1004', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Tyuk Wretchedness',
'Why hope? Why dreams? You are good for nothing! Give up like everyone else.',
CURRENT_TIMESTAMP(),
0,
4,
0,
3,
3,
3
,'shop'
,'map_step'
,'start'
,true
,'im1'
,4
,'I sense you are yielding. Look into my eyes and see how many spirits have chosen me: listen to their voices!'
,''
,'"How could you beat your own wretchedness? What are you? I thought humans all gave up on this world...". The demon explodes into dazzling light and spirits spring out: they are free now!'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fwretchedness.gif?alt=media&token=c1707d3e-c43b-45b8-ae8a-c87b843e7c84'
,20
,5
,1
,600
,750
,430
,630
,420
,450
,500
,550
,530
,630
);



