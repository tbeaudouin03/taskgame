
-- Curing motivation

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i2001', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'The Happy Elf',
'Viruses and other diseases have spread to Dream Shire. If nobody finds ways to fight them, our dreams will die. The Will Mills are contaminated and people started behaving strangely in the whereabouts of the Singing Hills. Please, help us!',
CURRENT_TIMESTAMP(),
0,
0,
0,
0,
0,
0
,'shop'
,'map_step'
,'start'
,true
,'im2'
,1
,'Thank you for freeing the spirits in Tyuk ruins! Many years had passed since hope could be seen on the faces of TaskTown Spirits.'
,''
,'Thank you again! Let us stir the fire of hope you sparked!'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fyoung_elf.png?alt=media&token=3640f24c-b94c-4b78-b150-fbecafcaa7ae'
,0
,0
,0
,360
,125
,260
,200
,180
,100
,180
,100
,180
,100
);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i2002', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Demotivational Virus',
'There is always someone better than you - whatever you do, no matter how much you work. Life is gloomy these days! Has it ever been happy? I do not remember. You neither.',
CURRENT_TIMESTAMP(),
0,
4,
0,
3,
3,
3
,'shop'
,'map_step'
,'start'
,true
,'im2'
,2
,'Here is a good environment for me! These Will Mills supply dream grains to all the spirits in Dream Shire. Soon, I will have spread all over!'
,''
,'Your motivational antibodies are too strong! Where do you get this motivation from?'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdemotivational_virus.gif?alt=media&token=6f91f490-29d3-43b2-99c8-3849991387f0'
,10
,5
,1
,415
,80
,350
,200
,180
,100
,280
,150
,450
,200
);


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE,
DESCRIPTION, -- on appear message
CREATED_AT, 
price, love_price, dark_price,
reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique, 
id_map,
map_step_order,
on_buy_message, -- on positive answer
  on_use_message, -- on negative answer
  on_sell_message, -- on disappear message
  character_url,
  character_coins_bonus,
  character_love_bonus,
  character_dark_bonus,
   monster_x_position,
   monster_y_position,
   team_x_position,
   team_y_position,
   hero_x1_position,
   hero_y1_position,
   hero_x2_position,
   hero_y2_position,
   hero_x3_position,
   hero_y3_position)
VALUES('i2003', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fdream_shire.png?alt=media&token=21ded116-4bfb-4663-a76b-4726b09ee566',
'Procrastination Bug',
'Tomorrow! Ah... Tomorrow! Why do today what you can do tomorrow? Dreaming is all about tomorrow! Let us wait until then - forever!',
CURRENT_TIMESTAMP(),
0,
4,
0,
3,
3,
3
,'shop'
,'map_step'
,'start'
,true
,'im2'
,3
,'These nice spirits I stung: they just lay down in the Hills waiting for tomorrow. They fear as much as they hope for tomorrow - and they feel powerless!'
,''
,'How dare you complete your tasks! This is too much will power! I am smashed!'
,'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/maps%2Fdream_shire%2Fprocrastination_bug.gif?alt=media&token=438ae9e4-ca8b-4b40-a10e-98cb070ae28d'
,10
,5
,1
,620
,220
,470
,250
,450
,200
,460
,150
,470
,130
);