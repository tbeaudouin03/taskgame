

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i1', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel1%2Fjess.gif?alt=media&token=366051a3-b8f0-4ed3-8a15-5f0e7ac74be8',
'Jess',
'Always forward! Always! Let us go!... Did you remember to feed the cat? Ok! Then nothing can hold us back!',
100,
0,
0,
CURRENT_TIMESTAMP(),
100,
100,
200
,'shop'
,'hero'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i2', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel1%2Fmartina.gif?alt=media&token=2d943520-eec4-49c8-8052-497a0a482a85',
'Ada',
'One, two, one two. Do you want to jump with me into a world of magic? I wish I were a vampire... I love Halloween! Will you dance with me under the moon?',
100,
0,
0,
CURRENT_TIMESTAMP(),
0,
200,
200
,'shop'
,'hero'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i3', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel1%2Fal_nf2.png?alt=media&token=fcc5abe7-abb3-4a01-892a-6768f2fd931f',
'Al',
'Hey! You can call me Al. I like soda and video games. I will help you with your tasks anytime. I mean except on weekdays... and only if you grab me a non-alcoholic beverage!',
100,
0,
0,
CURRENT_TIMESTAMP(),
0,
350
,100
,'shop'
,'hero'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i4', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel1%2Feric2.png?alt=media&token=e81ceebe-c2b7-4719-bfb8-cb99cf2606be',
'Eric',
'Good day! Can I do anything for you? I can prepare the best tofu and help you fix up your internet! My super power? I can eat with chopsticks!',
100,
0,
0,
CURRENT_TIMESTAMP(),
100,
200,
100
,'shop'
,'hero'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i5', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel1%2Fjimmy.png?alt=media&token=8fc79ce2-3e3a-43a8-a5f6-0e7585ca191e',
'Jimmy',
'With me, you will accomplish all your goals in no time! We will be the first in everything! I will guide you to greatness! I have helped many great leaders!',
100,
0,
0,
CURRENT_TIMESTAMP(),
200,
0,
200
,'shop'
,'hero'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i6', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel1%2Fstephan.gif?alt=media&token=004035ce-457f-455e-b3c1-4131acc87bdf',
'Stephan',
'I am cool! I am so cool! I will make you cool! Take the chill pill: nature does not hurry yet everything is accomplished! Yep, I am not just cool: I am super spiritual!',
100,
0,
0,
CURRENT_TIMESTAMP(),
0,
200,
200
,'shop'
,'hero'
,'start'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i21', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fyatze2.png?alt=media&token=ff66c52a-e4cb-436d-aae8-4858b51bc9d8',
'Yatze',
'Now you see me. Now you do not. Now you are dead. Dishonor is my blade, ruse my arrow and darkness my shadow.',
200,
0,
0,
CURRENT_TIMESTAMP(),
0,
400,
500
,'shop'
,'hero'
,'ib1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i22', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fzora2.gif?alt=media&token=12e0ecbd-a937-4fdd-91fc-96601d33a099',
'Zora',
'Ole! Wait, why Spanish? I am German. Or am I? Catch me if you can - nobody ever did. Let us do this!',
200,
0,
0,
CURRENT_TIMESTAMP(),
100,
400,
400
,'shop'
,'hero'
,'ib1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i23', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fostrogot.png?alt=media&token=b44d3b00-31ca-4c12-910b-a999f9e2273c',
'Ostrogot',
'Os-tro-got!OS-TRO-GOT! Choose me: cho-ose me! I can speak - hence, I am cle -ever. I will take the hearts of your ennemies - and make them fall in love with you-u',
200,
0,
0,
CURRENT_TIMESTAMP(),
0,
0,
900
,'shop'
,'hero'
,'robot1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i24', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fpan.gif?alt=media&token=74b0f4c4-2da8-45be-b4be-ea4e0b0bb2e4',
'Pan',
'Yaaaaaa - iiiiiii - yaaaaa! Hey there!... Just casually training. I am Pan! Like the God: not the cooking device! You choose me, you do not.. your loss, somebody will!',
200,
0,
0,
CURRENT_TIMESTAMP(),
300,
300,
300
,'shop'
,'hero'
,'ig1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i25', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fpenguy.gif?alt=media&token=f8157cbd-1c59-4390-80a0-2aa9ba3ab5d0',
'Penguy',
'Oh my! Why do penguins always have to cast in movies with cold weather? It is freezing here! I cannot wait for my vacation in the Bahamas. Margarita anyone?',
200,
0,
0,
CURRENT_TIMESTAMP(),
450,
300,
150
,'shop'
,'hero'
,'fairy1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i26', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Frain.png?alt=media&token=420a3261-f397-49ad-850b-8bc60c8be54b',
'Rain',
'Rain... I am the rain... I am the forest... I am the tree. I am the guardian of nature! Moooooooooooooooooooo! Ok, that was my reindeer, not me! Or was it?',
250,
0,
0,
CURRENT_TIMESTAMP(),
500,
300,
150
,'shop'
,'hero'
,'fairy2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i27', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel2%2Fsweet_pony.gif?alt=media&token=56869b05-fb77-43ce-8fa8-8471aeb3fc32',
'Sweet Pony',
'If you invest on the stock market, always diversify your assets: shares, bonds, ETFs... What? You should not judge a book by its cover! I might be a cute pony but I know my way around market finance! I am always discriminated against in this industry! Give me a hug now or I will eat your porridge!',
200,
0,
0,
CURRENT_TIMESTAMP(),
300,
1000,
100
,'shop'
,'hero'
,'fairy3'
,true);


MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i31', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel3%2Firma2.gif?alt=media&token=1c70ad28-6cb0-4130-8e60-72ce3a8ac75a',
'Irma',
'Aaaaaargh... Eeeeeerg... iiiiiirg... Hi there! Do you find me pretty? DO YOU? I love brainy people! You look clever... Clever is the new sexy you know! LET ME EAT YOUR BRAIN!!! I can raise the dead for you... that should help to accomplish your goals huh!',
350,
0,
0,
CURRENT_TIMESTAMP(),
100,
1200,
1200
,'shop'
,'hero'
,'b1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i32', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel3%2Farcadian.png?alt=media&token=7513cb1a-cd29-4fbf-93e1-70b44239f0c8',
'Arcadian',
'Welcome! Many dangers are ahead - many deamons shall be defeated - but know this: the fiercest fight lies within ourselves and only together shall we vanquish.',
350,
0,
0,
CURRENT_TIMESTAMP(),
1500,
500,
1000
,'shop'
,'hero'
,'g2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i33', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel3%2Fbrutus.gif?alt=media&token=d5196e86-017b-498b-8f2b-09916db75fe7',
'Brutus',
'I am Brutus but they call me Legion for we are many! The cohorts of hell shall be defeated by our blades.',
350,
0,
0,
CURRENT_TIMESTAMP(),
900,
600,
1000
,'shop'
,'hero'
,'g1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i34', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel3%2Fmenelas.gif?alt=media&token=b0f5f91a-cab0-4ea1-838e-21e9e408475e',
'Menelas',
'I will guide the legions to the end of the universe if you command. Your goal is my religion and your religion my soul.',
350,
0,
0,
CURRENT_TIMESTAMP(),
1200,
600,
700
,'shop'
,'hero'
,'g1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i35', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel3%2Fparis.gif?alt=media&token=deb16742-e8bf-41b7-853d-8056ad5c6c47',
'Paris',
'My arrow shall strike its target with the fury of heavens: what is your target?',
350,
0,
0,
CURRENT_TIMESTAMP(),
950,
950,
600
,'shop'
,'hero'
,'g1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i36', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel3%2Fsuro.gif?alt=media&token=920c1be3-2726-4956-862c-3b0bb8717f52',
'Suro',
'Sakura! I once lost hope - but now I understand: my destiny is to help you. I will back down before nothing to win over evil - even using their own powers...',
350,
0,
0,
CURRENT_TIMESTAMP(),
300,
1100,
1100
,'shop'
,'hero'
,'ib2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i41', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fdino.png?alt=media&token=5f563714-65bf-43dd-a2a3-16f77ffa491d',
'Dino',
'Bip bip bip! Bip bip bip! 0-1-1-1-1-0-0-1! Translation activated: only I can finish this game! Do you want to stop dreaming? I do not think so',
500,
0,
0,
CURRENT_TIMESTAMP(),
300,
200,
100
,'shop'
,'hero'
,'pixel1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i42', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fdon_quijote.gif?alt=media&token=dbcc95aa-0bbd-4f95-9811-5f90cb17ff38',
'Don Quijote',
'Adventure... Why adventure? I hear them say. If we stop writing the story: do we still exist? What can motivate us if not the yearning for a story to unfold?',
547,
0,
0,
CURRENT_TIMESTAMP(),
1234,
2367,
888
,'shop'
,'hero'
,'ig2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i43', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fgiphy.gif?alt=media&token=0e281f35-23ea-4aa4-9d8b-8090d21323d9',
'Giphy',
'Do not let me expire please! I still have a lot to do in this world! I can freeze... I can freeze anything! Winter shall come and I shall be me again!',
500,
0,
0,
CURRENT_TIMESTAMP(),
750,
1150,
1550
,'shop'
,'hero'
,'ig2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i44', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fjack.gif?alt=media&token=5abeb36b-bc3d-4b66-a1ea-7a787e505a2e',
'Jack',
'Aaaaaah! Tatatatatatatata! Die! Die! I am Jack! Jaaaaaack!',
510,
0,
0,
CURRENT_TIMESTAMP(),
100,
100,
3000
,'shop'
,'hero'
,'ib3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i45', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fknightgor.gif?alt=media&token=2fd9d1b2-0cea-4811-94ea-238d696d2db5',
'Knightgor',
'No way man! How many times should I tell you? Yes, I am a homosexual knight! But no: I do not like rainbows! Crane attack!!! You will think twice next time huh!',
500,
0,
0,
CURRENT_TIMESTAMP(),
1000,
1500,
1000
,'shop'
,'hero'
,'ig3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i46', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Flucian.png?alt=media&token=4bf1d66a-e883-41d4-adb7-a134d37608a1',
'Lucian',
'Wait, you really thought I was a plastic figurine!? AHAHAH! Classic magician technique! You are a fool! Wait... I cannot move! I cannot moooooove!!',
555,
0,
0,
CURRENT_TIMESTAMP(),
800,
2555,
0
,'shop'
,'hero'
,'mg1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i47', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fmatyatsu.gif?alt=media&token=08c4b485-7a0f-4a5f-9307-0192f402c559',
'Matyatsu',
'Meooooooow! They say cats would rule the world had they got opposable thumbs... now, I just got myself a nuclear blaster! What is next? Meooooooow!',
501,
0,
0,
CURRENT_TIMESTAMP(),
500,
1500,
1500
,'shop'
,'hero'
,'ib3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i48', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Foutini.gif?alt=media&token=4763f122-c7fb-4388-9e65-1d365a21cc6b',
'Outini',
'Outini! Latsi koramanaka? Chi chi yata ah! Done! Done! Done! All you wishes just came true!',
501,
0,
0,
CURRENT_TIMESTAMP(),
500,
1500,
1500
,'shop'
,'hero'
,'mb1'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i49', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fzak.gif?alt=media&token=9566e09e-aa0c-48ae-8f9e-b579d3f0eb19',
'Zak',
'Irmaaaaaaa! You summoned me! Together agaaaain... Beyond centuries... Beyond death... Ghosts, zombies: reep their souls! Feed on their hopes and nourish their fears! Let us unleash hell!',
466,
0,
0,
CURRENT_TIMESTAMP(),
0,
1250,
1750
,'shop'
,'hero'
,'b2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i410', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fsara_poke.gif?alt=media&token=69cb811c-388b-429c-915f-1f61684ffe52',
'Sara',
'I love tomatoes! Tomato, go! We have to catch them all: all the tomatoes! Imagine how great of a sauce we can make! What? It is not a tomato? What CAN it be then?',
450,
0,
0,
CURRENT_TIMESTAMP(),
1000,
1500,
1000
,'shop'
,'hero'
,'ig3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i411', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fsir_leon.gif?alt=media&token=dfa62ffe-b389-4883-892e-78330854dd3e',
'Sir Leon',
'What should I do? Stop beating around the bush: do you have a job for me? I have nothing to lose anymore. I know you now! So you had better buy me out...',
546,
0,
0,
CURRENT_TIMESTAMP(),
200,
1650,
1650
,'shop'
,'hero'
,'ib3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i412', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Froy.gif?alt=media&token=6a55f612-0589-4e32-8a22-85e9603dac55',
'Roy',
'I lost my legs... I gained four of them back! And another brain... and a hell of an artillery! Now, where are we headed?',
595,
0,
0,
CURRENT_TIMESTAMP(),
500,
1700,
1700
,'shop'
,'hero'
,'robot2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i413', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel4%2Fpixelou.png?alt=media&token=284afcab-9b5d-49d8-818e-edb4bdd131d6',
'Pixelou',
'Hey sis / bro! I can get you any pixel you want in this town - if you see what I mean... You do not see what I mean?! Get out of here! Bip your way out! Bip bip bip bip... Translation over.',
1000,
0,
0,
CURRENT_TIMESTAMP(),
3000,
3000,
3000
,'shop'
,'hero'
,'pixel2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i51', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Fshatan2.gif?alt=media&token=cc4ab467-1ade-45f7-8906-c1b7e0630f9f',
'Shatan',
'The only way to get rid of a temptation is to yield to it... Let it burn yourself - come to me! I am the true path for only your sins can wash off your self! Dance my friend! Never stop entertaining! Our thirst shall be relentless!',
666,
0,
0,
CURRENT_TIMESTAMP(),
0,
3000,
1500
,'shop'
,'hero'
,'b3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i52', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Farumbosh.gif?alt=media&token=53ab9e46-f255-44f3-9c38-07a1aa803c6e',
'Arumbosh',
'I just put on my iron shirt! Where is the devil? I am gonna take the him out of earth!',
630,
0,
0,
CURRENT_TIMESTAMP(),
1500,
1500,
1500
,'shop'
,'hero'
,'ig4'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i53', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Fblue_shadow.gif?alt=media&token=43d50ea3-2e8f-4965-87ee-61d7662bef1c',
'Blue Shadow',
'I am sorrow and despair... The courage of your foes will shatter like the mirror of their broken dreams',
650,
0,
0,
CURRENT_TIMESTAMP(),
500,
2500,
1500
,'shop'
,'hero'
,'mb2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i54', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Fbrad.gif?alt=media&token=9cb31dce-d770-404b-bbf3-02ad92639b97',
'Brad',
'Lies, treachury, compromises, fear, doubt: take this! The sun will rise again tomorrow but you shall soon be forgotten',
700,
0,
0,
CURRENT_TIMESTAMP(),
1500,
1500,
2000
,'shop'
,'hero'
,'mg2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i55', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Fgrundolf.gif?alt=media&token=c923fc35-73a4-4032-893d-6d2ee2251c6e',
'Grundof',
'Magic? There is no such thing as magic! There is only the power of will - and the power to choose what to do with the time we have! Ah! Yes, and magic teleportation also - and all the other spells I came accross',
700,
0,
0,
CURRENT_TIMESTAMP(),
500,
3000,
1500
,'shop'
,'hero'
,'mg2'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i56', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Flence.gif?alt=media&token=83457040-edb8-4458-a28e-909775c6b861',
'Lence',
'Blaster maximum power. What can sword and magic do against our latest lazers? They can burn. The good should become the strong.',
700,
0,
0,
CURRENT_TIMESTAMP(),
1000,
1000,
3000
,'shop'
,'hero'
,'ig4'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i57', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Fmiramar2.png?alt=media&token=b0eac7ae-d08b-4228-85e9-070ed4aab5d6',
'Miramar',
'Black magic is an art which consumes the soul: but who needs a soul when one is immortal!',
700,
0,
0,
CURRENT_TIMESTAMP(),
100,
1900,
3000
,'shop'
,'hero'
,'mb3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i58', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel5%2Fnikia.gif?alt=media&token=208d7636-3266-427b-b221-1de1ff729ae7',
'Nikia',
'I belong to the order of the Shinigamis. My magic sword is made to kill deamons but also to guide good ghosts to heaven',
800,
0,
0,
CURRENT_TIMESTAMP(),
500,
2000,
3000
,'shop'
,'hero'
,'ib4'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i61', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel6%2Factibus.gif?alt=media&token=c4bf7725-47cf-4be1-b5ca-14681c27e506',
'Actibus',
'No hell can pierce my armor - and no angel can stop my fire. I will protect man kind against evil at all costs',
1500,
0,
0,
CURRENT_TIMESTAMP(),
2000,
2000,
3000
,'shop'
,'hero'
,'ig5'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i62', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel6%2Farashtor.gif?alt=media&token=2c260f3c-a379-42b4-a1e2-cf439edef5ec',
'Arashtor',
'Perish you fools - for you have awoken the scourge of the earth, the deamon of the deamons, the burner of worlds, the devourer of souls...',
1600,
0,
0,
CURRENT_TIMESTAMP(),
500,
2000,
4000
,'shop'
,'hero'
,'b4'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i63', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel6%2Farubar.webp?alt=media&token=cb2b46bf-3766-4102-9806-091688d04d6d',
'Arubar',
'I am just a big blue giant jin who will kick the hell out of anything! Deamons, angels, spirits, death itself... I have no problem! So bring it on',
1500,
0,
0,
CURRENT_TIMESTAMP(),
500,
1500,
4500
,'shop'
,'hero'
,'b4'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i64', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel6%2Fneji.gif?alt=media&token=613bb4de-9fc3-4c3c-9b42-921ea73ae7ba',
'Neji',
'Soul and body together can move the air and bend the earth. Inner peace is key - movement is an art. Power is just a consequence',
1700,
0,
0,
CURRENT_TIMESTAMP(),
1000,
2000,
4000
,'shop'
,'hero'
,'mg3'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i65', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel6%2Freaper.gif?alt=media&token=c02734c2-0f50-4425-88a3-950f22b4f658',
'Reaper',
'You can laugh, you can enjoy... You can fear, you can complain... In the end, I am your only friend for eventually, I will be your last friend - and only I will be the one holding your hand on your last breath',
1800,
0,
0,
CURRENT_TIMESTAMP(),
1000,
5500,
500
,'shop'
,'hero'
,'b5'
,true);

MERGE INTO SHOPOBJECT(ID_SHOP_OBJECT, URL, TITLE, DESCRIPTION, price, love_price, dark_price, CREATED_AT, reputation, intelligence, fitness, image_type, shop_type, shop_rule, is_unique)
VALUES('i66', 
'https://firebasestorage.googleapis.com/v0/b/taskgame-225414.appspot.com/o/heros%2Flevel6%2Fswatika.png?alt=media&token=61414af7-5e8c-47df-8ba0-10d73f2bf4d6',
'Swatika',
'Life is balance. Death is only a passage but life... life goes on! I am the water of life - the spring of light. Destruction is just another state of matter. Energy cannot be destroyed: I am the energy of the universe.',
2150,
0,
0,
CURRENT_TIMESTAMP(),
5000,
5000,
500
,'shop'
,'hero'
,'g5'
,true);

