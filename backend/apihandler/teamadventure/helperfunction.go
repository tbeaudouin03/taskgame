package teamadventure

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"
)

func ResetHeroMonsterState(ctx context.Context, db *sql.DB, iDHero string) {

	stmtStr := `
		UPDATE PUBLIC.Hero
		SET monster_is_introduced = false
		,is_fight_accepted = false
		,is_fight_started = false
		WHERE id_hero = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

func ResetTeamFollowerMonsterState(ctx context.Context, db *sql.DB, iDHero string) {

	stmtStr := `
		UPDATE PUBLIC.Hero
		SET monster_is_introduced = false
		,is_fight_accepted = false
		,is_fight_started = false
		WHERE id_team = (select id_team from public.hero where id_hero = ?)
		AND (IS_BONDED_TEAM_LEADER = true OR IS_BONDED_TO_TEAM = true)
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
