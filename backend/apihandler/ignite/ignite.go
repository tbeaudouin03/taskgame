package ignite

import (
	"context"
	"database/sql"
	"encoding/json"
	"io/ioutil"

	"log"

	_ "github.com/amsokol/ignite-go-client/sql"
)

type config struct {
	IgniteHost     string `json:"ignite_host"`
	IgniteUser     string `json:"ignite_user"`
	IgnitePw       string `json:"ignite_pw"`
	IgnitePageSize string `json:"ignite_page_size"`
	IgniteTimeout  string `json:"ignite_timeout"`
	IgniteVersion  string `json:"ignite_version"`
}

func IgniteConnect(ctx context.Context) *sql.DB {

	configByte, _ := ioutil.ReadFile("config.json")
	var config config
	err := json.Unmarshal(configByte, &config)
	if err != nil {
		log.Printf("could not unmarshal config to json: %v", err)
	}

	// open connection
	db, err := sql.Open("ignite", "tcp://"+config.IgniteHost+"/TaskCache?"+
		"version="+config.IgniteVersion+
		"&username="+config.IgniteUser+
		"&password="+config.IgnitePw+
		"&page-size="+config.IgnitePageSize+
		"&timeout="+config.IgniteTimeout)
	if err != nil {
		log.Printf("failed to open connection: %v", err)
	}

	// ping
	if err = db.PingContext(ctx); err != nil {
		log.Printf("ping failed: %v", err)
	}

	return db
}
