package leaderboard

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type LeaderBoardData struct {
	LeaderBoardObjects []LeaderBoardObject
}

type LeaderBoardObject struct {
	Key                  string `json:"key"`
	GivenName            string
	Mood                 string
	Status               string
	Email                string
	FacebookProfile      string
	LinkedinProfile      string
	URL                  string
	Level                int
	JudgeLevel           int
	Coins                int
	LoveEnergy           int
	DarkEnergy           int
	Reputation           int
	Intelligence         int
	Fitness              int
	TaskCount            int
	TaskVerifyCount      int
	JudgementCount       int
	JudgementVerifyCount int
	IDToken              string
	IDHero               string
	LeaderBoardType      string
	Score                int
	JudgeScore           int
	TeamName             string
	IsBondedTeamLeader   bool
	IsBondedToTeam       bool
	IsFightCompanion     bool
	LastDamageToMonster  int
}

func (leaderBoardData *LeaderBoardData) AddLeaderBoardObject(leaderBoardObject LeaderBoardObject) {
	leaderBoardData.LeaderBoardObjects = append(leaderBoardData.LeaderBoardObjects, leaderBoardObject)
}

func GetLeaderBoardData(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")
	iDTeam := c.Request.Header.Get("IDTeam")

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	var leaderBoardData LeaderBoardData

	if iDTeam == "" {

		getLeaderBoardDataIgnite(ctx, db, &leaderBoardData)
		leaderBoardDataByte, err := json.Marshal(leaderBoardData)
		if err != nil {
			fmt.Println(err)
		}
		c.Writer.Write(leaderBoardDataByte)

	} else {

		getTeamLeaderBoardDataIgnite(ctx, db, &leaderBoardData, iDTeam, heroInfo.IDHero)
		leaderBoardDataByte, err := json.Marshal(leaderBoardData)
		if err != nil {
			fmt.Println(err)
		}
		c.Writer.Write(leaderBoardDataByte)

	}
}

func getTeamLeaderBoardDataIgnite(ctx context.Context, db *sql.DB, leaderBoardData *LeaderBoardData, iDTeam string, iDHero string) {

	stmtStr := `
	WITH TopHero AS (
		SELECT TOP 30
		COALESCE(ID_Hero,'') ID_Hero
		,COALESCE(Given_Name,'') Given_Name
		,COALESCE(Mood,'') Mood
		,COALESCE(Status,'') Status
		,COALESCE(Email,'') Email
		,COALESCE(Facebook_Profile,'') Facebook_Profile
		,COALESCE(Linkedin_Profile,'') Linkedin_Profile
		,COALESCE(is_bonded_team_leader,false) is_bonded_team_leader
		,COALESCE(is_bonded_to_team,false) is_bonded_to_team
		,COALESCE(URL,'') URL
		,COALESCE(Level,0) Level
		,COALESCE(Judge_Level,0) Judge_Level
		,COALESCE(Coins,0) Coins
		,COALESCE(Love_Energy,0) Love_Energy
		,COALESCE(Dark_Energy,0) Dark_Energy
		,COALESCE(Reputation,0) Reputation
		,COALESCE(Intelligence,0) Intelligence
		,COALESCE(Fitness,0) Fitness
		,COALESCE(Task_Count,0) Task_Count
		,COALESCE(Task_Verify_Count,0) Task_Verify_Count
		,COALESCE(Judgement_Count,0) Judgement_Count
		,COALESCE(Judgement_Verify_Count,0) Judgement_Verify_Count
		,COALESCE(Leader_Board_Type,'') Leader_Board_Type
		,COALESCE(Score,0) Score
		,COALESCE(Judge_Score,0) Judge_Score
		FROM
		(
		SELECT
		hro.ID_Hero
		,hro.Given_Name
		,hro.Mood
		,hro.Status
		,hro.Email
		,hro.Facebook_Profile
		,hro.Linkedin_Profile
		,hro.is_bonded_team_leader
		,hro.is_bonded_to_team
		,sho.URL
		,hrs.Level
		,hrs.Judge_Level
		,hrs.Coins
		,hrs.Love_Energy
		,hrs.Dark_Energy
		,hrs.Reputation
		,hrs.Intelligence
		,hrs.Fitness
		,hrs.Task_Count
		,hrs.Task_Verify_Count
		,hrs.Judgement_Count
		,hrs.Judgement_Verify_Count
		,'hero' Leader_Board_Type
		,hrs.Score
		,hrs.Judge_Score
		,random() rand_rank
		
		FROM PUBLIC.Hero hro
		JOIN PUBLIC.ShopObject sho
		ON hro.ID_Image = sho.id_shop_object
		JOIN PUBLIC.HeroStat hrs
		ON hro.ID_Hero = hrs.ID_Hero
		WHERE hro.ID_TEAM = ?
		) temp
		ORDER BY Score DESC, rand_rank
		),
		TopJudge AS (
		SELECT TOP 30
		COALESCE(ID_Hero,'') ID_Hero
		,COALESCE(Given_Name,'') Given_Name
		,COALESCE(Mood,'') Mood
		,COALESCE(Status,'') Status
		,COALESCE(Email,'') Email
		,COALESCE(Facebook_Profile,'') Facebook_Profile
		,COALESCE(Linkedin_Profile,'') Linkedin_Profile
		,COALESCE(is_bonded_team_leader,false) is_bonded_team_leader
		,COALESCE(is_bonded_to_team,false) is_bonded_to_team
		,COALESCE(URL,'') URL
		,COALESCE(Level,0) Level
		,COALESCE(Judge_Level,0) Judge_Level
		,COALESCE(Coins,0) Coins
		,COALESCE(Love_Energy,0) Love_Energy
		,COALESCE(Dark_Energy,0) Dark_Energy
		,COALESCE(Reputation,0) Reputation
		,COALESCE(Intelligence,0) Intelligence
		,COALESCE(Fitness,0) Fitness
		,COALESCE(Task_Count,0) Task_Count
		,COALESCE(Task_Verify_Count,0) Task_Verify_Count
		,COALESCE(Judgement_Count,0) Judgement_Count
		,COALESCE(Judgement_Verify_Count,0) Judgement_Verify_Count
		,COALESCE(Leader_Board_Type,'') Leader_Board_Type
		,COALESCE(Score,0) Score
		,COALESCE(Judge_Score,0) Judge_Score
		FROM
		(
		SELECT
		hro.ID_Hero
		,hro.Given_Name
		,hro.Mood
		,hro.Status
		,hro.Email
		,hro.Facebook_Profile
		,hro.Linkedin_Profile
		,hro.is_bonded_team_leader
		,hro.is_bonded_to_team
		,sho.URL
		,hrs.Level
		,hrs.Judge_Level
		,hrs.Coins
		,hrs.Love_Energy
		,hrs.Dark_Energy
		,hrs.Reputation
		,hrs.Intelligence
		,hrs.Fitness
		,hrs.Task_Count
		,hrs.Task_Verify_Count
		,hrs.Judgement_Count
		,hrs.Judgement_Verify_Count
		,'judge' Leader_Board_Type
		,hrs.Score
		,hrs.Judge_Score
		,random() rand_rank
		
		FROM PUBLIC.Hero hro
		JOIN PUBLIC.ShopObject sho
		ON hro.ID_Image = sho.id_shop_object
		JOIN PUBLIC.HeroStat hrs
		ON hro.ID_Hero = hrs.ID_Hero
		WHERE hro.ID_TEAM = ?
		) temp
		ORDER BY Judge_Score DESC, rand_rank
		),
		HeroCombatParty0 AS (
		SELECT
		id_hero_companion
		,is_active
		,last_damage_to_monster
		FROM PUBLIC.HeroCombatParty
		WHERE id_hero_primary = ?
)
		SELECT 
		COALESCE(ID_Hero,'') ID_Hero
		,COALESCE(Given_Name,'') Given_Name
		,COALESCE(Mood,'') Mood
		,COALESCE(Status,'') Status
		,COALESCE(Email,'') Email
		,COALESCE(Facebook_Profile,'') Facebook_Profile
		,COALESCE(Linkedin_Profile,'') Linkedin_Profile
		,COALESCE(is_bonded_team_leader,false) is_bonded_team_leader
		,COALESCE(is_bonded_to_team,false) is_bonded_to_team
		,COALESCE(URL,'') URL
		,COALESCE(Level,0) Level
		,COALESCE(Judge_Level,0) Judge_Level
		,COALESCE(Coins,0) Coins
		,COALESCE(Love_Energy,0) Love_Energy
		,COALESCE(Dark_Energy,0) Dark_Energy
		,COALESCE(Reputation,0) Reputation
		,COALESCE(Intelligence,0) Intelligence
		,COALESCE(Fitness,0) Fitness
		,COALESCE(Task_Count,0) Task_Count
		,COALESCE(Task_Verify_Count,0) Task_Verify_Count
		,COALESCE(Judgement_Count,0) Judgement_Count
		,COALESCE(Judgement_Verify_Count,0) Judgement_Verify_Count
		,COALESCE(Leader_Board_Type,'') Leader_Board_Type
		,COALESCE(Score,0) Score
		,COALESCE(Judge_Score,0) Judge_Score
	  ,COALESCE(hcp.is_active,false) IsFightCompanion
		,COALESCE(hcp.last_damage_to_monster,0) LastDamageToMonster
		FROM
		(
		SELECT * FROM TopHero 
		UNION ALL 
		SELECT * FROM TopJudge
		) top
		LEFT JOIN HeroCombatParty0 hcp
		ON top.id_hero = hcp.id_hero_companion
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDTeam, iDTeam, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var leaderBoardObject LeaderBoardObject

	for rows.Next() {
		if err := rows.Scan(
			&leaderBoardObject.Key,
			&leaderBoardObject.GivenName,
			&leaderBoardObject.Mood,
			&leaderBoardObject.Status,
			&leaderBoardObject.Email,
			&leaderBoardObject.FacebookProfile,
			&leaderBoardObject.LinkedinProfile,
			&leaderBoardObject.IsBondedTeamLeader,
			&leaderBoardObject.IsBondedToTeam,
			&leaderBoardObject.URL,
			&leaderBoardObject.Level,
			&leaderBoardObject.JudgeLevel,
			&leaderBoardObject.Coins,
			&leaderBoardObject.LoveEnergy,
			&leaderBoardObject.DarkEnergy,
			&leaderBoardObject.Reputation,
			&leaderBoardObject.Intelligence,
			&leaderBoardObject.Fitness,
			&leaderBoardObject.TaskCount,
			&leaderBoardObject.TaskVerifyCount,
			&leaderBoardObject.JudgementCount,
			&leaderBoardObject.JudgementVerifyCount,
			&leaderBoardObject.LeaderBoardType,
			&leaderBoardObject.Score,
			&leaderBoardObject.JudgeScore,
			&leaderBoardObject.IsFightCompanion,
			&leaderBoardObject.LastDamageToMonster,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		leaderBoardData.AddLeaderBoardObject(leaderBoardObject)

	}

}

func getLeaderBoardDataIgnite(ctx context.Context, db *sql.DB, leaderBoardData *LeaderBoardData) {

	stmtStr := `
	WITH TopHero AS (
		SELECT TOP 30
		COALESCE(ID_Hero,'') ID_Hero
		,COALESCE(Given_Name,'') Given_Name
		,COALESCE(Mood,'') Mood
		,COALESCE(Status,'') Status
		,COALESCE(Email,'') Email
		,COALESCE(Facebook_Profile,'') Facebook_Profile
		,COALESCE(Linkedin_Profile,'') Linkedin_Profile
		,COALESCE(is_bonded_team_leader,false) is_bonded_team_leader
		,COALESCE(is_bonded_to_team,false) is_bonded_to_team
		,COALESCE(URL,'') URL
		,COALESCE(Level,0) Level
		,COALESCE(Judge_Level,0) Judge_Level
		,COALESCE(Coins,0) Coins
		,COALESCE(Love_Energy,0) Love_Energy
		,COALESCE(Dark_Energy,0) Dark_Energy
		,COALESCE(Reputation,0) Reputation
		,COALESCE(Intelligence,0) Intelligence
		,COALESCE(Fitness,0) Fitness
		,COALESCE(Task_Count,0) Task_Count
		,COALESCE(Task_Verify_Count,0) Task_Verify_Count
		,COALESCE(Judgement_Count,0) Judgement_Count
		,COALESCE(Judgement_Verify_Count,0) Judgement_Verify_Count
		,COALESCE(Leader_Board_Type,'') Leader_Board_Type
		,COALESCE(Score,0) Score
		,COALESCE(Judge_Score,0) Judge_Score
		,COALESCE(team_name,'') team_name
		FROM
		(
		SELECT
		hro.ID_Hero
		,hro.Given_Name
		,hro.Mood
		,hro.Status
		,hro.Email
		,hro.Facebook_Profile
		,hro.Linkedin_Profile
		,hro.is_bonded_team_leader
		,hro.is_bonded_to_team
		,sho.URL
		,hrs.Level
		,hrs.Judge_Level
		,hrs.Coins
		,hrs.Love_Energy
		,hrs.Dark_Energy
		,hrs.Reputation
		,hrs.Intelligence
		,hrs.Fitness
		,hrs.Task_Count
		,hrs.Task_Verify_Count
		,hrs.Judgement_Count
		,hrs.Judgement_Verify_Count
		,'hero' Leader_Board_Type
		,hrs.Score
		,hrs.Judge_Score
		,random() rand_rank
		,tm.name team_name
		
		FROM PUBLIC.Hero hro
		JOIN PUBLIC.ShopObject sho
		ON hro.ID_Image = sho.id_shop_object
		JOIN PUBLIC.HeroStat hrs
		ON hro.ID_Hero = hrs.ID_Hero
		LEFT JOIN PUBLIC.Team tm
		ON tm.id_team = hro.id_team
		) temp
		ORDER BY Score DESC, rand_rank
		),
		TopJudge AS (
		SELECT TOP 30
		COALESCE(ID_Hero,'') ID_Hero
		,COALESCE(Given_Name,'') Given_Name
		,COALESCE(Mood,'') Mood
		,COALESCE(Status,'') Status
		,COALESCE(Email,'') Email
		,COALESCE(Facebook_Profile,'') Facebook_Profile
		,COALESCE(Linkedin_Profile,'') Linkedin_Profile
		,COALESCE(is_bonded_team_leader,false) is_bonded_team_leader
		,COALESCE(is_bonded_to_team,false) is_bonded_to_team
		,COALESCE(URL,'') URL
		,COALESCE(Level,0) Level
		,COALESCE(Judge_Level,0) Judge_Level
		,COALESCE(Coins,0) Coins
		,COALESCE(Love_Energy,0) Love_Energy
		,COALESCE(Dark_Energy,0) Dark_Energy
		,COALESCE(Reputation,0) Reputation
		,COALESCE(Intelligence,0) Intelligence
		,COALESCE(Fitness,0) Fitness
		,COALESCE(Task_Count,0) Task_Count
		,COALESCE(Task_Verify_Count,0) Task_Verify_Count
		,COALESCE(Judgement_Count,0) Judgement_Count
		,COALESCE(Judgement_Verify_Count,0) Judgement_Verify_Count
		,COALESCE(Leader_Board_Type,'') Leader_Board_Type
		,COALESCE(Score,0) Score
		,COALESCE(Judge_Score,0) Judge_Score
    	,COALESCE(team_name,'') team_name
		FROM
		(
		SELECT
		hro.ID_Hero
		,hro.Given_Name
		,hro.Mood
		,hro.Status
		,hro.Email
		,hro.Facebook_Profile
		,hro.Linkedin_Profile
		,hro.is_bonded_team_leader
		,hro.is_bonded_to_team
		,sho.URL
		,hrs.Level
		,hrs.Judge_Level
		,hrs.Coins
		,hrs.Love_Energy
		,hrs.Dark_Energy
		,hrs.Reputation
		,hrs.Intelligence
		,hrs.Fitness
		,hrs.Task_Count
		,hrs.Task_Verify_Count
		,hrs.Judgement_Count
		,hrs.Judgement_Verify_Count
		,'judge' Leader_Board_Type
		,hrs.Score
		,hrs.Judge_Score
		,random() rand_rank
		,tm.name team_name
		
		FROM PUBLIC.Hero hro
		JOIN PUBLIC.ShopObject sho
		ON hro.ID_Image = sho.id_shop_object
		JOIN PUBLIC.HeroStat hrs
		ON hro.ID_Hero = hrs.ID_Hero
		LEFT JOIN PUBLIC.Team tm
		ON tm.id_team = hro.id_team
		) temp
		ORDER BY Judge_Score DESC, rand_rank
		)
		
		SELECT * FROM TopHero 
		UNION ALL 
		SELECT * FROM TopJudge;

	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var leaderBoardObject LeaderBoardObject

	for rows.Next() {
		if err := rows.Scan(
			&leaderBoardObject.Key,
			&leaderBoardObject.GivenName,
			&leaderBoardObject.Mood,
			&leaderBoardObject.Status,
			&leaderBoardObject.Email,
			&leaderBoardObject.FacebookProfile,
			&leaderBoardObject.LinkedinProfile,
			&leaderBoardObject.IsBondedTeamLeader,
			&leaderBoardObject.IsBondedToTeam,
			&leaderBoardObject.URL,
			&leaderBoardObject.Level,
			&leaderBoardObject.JudgeLevel,
			&leaderBoardObject.Coins,
			&leaderBoardObject.LoveEnergy,
			&leaderBoardObject.DarkEnergy,
			&leaderBoardObject.Reputation,
			&leaderBoardObject.Intelligence,
			&leaderBoardObject.Fitness,
			&leaderBoardObject.TaskCount,
			&leaderBoardObject.TaskVerifyCount,
			&leaderBoardObject.JudgementCount,
			&leaderBoardObject.JudgementVerifyCount,
			&leaderBoardObject.LeaderBoardType,
			&leaderBoardObject.Score,
			&leaderBoardObject.JudgeScore,
			&leaderBoardObject.TeamName,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		leaderBoardData.AddLeaderBoardObject(leaderBoardObject)

	}

}
