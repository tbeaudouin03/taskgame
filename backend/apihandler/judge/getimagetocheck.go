package judge

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type ImageToCheck struct {
	IDTaskCheck string
	IDTask1     string
	IDTask2     string
	IDHero      string
	Title       string
	URL         string
}

func GetImageToCheck(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	imageToCheck := getImageToCheckIgnite(ctx, db, heroInfo.IDHero)

	//Convert the `purchaseRequestFormInputTable` variable to json
	imageToCheckByte, err := json.Marshal(imageToCheck)
	if err != nil {
		fmt.Println(err)
	}

	// If all goes well, write the JSON list of purchaseRequestFormInputTable to the response
	c.Writer.Write(imageToCheckByte)
}

func getImageToCheckIgnite(ctx context.Context, db *sql.DB, iDHero string) []ImageToCheck {

	stmtStr := `SELECT TOP 20
	ID_TASK_CHECK
	,ID_TASK_1
	,ID_TASK_2
	,ID_HERO
	,TITLE
	,URL 
	FROM
	(
	SELECT 
	tsc.ID_TASK_CHECK
	,tsc.ID_TASK_1
	,tsc.ID_TASK_2
	,tsc.ID_HERO
	,tsc.TITLE
	,img.URL
	,RAND() RAND_RANK
	FROM PUBLIC.TASKCHECK tsc
	JOIN PUBLIC.IMAGE img
	ON tsc.ID_IMAGE = img.ID_IMAGE
	WHERE tsc.IS_VERIFIED <> true
	AND tsc.ID_HERO <> ?
	AND tsc.ID_TASK_CHECK NOT IN ( SELECT ID_TASK_CHECK FROM JUDGEOPINION WHERE ID_JUDGE = ?)
	) temp
	
	ORDER BY RAND_RANK`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	var imageToCheck ImageToCheck
	var arrayOfImageToCheck []ImageToCheck

	for rows.Next() {
		if err := rows.Scan(
			&imageToCheck.IDTaskCheck,
			&imageToCheck.IDTask1,
			&imageToCheck.IDTask2,
			&imageToCheck.IDHero,
			&imageToCheck.Title,
			&imageToCheck.URL,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		arrayOfImageToCheck = append(arrayOfImageToCheck, imageToCheck)

	}

	return arrayOfImageToCheck

}
