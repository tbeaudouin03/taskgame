package judge

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"
	"math/rand"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type JudgeOpinion struct {
	CoinValueChosen int
	SkillChosen     string
	IDTaskCheck     string
	IDTask1         string
	IDTask2         string
	IDHero          string // this IDHero is the ID of the Hero BEING JUDGED, not the ID of the judge
	IDToken         string
}

type JudgeReputationCoinsEnergy struct {
	IDJudge    string
	Reputation int
	Coins      int
	LoveEnergy int
	DarkEnergy int
}

func PostJudgement(c *gin.Context) {

	JudgeOpinion := new(JudgeOpinion)
	err := c.Bind(JudgeOpinion)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(JudgeOpinion.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// heroInfo.IDHero is IDJudge while JudgeOpinion.IDHero is the ID of the Hero BEING JUDGED
	postJudgementIgnite(db, ctx, JudgeOpinion, heroInfo.IDHero)
	isEnoughJudgement := isEnoughJudgement(db, ctx, JudgeOpinion.IDTaskCheck)

	log.Println(`isEnoughJudgement: `, isEnoughJudgement)
	// if isEnoughJudgement then calculate the resulting bonuses in taskrate
	if isEnoughJudgement {
		verifyTaskAndDistributePoint(db, ctx, JudgeOpinion, heroInfo.IDHero)
	}

}

func postJudgementIgnite(db *sql.DB, ctx context.Context, JudgeOpinion *JudgeOpinion, iDJudge string) {

	stmtStr := `MERGE INTO PUBLIC.JUDGEOPINION(
		ID_JUDGE_OPINION
		, ID_JUDGE
		, ID_TASK_CHECK
		, ID_HERO
		, ID_TASK_1
		, ID_TASK_2
		, CREATED_AT
		, UPDATED_AT
		, COINS
		, MAIN_SKILL) VALUES (?,?,?,?,?,?,current_timestamp(), current_timestamp(), ?,?)`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		iDJudge+JudgeOpinion.IDTaskCheck,
		iDJudge,
		JudgeOpinion.IDTaskCheck,
		JudgeOpinion.IDHero,
		JudgeOpinion.IDTask1,
		JudgeOpinion.IDTask2,
		JudgeOpinion.CoinValueChosen,
		JudgeOpinion.SkillChosen,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// add to judgement_count in taskrate
	stmtStr = `UPDATE PUBLIC.taskrate set judgement_count = (
		select 
		COALESCE(judgement_count,0) + 1 JudgementCount
		from PUBLIC.taskrate
		where 
		id_hero = ?
		and id_task_1 = ?
		and id_task_2 = ?
		)
		where id_hero = ?
		and id_task_1 = ?
		and id_task_2 = ?
		`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		JudgeOpinion.IDHero,
		JudgeOpinion.IDTask1,
		JudgeOpinion.IDTask2,
		JudgeOpinion.IDHero,
		JudgeOpinion.IDTask1,
		JudgeOpinion.IDTask2,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func isEnoughJudgement(db *sql.DB, ctx context.Context, iDTaskCheck string) (isEnoughJudgement bool) {

	stmtStr := `WITH CountActiveHero AS (SELECT COUNT(ID_HERO)-1 CountActiveHero FROM PUBLIC.HERO),
	SomeCount AS (SELECT COUNT(ID_TASK_CHECK) SomeCount FROM PUBLIC.JUDGEOPINION WHERE COINS = 3 AND ID_TASK_CHECK = ?),
	LotCount AS (SELECT COUNT(ID_TASK_CHECK) LotCount FROM PUBLIC.JUDGEOPINION WHERE COINS = 4 AND ID_TASK_CHECK = ?),
	EvenMoreCount AS (SELECT COUNT(ID_TASK_CHECK) EvenMoreCount FROM PUBLIC.JUDGEOPINION WHERE COINS = 5 AND ID_TASK_CHECK = ?),
	TotalCount AS (SELECT COUNT(ID_TASK_CHECK) TotalCount FROM PUBLIC.JUDGEOPINION WHERE ID_TASK_CHECK = ?),
	
	RequiredJudgementCount AS (SELECT CEIL(ROUND(3 + SQRT(10 * EvenMoreCount) + SQRT(5 * LotCount) + SQRT(2 * SomeCount))) RequiredJudgementCount
								FROM EvenMoreCount CROSS JOIN LotCount CROSS JOIN SomeCount),
	RequiredJudgementCountWithHero AS (SELECT CASE WHEN RequiredJudgementCount < CountActiveHero THEN RequiredJudgementCount ELSE CountActiveHero END RequiredJudgementCountWithHero
										FROM CountActiveHero CROSS JOIN RequiredJudgementCount)
								
	SELECT 
	CASE WHEN TotalCount >= RequiredJudgementCountWithHero THEN TRUE ELSE FALSE END IsEnoughJudgement
	FROM RequiredJudgementCountWithHero
	CROSS JOIN TotalCount`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&isEnoughJudgement,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return isEnoughJudgement

}

func verifyTaskAndDistributePoint(db *sql.DB, ctx context.Context, JudgeOpinion *JudgeOpinion, iDJudge string) {

	totalSum, reputation, intelligence, fitness := getTotalSumCoinAndSkillValue(db, ctx, JudgeOpinion.IDTaskCheck)

	// totalSum used to give points to hero, judgementMean to give points to judge
	// give points to hero: update taskrate with totalSum, reputation, intelligence and fitness

	if totalSum > 0 {
		givePointToHero(db, ctx, JudgeOpinion, totalSum, reputation, intelligence, fitness)
	}

	// give points to judge: update judgeopinion
	givePointToJudges(db, ctx, JudgeOpinion)

	verifyTaskCheck(db, ctx, JudgeOpinion)

}

func verifyTaskCheck(db *sql.DB, ctx context.Context, JudgeOpinion *JudgeOpinion) {

	// update taskcheck
	stmtStr := `
	UPDATE PUBLIC.TASKCHECK tsc
	SET tsc.IS_VERIFIED = true
	,tsc.VERIFIED_AT = CURRENT_TIMESTAMP()
	WHERE tsc.ID_TASK_CHECK = ?
	AND tsc.ID_HERO = ?
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		JudgeOpinion.IDTaskCheck,
		JudgeOpinion.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// update taskrate
	stmtStr = `
	UPDATE PUBLIC.TASKRATE tsc
	SET tsc.IS_VERIFIED = true
	,tsc.VERIFIED_AT = CURRENT_TIMESTAMP()
	where id_hero = ?
	and id_task_1 = ?
	and id_task_2 = ?
		`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		JudgeOpinion.IDHero,
		JudgeOpinion.IDTask1,
		JudgeOpinion.IDTask2,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

func givePointToJudges(db *sql.DB, ctx context.Context, JudgeOpinion *JudgeOpinion) {

	judgeReputationCoinsEnergyTable := getJudgesReputationCoinsEnergy(db, ctx, JudgeOpinion.IDTaskCheck)

	for _, judgeReputationCoinsEnergy := range judgeReputationCoinsEnergyTable {

		stmtStr := `UPDATE PUBLIC.JUDGEOPINION jo
		SET jo.JUDGE_COINS = ?
		,jo.Judge_Love = ?
		,jo.Judge_Dark = ?
		,jo.JUDGE_REPUTATION = ?
		WHERE jo.ID_JUDGE = ?
		AND jo.ID_TASK_CHECK = ?
		`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			judgeReputationCoinsEnergy.Coins,
			judgeReputationCoinsEnergy.LoveEnergy,
			judgeReputationCoinsEnergy.DarkEnergy,
			judgeReputationCoinsEnergy.Reputation,
			judgeReputationCoinsEnergy.IDJudge,
			JudgeOpinion.IDTaskCheck,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}

	}

}

func getJudgesReputationCoinsEnergy(db *sql.DB, ctx context.Context, iDTaskCheck string) []JudgeReputationCoinsEnergy {

	stmtStr := `WITH JudgementMean AS (SELECT AVG(COINS) JudgementMean FROM PUBLIC.JUDGEOPINION WHERE ID_TASK_CHECK = ?),
	JudgeRatio AS (
	SELECT 
	ID_JUDGE
	,CEIL(((5/(0.5 +ABS(COINS - JudgementMean))) - ABS(COINS - JudgementMean)) * 10) JudgeRatio
	
	FROM PUBLIC.JUDGEOPINION
	CROSS JOIN JudgementMean
	WHERE ID_TASK_CHECK = ?
	)
	
	SELECT 
	ID_JUDGE
	, CEIL(JudgeRatio * 5 * (3 + RAND())/100) Reputation
	, CEIL((JudgeRatio + 20) * (10 + RAND())) Coins
	FROM JudgeRatio
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDTaskCheck,
		iDTaskCheck,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	var judgeReputationCoinsEnergy JudgeReputationCoinsEnergy
	var judgeReputationCoinsEnergyTable []JudgeReputationCoinsEnergy
	for rows.Next() {
		if err := rows.Scan(
			&judgeReputationCoinsEnergy.IDJudge,
			&judgeReputationCoinsEnergy.Reputation,
			&judgeReputationCoinsEnergy.Coins,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		if judgeReputationCoinsEnergy.Reputation > 0 {
			loveEnergy, darkEnergy := getEnergyHero()
			judgeReputationCoinsEnergy.LoveEnergy = loveEnergy
			judgeReputationCoinsEnergy.DarkEnergy = darkEnergy
			judgeReputationCoinsEnergyTable = append(judgeReputationCoinsEnergyTable, judgeReputationCoinsEnergy)
		} else {
			judgeReputationCoinsEnergyTable = append(judgeReputationCoinsEnergyTable, judgeReputationCoinsEnergy)
		}

	}

	return judgeReputationCoinsEnergyTable

}

func givePointToHero(db *sql.DB, ctx context.Context, JudgeOpinion *JudgeOpinion, TotalSumCoin int, Reputation int, Intelligence int, Fitness int) {

	loveEnergy, darkEnergy := getEnergyHero()

	stmtStr := `UPDATE PUBLIC.TASKRATE tsr
	SET tsr.COINS = ?
	, tsr.Love_Energy = ?
	, tsr.Dark_Energy = ?
	, tsr.REPUTATION = ?
	, tsr.INTELLIGENCE = ?
	, tsr.FITNESS = ?
	WHERE tsr.ID_TASK_1 = ?
	AND tsr.ID_TASK_2 = ?
	AND tsr.ID_HERO = ?
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		TotalSumCoin,
		loveEnergy,
		darkEnergy,
		Reputation,
		Intelligence,
		Fitness,
		JudgeOpinion.IDTask1,
		JudgeOpinion.IDTask2,
		JudgeOpinion.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func getTotalSumCoinAndSkillValue(db *sql.DB, ctx context.Context, iDTaskCheck string) (TotalSumCoin int, Reputation int, Intelligence int, Fitness int) {

	stmtStr := `WITH
	VeryFewSum AS (SELECT COALESCE(SUM(COINS),0) VeryFewSum FROM PUBLIC.JUDGEOPINION WHERE COINS = 1 AND ID_TASK_CHECK = ?),
	FewSum AS (SELECT COALESCE(SUM(COINS),0) FewSum FROM PUBLIC.JUDGEOPINION WHERE COINS = 2 AND ID_TASK_CHECK = ?),
	SomeSum AS (SELECT COALESCE(SUM(COINS),0) SomeSum FROM PUBLIC.JUDGEOPINION WHERE COINS = 3 AND ID_TASK_CHECK = ?),
	LotSum AS (SELECT COALESCE(SUM(COINS),0) LotSum FROM PUBLIC.JUDGEOPINION WHERE COINS = 4 AND ID_TASK_CHECK = ?),
	EvenMoreSum AS (SELECT COALESCE(SUM(COINS),0) EvenMoreSum FROM PUBLIC.JUDGEOPINION WHERE COINS = 5 AND ID_TASK_CHECK = ?),
	ReputationCount AS (SELECT COUNT(ID_TASK_CHECK) ReputationCount FROM PUBLIC.JUDGEOPINION WHERE MAIN_SKILL = 'Reputation' AND ID_TASK_CHECK = ?),
	IntelligenceCount AS (SELECT COUNT(ID_TASK_CHECK) IntelligenceCount FROM PUBLIC.JUDGEOPINION WHERE MAIN_SKILL = 'Intelligence' AND ID_TASK_CHECK = ?),
	FitnessCount AS (SELECT COUNT(ID_TASK_CHECK) FitnessCount FROM PUBLIC.JUDGEOPINION WHERE MAIN_SKILL = 'Fitness' AND ID_TASK_CHECK = ?),
	TotalCount AS (SELECT COUNT(ID_TASK_CHECK) TotalCount FROM PUBLIC.JUDGEOPINION WHERE ID_TASK_CHECK = ?),
	FinalStat AS (                           
	SELECT 
	CASE WHEN LotSum + EvenMoreSum < 32 
	THEN CEIL(POWER(VeryFewSum + FewSum + SomeSum, 1.5) + POWER(LotSum + EvenMoreSum, 2))
	ELSE CEIL(POWER(VeryFewSum + FewSum + SomeSum, 1.5) + POWER(32, 2) + POWER(LotSum + EvenMoreSum - 32, 2))
	END TotalSumCoin
	,CASE WHEN VeryFewSum + FewSum + SomeSum + LotSum + EvenMoreSum < 5  
	THEN CEIL(POWER(VeryFewSum + FewSum + SomeSum + LotSum + EvenMoreSum, 2.5))
	ELSE POWER(5, 2.5) + CEIL(POWER(VeryFewSum + FewSum + SomeSum + LotSum + EvenMoreSum - 5, 1.5))
	END SkillSum
	,ReputationCount
	,IntelligenceCount
	,FitnessCount
	,TotalCount
	FROM VeryFewSum
	CROSS JOIN FewSum
	CROSS JOIN SomeSum
	CROSS JOIN LotSum
	CROSS JOIN EvenMoreSum
	CROSS JOIN ReputationCount
	CROSS JOIN IntelligenceCount
	CROSS JOIN FitnessCount
	CROSS JOIN TotalCount
	)
	
	SELECT 
	CEIL(ROUND(TotalSumCoin * 160*(1 + RAND()))) TotalSumCoin
	,CEIL(ROUND(SkillSum * ReputationCount * (2 + 0.5 * RAND()) / TotalCount))  Reputation
	,CEIL(ROUND(SkillSum * IntelligenceCount * (2 + 0.5 * RAND()) / TotalCount)) Intelligence
	,CEIL(ROUND(SkillSum * FitnessCount * (2 + 0.5 * RAND()) / TotalCount)) Fitness
	FROM FinalStat
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
		iDTaskCheck,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&TotalSumCoin,
			&Reputation,
			&Intelligence,
			&Fitness,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return TotalSumCoin, Reputation, Intelligence, Fitness

}

func getEnergyHero() (loveEnergy, darkEnergy int) {

	if rand.Intn(100) < 20 {
		return 1, 1
	}
	if rand.Intn(100) < 50 {
		return 0, 1
	}
	if rand.Intn(100) < 90 {
		return 1, 0
	}

	return 0, 0
}
