package shop

import (
	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

// does not need to recheck if they have it since it is checked in front end and if they do not have it then there is nothing to update
func PostUsedShopObject(c *gin.Context) {

	ShopObject := new(ShopObject)
	err := c.Bind(ShopObject)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(ShopObject.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	if ShopObject.ShopType == `hero` {
		currentIDImage := getHeroCurrentIDImage(ctx, db, heroInfo)
		updateHeroIDImage(ctx, db, heroInfo, ShopObject, currentIDImage)
	} else {
		if ShopObject.ShopType == `map` {
			lastHeroMapStepWithMonsterIsNotDefeated := determineLastHeroMapStepWithMonsterIsNotDefeated(ctx, db, ShopObject, heroInfo)
			// if there is still one monster undefeated, then set map_step to that step ELSE do nothing
			if lastHeroMapStepWithMonsterIsNotDefeated != "" {
				markOneObjectAsUsed(ctx, db, heroInfo, ShopObject)
				markLastHeroMapStepWithMonsterIsNotDefeatedAsUsed(ctx, db, heroInfo, lastHeroMapStepWithMonsterIsNotDefeated)
			}
		} else {
			markOneObjectAsUsed(ctx, db, heroInfo, ShopObject)
		}
	}
}

func determineLastHeroMapStepWithMonsterIsNotDefeated(ctx context.Context, db *sql.DB, shopObject *ShopObject, heroInfo oauth.HeroInfo) string {
	stmtStr := `
	select top 1 
		huo.id_image 
	from public.HeroUniqueObject huo
	join public.shopobject sho
	on huo.id_image = sho.id_shop_object
	where sho.id_map = ? 
	and huo.id_hero = ?
	and sho.shop_type = 'map_step' 
	and COALESCE(huo.monster_is_defeated,false) = false
	order by map_step_order
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, shopObject.IDMap, heroInfo.IDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var lastHeroMapStepWithMonsterIsNotDefeated string

	for rows.Next() {
		if err := rows.Scan(
			&lastHeroMapStepWithMonsterIsNotDefeated,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	return lastHeroMapStepWithMonsterIsNotDefeated
}

func markLastHeroMapStepWithMonsterIsNotDefeatedAsUsed(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, lastHeroMapStepWithMonsterIsNotDefeated string) {

	// set all hero map_step as not used
	stmtStr := `
	UPDATE PUBLIC.HeroUniqueObject huo
		SET huo.is_used = false
	WHERE huo.id_image IN (SELECT sho.id_shop_object 
		                        FROM PUBLIC.HeroUniqueObject huo2
		                        JOIN  PUBLIC.ShopObject sho 
		                        ON huo2.id_image = sho.id_shop_object
								WHERE sho.shop_type = 'map_step'
								AND huo2.id_hero = ?
							)
	AND huo.id_hero = ?
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		heroInfo.IDHero,
		heroInfo.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// set LastHeroMapStepWithMonsterIsNotDefeatedAsUsed
	stmtStr = `
		UPDATE PUBLIC.HeroUniqueObject huo
			SET huo.is_used = true
			WHERE huo.id_image = ?
			AND huo.id_hero = ?
		`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		lastHeroMapStepWithMonsterIsNotDefeated,
		heroInfo.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func getHeroCurrentIDImage(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo) string {
	stmtStr := `
	SELECT ID_IMAGE FROM PUBLIC.HERO WHERE ID_HERO = ?
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, heroInfo.IDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var currentIDImage string

	for rows.Next() {
		if err := rows.Scan(
			&currentIDImage,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	return currentIDImage
}

// need currentIDImage to cleanly mark unused currentIDImage in HeroUniqueObject
func updateHeroIDImage(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, shopObject *ShopObject, currentIDImage string) {

	stmtStr := `UPDATE PUBLIC.Hero hr
				SET hr.ID_IMAGE = ?
				WHERE hr.ID_HERO = ?
				`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		shopObject.Key,
		heroInfo.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// SET IS_USED = false in HeroUniqueObject for currentIDImage
	stmtStr = `UPDATE PUBLIC.HeroUniqueObject huo
				SET huo.IS_USED = false
				WHERE huo.ID_IMAGE = ?
				AND huo.ID_HERO = ?
				`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		currentIDImage,
		heroInfo.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// SET IS_USED = true in HeroUniqueObject for new IDImage
	stmtStr = `UPDATE PUBLIC.HeroUniqueObject huo
				SET huo.IS_USED = true
				WHERE huo.ID_IMAGE = ?
				AND huo.ID_HERO = ?
				`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		shopObject.Key,
		heroInfo.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func markOneObjectAsUsed(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, shopObject *ShopObject) {

	if shopObject.IsUnique {

		stmtStr := `UPDATE PUBLIC.HeroUniqueObject huo
				SET huo.IS_USED = true
				WHERE huo.ID_IMAGE = ?
				AND huo.ID_HERO = ?
				`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			shopObject.Key,
			heroInfo.IDHero,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	} else {

		unusedIDHeroObject := getOneUnusedIDHeroObject(ctx, db, heroInfo, shopObject)

		if len(unusedIDHeroObject) == 1 {

			stmtStr := `UPDATE PUBLIC.HeroObject ho
				SET ho.IS_USED = true
				WHERE ho.ID_IMAGE = ?
				AND ho.ID_HERO = ?
				AND ho.ID_HERO_OBJECT = ?
				`

			stmt, err := db.PrepareContext(ctx, stmtStr)
			if err != nil {
				log.Printf("failed to prepare statement: %v", err)
			}

			_, err = stmt.ExecContext(ctx,
				shopObject.Key,
				heroInfo.IDHero,
				unusedIDHeroObject[0],
			)
			if err != nil {
				log.Printf("failed sql execute: %v", err)
			}
		}
	}

}

func getOneUnusedIDHeroObject(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, shopObject *ShopObject) []string {
	stmtStr := `
	SELECT TOP 1
	ID_HERO_OBJECT 
	FROM PUBLIC.HEROOBJECT 
	WHERE ID_IMAGE = ? 
	AND ID_HERO = ?
	AND IS_USED = false
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, shopObject.Key, heroInfo.IDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	// I use an array to be able to use the condition if len(array) = 0 then to check if no row is returned
	var unusedIDHeroObject string
	var unusedIDHeroObjectArray []string

	for rows.Next() {
		if err := rows.Scan(
			&unusedIDHeroObject,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
		unusedIDHeroObjectArray = append(unusedIDHeroObjectArray, unusedIDHeroObject)
	}

	return unusedIDHeroObjectArray
}
