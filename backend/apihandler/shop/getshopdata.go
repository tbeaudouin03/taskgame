package shop

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type ShopData struct {
	ShopObjects []ShopObject
}

// NB: Key = id_image = id_shop_object
type ShopObject struct {
	Key          string `json:"key"`
	Title        string
	URL          string
	Description  string
	Price        int
	LovePrice    int
	DarkPrice    int
	Reputation   int
	Intelligence int
	Fitness      int
	IsUsed       bool
	IsUnique     bool
	ObjectCount  int
	IDToken      string
	IDHeroObject string
	ShopType     string
	ShopRule     string
	OnBuyMessage string
	OnUseMessage string
	IDMap        string
}

func (shopData *ShopData) AddShopObject(shopObject ShopObject) {
	shopData.ShopObjects = append(shopData.ShopObjects, shopObject)
}

func GetShopData(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	var shopData ShopData

	getShopDataIgnite(ctx, db, heroInfo, &shopData)

	shopDataByte, err := json.Marshal(shopData)
	if err != nil {
		fmt.Println(err)
	}

	c.Writer.Write(shopDataByte)
}

func getShopDataIgnite(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, shopData *ShopData) {

	stmtStr := `
	WITH HeroObjectCount AS (SELECT ID_IMAGE, COUNT(ID_IMAGE) HeroObjectCount FROM PUBLIC.HEROOBJECT WHERE ID_HERO = ? AND IS_USED <> true GROUP BY ID_IMAGE),
	HeroUniqueObjectCount AS (SELECT ID_IMAGE, COUNT(ID_IMAGE) HeroUniqueObjectCount FROM PUBLIC.HEROUNIQUEOBJECT WHERE ID_HERO = ? GROUP BY ID_IMAGE)
		SELECT 
		COALESCE(sho.id_shop_object,'') ID_Image
		,COALESCE(sho.Title,'') Title
		,COALESCE(sho.URL,'') URL
		,COALESCE(sho.Description,'') Description
		,COALESCE(sho.Price, 0) Price 
		,COALESCE(sho.Love_Price, 0) LovePrice 
		,COALESCE(sho.Dark_Price, 0) DarkPrice 
		,COALESCE(sho.Reputation, 0) Reputation
		,COALESCE(sho.Intelligence, 0) Intelligence
		,COALESCE(sho.Fitness, 0) Fitness
		,COALESCE(sho.is_unique, false) is_unique
		,COALESCE(sho.shop_type,'') shop_type
		,COALESCE(sho.shop_rule,'') shop_rule
		,COALESCE(sho.on_buy_message,'') on_buy_message
		,COALESCE(sho.on_use_message,'') on_use_message
		,COALESCE(sho.id_map,'') id_map
		,COALESCE(hoc.HeroObjectCount, HeroUniqueObjectCount, 0) ObjectCount
		FROM PUBLIC.ShopObject sho
		LEFT JOIN HeroObjectCount hoc
		ON sho.id_shop_object = hoc.ID_IMAGE
		LEFT JOIN HeroUniqueObjectCount huoc
		ON sho.id_shop_object = huoc.ID_IMAGE
		WHERE sho.shop_type IN ('map', 'artefact', 'hero')
		ORDER BY sho.Price;
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, heroInfo.IDHero, heroInfo.IDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var shopObject ShopObject

	for rows.Next() {
		if err := rows.Scan(
			&shopObject.Key,
			&shopObject.Title,
			&shopObject.URL,
			&shopObject.Description,
			&shopObject.Price,
			&shopObject.LovePrice,
			&shopObject.DarkPrice,
			&shopObject.Reputation,
			&shopObject.Intelligence,
			&shopObject.Fitness,
			&shopObject.IsUnique,
			&shopObject.ShopType,
			&shopObject.ShopRule,
			&shopObject.OnBuyMessage,
			&shopObject.OnUseMessage,
			&shopObject.IDMap,
			&shopObject.ObjectCount,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		shopData.AddShopObject(shopObject)

	}

}
