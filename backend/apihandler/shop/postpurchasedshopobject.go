package shop

import (
	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

// check if enough money?? you alredy check in frontend, it's ok? yes, it is ok.. no check again
// if enough money then add to HeroObject
// and withdraw money from coins
func PostPurchasedShopObject(c *gin.Context) {

	ShopObject := new(ShopObject)
	err := c.Bind(ShopObject)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(ShopObject.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	coins, loveEnergy, darkEnergy := getCoinsEnergy(ctx, db, heroInfo)

	// you do not need to "addCoinsExpense" since you can get these expenses just by joining HeroObject to Image
	if coins-ShopObject.Price >= 0 &&
		loveEnergy-ShopObject.LovePrice >= 0 &&
		darkEnergy-ShopObject.DarkPrice >= 0 {
		if ShopObject.IsUnique {
			addShopObjectToHeroUniqueObject(ctx, db, heroInfo, ShopObject)
		} else {
			addShopObjectToHeroObject(ctx, db, heroInfo, ShopObject)
		}
	}

}

func getCoinsEnergy(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo) (coins, loveEnergy, darkEnergy int) {
	stmtStr := `
	SELECT 
	COALESCE(COINS,0) COINS
	,COALESCE(Love_Energy,0) Love_Energy
	,COALESCE(Dark_Energy,0) Dark_Energy
	FROM PUBLIC.HEROSTAT 
	WHERE ID_HERO = ?
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, heroInfo.IDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&coins,
			&loveEnergy,
			&darkEnergy,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	return coins, loveEnergy, darkEnergy
}

func addShopObjectToHeroObject(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, shopObject *ShopObject) {

	stmtStr := `MERGE INTO PUBLIC.HeroObject(
		ID_HERO_OBJECT
		, ID_HERO
		, ID_IMAGE
		, CREATED_AT
		, UPDATED_AT
		, IS_USED
		) VALUES (?,?,?,current_timestamp(),current_timestamp(), false)`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		shopObject.IDHeroObject,
		heroInfo.IDHero,
		shopObject.Key,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

// for map object, no need to set damage_to_monster, monster_is_introduced etc. because NULLs in db will eventually equal 0 and false in Go
func addShopObjectToHeroUniqueObject(ctx context.Context, db *sql.DB, heroInfo oauth.HeroInfo, shopObject *ShopObject) {
	if shopObject.ShopType != "map" {
		stmtStr := `
			MERGE INTO PUBLIC.HeroUniqueObject(
			ID_HERO
			,ID_IMAGE
			,CREATED_AT
			,UPDATED_AT
			,IS_USED
			) VALUES (?,?,current_timestamp(),current_timestamp(), false)`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			heroInfo.IDHero,
			shopObject.Key,
		)
		if err != nil {
			log.Fatalf("failed sql execute: %v", err)
		}
	} else {
		stmtStr := `
			MERGE INTO PUBLIC.HeroUniqueObject(
				id_hero
				, id_image
				, created_at
				, updated_at
				, is_used
			)
			(SELECT 
				?, id_shop_object, current_timestamp(), current_timestamp(), false FROM PUBLIC.ShopObject WHERE id_map = ?
			)
		`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			heroInfo.IDHero,
			shopObject.IDMap,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}

}
