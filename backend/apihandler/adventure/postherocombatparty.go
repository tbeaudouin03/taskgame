package adventure

import (
	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type HeroCombatParty struct {
	IDToken         string
	HeroCombatParty []string
}

// should check in backend that the ones chosen can actually be chosen:
// use getFightStatusIgnite and check: coins, is follower / leader of each teamate
// conditions: is follower or leader AND has enough coins COMPARED to the monster to be fought (hence fightStatus)

/*2 * FightStatus.CharacterFitness - HeroData.Reputation >
HeroData.Coins*/
func PostHeroCombatParty(c *gin.Context) {

	HeroCombatParty := new(HeroCombatParty)
	err := c.Bind(HeroCombatParty)
	if err != nil {
		log.Printf("could not read HeroCombatParty: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(HeroCombatParty.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// to check if chosen ones are actually followers at request time
	mapOfIDHeroAllFollower := getAllTeamFollower(ctx, db, heroInfo.IDHero)
	// to check the strength of current monster and compare with the coins of the desired companion
	iDHeroLeader := getTeamLeader(ctx, db, heroInfo.IDHero)
	// better to have two requests: one to get the leader, one to use its ID because getTeamLeader also DEFINES the leader if any issue like no leader or two leaders...
	fitnessLeaderMonster := getFitnessLeaderMonster(ctx, db, iDHeroLeader)

	postHeroCombatPartyIgnite(ctx, db, heroInfo.IDHero, HeroCombatParty, mapOfIDHeroAllFollower, fitnessLeaderMonster)

}

func postHeroCombatPartyIgnite(ctx context.Context, db *sql.DB, iDHero string, HeroCombatParty *HeroCombatParty, mapOfIDHeroAllFollower map[string]bool, fitnessLeaderMonster int) {

	// set all previous companion relationships to is_active = false
	stmtStr := `
	UPDATE PUBLIC.HeroCombatParty
	SET is_active = false
	WHERE id_hero_primary = ?
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// for each companion chosen, set companion relationship to is_active = true and create relationship if first time
	for _, iDHeroCompanion := range HeroCombatParty.HeroCombatParty {

		// only choose the companion if it is a follower
		if mapOfIDHeroAllFollower[iDHeroCompanion] {

			coinsCompanion, reputationCompanion := getCoinsReputationHero(ctx, db, iDHero)

			// companion can be companion only if they have enough coins + reputation compared to monster
			if 2*fitnessLeaderMonster <= reputationCompanion+coinsCompanion {
				stmtStr := `
				MERGE INTO PUBLIC.HeroCombatParty(id_hero_primary, id_hero_companion, is_active)
				VALUES(?, ?, true)
				`

				stmt, err := db.PrepareContext(ctx, stmtStr)
				if err != nil {
					log.Printf("failed to prepare statement: %v", err)
				}

				_, err = stmt.ExecContext(ctx, iDHero, iDHeroCompanion)
				if err != nil {
					log.Printf("failed sql execute: %v", err)
				}
			}

		}
	}

}

func getCoinsReputationHero(ctx context.Context, db *sql.DB, iDHero string) (coins int, reputation int) {

	stmtStr := `
			select
			COALESCE(coins,0)
			,COALESCE(reputation,0)
			from public.HeroStat
			where id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDHero,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&coins,
			&reputation,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return coins, reputation

}

func getFitnessLeaderMonster(ctx context.Context, db *sql.DB, iDHero string) (fitnessLeaderMonster int) {

	stmtStr := `
			select
			COALESCE(sho.fitness,0) CharacterFitness
			from public.HeroUniqueObject huo
			join public.ShopObject sho
			on huo.id_image = sho.id_shop_object
			where huo.id_hero = ?
			and sho.shop_type = 'map_step'
			and huo.is_used = true
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDHero,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&fitnessLeaderMonster,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return fitnessLeaderMonster

}

func getHeroCombatParty(ctx context.Context, db *sql.DB, iDHero string) (arrayOfIDHeroCompanion []string) {

	stmtStr := `
			select
			id_hero_companion
			from public.HeroCombatParty
			where id_hero_primary = ?
			and is_active = true
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDHero,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	var iDHeroCompanion string
	for rows.Next() {
		if err := rows.Scan(
			&iDHeroCompanion,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		arrayOfIDHeroCompanion = append(arrayOfIDHeroCompanion, iDHeroCompanion)

	}

	return arrayOfIDHeroCompanion

}
