package adventure

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/teamadventure"
)

type MonsterState struct {
	IDToken              string // only need IDToken in the request, the rest can be fetched from db to make team stuff centrally synchronized
	IDShopObject         string
	IDMap                string
	MonsterRemainingLife int
	MapStepOrder         int
}

type HeroLeaderFollowerStatus struct {
	IsBondedTeamLeader bool
	IsBondedToTeam     bool
}

func getHeroLeaderFollowerStatus(ctx context.Context, db *sql.DB, iDHero string) (HeroLeaderFollowerStatus HeroLeaderFollowerStatus) {

	stmtStr := `
			select 
			COALESCE(IS_BONDED_TEAM_LEADER, false)
			,COALESCE(is_bonded_to_team, false)
			from public.Hero
			where id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&HeroLeaderFollowerStatus.IsBondedTeamLeader,
			&HeroLeaderFollowerStatus.IsBondedToTeam,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return HeroLeaderFollowerStatus
}

func getMonsterState(ctx context.Context, db *sql.DB, iDHero string) (MonsterState MonsterState) {

	stmtStr := `
			select TOP 1
			sho.id_shop_object
			,COALESCE(sho.reputation,0) - COALESCE(huo.damage_to_monster,0) MonsterRemainingLife
			,COALESCE(sho.map_step_order,0) MapStepOrder
			,COALESCE(sho.id_map,'') IDMap
		from public.HeroUniqueObject huo
		join public.ShopObject sho
		on huo.id_image = sho.id_shop_object
		where huo.id_hero = ?
		and huo.is_used = true
		and COALESCE(sho.id_map,'') <> ''
		and sho.map_step_order IS NOT NULL
		order by map_step_order DESC
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&MonsterState.IDShopObject,
			&MonsterState.MonsterRemainingLife,
			&MonsterState.MapStepOrder,
			&MonsterState.IDMap,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return MonsterState
}

func PostMonsterIsIntroduced(c *gin.Context) {

	MonsterState := new(MonsterState)
	err := c.Bind(MonsterState)
	if err != nil {
		log.Printf("could not read MonsterState: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MonsterState.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// NB : postMonsterIsIntroducedIgnite is hero specific and does not depend on leader! So that each hero participating gets the introduction
	postMonsterIsIntroducedIgnite(ctx, db, heroInfo.IDHero)

}

func postMonsterIsIntroducedIgnite(ctx context.Context, db *sql.DB, iDHero string) {

	stmtStr := `
			UPDATE PUBLIC.Hero
			SET 
			monster_is_introduced = true
			WHERE id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

// NB: if we want is_fight_accepted = false then we simply don't call this endpoint...
func PostIsFightAccepted(c *gin.Context) {

	MonsterState := new(MonsterState)
	err := c.Bind(MonsterState)
	if err != nil {
		log.Printf("could not read MonsterState: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MonsterState.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postIsFightAcceptedIgnite(ctx, db, heroInfo.IDHero)

}

func postIsFightAcceptedIgnite(ctx context.Context, db *sql.DB, iDHero string) {

	stmtStr := `
			UPDATE PUBLIC.Hero
			SET 
			is_fight_accepted = true
			WHERE id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

func PostIsFightStarted(c *gin.Context) {

	MonsterState := new(MonsterState)
	err := c.Bind(MonsterState)
	if err != nil {
		log.Printf("could not read MonsterState: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MonsterState.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postIsFightStartedIgnite(ctx, db, heroInfo.IDHero)

}

func postIsFightStartedIgnite(ctx context.Context, db *sql.DB, iDHero string) {

	stmtStr := `
			UPDATE PUBLIC.Hero
			SET 
			is_fight_started = true
			WHERE id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

// set current map step monster_is_defeated = true
// reset all of followers is_monster_introduced etc.
// if current map step + 1 exists then set map_step + 1 is_used = true
func PostMonsterIsDefeated(c *gin.Context) {

	MonsterState := new(MonsterState)
	err := c.Bind(MonsterState)
	if err != nil {
		log.Printf("could not read MonsterState: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MonsterState.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	HeroLeaderFollowerStatus := getHeroLeaderFollowerStatus(ctx, db, heroInfo.IDHero)

	// if hero is not a follower and is not the leader, then use heroInfo.IDHero
	if !HeroLeaderFollowerStatus.IsBondedTeamLeader && !HeroLeaderFollowerStatus.IsBondedToTeam {
		MonsterState2 := getMonsterState(ctx, db, heroInfo.IDHero)
		postMonsterIsDefeatedIgnite(ctx, db, heroInfo.IDHero, MonsterState2, HeroLeaderFollowerStatus)

		// else use iDHeroTeamLeader
	} else {
		iDHeroTeamLeader := getTeamLeader(ctx, db, heroInfo.IDHero)
		MonsterState2 := getMonsterState(ctx, db, iDHeroTeamLeader)
		postMonsterIsDefeatedIgnite(ctx, db, iDHeroTeamLeader, MonsterState2, HeroLeaderFollowerStatus)
	}

}

func postMonsterIsDefeatedIgnite(ctx context.Context, db *sql.DB, iDHero string, MonsterState MonsterState, HeroLeaderFollowerStatus HeroLeaderFollowerStatus) {

	// if MonsterRemainingLife <= 0 then update current map_step with monster_is_defeated = true and is_used = false
	// and set next map_step is_used = true
	if MonsterState.MonsterRemainingLife <= 0 {

		if !HeroLeaderFollowerStatus.IsBondedTeamLeader && !HeroLeaderFollowerStatus.IsBondedToTeam {
			teamadventure.ResetHeroMonsterState(ctx, db, iDHero)
		} else {
			teamadventure.ResetTeamFollowerMonsterState(ctx, db, iDHero)
		}

		// set next map_step is_used = true
		stmtStr := `
		UPDATE PUBLIC.HeroUniqueObject
		SET 
		is_used = true
		WHERE id_hero = ?
		AND id_image = (SELECT 
						id_shop_object
						FROM PUBLIC.ShopObject
						WHERE id_map = ?
						AND map_step_order = ?
						)
		`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			iDHero,
			MonsterState.IDMap,
			MonsterState.MapStepOrder+1,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}

		// set current map step is_used = false and monster_is_defeated = true
		stmtStr = `
		UPDATE PUBLIC.HeroUniqueObject
		SET 
		monster_is_defeated = true
		, is_used = false
		WHERE id_hero = ?
		AND id_image = (SELECT 
						id_shop_object
						FROM PUBLIC.ShopObject
						WHERE id_map = ?
						AND map_step_order = ?
						)
		`

		stmt, err = db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			iDHero,
			MonsterState.IDMap,
			MonsterState.MapStepOrder,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}

	}
}
