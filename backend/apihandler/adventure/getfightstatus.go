package adventure

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"encoding/json"
	"log"
	"math"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/team"
)

type FightStatus struct {
	GivenName             string
	IDShopObject          string
	IDMap                 string
	URL                   string
	Title                 string
	Description           string
	OnBuyMessage          string
	OnUseMessage          string
	OnSellMessage         string
	CharacterURL          string
	MonsterIsIntroduced   bool
	MonsterIsDefeated     bool
	IsFightAccepted       bool
	IsFightStarted        bool
	DamageToMonster       int
	NewDamageToMonster    int
	MonsterRemainingLife  int
	DamageToHero          int
	NewDamageToHero       int
	HeroRemainingLife     int
	FightStatusTeam       []FightStatusTeam
	MapStepOrder          int
	CharacterReputation   int
	CharacterIntelligence int
	CharacterFitness      int
	CharacterCoinsBonus   int
	CharacterLoveBonus    int
	CharacterDarkBonus    int
	Price                 int
	LovePrice             int
	DarkPrice             int
	HeroReputation        int
	HeroIntelligence      int
	HeroFitness           int
	HeroURL               string
	TeamXPosition         int
	TeamYPosition         int
	MonsterXPosition      int
	MonsterYPosition      int
	HeroX1Position        int
	HeroY1Position        int
	HeroX2Position        int
	HeroY2Position        int
	HeroX3Position        int
	HeroY3Position        int
}

type FightStatusTeam struct {
	IDHero             string
	GivenName          string
	NewDamageToMonster int
	DamageToHero       int
	NewDamageToHero    int
	HeroRemainingLife  int
	HeroURL            string
	HeroReputation     int
	HeroIntelligence   int
	HeroFitness        int
}

func (fightStatus *FightStatus) AddFightStatusTeam(fightStatusTeam FightStatusTeam) {
	fightStatus.FightStatusTeam = append(fightStatus.FightStatusTeam, fightStatusTeam)
}

func GetFightStatus(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")
	isFightStr := c.Request.Header.Get("IsFight")
	isFight, err := strconv.ParseBool(isFightStr)
	if err != nil {
		log.Fatalf(`error oauth: %v`, err)
		return
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	arrayOfIDHeroCompanion := getHeroCombatParty(ctx, db, heroInfo.IDHero)
	iDHeroLeader := getTeamLeader(ctx, db, heroInfo.IDHero)
	mapOfIDHeroAllFollower := getAllTeamFollower(ctx, db, heroInfo.IDHero)

	// if it is not a fight, then just get info about the fight status
	if !isFight {
		//  if the hero sending request IS a follower or the leader, then use iDHeroLeader and go through the team logic
		if mapOfIDHeroAllFollower[heroInfo.IDHero] {

			var fightStatus FightStatus

			// for each follower, get their fight info
			for _, iDHeroCompanion := range arrayOfIDHeroCompanion {

				// check that the follower is indeed a team follower in the backend
				if mapOfIDHeroAllFollower[iDHeroCompanion] {

					// get the fight info of the team follower
					var fightStatusHeroTeam FightStatus
					getFightStatusIgnite(ctx, db, iDHeroCompanion, iDHeroLeader, isFight, arrayOfIDHeroCompanion, &fightStatusHeroTeam)
					fightStatus.AddFightStatusTeam(FightStatusTeam{
						IDHero:             iDHeroCompanion,
						GivenName:          fightStatusHeroTeam.GivenName,
						NewDamageToMonster: fightStatusHeroTeam.NewDamageToMonster,
						DamageToHero:       fightStatusHeroTeam.DamageToHero,
						NewDamageToHero:    fightStatusHeroTeam.NewDamageToHero,
						HeroRemainingLife:  fightStatusHeroTeam.HeroRemainingLife,
						HeroReputation:     fightStatusHeroTeam.HeroReputation,
						HeroIntelligence:   fightStatusHeroTeam.HeroIntelligence,
						HeroFitness:        fightStatusHeroTeam.HeroFitness,
						HeroURL:            fightStatusHeroTeam.HeroURL,
					})
				}
			}

			getFightStatusIgnite(ctx, db, heroInfo.IDHero, iDHeroLeader, isFight, arrayOfIDHeroCompanion, &fightStatus)

			fightStatusByte, err := json.Marshal(fightStatus)
			if err != nil {
				log.Fatalln(err)
			}

			c.Writer.Write(fightStatusByte)
			// else (if the hero sending the request is not a follower or the leader) use heroInfo.IDHero and DO NOT go through the team logic
		} else {

			var fightStatus FightStatus

			getFightStatusIgnite(ctx, db, heroInfo.IDHero, heroInfo.IDHero, isFight, arrayOfIDHeroCompanion, &fightStatus)

			fightStatusByte, err := json.Marshal(fightStatus)
			if err != nil {
				log.Fatalln(err)
			}

			c.Writer.Write(fightStatusByte)
		}

		// if it is a fight, engage the logic for fighting
	} else {

		var fightStatus FightStatus

		// if the hero sending request IS a follower or the leader, then use iDHeroLeader and go through the team logic
		if mapOfIDHeroAllFollower[heroInfo.IDHero] {

			// for each follower, fight the monster according to their strength
			for _, iDHeroCompanion := range arrayOfIDHeroCompanion {

				// check that the follower is indeed a team follower in the backend
				if mapOfIDHeroAllFollower[iDHeroCompanion] {

					// fight the monster with team member chosen
					var fightStatusHeroTeam FightStatus
					fightMonsterIgnite(ctx, db, heroInfo.IDHero, iDHeroLeader, iDHeroCompanion, isFight, arrayOfIDHeroCompanion, mapOfIDHeroAllFollower, &fightStatusHeroTeam)
					fightStatus.AddFightStatusTeam(FightStatusTeam{
						IDHero:             iDHeroCompanion,
						GivenName:          fightStatusHeroTeam.GivenName,
						NewDamageToMonster: fightStatusHeroTeam.NewDamageToMonster,
						DamageToHero:       fightStatusHeroTeam.DamageToHero,
						NewDamageToHero:    fightStatusHeroTeam.NewDamageToHero,
						HeroRemainingLife:  fightStatusHeroTeam.HeroRemainingLife,
						HeroReputation:     fightStatusHeroTeam.HeroReputation,
						HeroIntelligence:   fightStatusHeroTeam.HeroIntelligence,
						HeroFitness:        fightStatusHeroTeam.HeroFitness,
						HeroURL:            fightStatusHeroTeam.HeroURL,
					})
				}
			}

			// finally, fight the monster with the hero sending the request himself
			// NB: if hero is the leader
			fightMonsterIgnite(ctx, db, heroInfo.IDHero, iDHeroLeader, heroInfo.IDHero, isFight, arrayOfIDHeroCompanion, mapOfIDHeroAllFollower, &fightStatus)

			// else (if the hero sending the request is not a follower or the leader) use heroInfo.IDHero and DO NOT go through the team logic
		} else {

			fightMonsterIgnite(ctx, db, heroInfo.IDHero, heroInfo.IDHero, heroInfo.IDHero, isFight, arrayOfIDHeroCompanion, mapOfIDHeroAllFollower, &fightStatus)

		}

		fightStatusByte, err := json.Marshal(fightStatus)
		if err != nil {
			log.Fatalln(err)
		}

		c.Writer.Write(fightStatusByte)

	}

}

// NB: if hero follow his own path the iDHeroLeader = iDHero
func getFightStatusIgnite(ctx context.Context, db *sql.DB, iDHero string, iDHeroLeader string, isFight bool, arrayOfIDHeroCompanion []string, fightStatus *FightStatus) {

	stmtStr := `
	WITH HeroStat0 AS (
		select 
		COALESCE(hro.given_name,'') GivenName
		,COALESCE(hro.monster_is_introduced,false) MonsterIsIntroduced
		,COALESCE(hro.is_fight_accepted,false) IsFightAccepted
		,COALESCE(hro.is_fight_started,false) IsFightStarted
		,COALESCE(hrs.reputation,0) HeroReputation
		,COALESCE(hrs.intelligence,0) HeroIntelligence
		,COALESCE(hrs.fitness,0) HeroFitness
		,COALESCE(hrs.damage_to_hero,0) DamageToHero
		,COALESCE(sho.url,'') HeroURL
		from public.Hero hro
		join public.HeroStat hrs
		on hro.id_hero = hrs.id_hero
		left join public.ShopObject sho
		on hro.id_image = sho.id_shop_object
		where hro.id_hero = ?)
		
		, CharacterStat AS (
		select 
		COALESCE(huo.id_image,'') IDShopObject
		,COALESCE(sho.id_map,'') IDMap
		,COALESCE(sho.reputation,0) CharacterReputation
		,COALESCE(sho.intelligence,0) CharacterIntelligence
		,COALESCE(sho.fitness,0) CharacterFitness
		,COALESCE(huo.damage_to_monster,0) DamageToMonster
		,COALESCE(sho.URL,'') URL
		,COALESCE(sho.Title,'') Title
		,COALESCE(sho.Description,'') Description
		,COALESCE(sho.On_buy_message,'') OnBuyMessage
		,COALESCE(sho.On_use_message,'') OnUseMessage
		,COALESCE(sho.On_sell_message,'') OnSellMessage
		,COALESCE(sho.character_url,'') CharacterURL
		,COALESCE(sho.map_step_order,0) MapStepOrder
		,COALESCE(sho.character_coins_bonus,0) CharacterCoinsBonus
		,COALESCE(sho.character_love_bonus,0) CharacterLoveBonus
		,COALESCE(sho.character_dark_bonus,0) CharacterDarkBonus
		,COALESCE(huo.monster_is_defeated,false) MonsterIsDefeated
		,COALESCE(sho.price,0) Price
		,COALESCE(sho.love_price,0) LovePrice
		,COALESCE(sho.dark_price,0) DarkPrice
		,COALESCE(sho.team_x_position,0) TeamXPosition
		,COALESCE(sho.team_y_position,0) TeamYPosition
		,COALESCE(sho.monster_x_position,0) MonsterXPosition
		,COALESCE(sho.monster_y_position,0) MonsterYPosition
		,COALESCE(sho.hero_x1_position,0) HeroX1Position
		,COALESCE(sho.hero_y1_position,0) HeroY1Position
		,COALESCE(sho.hero_x2_position,0) HeroX2Position
		,COALESCE(sho.hero_y2_position,0) HeroY2Position
		,COALESCE(sho.hero_x3_position,0) HeroX3Position
		,COALESCE(sho.hero_y3_position,0) HeroY3Position

		from public.HeroUniqueObject huo
		join public.ShopObject sho
		on huo.id_image = sho.id_shop_object
		where huo.id_hero = ?
		and sho.shop_type = 'map_step'
		and huo.is_used = true)
		, FightStatus AS (
		select
		GivenName
		,IDShopObject
		,IDMap
		,MonsterIsIntroduced
		,DamageToMonster
		,DamageToHero 
		,URL
		,Title
		,Description
		,OnBuyMessage
		,OnUseMessage
		,OnSellMessage
		,CharacterURL
		,MapStepOrder
		,CharacterReputation
		,CharacterIntelligence
		,CharacterFitness
		,CharacterCoinsBonus
		,CharacterLoveBonus
		,CharacterDarkBonus
		,MonsterIsDefeated
		,IsFightAccepted
		,IsFightStarted
		,Price
		,LovePrice
		,DarkPrice
		,HeroReputation
		,HeroIntelligence
		,HeroFitness
		,HeroURL
		,TeamXPosition
		,TeamYPosition
		,MonsterXPosition
		,MonsterYPosition
		,HeroX1Position
		,HeroY1Position
		,HeroX2Position
		,HeroY2Position
		,HeroX3Position
		,HeroY3Position
		from HeroStat0
		CROSS JOIN CharacterStat)
		
		select
		GivenName
		,IDShopObject
		,IDMap
		,MonsterIsIntroduced
		,DamageToMonster
		,CharacterReputation - DamageToMonster MonsterRemainingLife
		,DamageToHero
		,HeroReputation - DamageToHero HeroRemainingLife
		,URL
		,Title
		,Description
		,OnBuyMessage
		,OnUseMessage
		,OnSellMessage
		,CharacterURL
		,MapStepOrder
		,CharacterReputation
		,CharacterIntelligence
		,CharacterFitness
		,CharacterCoinsBonus
		,CharacterLoveBonus
		,CharacterDarkBonus
		,MonsterIsDefeated
		,IsFightAccepted
		,IsFightStarted
		,CEIL(Price * POWER(?, 0.85))
		,CEIL(LovePrice * POWER(?, 0.85))
		,CEIL(DarkPrice * POWER(?, 0.85))
		,HeroReputation
		,HeroIntelligence
		,HeroFitness
		,HeroURL
		,TeamXPosition
		,TeamYPosition
		,MonsterXPosition
		,MonsterYPosition
		,HeroX1Position
		,HeroY1Position
		,HeroX2Position
		,HeroY2Position
		,HeroX3Position
		,HeroY3Position
		from FightStatus
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDHero, iDHeroLeader,
		len(arrayOfIDHeroCompanion)+1, len(arrayOfIDHeroCompanion)+1, len(arrayOfIDHeroCompanion)+1)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&fightStatus.GivenName,
			&fightStatus.IDShopObject,
			&fightStatus.IDMap,
			&fightStatus.MonsterIsIntroduced,
			&fightStatus.DamageToMonster,
			&fightStatus.MonsterRemainingLife,
			&fightStatus.DamageToHero,
			&fightStatus.HeroRemainingLife,
			&fightStatus.URL,
			&fightStatus.Title,
			&fightStatus.Description,
			&fightStatus.OnBuyMessage,
			&fightStatus.OnUseMessage,
			&fightStatus.OnSellMessage,
			&fightStatus.CharacterURL,
			&fightStatus.MapStepOrder,
			&fightStatus.CharacterReputation,
			&fightStatus.CharacterIntelligence,
			&fightStatus.CharacterFitness,
			&fightStatus.CharacterCoinsBonus,
			&fightStatus.CharacterLoveBonus,
			&fightStatus.CharacterDarkBonus,
			&fightStatus.MonsterIsDefeated,
			&fightStatus.IsFightAccepted,
			&fightStatus.IsFightStarted,
			&fightStatus.Price,
			&fightStatus.LovePrice,
			&fightStatus.DarkPrice,
			&fightStatus.HeroReputation,
			&fightStatus.HeroIntelligence,
			&fightStatus.HeroFitness,
			&fightStatus.HeroURL,
			&fightStatus.TeamXPosition,
			&fightStatus.TeamYPosition,
			&fightStatus.MonsterXPosition,
			&fightStatus.MonsterYPosition,
			&fightStatus.HeroX1Position,
			&fightStatus.HeroY1Position,
			&fightStatus.HeroX2Position,
			&fightStatus.HeroY2Position,
			&fightStatus.HeroX3Position,
			&fightStatus.HeroY3Position,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

}

func fightMonsterIgnite(ctx context.Context, db *sql.DB, iDHero string, iDHeroLeader string, iDHeroCompanion string, isFight bool, arrayOfIDHeroCompanion []string, mapOfIDHeroAllFollower map[string]bool, fightStatus *FightStatus) {

	stmtStr := `
	WITH HeroStat0 AS (
		select 
		COALESCE(hro.given_name,'') GivenName
		,COALESCE(hro.monster_is_introduced,false) MonsterIsIntroduced
		,COALESCE(hro.is_fight_accepted,false) IsFightAccepted
		,COALESCE(hro.is_fight_started,false) IsFightStarted
		,COALESCE(hrs.reputation,0) HeroReputation
		,COALESCE(hrs.intelligence,0) HeroIntelligence
		,COALESCE(hrs.fitness,0) HeroFitness
		,COALESCE(hrs.damage_to_hero,0) DamageToHero
		,COALESCE(sho.url,'') HeroURL
		from public.Hero hro
		join public.HeroStat hrs
		on hro.id_hero = hrs.id_hero
		left join public.ShopObject sho
		on hro.id_image = sho.id_shop_object
		where hro.id_hero = ?)
		
		, CharacterStat AS (
		select 
		COALESCE(huo.id_image,'') IDShopObject
		,COALESCE(sho.id_map,'') IDMap
		,COALESCE(sho.reputation,0) CharacterReputation
		,COALESCE(sho.intelligence,0) CharacterIntelligence
		,COALESCE(sho.fitness,0) CharacterFitness
		,COALESCE(huo.damage_to_monster,0) DamageToMonster
		,COALESCE(sho.URL,'') URL
		,COALESCE(sho.Title,'') Title
		,COALESCE(sho.Description,'') Description
		,COALESCE(sho.On_buy_message,'') OnBuyMessage
		,COALESCE(sho.On_use_message,'') OnUseMessage
		,COALESCE(sho.On_sell_message,'') OnSellMessage
		,COALESCE(sho.character_url,'') CharacterURL
		,COALESCE(sho.map_step_order,0) MapStepOrder
		,COALESCE(sho.character_coins_bonus,0) CharacterCoinsBonus
		,COALESCE(sho.character_love_bonus,0) CharacterLoveBonus
		,COALESCE(sho.character_dark_bonus,0) CharacterDarkBonus
		,COALESCE(huo.monster_is_defeated,false) MonsterIsDefeated
		,COALESCE(sho.price,0) Price
		,COALESCE(sho.love_price,0) LovePrice
		,COALESCE(sho.dark_price,0) DarkPrice
		,COALESCE(sho.team_x_position,0) TeamXPosition
		,COALESCE(sho.team_y_position,0) TeamYPosition
		,COALESCE(sho.monster_x_position,0) MonsterXPosition
		,COALESCE(sho.monster_y_position,0) MonsterYPosition
		,COALESCE(sho.hero_x1_position,0) HeroX1Position
		,COALESCE(sho.hero_y1_position,0) HeroY1Position
		,COALESCE(sho.hero_x2_position,0) HeroX2Position
		,COALESCE(sho.hero_y2_position,0) HeroY2Position
		,COALESCE(sho.hero_x3_position,0) HeroX3Position
		,COALESCE(sho.hero_y3_position,0) HeroY3Position
		from public.HeroUniqueObject huo
		join public.ShopObject sho
		on huo.id_image = sho.id_shop_object
		where huo.id_hero = ?
		and sho.shop_type = 'map_step'
		and huo.is_used = true)
		, FightStatus AS (
		select
		GivenName
		,IDShopObject
		,IDMap
		,MonsterIsIntroduced
		,DamageToMonster
		,FLOOR(HeroFitness * 1.5 * rand() * (CASE WHEN rand() < 0.03 THEN 0
													WHEN rand() < ln(HeroIntelligence)/20 THEN 2 
													ELSE 1 END)) NewDamageToMonster
		,DamageToHero
		,CEIL(CharacterFitness * 1.5 * rand() * (CASE WHEN rand() < 0.03 THEN 0
														WHEN rand() < ln(CharacterIntelligence)/20 THEN 2 
														ELSE 1 END)) NewDamageToHero
		,URL
		,Title
		,Description
		,OnBuyMessage
		,OnUseMessage
		,OnSellMessage
		,CharacterURL
		,MapStepOrder
		,CharacterReputation
		,CharacterIntelligence
		,CharacterFitness
		,CharacterCoinsBonus
		,CharacterLoveBonus
		,CharacterDarkBonus
		,MonsterIsDefeated
		,IsFightAccepted
		,IsFightStarted
		,Price
		,LovePrice
		,DarkPrice
		,HeroReputation
		,HeroIntelligence
		,HeroFitness
		,HeroURL
		,TeamXPosition
		,TeamYPosition
		,MonsterXPosition
		,MonsterYPosition
		,HeroX1Position
		,HeroY1Position
		,HeroX2Position
		,HeroY2Position
		,HeroX3Position
		,HeroY3Position
		from HeroStat0
		CROSS JOIN CharacterStat)
		
		select
		GivenName
		,IDShopObject
		,IDMap
		,MonsterIsIntroduced
		,DamageToMonster + NewDamageToMonster DamageToMonster
		,NewDamageToMonster
		,CharacterReputation - DamageToMonster - NewDamageToMonster MonsterRemainingLife
		,DamageToHero + NewDamageToHero DamageToHero
		,NewDamageToHero
		,HeroReputation - DamageToHero - NewDamageToHero HeroRemainingLife
		,URL
		,Title
		,Description
		,OnBuyMessage
		,OnUseMessage
		,OnSellMessage
		,CharacterURL
		,MapStepOrder
		,CharacterReputation
		,CharacterIntelligence
		,CharacterFitness
		,CharacterCoinsBonus
		,CharacterLoveBonus
		,CharacterDarkBonus
		,MonsterIsDefeated
		,IsFightAccepted
		,IsFightStarted
		,CEIL(Price * POWER(?, 0.85))
		,CEIL(LovePrice * POWER(?, 0.85))
		,CEIL(DarkPrice * POWER(?, 0.85))
		,HeroReputation
		,HeroIntelligence
		,HeroFitness
		,HeroURL
		,TeamXPosition
		,TeamYPosition
		,MonsterXPosition
		,MonsterYPosition
		,HeroX1Position
		,HeroY1Position
		,HeroX2Position
		,HeroY2Position
		,HeroX3Position
		,HeroY3Position
		from FightStatus
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDHeroCompanion, iDHeroLeader,
		len(arrayOfIDHeroCompanion)+1, len(arrayOfIDHeroCompanion)+1, len(arrayOfIDHeroCompanion)+1)

	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {

		if err := rows.Scan(
			&fightStatus.GivenName,
			&fightStatus.IDShopObject,
			&fightStatus.IDMap,
			&fightStatus.MonsterIsIntroduced,
			&fightStatus.DamageToMonster,
			&fightStatus.NewDamageToMonster,
			&fightStatus.MonsterRemainingLife,
			&fightStatus.DamageToHero,
			&fightStatus.NewDamageToHero,
			&fightStatus.HeroRemainingLife,
			&fightStatus.URL,
			&fightStatus.Title,
			&fightStatus.Description,
			&fightStatus.OnBuyMessage,
			&fightStatus.OnUseMessage,
			&fightStatus.OnSellMessage,
			&fightStatus.CharacterURL,
			&fightStatus.MapStepOrder,
			&fightStatus.CharacterReputation,
			&fightStatus.CharacterIntelligence,
			&fightStatus.CharacterFitness,
			&fightStatus.CharacterCoinsBonus,
			&fightStatus.CharacterLoveBonus,
			&fightStatus.CharacterDarkBonus,
			&fightStatus.MonsterIsDefeated,
			&fightStatus.IsFightAccepted,
			&fightStatus.IsFightStarted,
			&fightStatus.Price,
			&fightStatus.LovePrice,
			&fightStatus.DarkPrice,
			&fightStatus.HeroReputation,
			&fightStatus.HeroIntelligence,
			&fightStatus.HeroFitness,
			&fightStatus.HeroURL,
			&fightStatus.TeamXPosition,
			&fightStatus.TeamYPosition,
			&fightStatus.MonsterXPosition,
			&fightStatus.MonsterYPosition,
			&fightStatus.HeroX1Position,
			&fightStatus.HeroY1Position,
			&fightStatus.HeroX2Position,
			&fightStatus.HeroY2Position,
			&fightStatus.HeroX3Position,
			&fightStatus.HeroY3Position,
		); err != nil {
			log.Fatalln("failed to get row: %v", err)
		}

	}

	// record damage to monster
	stmtStr = `
			UPDATE PUBLIC.HeroUniqueObject
			SET damage_to_monster = ?
			, updated_at = current_timestamp()
			WHERE id_hero = ?
			AND id_image = ?
			`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		fightStatus.DamageToMonster,
		iDHeroLeader,
		fightStatus.IDShopObject,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// record damage to hero
	stmtStr = `
			UPDATE PUBLIC.HeroStat
			SET damage_to_hero = ?
			, updated_at = current_timestamp()
			WHERE id_hero = ?
			`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		fightStatus.DamageToHero,
		iDHeroCompanion,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// record cost for attacking the monster in taskrate,
	// record only once even if for team attack because only the one launching attack pays for everyone
	if iDHero == iDHeroCompanion {

		guid := xid.New()
		iDAttackCost := `AttackCost` + guid.String()
		stmtStr = `
		MERGE INTO PUBLIC.TaskRate(
			id_task_1
			,id_task_2
			,id_hero
			,coins
			,love_energy
			,dark_energy
			,created_at
		)
		VALUES(?, ?, ?, ?, ?, ?, current_timestamp())
		`

		stmt, err = db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			fightStatus.IDShopObject,
			iDAttackCost,
			iDHero,
			-fightStatus.Price*100,
			-fightStatus.LovePrice,
			-fightStatus.DarkPrice,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}

	// if HeroRemainingLife < 0 then record coins malus in task rate = HeroRemainingLife*100 (which is negative)
	if fightStatus.HeroRemainingLife < 0 {
		stmtStr = `
			MERGE INTO PUBLIC.TaskRate(
				id_task_1
				,id_task_2
				,id_hero
				,coins
				,created_at
			)
			VALUES(?, 'StolenByMonster', ?, ?, current_timestamp())
			`

		stmt, err = db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			fightStatus.IDShopObject,
			iDHeroCompanion,
			fightStatus.HeroRemainingLife*100,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}

	// if MonsterRemainingLife <= 0 and the IDHero is the hero that is attacking with his team (ie iDHero == iDHeroCompanion)
	// because he is the last to attack and is therefore responsible for giving rewards if monster is dead
	// then record bonus for ALL FOLLOWERS in task rate (not only the ones participating in the attack)
	if fightStatus.MonsterRemainingLife <= 0 && iDHero == iDHeroCompanion {

		// only give to hero sending the request IF he is not a follower or the leader (follow his own path)
		if !mapOfIDHeroAllFollower[iDHero] {

			// give bonus to hero
			stmtStr = `
				MERGE INTO PUBLIC.TaskRate(
					id_task_1
					,id_task_2
					,id_hero
					,coins
					,love_energy
					,dark_energy
					,created_at
				)
				VALUES(?, 'MonsterBonus', ?, ?, ?, ?, current_timestamp())
				`

			stmt, err = db.PrepareContext(ctx, stmtStr)
			if err != nil {
				log.Printf("failed to prepare statement: %v", err)
			}

			_, err = stmt.ExecContext(ctx,
				fightStatus.IDShopObject,
				iDHero,
				fightStatus.CharacterCoinsBonus*100,
				fightStatus.CharacterLoveBonus,
				fightStatus.CharacterDarkBonus,
			)
			if err != nil {
				log.Printf("failed sql execute: %v", err)
			}

			// heal hero
			stmtStr = `
				UPDATE PUBLIC.HeroStat 
				set damage_to_hero = 0
				, updated_at = current_timestamp() 
				where id_hero = ?
				`

			stmt, err = db.PrepareContext(ctx, stmtStr)
			if err != nil {
				log.Printf("failed to prepare statement: %v", err)
			}

			_, err = stmt.ExecContext(ctx, iDHero)
			if err != nil {
				log.Printf("failed sql execute: %v", err)
			}

			// ELSE ONLY IF HERO SENDING REQUEST IS A FOLLOWER OR LEADER THEN give to all the FOLLOWERS in the team !
		} else {

			// mapOfIDHeroAllFollower ONLY includes team FOLLOWERS
			arrayOfIDHeroAllLength64 := float64(len(mapOfIDHeroAllFollower))
			teamRewardRatio := int(math.Ceil(math.Pow(arrayOfIDHeroAllLength64, 0.85) / arrayOfIDHeroAllLength64))

			for iDHeroAllFollower := range mapOfIDHeroAllFollower {

				// give bonus to hero
				stmtStr = `
						MERGE INTO PUBLIC.TaskRate(
							id_task_1
							,id_task_2
							,id_hero
							,coins
							,love_energy
							,dark_energy
							,created_at
						)
						VALUES(?, 'MonsterBonus', ?, ?, ?, ?, current_timestamp())
						`

				stmt, err = db.PrepareContext(ctx, stmtStr)
				if err != nil {
					log.Printf("failed to prepare statement: %v", err)
				}

				_, err = stmt.ExecContext(ctx,
					fightStatus.IDShopObject,
					iDHeroAllFollower,
					fightStatus.CharacterCoinsBonus*100*teamRewardRatio,
					fightStatus.CharacterLoveBonus*teamRewardRatio,
					fightStatus.CharacterDarkBonus*teamRewardRatio,
				)
				if err != nil {
					log.Printf("failed sql execute: %v", err)
				}

				// heal hero
				stmtStr = `
					UPDATE PUBLIC.HeroStat 
					set damage_to_hero = 0
					, updated_at = current_timestamp() 
					where id_hero = ?
					`

				stmt, err = db.PrepareContext(ctx, stmtStr)
				if err != nil {
					log.Printf("failed to prepare statement: %v", err)
				}

				_, err = stmt.ExecContext(ctx, iDHeroAllFollower)
				if err != nil {
					log.Printf("failed sql execute: %v", err)
				}

			}
		}

		// NB: we have an independent request to set monster_is_defeated = true so that we can first display the last word of the monster when life <=0

	}
	// NB: if MonsterIsIntroduced =  false, although fightStatus contains damages, they will not be reflected in db since there was no update in db (we will not show these damages in frontend)

}

func getAllTeamFollower(ctx context.Context, db *sql.DB, iDHero string) map[string]bool {

	mapOfIDHeroAllFollower := make(map[string]bool)

	stmtStr := `
		select 
		id_hero
		from public.Hero
		where id_team = (select id_team from public.Hero where id_hero = ?)
		and (IS_BONDED_TO_TEAM = true OR IS_BONDED_TEAM_LEADER = true)
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	var iDHeroFollower string
	for rows.Next() {
		if err := rows.Scan(&iDHeroFollower); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
		mapOfIDHeroAllFollower[iDHeroFollower] = true
	}

	return mapOfIDHeroAllFollower
}

type teamLeader struct {
	IDHero string
	IDTeam string
	Level  int
}

func getTeamLeader(ctx context.Context, db *sql.DB, iDHero string) string {
	stmtStr := `
		select 
		hro.id_hero
		,hro.id_team
		,COALESCE(hrs.level,0)
		from public.Hero hro
		left join public.HeroStat hrs
		on hro.id_hero = hrs.id_hero
		where hro.id_team = (select id_team from public.Hero where id_hero = ?)
		and hro.IS_BONDED_TEAM_LEADER = true
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	var (
		teamLeaders []teamLeader
		teamLeader  teamLeader
	)

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&teamLeader.IDHero,
			&teamLeader.IDTeam,
			&teamLeader.Level,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
		teamLeaders = append(teamLeaders, teamLeader)

		// always store the leader with highest level in teamLeader[0]
		// FYI: there could be several leaders if people mess around with changing teams all the time
		if teamLeaders[0].Level < teamLeader.Level {
			teamLeaders[0] = teamLeader
		}

	}

	if len(teamLeaders) > 1 {
		TeamLeaderChoice := new(team.TeamLeaderChoice)
		TeamLeaderChoice.IDHero = teamLeaders[0].IDHero
		TeamLeaderChoice.IDTeam = teamLeaders[0].IDTeam
		team.PostTeamLeaderChoiceIgnite(ctx, db, TeamLeaderChoice)
	}

	// if there is no leader, elect the first one in the team to attack as leader
	if len(teamLeaders) == 0 {
		electHeroAsLeader(ctx, db, iDHero)
		return iDHero
	}

	return teamLeaders[0].IDHero
}

func electHeroAsLeader(ctx context.Context, db *sql.DB, iDHero string) {
	stmtStr := `
		update PUBLIC.Hero 
		set IS_BONDED_TEAM_LEADER = true
		where id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

}
