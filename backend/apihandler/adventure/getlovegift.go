package adventure

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"encoding/json"
	"log"

	"github.com/rs/xid"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type LoveGift struct {
	MinuteUntilNextLoveGift int
	LoveEnergyGift          int
}

func GetLoveGift(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	minuteBetweenLoveGift := 75
	loveEnergyGift := 10
	minuteUntilNextLoveGift := getLoveGiftIgnite(ctx, db, heroInfo.IDHero, minuteBetweenLoveGift)
	if minuteUntilNextLoveGift == minuteBetweenLoveGift {
		postLoveEnergyGiftIgnite(ctx, db, heroInfo.IDHero, loveEnergyGift)
		updateMinuteUntilNextLoveGift(ctx, db, heroInfo.IDHero, minuteUntilNextLoveGift)

		var loveGift LoveGift
		loveGift.MinuteUntilNextLoveGift = minuteUntilNextLoveGift
		loveGift.LoveEnergyGift = loveEnergyGift

		loveGiftByte, err := json.Marshal(loveGift)
		if err != nil {
			log.Fatalln(err)
		}

		c.Writer.Write(loveGiftByte)
	} else {
		updateMinuteUntilNextLoveGift(ctx, db, heroInfo.IDHero, minuteUntilNextLoveGift)

		var loveGift LoveGift
		loveGift.MinuteUntilNextLoveGift = minuteUntilNextLoveGift
		loveGift.LoveEnergyGift = 0

		loveGiftByte, err := json.Marshal(loveGift)
		if err != nil {
			log.Fatalln(err)
		}

		c.Writer.Write(loveGiftByte)
	}

}

func updateMinuteUntilNextLoveGift(ctx context.Context, db *sql.DB, iDHero string, minuteUntilNextLoveGift int) {
	stmtStr := `
			UPDATE PUBLIC.Hero
			SET 
			minute_until_next_love_gift = ?
			WHERE id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, minuteUntilNextLoveGift, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

func postLoveEnergyGiftIgnite(ctx context.Context, db *sql.DB, iDHero string, loveEnergyGift int) {

	// update Hero's LAST_FREE_LOVE_ENERGY_AT
	stmtStr := `
			UPDATE PUBLIC.Hero
			SET 
			LAST_FREE_LOVE_ENERGY_AT = current_timestamp()
			WHERE id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// add free love bonus to taskrate table
	guid := xid.New()
	iDLoveBonus := guid.String()
	stmtStr = `
		MERGE INTO PUBLIC.TASKRATE(ID_TASK_1, ID_TASK_2, ID_HERO, LOVE_ENERGY, created_at)
		VALUES(?, 'LoveGift', ?, ?, current_timestamp());`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDLoveBonus, iDHero, loveEnergyGift)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

// minuteUntilNextLoveGift is calculated in backend so that we can dynamically and safely personalize it per user in the future
// indeed, minuteBetweenLoveGift does not have to be 6*60 minutes, it can be a function
func getLoveGiftIgnite(ctx context.Context, db *sql.DB, iDHero string, minuteBetweenLoveGift int) (minuteUntilNextLoveGift int) {

	stmtStr := `
		select 
		? - COALESCE(DATEDIFF('MINUTE',last_free_love_energy_at, current_timestamp()), 100000) 
		from PUBLIC.Hero
		where id_hero = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, minuteBetweenLoveGift, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(&minuteUntilNextLoveGift); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	if minuteUntilNextLoveGift <= 0 {

		return minuteBetweenLoveGift
	}

	return minuteUntilNextLoveGift

}
