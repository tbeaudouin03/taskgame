package oauth

import (

	//"google.golang.org/api/appengine/v1"

	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"log"

	jwt "github.com/dgrijalva/jwt-go"
)

type config struct {
	Aud          []string `json:"aud"`
	Typ          string   `json:"typ"`
	Iss1         string   `json:"iss1"`
	Iss2         string   `json:"iss2"`
	PublicKeyURL string   `json:"public_key_url"`
}

type GooglePublicKey struct {
	Keys []Key `json:"keys"`
}

type Key struct {
	E   string `json:"e"`
	Kty string `json:"kty"`
	Alg string `json:"alg"`
	N   string `json:"n"`
	Sig string `json:"sig"`
	Kid string `json:"kid"`
}

type HeroInfo struct {
	IDHero               string
	GivenName            string
	FamilyName           string
	Email                string
	TaskCount            int
	TaskVerifyCount      int
	JudgementCount       int
	JudgementVerifyCount int
	W0TaskCount          int
	W1TaskCount          int
	W2TaskCount          int
	W3TaskCount          int
	W4TaskCount          int
	Coins                int
	LoveEnergy           int
	DarkEnergy           int
	Reputation           int
	Intelligence         int
	Fitness              int
	Reliability          int
	Level                int
	JudgeLevel           int
	Rank                 int
	JudgeRank            int
	Score                int
	JudgeScore           int
}

// VerifyIDToken PARTLY verifies the ID token sent by the client and establishes a session
// the full verification still needs to be fully implemented -
// namely, the token signature verification should still be implemented
// cf. explored solutions after the function
// or better: have a nodejs microservice just for identification because they have good package in nodejs
func VerifyIDTokenReturnHeroInfo(idToken string) (HeroInfo, error) {

	configByte, _ := ioutil.ReadFile("config.json")
	var config config
	err := json.Unmarshal(configByte, &config)
	if err != nil {
		log.Printf("could not unmarshal config to json: %v", err)
	}

	// 2. Parse idToken to get claims etc.
	token, err := jwt.Parse(idToken, func(token *jwt.Token) (interface{}, error) {
		return []byte("AllYourBase"), nil
	})
	if err != nil {
		log.Printf("jwt error... ignore if it says key is of invalid type: %v", err)
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return HeroInfo{}, errors.New("assertion error: token.Claims is not jwt.MapClaims type")
	}

	if claims["iss"] != config.Iss1 && claims["iss"] != config.Iss2 {
		return HeroInfo{}, errors.New("iss of token does not match secret iss")
	}

	aud, ok := claims["aud"].(string)
	if !ok {
		return HeroInfo{}, errors.New("aud is not a string")
	}
	if !stringInSlice(aud, config.Aud) {
		return HeroInfo{}, errors.New("aud of token does not match secret aud")
	}

	if token.Header["typ"] != config.Typ {
		return HeroInfo{}, errors.New("typ of token does not match secret typ")
	}

	// 3. Get the list of Google public keyS and decode the list into GooglePublicKey struct
	res, err := http.Get("https://www.googleapis.com/oauth2/v3/certs")
	if err != nil {
		return HeroInfo{}, errors.New("could not fetch Google public keys")
	}
	googlePublicKey := new(GooglePublicKey)
	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&googlePublicKey)

	// 4. Get kid of client token to choose the public key by comparing with the list of public keys kids
	kidClient := token.Header["kid"]
	kidClientStr, ok := kidClient.(string)
	if !ok {
		log.Printf("assertion error - kid of client is not a string")
		return HeroInfo{}, errors.New("iss of token does not match secret iss")
	}

	publicKey := new(Key)
	for _, key := range googlePublicKey.Keys {
		if key.Kid == kidClientStr {
			publicKey = &key
		}
	}

	if publicKey.Kid == "" {
		log.Printf("kidClient is empty - ie there was no public key kid matching token kid")
		return HeroInfo{}, errors.New("iss of token does not match secret iss")
	}

	log.Printf("Token is valid - all tests successfully passed - starting hero's session")
	iDHeroI := claims["sub"]
	iDHero, ok := iDHeroI.(string)
	if !ok {
		log.Printf("iDHero: %v", iDHeroI)
		return HeroInfo{}, errors.New("assertion error - iDHero is not a string")
	}
	// ignite does not like long strings only made of numbers AND does not like very long numbers so we add i to create a db friendly ID
	// if google changes their IDs pattern, string format will always be ok, hence we choose string for flexibility
	iDHero = `i` + iDHero

	givenNameI := claims["given_name"]
	givenName, ok := givenNameI.(string)
	if !ok {
		log.Printf("givenName: %v", givenNameI)
		return HeroInfo{}, errors.New("assertion error - givenName is not a string")
	}
	familyNameI := claims["family_name"]
	familyName, ok := familyNameI.(string)
	if !ok {
		log.Printf("familyName: %v", familyNameI)
		return HeroInfo{}, errors.New("assertion error - familyName is not a string")
	}
	emailI := claims["email"]
	email, ok := emailI.(string)
	if !ok {
		log.Printf("email: %v", emailI)
		return HeroInfo{}, errors.New("assertion error - email is not a string")
	}
	return HeroInfo{
		IDHero:     iDHero,
		GivenName:  givenName,
		FamilyName: familyName,
		Email:      email,
	}, nil

}

// randToken returns a random token of i bytes
func randToken(i int) string {
	b := make([]byte, i)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

/*



	// 5. Get Google public key modulo and exponent in rsa package format

	publicKeyFormatted := getModuloExponentPublicKey(publicKey.N, publicKey.E)
	var publicKeyInterface interface{}
	publicKeyInterface = publicKeyFormatted

	log.Println(`RSA KEY 0: `, publicKeyInterface)

	rsaKey, ok := publicKeyInterface.(*rsa.PublicKey)
	log.Println(`RSA KEY 1: `, rsaKey)
	log.Println(`OK 1: `, ok)
	if !ok {
		log.Println("Fast like a biiiiiiiiiiiiiiiiiiiiiiiiiiiiird")
	}

	log.Println(`here2`)

	// 6. verify the token with publicKeyFormatted, signatureString and signature

	signingString, err := token.SigningString()
	if err != nil {
		log.Fatal(err)
	}

	log.Println(`here3`)
	signingMethodRSA := &jwt.SigningMethodRSA{
		Name: "test",
		Hash: crypto.SHA1,
	}
	err = signingMethodRSA.Verify(signingString, token.Signature, publicKeyInterface)
	if err != nil {
		log.Println(err)
	}

func getModuloExponentPublicKey(nStr, eStr string) (publicKey *rsa.PublicKey) {

	decN, err := base64.StdEncoding.DecodeString(nStr)
	if err != nil {
		fmt.Println(err)
	}
	n := big.NewInt(0)
	n.SetBytes(decN)

	decE, err := base64.StdEncoding.DecodeString(eStr)
	if err != nil {
		fmt.Println(err)
	}
	var eBytes []byte
	if len(decE) < 8 {
		eBytes = make([]byte, 8-len(decE), 8)
		eBytes = append(eBytes, decE...)
	} else {
		eBytes = decE
	}
	eReader := bytes.NewReader(eBytes)
	var e uint64
	err = binary.Read(eReader, binary.BigEndian, &e)
	if err != nil {
		fmt.Println(err)
	}
	publicKey = &rsa.PublicKey{N: n, E: int(e)}

	return publicKey
}

*/
