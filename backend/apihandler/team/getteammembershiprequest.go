package team

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

func GetTeamMembershipRequest(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")
	iDTeam := c.Request.Header.Get("IDTeam")

	_, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	deactivateOldMembershipRequest(ctx, db, iDTeam)

	teamMembershipRequest := getTeamMembershipRequestIgnite(ctx, db, iDTeam)

	teamMembershipRequestByte, err := json.Marshal(teamMembershipRequest)
	if err != nil {
		fmt.Println(err)
	}

	c.Writer.Write(teamMembershipRequestByte)
}

func deactivateOldMembershipRequest(ctx context.Context, db *sql.DB, iDTeam string) {
	stmtStr := `
		update PUBLIC.TeamMembershipRequest 
		set IS_ACTIVE = false
		where ID_TEAM = ?
		and DATEDIFF('DAY',UPDATED_AT, current_timestamp()) > 6
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDTeam)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

func getTeamMembershipRequestIgnite(ctx context.Context, db *sql.DB, iDTeam string) []TeamMembershipRequest {

	stmtStr := `
		SELECT 
		COALESCE(tmr.id_hero,'')
		,COALESCE(tmr.id_team,'')
		,COALESCE(tmr.url,'')
		,COALESCE(hro.given_name,'')
		,COALESCE(hro.mood,'')
		,COALESCE(hro.status,'')
		,COALESCE(hrs.level,0)
		,COALESCE(hrs.judge_level,0)
		,COALESCE(hrs.task_count,0)
		,COALESCE(hrs.task_verify_count,0)
		,COALESCE(hrs.judgement_count,0)
		,COALESCE(hrs.judgement_verify_count,0)
		,COALESCE(hrs.xrank,0)
		,COALESCE(hrs.score,0)
		,COALESCE(hrs.coins,0)
		,COALESCE(hrs.love_energy,0)
		,COALESCE(hrs.dark_energy,0)
		FROM PUBLIC.TeamMembershipRequest tmr
		JOIN PUBLIC.Hero hro
		ON hro.id_hero = tmr.id_hero
		JOIN PUBLIC.HeroStat hrs
		ON hro.id_hero = hrs.id_hero
		WHERE tmr.id_team = ? and tmr.is_active = true
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDTeam)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	var teamMembershipRequest TeamMembershipRequest
	var arrayOfTeamMembershipRequest []TeamMembershipRequest

	for rows.Next() {
		if err := rows.Scan(
			&teamMembershipRequest.IDHero,
			&teamMembershipRequest.IDTeam,
			&teamMembershipRequest.URL,
			&teamMembershipRequest.GivenName,
			&teamMembershipRequest.Mood,
			&teamMembershipRequest.Status,
			&teamMembershipRequest.Level,
			&teamMembershipRequest.JudgeLevel,
			&teamMembershipRequest.TaskCount,
			&teamMembershipRequest.TaskVerifyCount,
			&teamMembershipRequest.JudgementCount,
			&teamMembershipRequest.JudgementVerifyCount,
			&teamMembershipRequest.Rank,
			&teamMembershipRequest.Score,
			&teamMembershipRequest.Coins,
			&teamMembershipRequest.LoveEnergy,
			&teamMembershipRequest.DarkEnergy,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		arrayOfTeamMembershipRequest = append(arrayOfTeamMembershipRequest, teamMembershipRequest)

	}

	return arrayOfTeamMembershipRequest

}
