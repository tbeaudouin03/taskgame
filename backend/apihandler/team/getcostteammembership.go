package team

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type CostTeamMembership struct {
	CostTeamMembership int
}

func GetCostTeamMembership(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")
	heroLevel := c.Request.Header.Get("Level")
	iDTeam := c.Request.Header.Get("IDTeam")

	heroLevelInt, err := strconv.Atoi(heroLevel)
	if err != nil {
		log.Printf(`heroLevelInt: `, err)
		return
	}
	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	log.Println(heroLevelInt, iDTeam)
	costTeamMembership := getCostTeamMembershipIgnite(ctx, db, heroInfo.IDHero, heroLevelInt, iDTeam)
	log.Println(costTeamMembership)
	costTeamMembershipByte, err := json.Marshal(costTeamMembership)
	if err != nil {
		fmt.Println(err)
	}

	c.Writer.Write(costTeamMembershipByte)
}

func getCostTeamMembershipIgnite(ctx context.Context, db *sql.DB, iDHero string, heroLevelInt int, iDTeam string) CostTeamMembership {

	// if isCurrentTeam or !teamExist then return CostTeamMembership = 0
	isCurrentTeam := isCurrentTeam(ctx, db, iDHero, iDTeam)

	if !isCurrentTeam {

		teamExist := teamExist(ctx, db, iDTeam)

		if teamExist {

			// CASE WHEN so that the only case when cost = 0 is when the team does not exist
			// TeamRequestCount so that if someone keeps changing team, the bonus becomes lower and lower
			stmtStr := `
				WITH TeamRequestCount AS (
					SELECT 
					COUNT(tmr.id_hero) TeamRequestCount
					FROM PUBLIC.TeamMembershipRequest tmr
					WHERE tmr.id_hero = ?
					)
				SELECT 
				COALESCE(
				CASE WHEN (FLOOR(CEIL(COUNT(hro.id_hero)/3.0) * (1+POWER(ABS(AVG(hrs.level)- ? ),1.2))) - 15 + TeamRequestCount) = 0 THEN 1
				ELSE FLOOR(CEIL(COUNT(hro.id_hero)/3.0) * (1+POWER(ABS(AVG(hrs.level)- ? ),1.2))) - 15 + TeamRequestCount END
				, 0)
				FROM PUBLIC.Hero hro
				JOIN PUBLIC.HeroStat hrs
				ON hro.id_hero = hrs.id_hero
				CROSS JOIN TeamRequestCount
				WHERE hro.id_team = ?
				`

			stmt, err := db.PrepareContext(ctx, stmtStr)
			if err != nil {
				log.Printf("failed to prepare statement: %v", err)
			}

			rows, err := stmt.QueryContext(ctx, iDHero, heroLevelInt, heroLevelInt, iDTeam)
			if err != nil {
				log.Fatalf("failed sql query: %v", err)
			}
			var costTeamMembership CostTeamMembership

			for rows.Next() {
				if err := rows.Scan(
					&costTeamMembership.CostTeamMembership,
				); err != nil {
					log.Fatalf("failed to get row: %v", err)
				}

			}

			return costTeamMembership
		}
	}

	var costTeamMembership CostTeamMembership
	costTeamMembership.CostTeamMembership = 0
	return costTeamMembership

}

func isCurrentTeam(ctx context.Context, db *sql.DB, iDHero string, iDTeam string) bool {

	stmtStr := `
			SELECT
			COALESCE(hro.id_team,'')
			FROM PUBLIC.Hero hro
			WHERE hro.id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var currentIDTeam string

	for rows.Next() {
		if err := rows.Scan(
			&currentIDTeam,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	if currentIDTeam == iDTeam {
		return true
	}
	return false

}

func teamExist(ctx context.Context, db *sql.DB, iDTeam string) bool {

	stmtStr := `
			SELECT
			COALESCE(MAX(hro.id_team),'')
			FROM PUBLIC.Hero hro
			WHERE hro.id_team = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDTeam)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var iDTeamExist string

	for rows.Next() {
		if err := rows.Scan(
			&iDTeamExist,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	if iDTeamExist != "" {
		return true
	}
	return false

}
