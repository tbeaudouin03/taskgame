package team

import (
	"github.com/tbeaudouin03/taskgame/backend/apihandler/teamadventure"

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

func PostAcceptTeamMembership(c *gin.Context) {

	TeamMembershipRequest := new(TeamMembershipRequest)
	err := c.Bind(TeamMembershipRequest)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	_, err = oauth.VerifyIDTokenReturnHeroInfo(TeamMembershipRequest.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postAcceptTeamMembershipIgnite(ctx, db, TeamMembershipRequest)
	teamadventure.ResetHeroMonsterState(ctx, db, TeamMembershipRequest.IDHero)

}

func postAcceptTeamMembershipIgnite(ctx context.Context, db *sql.DB, TeamMembershipRequest *TeamMembershipRequest) {

	// link hero to requested team
	stmtStr := `
		update PUBLIC.Hero 
		set ID_TEAM = ?
		, UPDATED_AT = current_timestamp()
		where ID_HERO = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		TeamMembershipRequest.IDTeam,
		TeamMembershipRequest.IDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// deactivate TeamMembershipRequest in TeamMembershipRequest table
	stmtStr = `
		update PUBLIC.TeamMembershipRequest 
		set IS_ACTIVE = false
		, IS_APPROVED = true
		, UPDATED_AT = current_timestamp()
		where ID_HERO = ? and ID_TEAM = ?
		`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		TeamMembershipRequest.IDHero,
		TeamMembershipRequest.IDTeam,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
