package team

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type TeamInfo struct {
	IDToken     string
	IDTeam      string
	Name        string
	FacebookURL string
	LinkedinURL string
}

func PostTeamInfo(c *gin.Context) {

	TeamInfo := new(TeamInfo)
	err := c.Bind(TeamInfo)
	if err != nil {
		log.Printf("could not read TeamInfo: %v", err)
	}

	_, err = oauth.VerifyIDTokenReturnHeroInfo(TeamInfo.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postTeamInfoIgnite(db, ctx, TeamInfo)

}

func postTeamInfoIgnite(db *sql.DB, ctx context.Context, TeamInfo *TeamInfo) {

	if TeamInfo.Name != "" {
		setTeamName(db, ctx, TeamInfo)
	}

	if TeamInfo.FacebookURL != "" {
		setFacebookGroup(db, ctx, TeamInfo)
	}

	if TeamInfo.LinkedinURL != "" {
		setLinkedinGroup(db, ctx, TeamInfo)
	}

}

func setTeamName(db *sql.DB, ctx context.Context, TeamInfo *TeamInfo) {

	stmtStr := `
		UPDATE PUBLIC.Team
		SET name = ?, updated_at = current_timestamp()
		WHERE id_team = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}
	if TeamInfo.Name == "delete" {
		_, err = stmt.ExecContext(ctx,
			"",
			TeamInfo.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	} else {
		_, err = stmt.ExecContext(ctx,
			TeamInfo.Name,
			TeamInfo.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}

}

func setFacebookGroup(db *sql.DB, ctx context.Context, TeamInfo *TeamInfo) {
	stmtStr := `
		UPDATE PUBLIC.Team
		SET facebook_group = ?, updated_at = current_timestamp()
		WHERE id_team = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}
	if TeamInfo.FacebookURL == "delete" {
		_, err = stmt.ExecContext(ctx,
			"",
			TeamInfo.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	} else {
		_, err = stmt.ExecContext(ctx,
			TeamInfo.FacebookURL,
			TeamInfo.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}

}

func setLinkedinGroup(db *sql.DB, ctx context.Context, TeamInfo *TeamInfo) {
	stmtStr := `
		UPDATE PUBLIC.Team
		SET linkedin_group = ?, updated_at = current_timestamp()
		WHERE id_team = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	if TeamInfo.LinkedinURL == "delete" {
		_, err = stmt.ExecContext(ctx,
			"",
			TeamInfo.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	} else {
		_, err = stmt.ExecContext(ctx,
			TeamInfo.LinkedinURL,
			TeamInfo.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}
}
