package team

import (
	"github.com/tbeaudouin03/taskgame/backend/apihandler/teamadventure"

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type MostAdaptedTeam struct {
	IDToken            string
	IDTeam             string
	RookieTrainedCount int
	HasFollower        int
	RandomTeamIDHero   string
}

func PostChooseRandomTeam(c *gin.Context) {

	MostAdaptedTeam := new(MostAdaptedTeam)
	err := c.Bind(MostAdaptedTeam)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MostAdaptedTeam.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	teamadventure.ResetHeroMonsterState(ctx, db, heroInfo.IDHero)

	getMostAdaptedTeam(ctx, db, heroInfo.IDHero, MostAdaptedTeam)

	if MostAdaptedTeam.HasFollower == 0 {
		HelpTeamLeader := new(HelpTeamLeader)
		HelpTeamLeader.Help = true
		HelpTeamLeader.IDTeam = MostAdaptedTeam.IDTeam
		postHelpTeamLeaderIgnite(ctx, db, HelpTeamLeader, MostAdaptedTeam.RandomTeamIDHero)
	}

	updateRookieTrainedCount(ctx, db, MostAdaptedTeam)

	TeamMembershipRequest := new(TeamMembershipRequest)
	TeamMembershipRequest.IDTeam = MostAdaptedTeam.IDTeam
	postTeamMembershipRequestIgnite(ctx, db, TeamMembershipRequest, heroInfo.IDHero, 0)
	TeamMembershipRequest.IDHero = heroInfo.IDHero
	postAcceptTeamMembershipIgnite(ctx, db, TeamMembershipRequest)
}

func getMostAdaptedTeam(ctx context.Context, db *sql.DB, iDHero string, MostAdaptedTeam *MostAdaptedTeam) {

	// nb: we deal with is_on_map by showing fake map and fake fight during tutorial
	// we deal with has_follower_leader by setting randomly one team member as follower if rookie joins
	stmtStr := `
	WITH TeamInfo AS (
		select 
		hro.id_team
		,MAX(hro.id_hero) RandomTeamIDHero
		,COALESCE(tm.rookie_trained_count,0) RookieTrainedCount
		,AVG(COALESCE(hrs.level,0)) AvgTeamLevel
		,MAX(CASE WHEN hro.is_bonded_team_leader OR hro.is_bonded_to_team THEN 1 ELSE 0 END) HasFollower
		from public.HeroStat hrs
		join public.Hero hro
		on hrs.id_hero = hro.id_hero
		join public.team tm
		on hro.id_team = tm.id_team
		where hro.id_hero <> ?
		group by hro.id_team
		having AvgTeamLevel < 5
		)
		, HeroInfo AS (
		select
		hrs.level HeroLevel
		from public.HeroStat hrs
		where hrs.id_hero = ?
		)
		
		select top 1
		id_team
		,RookieTrainedCount
		,HasFollower
		,RandomTeamIDHero
		from TeamInfo
		cross join HeroInfo
		order by 5/CAST(1+ABS(AvgTeamLevel-HeroLevel) as DOUBLE) + HasFollower + 3/CAST(1+RookieTrainedCount as DOUBLE) DESC
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&MostAdaptedTeam.IDTeam,
			&MostAdaptedTeam.RookieTrainedCount,
			&MostAdaptedTeam.HasFollower,
			&MostAdaptedTeam.RandomTeamIDHero,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

}

func updateRookieTrainedCount(ctx context.Context, db *sql.DB, MostAdaptedTeam *MostAdaptedTeam) {

	stmtStr := `
		UPDATE public.Team
		SET rookie_trained_count = ?
		WHERE id_team = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		MostAdaptedTeam.RookieTrainedCount+1,
		MostAdaptedTeam.IDTeam,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

}
