package team

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type MoodStatus struct {
	IDToken string
	Mood    string
	Status  string
}

func PostMoodStatus(c *gin.Context) {

	MoodStatus := new(MoodStatus)
	err := c.Bind(MoodStatus)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MoodStatus.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postMoodStatusIgnite(db, ctx, MoodStatus, heroInfo.IDHero)

}

func postMoodStatusIgnite(db *sql.DB, ctx context.Context, MoodStatus *MoodStatus, iDHero string) {

	if MoodStatus.Mood != "" {
		setMood(db, ctx, MoodStatus, iDHero)
	}

	if MoodStatus.Status != "" {
		setStatus(db, ctx, MoodStatus, iDHero)
	}

}

func setMood(db *sql.DB, ctx context.Context, MoodStatus *MoodStatus, iDHero string) {

	stmtStr := `
		UPDATE PUBLIC.Hero
		SET mood = ?
		WHERE id_hero = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		MoodStatus.Mood,
		iDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func setStatus(db *sql.DB, ctx context.Context, MoodStatus *MoodStatus, iDHero string) {

	stmtStr := `
		UPDATE PUBLIC.Hero
		SET status = ?
		WHERE id_hero = ?
		`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		MoodStatus.Status,
		iDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}
