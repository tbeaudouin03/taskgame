package team

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type TeamMembershipRequest struct {
	IDHero               string
	IDTeam               string
	IDToken              string
	URL                  string
	GivenName            string
	Mood                 string
	Status               string
	Level                int
	JudgeLevel           int
	TaskCount            int
	TaskVerifyCount      int
	JudgementCount       int
	JudgementVerifyCount int
	Rank                 int
	Score                int
	Coins                int
	LoveEnergy           int
	DarkEnergy           int
	CostTeamMembership   int
	IsApproved           bool
}

func PostTeamMembershipRequest(c *gin.Context) {

	TeamMembershipRequest := new(TeamMembershipRequest)
	err := c.Bind(TeamMembershipRequest)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(TeamMembershipRequest.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// cost comes from backend, not frontend, like this even if frontend is hacked, one cannot give oneself bonuses...
	costTeamMembership := getCostTeamMembershipIgnite(ctx, db, heroInfo.IDHero, TeamMembershipRequest.Level, TeamMembershipRequest.IDTeam)

	heroLoveEnergy := getHeroLoveEnergy(ctx, db, heroInfo.IDHero)
	if heroLoveEnergy-costTeamMembership.CostTeamMembership >= 0 {
		postTeamMembershipRequestIgnite(ctx, db, TeamMembershipRequest, heroInfo.IDHero, costTeamMembership.CostTeamMembership)
	}

}

func postTeamMembershipRequestIgnite(ctx context.Context, db *sql.DB, TeamMembershipRequest *TeamMembershipRequest, iDHero string, costTeamMembership int) {

	// cost / bonus is incurred when the request IS_APPROVED
	// team can accept a request when it IS_ACTIVE
	// if the request was approved before, then we just update IS_ACTIVE and url BUT NOT THE COST or is_approved
	// if the request is new, we update everything with merge (merge allow people to update pending requests but could be insert, not a big deal)
	// the hero can switch easily between teams he previously belonged to! A team member just has to approve.

	isPreviousApprovedTeam := isPreviousApprovedTeam(ctx, db, iDHero, TeamMembershipRequest.IDTeam)

	if isPreviousApprovedTeam {

		stmtStr := `
			UPDATE PUBLIC.TeamMembershipRequest 
			SET 
			is_active = true
			, url = ?
			, UPDATED_AT = current_timestamp()
			WHERE id_hero = ? 
			AND id_team = ?
			`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			TeamMembershipRequest.URL,
			iDHero,
			TeamMembershipRequest.IDTeam,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}

	} else {
		stmtStr := `
				MERGE INTO PUBLIC.TeamMembershipRequest(
				ID_HERO
				, ID_TEAM
				, URL
				, IS_ACTIVE
				, IS_APPROVED
				, CREATED_AT
				, UPDATED_AT
				, COST
				) VALUES (?,?,?, true, false, current_timestamp(), current_timestamp(), ?)
			`

		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx,
			iDHero,
			TeamMembershipRequest.IDTeam,
			TeamMembershipRequest.URL,
			costTeamMembership,
		)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}

}

func getHeroLoveEnergy(ctx context.Context, db *sql.DB, iDHero string) (loveEnergy int) {

	stmtStr := `
			SELECT 
			love_energy
			FROM PUBLIC.HeroStat
			WHERE id_hero = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&loveEnergy,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return loveEnergy

}

func isPreviousApprovedTeam(ctx context.Context, db *sql.DB, iDHero string, iDTeam string) (isPreviousApprovedTeam bool) {

	// if at least one id_team in TeamMembershipRequest is the same as iDTeam, then return TRUE
	// NB: if query returns no row, Golang default value for bool = false hence isPreviousApprovedTeam = false
	stmtStr := `
			SELECT
			COALESCE(CASE WHEN tmr.id_team IS NOT NULL THEN true
			ELSE false END, false) isPreviousApprovedTeam
			FROM PUBLIC.TeamMembershipRequest tmr
			WHERE tmr.id_hero = ?
			AND tmr.id_team = ?
			AND tmr.is_approved = true
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero, iDTeam)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&isPreviousApprovedTeam,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return isPreviousApprovedTeam

}
