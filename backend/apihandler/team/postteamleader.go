package team

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/teamadventure"
)

type TeamLeaderChoice struct {
	IDHero  string // IDHero of the chosen leader
	IDTeam  string
	IDToken string
}

// you should reset all monster_is_introduced etc. when changing is follower or is leader

func PostTeamLeaderChoice(c *gin.Context) {

	TeamLeaderChoice := new(TeamLeaderChoice)
	err := c.Bind(TeamLeaderChoice)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	_, err = oauth.VerifyIDTokenReturnHeroInfo(TeamLeaderChoice.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	PostTeamLeaderChoiceIgnite(ctx, db, TeamLeaderChoice)
	teamadventure.ResetTeamFollowerMonsterState(ctx, db, TeamLeaderChoice.IDHero)

}

func PostTeamLeaderChoiceIgnite(ctx context.Context, db *sql.DB, TeamLeaderChoice *TeamLeaderChoice) {

	// erase any previous team leader
	stmtStr := `
			UPDATE PUBLIC.Hero
			SET is_bonded_team_leader = false
			, UPDATED_AT = current_timestamp()
			WHERE id_team = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		TeamLeaderChoice.IDTeam,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// elect new team leader
	stmtStr = `
			UPDATE PUBLIC.Hero
			SET 
			is_bonded_team_leader = true
			, UPDATED_AT = current_timestamp()
			WHERE id_hero = ? 
			AND id_team = ?
			`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		TeamLeaderChoice.IDHero,
		TeamLeaderChoice.IDTeam,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

type HelpTeamLeader struct {
	Help    bool
	IDTeam  string
	IDToken string
}

func PostHelpTeamLeader(c *gin.Context) {

	HelpTeamLeader := new(HelpTeamLeader)
	err := c.Bind(HelpTeamLeader)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(HelpTeamLeader.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postHelpTeamLeaderIgnite(ctx, db, HelpTeamLeader, heroInfo.IDHero)
	teamadventure.ResetHeroMonsterState(ctx, db, heroInfo.IDHero)

}

func postHelpTeamLeaderIgnite(ctx context.Context, db *sql.DB, HelpTeamLeader *HelpTeamLeader, iDHero string) {

	stmtStr := `
			UPDATE PUBLIC.Hero
			SET 
			is_bonded_to_team = ?
			, UPDATED_AT = current_timestamp()
			WHERE id_hero = ? 
			AND id_team = ?
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		HelpTeamLeader.Help,
		iDHero,
		HelpTeamLeader.IDTeam,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
