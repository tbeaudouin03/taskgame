package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type TeamTutorialLevel struct {
	TeamTutorialLevel int
	IDToken           string
}

func PostTeamTutorialLevel(c *gin.Context) {

	TeamTutorialLevel := new(TeamTutorialLevel)
	err := c.Bind(TeamTutorialLevel)
	if err != nil {
		log.Printf("could not read TeamTutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(TeamTutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateTeamTutorialLevel(db, ctx, TeamTutorialLevel, heroInfo)

}

func updateTeamTutorialLevel(db *sql.DB, ctx context.Context, TeamTutorialLevel *TeamTutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.team_tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, TeamTutorialLevel.TeamTutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
