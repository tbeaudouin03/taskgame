package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type TutorialLevel struct {
	TutorialLevel int
	IDToken       string
}

func PostTutorialLevel(c *gin.Context) {

	TutorialLevel := new(TutorialLevel)
	err := c.Bind(TutorialLevel)
	if err != nil {
		log.Printf("could not read TutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(TutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateTutorialLevel(db, ctx, TutorialLevel, heroInfo)

}

func updateTutorialLevel(db *sql.DB, ctx context.Context, TutorialLevel *TutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, TutorialLevel.TutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
