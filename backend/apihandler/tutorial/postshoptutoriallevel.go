package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type ShopTutorialLevel struct {
	ShopTutorialLevel int
	IDToken           string
}

func PostShopTutorialLevel(c *gin.Context) {

	ShopTutorialLevel := new(ShopTutorialLevel)
	err := c.Bind(ShopTutorialLevel)
	if err != nil {
		log.Printf("could not read ShopTutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(ShopTutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateShopTutorialLevel(db, ctx, ShopTutorialLevel, heroInfo)

}

func updateShopTutorialLevel(db *sql.DB, ctx context.Context, ShopTutorialLevel *ShopTutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.shop_tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, ShopTutorialLevel.ShopTutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
