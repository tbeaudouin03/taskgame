package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type LeaderBoardTutorialLevel struct {
	LeaderBoardTutorialLevel int
	IDToken                  string
}

func PostLeaderBoardTutorialLevel(c *gin.Context) {

	LeaderBoardTutorialLevel := new(LeaderBoardTutorialLevel)
	err := c.Bind(LeaderBoardTutorialLevel)
	if err != nil {
		log.Printf("could not read LeaderBoardTutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(LeaderBoardTutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateLeaderBoardTutorialLevel(db, ctx, LeaderBoardTutorialLevel, heroInfo)

}

func updateLeaderBoardTutorialLevel(db *sql.DB, ctx context.Context, LeaderBoardTutorialLevel *LeaderBoardTutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.leaderboard_tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, LeaderBoardTutorialLevel.LeaderBoardTutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
