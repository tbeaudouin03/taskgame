package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type MenuTutorialLevel struct {
	MenuTutorialLevel int
	IDToken           string
}

func PostMenuTutorialLevel(c *gin.Context) {

	MenuTutorialLevel := new(MenuTutorialLevel)
	err := c.Bind(MenuTutorialLevel)
	if err != nil {
		log.Printf("could not read MenuTutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(MenuTutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateMenuTutorialLevel(db, ctx, MenuTutorialLevel, heroInfo)

}

func updateMenuTutorialLevel(db *sql.DB, ctx context.Context, MenuTutorialLevel *MenuTutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.menu_tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, MenuTutorialLevel.MenuTutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
