package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type JudgeTutorialLevel struct {
	JudgeTutorialLevel int
	IDToken            string
}

func PostJudgeTutorialLevel(c *gin.Context) {

	JudgeTutorialLevel := new(JudgeTutorialLevel)
	err := c.Bind(JudgeTutorialLevel)
	if err != nil {
		log.Printf("could not read JudgeTutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(JudgeTutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateJudgeTutorialLevel(db, ctx, JudgeTutorialLevel, heroInfo)

}

func updateJudgeTutorialLevel(db *sql.DB, ctx context.Context, JudgeTutorialLevel *JudgeTutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.judge_tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, JudgeTutorialLevel.JudgeTutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
