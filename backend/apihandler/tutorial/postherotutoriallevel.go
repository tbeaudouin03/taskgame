package tutorial

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type HeroTutorialLevel struct {
	HeroTutorialLevel int
	IDToken           string
}

func PostHeroTutorialLevel(c *gin.Context) {

	HeroTutorialLevel := new(HeroTutorialLevel)
	err := c.Bind(HeroTutorialLevel)
	if err != nil {
		log.Printf("could not read HeroTutorialLevel: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(HeroTutorialLevel.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	updateHeroTutorialLevel(db, ctx, HeroTutorialLevel, heroInfo)

}

func updateHeroTutorialLevel(db *sql.DB, ctx context.Context, HeroTutorialLevel *HeroTutorialLevel, heroInfo oauth.HeroInfo) {

	stmtStr := `
				UPDATE PUBLIC.Hero hro
				SET hro.hero_tutorial_level = ?
				WHERE hro.ID_HERO = ?
				`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, HeroTutorialLevel.HeroTutorialLevel, heroInfo.IDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}
