package hero

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"log"
	"time"

	//"strconv"
	"database/sql"

	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type InAppTask struct {
	IDToken string
	Text    string
}

func PostInAppTask(c *gin.Context) {

	InAppTask := new(InAppTask)
	err := c.Bind(InAppTask)
	if err != nil {
		log.Printf("could not read InAppTask: %v", err)
	}

	heroInfo, err1 := oauth.VerifyIDTokenReturnHeroInfo(InAppTask.IDToken)
	if err1 != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	postInAppTaskIgnit(db, ctx, InAppTask.Text, heroInfo.IDHero)

}
func postInAppTaskIgnit(db *sql.DB, ctx context.Context, text string, iDHero string) {
	now := time.Now()
	guid := xid.New()
	id := guid.String() + now.Format("20060102")
	stmtStr := `MERGE INTO PUBLIC.TASK(ID_TASK_1, ID_TASK_2, ID_HERO, CREATED_AT, UPDATED_AT, TITLE) VALUES (?,?,?,?,?,?)`
	args := make([]interface{}, 6)

	iDTask1 := id
	iDTask2 := "CreatedInTheApp" + now.Format("150405")
	args[0], args[1], args[2], args[3], args[4], args[5] = iDTask1, iDTask2, iDHero, now, now, text
	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}
