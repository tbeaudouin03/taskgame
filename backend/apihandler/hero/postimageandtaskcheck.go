package hero

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type TaskCheck struct {
	IDTask1 string
	IDTask2 string
	Title   string
	Checked bool
	URL     string
	IDImage string
	IDToken string
}

func PostImageAndTaskCheck(c *gin.Context) {

	TaskCheck := new(TaskCheck)
	err := c.Bind(TaskCheck)
	if err != nil {
		log.Printf("could not read TaskCheck: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(TaskCheck.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()
	updateTaskWithCheck(db, ctx, TaskCheck, heroInfo.IDHero)
	createTaskCheck(db, ctx, TaskCheck, heroInfo.IDHero)
	createImageCheck(db, ctx, TaskCheck, heroInfo.IDHero)

}

func updateTaskWithCheck(db *sql.DB, ctx context.Context, TaskCheck *TaskCheck, iDHero string) {

	stmtStr := `
	UPDATE PUBLIC.TASKRATE tsr
	SET tsr.CHECKED = true 
	WHERE tsr.ID_TASK_1 = ? 
	AND tsr.ID_TASK_2 = ?
	AND tsr.ID_HERO = ?;
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, TaskCheck.IDTask1, TaskCheck.IDTask2, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func createTaskCheck(db *sql.DB, ctx context.Context, TaskCheck *TaskCheck, iDHero string) {

	stmtStr := `MERGE INTO PUBLIC.TASKCHECK (ID_TASK_CHECK, ID_TASK_1, ID_TASK_2, ID_HERO, ID_IMAGE, TITLE, CREATED_AT, UPDATED_AT, IS_VERIFIED) 
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}
	now := time.Now()
	_, err = stmt.ExecContext(ctx, TaskCheck.IDImage, TaskCheck.IDTask1, TaskCheck.IDTask2, iDHero, TaskCheck.IDImage, TaskCheck.Title, now, now, false)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func createImageCheck(db *sql.DB, ctx context.Context, TaskCheck *TaskCheck, iDHero string) {

	stmtStr := `MERGE INTO PUBLIC.IMAGE (ID_IMAGE, URL, TITLE, CREATED_AT, UPDATED_AT) 
	VALUES (?, ?, ?, ?, ?);`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}
	now := time.Now()
	_, err = stmt.ExecContext(ctx, TaskCheck.IDImage, TaskCheck.URL, TaskCheck.Title, now, now)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}
