package hero

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

//"github.com/gin-gonic/gin"
// Required external App Engine library

type HeroData struct {
	IDHero                   string
	ServerLoggedOut          string
	ChartData                []Data
	Tasks                    []TaskGet
	Coins                    int
	LoveEnergy               int
	DarkEnergy               int
	Reputation               int
	Intelligence             int
	Fitness                  int
	Reliability              int
	TaskCount                int
	TaskVerifyCount          int
	JudgementCount           int
	JudgementVerifyCount     int
	Level                    int
	JudgeLevel               int
	URL                      string
	TutorialLevel            int
	HeroTutorialLevel        int
	JudgeTutorialLevel       int
	ShopTutorialLevel        int
	LeaderBoardTutorialLevel int
	TeamTutorialLevel        int
	MenuTutorialLevel        int
	MapTutorialLevel         int
	Rank                     int
	Score                    int
	JudgeScore               int
	JudgeRank                int
	ConnectionCount          int
	LastConnectAt            time.Time
	Mood                     string
	Status                   string
	Email                    string
	FacebookProfile          string
	LinkedinProfile          string
	IDTeam                   string
	TeamFacebookGroup        string
	TeamLinkedinGroup        string
	TeamName                 string
	IsBondedTeamLeader       bool
	IsBondedToTeam           bool
	MinuteUntilNextLoveGift  int
	MinuteUntilNextDarkGift  int
}

func (heroData *HeroData) AddTask(task TaskGet) {
	heroData.Tasks = append(heroData.Tasks, task)
}

func (heroData *HeroData) AddChartData(data Data) {
	heroData.ChartData = append(heroData.ChartData, data)
}

func (heroData *HeroData) SetURL(url string) {
	heroData.URL = url
}

func (data *Data) AddLine(lines ...Line) {

	for _, line := range lines {
		data.Data = append(data.Data, line)
	}
}

type Data struct {
	Data  []Line `json:"data"`
	Color string `json:"color"`
}

type Line struct {
	X string `json:"x"`
	Y int    `json:"y"`
}

type TaskGet struct {
	IDTask1        string
	IDTask2        string
	Title          string
	Checked        bool
	Coins          int
	LoveEnergy     int
	DarkEnergy     int
	Reputation     int
	Intelligence   int
	Fitness        int
	IsVerified     bool
	VerifiedAt     time.Time
	JudgementCount int
}

func GetHeroData(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	heroData := getHeroDataIgnite(ctx, db, heroInfo.IDHero)
	heroData.ServerLoggedOut = `false`

	//Convert the `purchaseRequestFormInputTable` variable to json
	heroDataByte, err := json.Marshal(heroData)
	if err != nil {
		fmt.Println(err)
	}

	// If all goes well, write the JSON list of purchaseRequestFormInputTable to the response
	c.Writer.Write(heroDataByte)
}

func getHeroDataIgnite(ctx context.Context, db *sql.DB, iDHero string) HeroData {

	// add hero statistics
	stmtStr := `SELECT 
	COALESCE(Task_Count,0), 
	COALESCE(Task_Verify_Count,0),
	COALESCE(Judgement_Count,0),
	COALESCE(Judgement_Verify_Count,0),
	COALESCE(W0_Task_Count,0),
	COALESCE(W1_Task_Count,0),
	COALESCE(W2_Task_Count,0),
	COALESCE(W3_Task_Count,0),
	COALESCE(W4_Task_Count,0),
	COALESCE(Coins,0),
	COALESCE(Love_Energy,0),
	COALESCE(Dark_Energy,0),
	COALESCE(Reputation,0),
	COALESCE(Intelligence,0),
	COALESCE(Fitness,0),
	COALESCE(Level,0),
	COALESCE(Judge_Level,0),
	COALESCE(xRank,0),
	COALESCE(xJudge_Rank,0),
	COALESCE(Score,0),
	COALESCE(Judge_Score,0)
	FROM PUBLIC.HEROSTAT
	WHERE ID_Hero = ?`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	var heroData HeroData
	var W0TaskCount,
		W1TaskCount,
		W2TaskCount,
		W3TaskCount,
		W4TaskCount int
	var data Data

	for rows.Next() {
		if err := rows.Scan(
			&heroData.TaskCount,
			&heroData.TaskVerifyCount,
			&heroData.JudgementCount,
			&heroData.JudgementVerifyCount,
			&W0TaskCount,
			&W1TaskCount,
			&W2TaskCount,
			&W3TaskCount,
			&W4TaskCount,
			&heroData.Coins,
			&heroData.LoveEnergy,
			&heroData.DarkEnergy,
			&heroData.Reputation,
			&heroData.Intelligence,
			&heroData.Fitness,
			&heroData.Level,
			&heroData.JudgeLevel,
			&heroData.Rank,
			&heroData.JudgeRank,
			&heroData.Score,
			&heroData.JudgeScore,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
		data.AddLine(
			Line{X: "W0 #Tasks", Y: W0TaskCount},
			Line{X: "W1 #Tasks", Y: W1TaskCount},
			Line{X: "W2 #Tasks", Y: W2TaskCount},
			Line{X: "W3 #Tasks", Y: W3TaskCount},
			Line{X: "W4 #Tasks", Y: W4TaskCount},
		)

		data.Color = "#4285F4"

		heroData.AddChartData(data)

	}

	// add tasks
	stmtStr = `
	SELECT TOP 25
	tsk.ID_Task_1
	,tsk.ID_Task_2
	,COALESCE(tsk.Title,'')
	,COALESCE(tsr.Checked,false)
	,COALESCE(tsr.Coins,0)
	,COALESCE(tsr.Love_Energy,0)
	,COALESCE(tsr.Dark_Energy,0)
	,COALESCE(tsr.Reputation,0)
	,COALESCE(tsr.Intelligence,0)
	,COALESCE(tsr.Fitness,0)
	,COALESCE(tsr.is_verified,false)
	,COALESCE(tsr.verified_at,'1900-01-01')
	,COALESCE(tsr.judgement_count,0)
	FROM PUBLIC.TASK tsk
	JOIN PUBLIC.TASKRATE tsr
	ON tsk.ID_Task_1 = tsr.ID_Task_1
	AND tsk.ID_Task_2 = tsr.ID_Task_2
	AND tsk.ID_Hero = tsr.ID_Hero
	WHERE tsk.ID_Hero = ?
	ORDER BY UPDATED_AT DESC
	`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err = stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var task TaskGet

	for rows.Next() {
		if err := rows.Scan(
			&task.IDTask1,
			&task.IDTask2,
			&task.Title,
			&task.Checked,
			&task.Coins,
			&task.LoveEnergy,
			&task.DarkEnergy,
			&task.Reputation,
			&task.Intelligence,
			&task.Fitness,
			&task.IsVerified,
			&task.VerifiedAt,
			&task.JudgementCount,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

		heroData.AddTask(task)

	}

	// add URL
	stmtStr = `
		SELECT
		sho.URL
		FROM PUBLIC.HERO hro
		JOIN PUBLIC.ShopObject sho
		ON hro.ID_IMAGE = sho.id_shop_object
		WHERE hro.ID_HERO = ?
		`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err = stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&heroData.URL,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	if heroData.URL == "" {
		heroData.SetURL("NA")
	}

	// add Hero info such as tutorial levels, last connection date, number of refreshes, status, mood, social_media, gmail, id_team
	stmtStr = `
	SELECT
	COALESCE(hro.id_hero, '')
	,COALESCE(hro.mood, '')
	,COALESCE(hro.status, '')
	,COALESCE(hro.email, '')
	,COALESCE(hro.facebook_profile, '')
	,COALESCE(hro.linkedin_profile, '')
	,COALESCE(hro.id_team, '')
	,COALESCE(hro.TUTORIAL_LEVEL, 0)
	,COALESCE(hro.HERO_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.JUDGE_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.SHOP_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.LEADERBOARD_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.TEAM_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.MAP_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.MENU_TUTORIAL_LEVEL, 0)
	,COALESCE(hro.CONNECTION_COUNT, 0)
	,COALESCE(hro.LAST_CONNECT_AT, current_timestamp())
	,COALESCE(hro.is_bonded_team_leader, false)
	,COALESCE(hro.is_bonded_to_team, false)
	,COALESCE(MINUTE_UNTIL_NEXT_LOVE_GIFT,0)
	,COALESCE(MINUTE_UNTIL_NEXT_DARK_GIFT,0)
	FROM PUBLIC.HERO hro
	WHERE hro.ID_HERO = ?
		`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err = stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&heroData.IDHero,
			&heroData.Mood,
			&heroData.Status,
			&heroData.Email,
			&heroData.FacebookProfile,
			&heroData.LinkedinProfile,
			&heroData.IDTeam,
			&heroData.TutorialLevel,
			&heroData.HeroTutorialLevel,
			&heroData.JudgeTutorialLevel,
			&heroData.ShopTutorialLevel,
			&heroData.LeaderBoardTutorialLevel,
			&heroData.TeamTutorialLevel,
			&heroData.MapTutorialLevel,
			&heroData.MenuTutorialLevel,
			&heroData.ConnectionCount,
			&heroData.LastConnectAt,
			&heroData.IsBondedTeamLeader,
			&heroData.IsBondedToTeam,
			&heroData.MinuteUntilNextLoveGift,
			&heroData.MinuteUntilNextDarkGift,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	log.Println(heroData.IDTeam)
	if heroData.IDTeam == "" {

		// create unique global id for id_team
		guid := xid.New()
		iDTeam := guid.String()

		// create hero's team
		createHeroTeam(ctx, db, iDHero, iDTeam)

		heroData.IDTeam = iDTeam

	} else {

		// add team social media groups
		stmtStr = `
			SELECT
			COALESCE(facebook_group, '')
			,COALESCE(linkedin_group, '')
			,COALESCE(name, '')
			FROM PUBLIC.team
			WHERE id_team = ?
		`

		stmt, err = db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		rows, err = stmt.QueryContext(ctx, heroData.IDTeam)
		if err != nil {
			log.Fatalf("failed sql query: %v", err)
		}

		for rows.Next() {
			if err := rows.Scan(
				&heroData.TeamFacebookGroup,
				&heroData.TeamLinkedinGroup,
				&heroData.TeamName,
			); err != nil {
				log.Fatalf("failed to get row: %v", err)
			}
		}
	}

	return heroData

}

func createHeroTeam(ctx context.Context, db *sql.DB, iDHero string, iDTeam string) {

	// create team in team table
	stmtStr := `insert into team (ID_TEAM, created_at) values (?, current_timestamp())`
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}
	_, err = stmt.ExecContext(ctx, iDTeam)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	// link hero to the team we just created
	stmtStr = `update hero set id_team = ? where id_hero = ?`
	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}
	_, err = stmt.ExecContext(ctx, iDTeam, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}
