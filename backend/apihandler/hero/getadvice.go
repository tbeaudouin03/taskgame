package hero

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type Advice struct {
	Greeting    string
	Punctuation string
	Message     string
}

func GetAdvice(c *gin.Context) {

	iDToken := c.Request.Header.Get("Authorization")
	screen := c.Request.Header.Get("Screen")
	taskCount := c.Request.Header.Get("TaskCount")
	var taskCountI int
	taskCountI, err := strconv.Atoi(taskCount)
	if err != nil {
		log.Println(`taskCountI is not an integer`, taskCount)
	}

	_, err = oauth.VerifyIDTokenReturnHeroInfo(iDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	advice := getAdviceIgnite(ctx, db, taskCountI, screen)

	adviceByte, err := json.Marshal(advice)
	if err != nil {
		fmt.Println(err)
	}

	c.Writer.Write(adviceByte)
}

func getAdviceIgnite(ctx context.Context, db *sql.DB, taskCountI int, screen string) Advice {

	adviceRules := getAdviceRule(ctx, db, screen)

	var buffer bytes.Buffer
	buffer.WriteString(`where task_count_unlock <= ? and (advice_rule = 'both'`)

	for _, adviceRule := range adviceRules {
		buffer.WriteString(` OR advice_rule = '` + adviceRule + `'`)
	}

	buffer.WriteString(`) `)

	adviceRulesSQL := buffer.String()

	stmtStr := `
			select top 1 
			greeting
			,punctuation
			,message
			from (
			select
			greeting
			,punctuation
			,message
			,RAND() RAND_RANK
			from PUBLIC.advice 
			` + adviceRulesSQL + `
			) temp
			order by RAND_RANK
			`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx, taskCountI)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	var advice Advice

	for rows.Next() {
		if err := rows.Scan(
			&advice.Greeting,
			&advice.Punctuation,
			&advice.Message,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}

	}

	return advice

}

func getAdviceRule(ctx context.Context, db *sql.DB, screen string /*, heroInfo oauth.HeroInfo*/) []string {

	var adviceRules []string

	adviceRules = append(adviceRules, screen)

	return adviceRules
}
