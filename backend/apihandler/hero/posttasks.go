package hero

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"database/sql"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type Tasks struct {
	Tasks   []Task
	IDToken string
}

type Task struct {
	ID        string
	Title     string
	Updated   string
	Status    string
	Completed string
}

func PostTasks(c *gin.Context) {

	Tasks := new(Tasks)
	err := c.Bind(Tasks)
	if err != nil {
		log.Printf("could not read Tasks: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(Tasks.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	go addConnectionCount(db, ctx, heroInfo.IDHero)

	// created_at = completed, updated_at = updated
	// first need to get current skills to calculate added coins and skills
	mergeTasksIntoIgnite(db, ctx, Tasks, heroInfo.IDHero)

	// get hero data from database
	heroExist := checkHeroExist(db, ctx, heroInfo.IDHero)

	// if heroInfo does not exist then create it
	if !heroExist {

		heroInfo.IDHero = heroInfo.IDHero
		heroInfo.Level = 0
		heroInfo.JudgeLevel = 0
		heroInfo.Coins = 0
		heroInfo.Reputation = 0
		heroInfo.Intelligence = 0
		heroInfo.Fitness = 0
		heroInfo.Reliability = 100
		heroInfo.Score = 0
		heroInfo.JudgeScore = 0

		createHero(heroInfo, ctx, db)

		rateTasks(heroInfo.IDHero, db, ctx)

		// else only rate tasks
	} else {
		rateTasks(heroInfo.IDHero, db, ctx)
	}

	// need to update hero
}

func addConnectionCount(db *sql.DB, ctx context.Context, iDHero string) {
	stmtStr := `
	UPDATE PUBLIC.HERO 
	SET last_connect_at = current_timestamp(), 
	connection_count = (SELECT COALESCE(connection_count,0) + 1 FROM PUBLIC.HERO WHERE id_hero = ?)
	WHERE id_hero = ?
	`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		iDHero,
		iDHero,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

func mergeTasksIntoIgnite(db *sql.DB, ctx context.Context, Tasks *Tasks, iDHero string) {

	stmtStr := `MERGE INTO PUBLIC.TASK(ID_TASK_1, ID_TASK_2, ID_HERO, CREATED_AT, UPDATED_AT, TITLE) VALUES (?,?,?,?,?,?)`
	args := make([]interface{}, 6)
	for _, task := range Tasks.Tasks {

		// split iDTask because database does not like very long string FOR KEY

		iDTask1 := task.ID[:len(task.ID)/2]
		iDTask2 := task.ID[len(task.ID)/2:]
		args[0], args[1], args[2], args[3], args[4], args[5] = iDTask1, iDTask2, iDHero, task.Completed, task.Updated, task.Title
		// insert using prepare statement
		stmt, err := db.PrepareContext(ctx, stmtStr)
		if err != nil {
			log.Printf("failed to prepare statement: %v", err)
		}

		_, err = stmt.ExecContext(ctx, args...)
		if err != nil {
			log.Printf("failed sql execute: %v", err)
		}
	}
}

func checkHeroExist(db *sql.DB, ctx context.Context, iDHero string) bool {
	// select
	stmt, err := db.PrepareContext(ctx,
		`SELECT hro.ID_HERO 
		FROM PUBLIC.HERO hro
		JOIN PUBLIC.HEROSTAT hrs
		ON hro.ID_HERO = hrs.ID_HERO
		WHERE hro.ID_HERO = ?
		AND hrs.COINS IS NOT NULL`)
	if err != nil {
		log.Fatalf("failed to prepare statement: %v", err)
	}
	rows, err := stmt.QueryContext(ctx, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}
	return rows.Next()
}

func createHero(heroInfo oauth.HeroInfo, ctx context.Context, db *sql.DB) {

	now := time.Now()

	stmtStr := `INSERT INTO PUBLIC.HERO(
		ID_HERO
		,ID_IMAGE
		,CREATED_AT
		,UPDATED_AT
		,EMAIL
		,GIVEN_NAME
		,FAMILY_NAME)
	VALUES (?,?,?,?,?,?,?)`

	// insert using prepare statement
	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		heroInfo.IDHero,
		`NA`,
		now,
		now,
		heroInfo.Email,
		heroInfo.GivenName,
		heroInfo.FamilyName)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	stmtStr = `INSERT INTO PUBLIC.HEROSTAT(
		ID_HERO
		,LEVEL
		,JUDGE_LEVEL
		,COINS
		,REPUTATION
		,INTELLIGENCE
		,FITNESS
		,RELIABILITY
		,SCORE
		,JUDGE_SCORE)
	VALUES (?,?,?,?,?,?,?,?);`

	// insert using prepare statement
	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		heroInfo.IDHero,
		heroInfo.Level,
		heroInfo.JudgeLevel,
		heroInfo.Coins,
		heroInfo.Reputation,
		heroInfo.Intelligence,
		heroInfo.Fitness,
		heroInfo.Reliability,
		heroInfo.Score,
		heroInfo.JudgeScore,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}
}

func rateTasks(iDHero string, db *sql.DB, ctx context.Context) {

	stmtStr := `
	WITH UNRATEDTASK AS (SELECT 
		tsk.ID_TASK_1
		, tsk.ID_TASK_2
	FROM PUBLIC.TASK tsk
	LEFT JOIN PUBLIC.TASKRATE tsr
	ON tsk.ID_TASK_1 = tsr.ID_TASK_1
	AND tsk.ID_TASK_2 = tsr.ID_TASK_2
	WHERE tsr.COINS IS NULL AND tsk.ID_HERO = ?),
	
	COEFRELIABILITY AS (
	SELECT RELIABILITY COEF_RELIABILITY
	FROM PUBLIC.HEROSTAT 
	WHERE ID_HERO = ?),
	
	COEFCOINS AS (
		SELECT 
		CASE WHEN FITNESS < 100
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.5)) 
		WHEN FITNESS < 150
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.52))
		WHEN FITNESS < 200
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.53))
		WHEN FITNESS < 200
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.57))
		WHEN FITNESS < 250
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.6))
		WHEN FITNESS < 300
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.63))
		WHEN FITNESS < 400
		THEN 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.66))
		ELSE 1 + 9 * EXP(-8 / POWER(1 + FITNESS,0.7))
		END
		COEF_COINS
	FROM PUBLIC.HEROSTAT
	WHERE ID_HERO = ?),
	
	COEFSKILL AS (
		SELECT 
		CASE WHEN INTELLIGENCE < 100
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.5)) 
		WHEN INTELLIGENCE < 150
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.52))
		WHEN INTELLIGENCE < 200
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.53))
		WHEN INTELLIGENCE < 200
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.57))
		WHEN INTELLIGENCE < 250
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.6))
		WHEN INTELLIGENCE < 300
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.63))
		WHEN INTELLIGENCE < 400
		THEN 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.66))
		ELSE 1 + 9 * EXP(-8 / POWER(1 + INTELLIGENCE,0.7))
		END
		COEF_SKILL
	FROM PUBLIC.HEROSTAT
	WHERE ID_HERO = ?)
	
	INSERT INTO PUBLIC.TASKRATE(ID_TASK_1, ID_TASK_2, ID_HERO, COINS, REPUTATION, INTELLIGENCE, FITNESS)
	(
	SELECT
	   urt.ID_TASK_1
	  ,urt.ID_TASK_2
	  ,? ID_HERO
	  ,CEIL(COALESCE(coc.COEF_COINS,1) * cor.COEF_RELIABILITY * 2 * (1 + RAND())) COINS
	  ,CEIL(ROUND(COALESCE(cok.COEF_SKILL,0.1) * cor.COEF_RELIABILITY * 0.02 * RAND() / 3)) REPUTATION
	  ,CEIL(ROUND(COALESCE(cok.COEF_SKILL,0.1) * cor.COEF_RELIABILITY * 0.02 * RAND() / 3)) INTELLIGENCE
	  ,CEIL(ROUND(COALESCE(cok.COEF_SKILL,0.1) * cor.COEF_RELIABILITY * 0.02 * RAND() / 3)) FITNESS
	FROM UNRATEDTASK urt
	CROSS JOIN COEFRELIABILITY cor
	CROSS JOIN COEFCOINS coc
	CROSS JOIN COEFSKILL cok
	);
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero, iDHero, iDHero, iDHero, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

	stmtStr = `
		MERGE INTO PUBLIC.TASKRATE(ID_TASK_1, ID_TASK_2, ID_HERO, COINS, LOVE_ENERGY, REPUTATION, INTELLIGENCE, FITNESS)
		VALUES(?, ?, ?, 19000, 100, 0, 0, 0);`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx, iDHero, iDHero, iDHero)
	if err != nil {
		log.Printf("failed sql execute: %v", err)
	}

}

/*
var buffer bytes.Buffer
	buffer.WriteString("MERGE INTO PUBLIC.TASK(ID_TASK, ID_HERO, CREATED_AT, UPDATED_AT, TITLE) VALUES (?,?,?,?,?)")
	var args []interface{}
	args = append(
		args,
		Tasks.Tasks[0].ID,
		iDHero,
		Tasks.Tasks[0].Completed,
		Tasks.Tasks[0].Updated,
		Tasks.Tasks[0].Title,
	)
	loopLen := len(Tasks.Tasks)
	if loopLen > 1 {
		for i := 1; i < loopLen; i++ {
			buffer.WriteString(",(?,?,?,?,?)")
			args = append(
				args,
				Tasks.Tasks[i].ID,
				iDHero,
				Tasks.Tasks[i].Completed,
				Tasks.Tasks[i].Updated,
				Tasks.Tasks[i].Title,
			)
		}
	}
	stmtStr := buffer.String()*/
