package hero

import (

	//"github.com/gin-gonic/gin"
	// Required external App Engine library

	"context"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/ignite"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/oauth"
)

type IDToken struct {
	IDToken string
}

func CalculateHeroStat(c *gin.Context) {

	IDToken := new(IDToken)
	err := c.Bind(IDToken)
	if err != nil {
		log.Printf("could not read IDToken: %v", err)
	}

	heroInfo, err := oauth.VerifyIDTokenReturnHeroInfo(IDToken.IDToken)
	if err != nil {
		log.Printf(`error oauth: %v`, err)
		return
	}

	iDHero := heroInfo.IDHero

	ctx := context.Background()
	db := ignite.IgniteConnect(ctx)
	defer db.Close()

	/*
		if we want a chart with past task performance, can have yet another query
		,
		W0TASKCOUNT AS (SELECT COUNT(ID_TASK_2) W0_TASK_COUNT FROM PUBLIC.TASK WHERE ID_HERO = ? AND CREATED_AT BETWEEN DATEADD('DAY', -7, CURRENT_TIMESTAMP()) AND CURRENT_TIMESTAMP()),
		W1TASKCOUNT AS (SELECT COUNT(ID_TASK_2) W1_TASK_COUNT FROM PUBLIC.TASK WHERE ID_HERO = ? AND CREATED_AT BETWEEN DATEADD('DAY', -14, CURRENT_TIMESTAMP()) AND DATEADD('DAY', -7, CURRENT_TIMESTAMP())),
		W2TASKCOUNT AS (SELECT COUNT(ID_TASK_2) W2_TASK_COUNT FROM PUBLIC.TASK WHERE ID_HERO = ? AND CREATED_AT BETWEEN DATEADD('DAY', -21, CURRENT_TIMESTAMP()) AND DATEADD('DAY', -14, CURRENT_TIMESTAMP())),
		W3TASKCOUNT AS (SELECT COUNT(ID_TASK_2) W3_TASK_COUNT FROM PUBLIC.TASK WHERE ID_HERO = ? AND CREATED_AT BETWEEN DATEADD('DAY', -28, CURRENT_TIMESTAMP()) AND DATEADD('DAY', -21, CURRENT_TIMESTAMP())),
		W4TASKCOUNT AS (SELECT COUNT(ID_TASK_2) W4_TASK_COUNT FROM PUBLIC.TASK WHERE ID_HERO = ? AND CREATED_AT BETWEEN DATEADD('DAY', -35, CURRENT_TIMESTAMP()) AND DATEADD('DAY', -28, CURRENT_TIMESTAMP()))
	*/

	// taskcount, taskverifycount, judgementcount, judgementverifycount
	stmtStr := `
	WITH TASKCOUNT AS (
		SELECT 
		COALESCE(COUNT(ID_TASK_2),0) TASK_COUNT 
		FROM PUBLIC.TASK 
		WHERE ID_HERO = ?),
	TASKVERIFYCOUNT AS (
		SELECT 
		COALESCE(COUNT(ID_TASK_2),0) 
		TASK_VERIFY_COUNT 
		FROM PUBLIC.TASKCHECK 
		WHERE ID_HERO = ? 
		AND IS_VERIFIED = true),
	JUDGEMENTCOUNT AS (
		SELECT 
		COALESCE(COUNT(ID_TASK_2),0) JUDGEMENT_COUNT 
		FROM PUBLIC.JUDGEOPINION 
		WHERE ID_HERO = ?),
	JUDGEMENTVERIFYCOUNT AS (
		SELECT 
		COALESCE(COUNT(jop.ID_TASK_2),0) JUDGEMENT_VERIFY_COUNT 
		FROM PUBLIC.JUDGEOPINION  jop
		JOIN PUBLIC.TASKCHECK tsc
		ON jop.id_task_check = tsc.id_task_check
		WHERE tsc.ID_HERO = ?
		and tsc.is_verified = true
	)
	
	SELECT
	TASK_COUNT
	,TASK_VERIFY_COUNT
	,JUDGEMENT_COUNT
	,JUDGEMENT_VERIFY_COUNT
	FROM TASKCOUNT
	CROSS JOIN TASKVERIFYCOUNT
	CROSS JOIN JUDGEMENTCOUNT
	CROSS JOIN JUDGEMENTVERIFYCOUNT
	`

	stmt, err := db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err := stmt.QueryContext(ctx,
		iDHero,
		iDHero,
		iDHero,
		iDHero,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&heroInfo.TaskCount,
			&heroInfo.TaskVerifyCount,
			&heroInfo.JudgementCount,
			&heroInfo.JudgementVerifyCount,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	// hero stats such as total number of coins, energy, levels, skills and score
	stmtStr = `
	WITH TASKSKILLCOINSENERGY AS (
		SELECT
		COALESCE(SUM(COINS),0) COINS
		, COALESCE(SUM(Love_Energy),0) Love_Energy
		, COALESCE(SUM(Dark_Energy),0) Dark_Energy
		, COALESCE(SUM(REPUTATION),0) REPUTATION
		, COALESCE(SUM(INTELLIGENCE),0) INTELLIGENCE
		, COALESCE(SUM(FITNESS),0) FITNESS
		FROM PUBLIC.TASKRATE
		WHERE ID_HERO = ?
),
JUDGEREPUTATIONCOINSENERGY AS (
		SELECT
		COALESCE(SUM(JUDGE_COINS),0) JUDGE_COINS
		,COALESCE(SUM(JUDGE_LOVE),0) JUDGE_LOVE
		,COALESCE(SUM(JUDGE_DARK),0) JUDGE_DARK
		,COALESCE(SUM(JUDGE_REPUTATION),0) JUDGE_REPUTATION
		FROM PUBLIC.JUDGEOPINION
		WHERE ID_JUDGE = ?
),
HeroObjectPrice AS (
		SELECT
		COALESCE(SUM(sho.Price),0) ObjectPrice
		,COALESCE(SUM(sho.Love_Price),0) ObjectLovePrice
		,COALESCE(SUM(sho.Dark_Price),0) ObjectDarkPrice
		FROM PUBLIC.ShopObject sho
		JOIN PUBLIC.HEROOBJECT ho
		ON sho.id_shop_object = ho.id_image
		WHERE ho.id_hero = ?
),
HeroObjectSkill AS (
		SELECT
		COALESCE(SUM(sho.Reputation),0) ObjectReputation
		,COALESCE(SUM(sho.Intelligence),0) ObjectIntelligence
		,COALESCE(SUM(sho.Fitness),0) ObjectFitness
		FROM PUBLIC.ShopObject sho
		JOIN PUBLIC.HEROOBJECT ho
		ON sho.id_shop_object = ho.id_image
		WHERE ho.id_hero = ?
		AND ho.is_used = true
),
HeroUniqueObjectPriceSkill AS (
		SELECT
		COALESCE(SUM(sho.Price),0) UniqueObjectPrice
		,COALESCE(SUM(sho.Love_Price),0) UniqueObjectLovePrice
		,COALESCE(SUM(sho.Dark_Price),0) UniqueObjectDarkPrice
		,COALESCE(SUM(sho.Reputation),0) UniqueObjectReputation
		,COALESCE(SUM(sho.Intelligence),0) UniqueObjectIntelligence
		,COALESCE(SUM(sho.Fitness),0) UniqueObjectFitness
		FROM PUBLIC.ShopObject sho
		JOIN PUBLIC.HEROUNIQUEOBJECT huo
		ON sho.id_shop_object = huo.id_image
		WHERE huo.id_hero = ?
),
TeamMembershipRequestCost AS (
		SELECT
		COALESCE(SUM(tmr.Cost),0) TeamMembershipRequestCost
		FROM PUBLIC.TeamMembershipRequest tmr
		WHERE tmr.id_hero = ?
		AND tmr.is_approved = true
),
PreFinal AS (
		SELECT
		0.01 * COINS SCORE
		,0.01 * JUDGE_COINS JUDGE_SCORE
		,(COINS + JUDGE_COINS) * 0.01 - ObjectPrice - UniqueObjectPrice COINS
		,(Love_Energy + JUDGE_Love) - ObjectLovePrice - UniqueObjectLovePrice - TeamMembershipRequestCost Love_Energy
		,(Dark_Energy + JUDGE_Dark) - ObjectDarkPrice - UniqueObjectDarkPrice Dark_Energy
		,0.01 * (CASE WHEN REPUTATION + ObjectReputation + UniqueObjectReputation + JUDGE_REPUTATION < 0 THEN 0
		ELSE REPUTATION + JUDGE_REPUTATION + ObjectReputation + UniqueObjectReputation END) REPUTATION
		,0.01 * (INTELLIGENCE + ObjectIntelligence + UniqueObjectIntelligence) INTELLIGENCE
		,0.01 * (FITNESS + ObjectFitness + UniqueObjectFitness) FITNESS
		,0.0011 *(REPUTATION + ObjectReputation + UniqueObjectReputation + INTELLIGENCE + ObjectIntelligence + UniqueObjectIntelligence + FITNESS + ObjectFitness + UniqueObjectFitness) LEVEL
		,CASE WHEN JUDGE_REPUTATION > 0
		THEN POWER(0.01 * (JUDGE_REPUTATION),0.95)
		ELSE JUDGE_REPUTATION END JUDGE_LEVEL

		FROM TASKSKILLCOINSENERGY
		CROSS JOIN JUDGEREPUTATIONCOINSENERGY
		CROSS JOIN HeroObjectPrice
		CROSS JOIN HeroObjectSkill
		CROSS JOIN HeroUniqueObjectPriceSkill
		CROSS JOIN TeamMembershipRequestCost
)

		SELECT
		CEIL(COINS) COINS
		,CEIL(Love_Energy) Love_Energy
		,CEIL(Dark_Energy) Dark_Energy
		,CEIL(POWER(REPUTATION,0.95)) REPUTATION
		,CEIL(POWER(INTELLIGENCE,0.95)) INTELLIGENCE
		,CEIL(POWER(FITNESS,0.95)) FITNESS
		,CEIL(POWER(LEVEL,0.95)) LEVEL
		,CEIL(JUDGE_LEVEL) JUDGE_LEVEL
		,CEIL(POWER(SCORE,0.95)) SCORE
		,CEIL(POWER(JUDGE_SCORE,0.95)) JUDGE_SCORE
		FROM PreFinal
	`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err = stmt.QueryContext(ctx,
		iDHero,
		iDHero,
		iDHero,
		iDHero,
		iDHero,
		iDHero,
	)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&heroInfo.Coins,
			&heroInfo.LoveEnergy,
			&heroInfo.DarkEnergy,
			&heroInfo.Reputation,
			&heroInfo.Intelligence,
			&heroInfo.Fitness,
			&heroInfo.Level,
			&heroInfo.JudgeLevel,
			&heroInfo.Score,
			&heroInfo.JudgeScore,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	// reliability
	stmtStr = `
	WITH DATESINCEVERIFY AS (SELECT MAX(VERIFIED_AT) DATE_SINCE_VERIFY FROM PUBLIC.TASKCHECK WHERE ID_HERO = ?),		
	NUMBEROFTASKSINCEVERIFY AS (SELECT COUNT(ID_TASK_2) NUMBER_OF_TASK_SINCE_VERIFY
	FROM PUBLIC.TASK tsk
	CROSS JOIN DATESINCEVERIFY dsv
	WHERE tsk.ID_HERO = ?
	AND tsk.CREATED_AT > CASE WHEN dsv.DATE_SINCE_VERIFY IS NULL THEN '1900-01-01' ELSE dsv.DATE_SINCE_VERIFY END)

	SELECT 
	ROUND(
	CASE WHEN NUMBER_OF_TASK_SINCE_VERIFY < 30
	THEN 150 / POWER(1 + NUMBER_OF_TASK_SINCE_VERIFY,0.2)
  	WHEN NUMBER_OF_TASK_SINCE_VERIFY < 60
	THEN 150 / POWER(1 + NUMBER_OF_TASK_SINCE_VERIFY,0.3)
	WHEN NUMBER_OF_TASK_SINCE_VERIFY < 100
	THEN 150 / POWER(1 + NUMBER_OF_TASK_SINCE_VERIFY,0.5) 
	ELSE 150 / POWER(1 + NUMBER_OF_TASK_SINCE_VERIFY,0.7)
	END
	) RELIABILITY 
	
	FROM NUMBEROFTASKSINCEVERIFY
	`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err = stmt.QueryContext(ctx, iDHero, iDHero)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(&heroInfo.Reliability); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	// rank and judgeRank
	stmtStr = `
	WITH xRank AS (SELECT COUNT (DISTINCT CASE WHEN Score > ? THEN Score ELSE 0 END) xRank FROM PUBLIC.HeroStat),
	xJudgeRank AS (SELECT COUNT (DISTINCT CASE WHEN Judge_Score > ? THEN Judge_Score ELSE 0 END) xJudgeRank FROM PUBLIC.HeroStat)
	SELECT
	xRank
	,xJudgeRank
	FROM xRank
	CROSS JOIN xJudgeRank
	`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	rows, err = stmt.QueryContext(ctx, heroInfo.Score, heroInfo.JudgeScore)
	if err != nil {
		log.Fatalf("failed sql query: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(
			&heroInfo.Rank,
			&heroInfo.JudgeRank,
		); err != nil {
			log.Fatalf("failed to get row: %v", err)
		}
	}

	// finally merge all the data into herostat
	stmtStr = `MERGE INTO PUBLIC.HEROSTAT(
		ID_HERO
		,TASK_COUNT
		,TASK_VERIFY_COUNT
		,JUDGEMENT_COUNT
		,JUDGEMENT_VERIFY_COUNT
		,W0_TASK_COUNT
		,W1_TASK_COUNT
		,W2_TASK_COUNT
		,W3_TASK_COUNT
		,W4_TASK_COUNT
		,COINS
		,LOVE_ENERGY
		,DARK_ENERGY
		,REPUTATION
		,INTELLIGENCE
		,FITNESS
		,RELIABILITY
		,LEVEL
		,JUDGE_LEVEL
		,xRANK
		,xJUDGE_RANK
		,SCORE
		,JUDGE_SCORE
		)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`

	stmt, err = db.PrepareContext(ctx, stmtStr)
	if err != nil {
		log.Printf("failed to prepare statement: %v", err)
	}

	_, err = stmt.ExecContext(ctx,
		iDHero,
		heroInfo.TaskCount,
		heroInfo.TaskVerifyCount,
		heroInfo.JudgementCount,
		heroInfo.JudgementVerifyCount,
		heroInfo.W0TaskCount,
		heroInfo.W1TaskCount,
		heroInfo.W2TaskCount,
		heroInfo.W3TaskCount,
		heroInfo.W4TaskCount,
		heroInfo.Coins,
		heroInfo.LoveEnergy,
		heroInfo.DarkEnergy,
		heroInfo.Reputation,
		heroInfo.Intelligence,
		heroInfo.Fitness,
		heroInfo.Reliability,
		heroInfo.Level,
		heroInfo.JudgeLevel,
		heroInfo.Rank,
		heroInfo.JudgeRank,
		heroInfo.Score,
		heroInfo.JudgeScore,
	)
	if err != nil {
		log.Printf("failed sql execute: %v", err)

	}
}
