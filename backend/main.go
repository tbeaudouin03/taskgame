package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/adventure"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/hero"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/judge"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/leaderboard"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/shop"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/team"
	"github.com/tbeaudouin03/taskgame/backend/apihandler/tutorial"
)

func main() {
	config := loadEnvConfig(`config.json`)

	//gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	//gob.Register(hero.Tasks{})

	r.POST("/api/PostTasks", hero.PostTasks)
	r.GET("/api/GetHeroData", hero.GetHeroData)
	r.POST("/api/PostImageAndTaskCheck", hero.PostImageAndTaskCheck)
	r.POST("/api/PostInAppTask", hero.PostInAppTask)

	r.POST("/api/CalculateHeroStat", hero.CalculateHeroStat)

	r.GET("/api/GetAdvice", hero.GetAdvice)

	r.GET("/api/GetImageToCheck", judge.GetImageToCheck)
	r.POST("/api/PostJudgement", judge.PostJudgement)

	r.GET("/api/GetShopData", shop.GetShopData)
	r.POST("/api/PostPurchasedShopObject", shop.PostPurchasedShopObject)
	r.POST("/api/PostUsedShopObject", shop.PostUsedShopObject)

	r.GET("/api/GetLeaderBoardData", leaderboard.GetLeaderBoardData)

	r.GET("/api/GetCostTeamMembership", team.GetCostTeamMembership)
	r.GET("/api/GetTeamMembershipRequest", team.GetTeamMembershipRequest)
	r.POST("/api/PostAcceptTeamMembership", team.PostAcceptTeamMembership)
	r.POST("/api/PostTeamMembershipRequest", team.PostTeamMembershipRequest)
	r.POST("/api/PostTeamInfo", team.PostTeamInfo)
	r.POST("/api/PostMoodStatus", team.PostMoodStatus)
	r.POST("/api/PostTeamLeaderChoice", team.PostTeamLeaderChoice)
	r.POST("/api/PostHelpTeamLeader", team.PostHelpTeamLeader)
	r.POST("/api/PostChooseRandomTeam", team.PostChooseRandomTeam)

	r.GET("/api/GetLoveGift", adventure.GetLoveGift)
	r.GET("/api/GetFightStatus", adventure.GetFightStatus)
	r.POST("/api/PostHeroCombatParty", adventure.PostHeroCombatParty)
	r.POST("/api/PostMonsterIsIntroduced", adventure.PostMonsterIsIntroduced)
	r.POST("/api/PostIsFightAccepted", adventure.PostIsFightAccepted)
	r.POST("/api/PostIsFightStarted", adventure.PostIsFightStarted)
	r.POST("/api/PostMonsterIsDefeated", adventure.PostMonsterIsDefeated)

	r.POST("/api/PostTutorialLevel", tutorial.PostTutorialLevel)
	r.POST("/api/PostHeroTutorialLevel", tutorial.PostHeroTutorialLevel)
	r.POST("/api/PostJudgeTutorialLevel", tutorial.PostJudgeTutorialLevel)
	r.POST("/api/PostShopTutorialLevel", tutorial.PostShopTutorialLevel)
	r.POST("/api/PostLeaderBoardTutorialLevel", tutorial.PostLeaderBoardTutorialLevel)
	r.POST("/api/PostTeamTutorialLevel", tutorial.PostTeamTutorialLevel)
	r.POST("/api/PostMapTutorialLevel", tutorial.PostMapTutorialLevel)
	r.POST("/api/PostMenuTutorialLevel", tutorial.PostMenuTutorialLevel)

	r.GET("/", healthCheckHandler)
	r.GET("/_ah/health", healthCheckHandler)

	r.Run(`:` + config.APIHandlerPort)

}

func healthCheckHandler(c *gin.Context) {
	fmt.Fprint(c.Writer, "ok")
}

type config struct {
	APIHandlerIP   string `json:"APIHandlerIP"`
	APIHandlerPort string `json:"APIHandlerPort"`
}

func loadEnvConfig(file string) config {
	var config config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}
